import React, { useState, useEffect } from 'react';
import './ScrollTopButton.css';
import { BackTop} from 'antd';
class ScrollTopButton extends React.Component {
  state = {
    isVisible: false
  };
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }
  handleScroll = () => {
    const currentScrollPos = window.pageYOffset;
    const screenHeight = window.innerHeight;
    if (currentScrollPos >= screenHeight) {
      this.setState({ isVisible: true });
    } else {
      this.setState({ isVisible: false });
    }
  };
  scrollToTop = () => {
    this.props.onClick();
  };
  render() {
    const { isVisible } = this.state;
    return (
      <div>
        {/* {isVisible && ( */}
          <div className="ant-back-top" onClick={this.scrollToTop}>
                <span className="split-line"></span>
                <div className="scrollTop"  onClick={this.scrollToTop}></div>
          </div>
        {/* )} */}
      </div>
    );
  }
}
export default ScrollTopButton;