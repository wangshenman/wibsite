//解决方案 【公用banner】
import React,{Component} from "react";
import './banner.less'
import { bannerConfig } from '../../config/bannerJson';
class Banner extends Component {
    constructor(props) {
       super(props)
       this.state={
           bannerInfo:[]
       }
    }
    componentDidMount() {
         let page = this.props.page ||''
         let bannerInfo = bannerConfig.filter(item => item.key === page);
        //  console.log('bannerInfo',bannerInfo)
         this.setState({
            bannerInfo
         })
    }
    render(){
        const { bannerInfo } = this.state;
        return (
            <div className="banner-wrapper">
            <div className="banner-content w1200">
               <div className="right-session">
                    <img className="imgshow" src={ bannerInfo && bannerInfo[0] && bannerInfo[0].bannerImg} alt=""/>
                    <img className="imgshowH5"src={ bannerInfo && bannerInfo[0] && bannerInfo[0].bannerImgH5} alt="" />
                </div>
                <div className="left-session">
                    <h1 className="title bannerTitle">{ bannerInfo && bannerInfo[0] && bannerInfo[0].title}</h1>
                    <ul className="describe bannerDesc">
                        { 
                            bannerInfo && bannerInfo[0] && bannerInfo[0].desc.map((item,index)=>{
                                return(<li key={index}>{item.text}</li>)
                            })
                        }
                    </ul>
                </div>
                
           </div>
           </div>
        )
    }
    
}

export default Banner
