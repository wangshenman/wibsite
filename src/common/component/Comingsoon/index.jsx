import React, { Component } from 'react';
import comingImg from '../../img/common/coming.png'
import './comingsoon.less'

export default class Comingsoon extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imgUrl: comingImg,
            message: '此功能正在建设中，敬请期待~'
        }
    }
    render() {
        return (
            <div className="comingsoon main">
                <div className="comingsoon-content">
                    <div className="comingsoon-img">
                        <img src={this.state.imgUrl} alt="" />
                    </div>
                    <div className="comingsoon-message">
                        <div className="comingsoon-text">{this.state.message}</div>
                    </div>
                </div>

        </div>)
    }
}

