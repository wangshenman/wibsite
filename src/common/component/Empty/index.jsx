import React, { Component } from 'react';
import emptyImg from '../../img/common/img_empty.png';

import './empty.less'

export default class Empty extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imgUrl: emptyImg,
            message: '暂无数据~'
        }
    }
    render() {
        const {message, img, imgHeight} = this.props;
        return (
            <div className="empty">
                <div className="empty-content">
                    <div className="empty-img">
                        <img  style={{height: `${imgHeight || 50}px`}} src={img || this.state.imgUrl} alt="" />
                    </div>
                    <p className="empty-text">{ message || this.state.message }</p>
                </div>

        </div>)
    }
}

