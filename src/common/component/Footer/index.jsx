import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import './footer.less';
import { connect } from "react-redux";
import store from "../../../store/index";
import { FOOTER_VISIBLE } from './../../../router/router.reducer';
import logoImg from '../../img/common/footer/footer_logo.png';
import qrcodeImg from '../../img/common/footer/weixin_qrcode.png';
import bimQRcode from '../../img/common/footer/QQ_qrcode.png';
class Footer extends Component {
    constructor(props) {
        super(props)
        this.state = {
        
        }
    }

    componentDidUpdate(preProps) {
        if (preProps.location.hash !== this.props.location.hash) {
            store.dispatch({
                type: FOOTER_VISIBLE,
                payload: false
            })
        }
    };

    render() {
        const { footerVisible } = this.props;
        return (
            <div className="footer">
            {
                footerVisible && 
                <div className="footer-wrapper">
                       <div className="footer-top">
                        <div className="contact-wrapper clear-fix">
                            <div className="contact-body-left">
                                <div className="company-logo"><img src={logoImg} alt="" /></div>
                                <div className="contact-us">联系我们</div>
                                <div className="contact-address">
                                    <div className='company-phone company-item'>电话：021-61234725</div>
                                    <div className='company-email company-item'>邮箱：yizhu@ezbim.net</div>
                                    <div className='company-address company-item'>地址：上海市杨浦区国权北路1688号湾谷科技园A6栋11F</div>
                                </div>
                            </div>
                            <div className="contact-body-right">
                                <div className="contact-menu">
                                    <dl className="contact-menu-item">
                                    <dt>产品</dt>
                                    <dd><Link to="/function">功能介绍</Link></dd>
                                    <dd><Link to="/example">BIM体验</Link></dd>
                                    <dd><Link to="/cases">客户案例</Link></dd>
                                    {/* <dd><Link to="/downloads">下载</Link></dd> */}
                                    </dl>
                                    {/* <dl className="contact-menu-item">
                                    <dt>文档</dt>
                                    <dd><Link to="/">开发者指南</Link></dd>
                                    <dd><Link to="/">常见问题</Link></dd>
                                    </dl> */}
                                    <dl className="contact-menu-item">
                                    <dt>关于</dt>
                                    <dd><Link to="/about">关于我们</Link></dd>
                                    </dl>
                                    {/* <dl className="contact-menu-item">
                                    <dt>关注EveryBIM图形引擎</dt>
                                    <dd className="contact-right flex-between">
                                            <div className="contact-qrcode flex-column-between">
                                                <img src={bimQRcode} alt="" />
                                                <span>官方QQ号</span>
                                            </div>
                                            <div className="contact-qrcode flex-column-between">
                                                <img src={qrcodeImg} alt="" />
                                                <span>微信公众号</span>
                                            </div>
                                    </dd>
                                    </dl> */}
                                </div>
                                <div className="contact-qr"><img src={bimQRcode} alt="" /></div>
                            </div>
                        </div>
                        </div>
                        <div className="record-wrapper">
                            <div className="record">
                                <div className="yizhu-link">友情链接：
                                    <a href="https://www.ezbim.net/" target="_blank" rel="noopener noreferrer">译筑科技</a> 
                                    <a href="https://www.ezbim.net/products/cooperativePlatform" target="_blank" rel="noopener noreferrer">译筑云</a>
                                    <a href="http://www.greetec.com/" target="_blank">青矩</a>
                                    <a href="https://www.baigongyi.com/" target="_blank">百工驿</a>
                                </div>
                                <div className="copyright">
                                    <span>Copyright © 2014-2023 译筑信息科技（上海）有限公司</span>  
                                    <a href="http://beian.miit.gov.cn" target="_blank" rel="noopener noreferrer" className="ipCode">沪ICP备15010930号-1</a>
                                </div> 
                            </div>
                        </div>
                   
                </div>
            }
            </div>
        )

    }

}
function mapStateToProps(state) {
    return {
        footerVisible: state.getIn(["router", "footer_visible"]),
    };
}

const mapActionCreators = {};

export default connect(mapStateToProps, mapActionCreators)(withRouter(Footer));