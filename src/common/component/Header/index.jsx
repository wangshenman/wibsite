import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Menu, Icon, Drawer,Dropdown } from 'antd';
import { routers } from '../../../router/routers';
import navLogo from '../../img/common/developer_logo.png';
import { openWindow } from '../../utils/Utility';
import{ oauthURL,loginURL } from '../../config/websiteConfig';
import './header.less';

const { SubMenu } = Menu;

class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeKey: '',
      currentCity: {},
      navLink: [],
      menuVisible:false,
    }
    this.pathname = window.location.pathname;

    this.onShowMenu = () => {
      this.setState({
        menuVisible: true
      })
    };

    this.onHiddenMenu = () => {
      this.setState({
        menuVisible: false
      })
    };

    this.handleClick = (menuItem) => {
      this.props.history.replace(menuItem.key)
    };

    this.handleLink = (key) => {
      switch(key){
        case 'login':
          return window.location.href = loginURL;
        case 'register':
          return window.location.href = `${oauthURL}/register?redirect=yizhu`;
        case 'changeLog':
          return this.props.history.push('/changeLog');
        case 'modelAPI':
          return openWindow('resources/document/model/index.html');
        case 'drawingAPI':
            return openWindow('resources/document/drawing/index.html');
        case 'server':
          return openWindow('resources/document/server/index.html');
        case 'developer':
          return openWindow('/developer');
        default:
          this.props.history.replace('/')
      }
    }
  }
  componentDidMount() {
    //是否为 demo包 显示
    let orderLink = [];
    orderLink = routers.filter(item => item.isNav);
    this.setState({
      navLink: orderLink,
      navSpecial: false,
    })
  }

  render() {
    const { navLink, menuVisible } = this.state;
    const { headerType } = this.props;
    let hashRoot = window.location.pathname.slice(1);
    let activeKey = hashRoot.indexOf('/') === -1 ? hashRoot : hashRoot.slice(0, hashRoot.indexOf('/'));
    let bgClass = activeKey ? 'white': headerType ? 'white' : 'back';
    const DropdownConfig =[
      {
        title: '更新日志',
        key: 'changeLog',
        action: () => this.handleLink('changeLog')
      },
      {
        title: '模型 API',
        key: 'modelAPI',
        action: () => this.handleLink('modelAPI')
      },
      {
        title: '图纸 API',
        key: 'drawingAPI',
        action: () => this.handleLink('drawingAPI')
      },
      {
        title: '服务端 API',
        key: 'server',
        action: () => this.handleLink('server')
      },
    ]
    // 设置下拉子菜单
    const setSubMenu = (key)=>{
      return(
        <Menu>
          {
          DropdownConfig.map(i => 
            <Menu.Item key={i.key} onClick={e => { e.domEvent.stopPropagation(); i.action()}}>
              <span>{i.title}</span>
            </Menu.Item>)
          }
        </Menu>
      )
    };

    return (
    
        <div className={`header ${bgClass}`}>
            <div className="nav-wrapper w1200">
              {/* PC 端 */}
              <Link to={'/'}>
                  <div className="nav-front flex-between">
                     <img src={navLogo} alt="中间件官网Logo" className={`nav-logo`} />
                     <span className="navText">EveryBIM 图形引擎</span>
                  </div>
              </Link>
              <div className="nav-link">
                  {
                    navLink && navLink.map(item =>
                      <div key={item.title} className="nav-button">
                        {
                          !item.isSubMenu ? 
                          <Link to={item.path} className={"/" + activeKey === item.path ? "active" : ""} title={item.title}>{item.title}</Link>
                          :
                        (<Dropdown overlay={() => setSubMenu(item.title)} trigger={['hover']} placement="bottomCenter">
                          <Link to={item.path} className={`show-submenu ${"/" + activeKey === item.path ? 'selected' : ''}`} title={item.title}>
                            <span className="nav-title">{item.title}</span>
                            <span className={`nav-img ${"/" + activeKey === item.path ? 'hover-up' : 'hover-down'}`}></span>
                          </Link>
                        </Dropdown>)
                           
                        }
                      </div>)
                  }
              </div>
              <div className="nav-open">
                  <div className="nav-button" onClick={()=>this.handleLink('developer')}><span>开发者中心</span></div>
                  {/* <div className="nav-button" onClick={()=>this.handleLink('login')}><span>登录</span></div>
                  <div className="nav-button" onClick={()=>this.handleLink('register')}><span>立即注册</span></div> */}
              </div>

              {/* 移动端适配 */}
              <div className="nav-menu" onClick={() => menuVisible ? this.onHiddenMenu() : this.onShowMenu()}>
                     <Icon type={menuVisible ? "close" : "menu"} style={{color:'#252B3A'}}/>
              </div>
              <Drawer
                title="Basic Drawer"
                placement="right"
                width={240}
                headerStyle={{ display: "none" }}
                bodyStyle={{ padding: "75px 0 15px 0" }}
                closable={false}
                onClose={this.onHiddenMenu}
                visible={menuVisible}
                getContainer={this.props.drawerContainerId ? document.getElementById(this.props.drawerContainerId) : 'body'}
              >
                <Menu
                  onClick={this.handleClick}
                  style={{ width: "100%" }}
                  defaultSelectedKeys={["/"]}
                  selectedKeys={["/" + AnalysisSubMenuActiveKey(hashRoot)]}
                  mode="inline"
                >
                  {
                    navLink && navLink.map(item => {
                      if (item.isSubMenu) {
                        return <SubMenu key={item.path} title={item.title}>
                          { DropdownConfig.map(child => <Menu.Item key={child.key} onClick={()=> child.action()}>{child.title}</Menu.Item>)}
                        </SubMenu>
                      } else {
                        return <Menu.Item key={item.path}>{item.title}</Menu.Item>
                      }
                    })
                }
                </Menu>
              </Drawer>
            </div>
        </div>)
  }
}

function AnalysisSubMenuActiveKey(hashRootPath) {
  let root = hashRootPath.split("/")[0];
  return root;
}

// function AnalysisSubMenus(subMenu) {
//   switch (subMenu.title) {
//     case "文档":
//       return products.map(item => {
//           return {
//             title: item.title,
//             key: `/products/${item.key}`
//         }
//       });
//     default:
//       return [];
//   }
// };

export default withRouter(Header);

