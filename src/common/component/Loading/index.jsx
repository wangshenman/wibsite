import React, { Component } from 'react';
import {Spin} from 'antd';
// import SVGA from 'svgaplayerweb';
// import loadingSVG from '../../img/common/loading.svga';
import './loading.less';

class Loading extends Component {
    constructor(props) {
        super(props)
        this.state = {
            text: this.props.message || 'Loading...'
        }
        // this.init =()=>{
        //     let player = new SVGA.Player('#SVGCanvas');
        //     let parser = new SVGA.Parser('#SVGCanvas'); // 如果你需要支持 IE6+，那么必须把同样的选择器传给 Parser。
        //     parser.load(loadingSVG, function(videoItem) {
        //         player.setVideoItem(videoItem);
        //         player.startAnimation();
        //     })
        // }
    }
    componentDidMount(){
        // this.init()
    }
    render() {
        return (
            <div className="loading-wrapper flex-vertical-center">
                {/* <div id="SVGCanvas" loops="2"></div> */}
                {/* <div className="line-wrapper">
                    <div className="k-line k-line-1"></div>
                    <div className="k-line k-line-2"></div>
                    <div className="k-line k-line-3"></div>
                    <div className="k-line k-line-4"></div>
                    <div className="k-line k-line-5"></div>
                </div> */}
                 <Spin size="large" tip={this.state.text}/>
            </div>
        )
    }
}

export default Loading

