/* 开发案例 */
import project1 from '../img/cases/project1.webp';
import project2 from '../img/cases/project2.webp';
import project3 from '../img/cases/project3.webp';
import project4 from '../img/cases/project4.webp';
import project5 from '../img/cases/project5.webp';
import project6 from '../img/cases/project6.webp';
import project7 from '../img/cases/project7.webp';
import project8 from '../img/cases/project8.webp';
import project9 from '../img/cases/project9.webp';
import project10 from '../img/cases/project10.webp';
import project11 from '../img/cases/project11.webp';
import project12 from '../img/cases/project12.webp';
/* 项目案例 */
import case1 from '../img/cases/case1.webp';
import case2 from '../img/cases/case2.webp';
import case3 from '../img/cases/case3.webp';
import case4 from '../img/cases/case4.webp';
import case5 from '../img/cases/case5.webp';
import case6 from '../img/cases/case6.webp';
import case7 from '../img/cases/case7.webp';
import case8 from '../img/cases/case8.webp';
import case9 from '../img/cases/case9.webp';
import case10 from '../img/cases/case10.webp';
import case11 from '../img/cases/case11.webp';
import case12 from '../img/cases/case12.webp';

import location from  '../img/about/location.svg';
import school from  '../img/about/school.svg';
import video from  '../img/about/video.svg';
import agent from  '../img/about/agent.svg';
import developImg from "../img/about/develop_img.svg";

export const cases = {
  title:'客户案例',
  desc:'EveryBIM图形引擎历经600+企业，10000+项目，50000+人员深度应用，具备领先的稳定性和可靠性。用户可基于EveryBIM图形引擎自行开发BIM系统，也可联合译筑科技共同打造专属应用系统',
  data:[
    {
      key:'1',
      tab:'开发案例',
      content:[
        {
          key: 1,
          title: "中建八局C8BIM项目管理平台",
          imgHref: project1,
          detail:{
              cooperationUnit: '合作单位：中国建筑第八工程局有限公司',
              applicationFunction: '系统名称：C8BIM项目管理平台',
              applicationValue: '系统简介：C8BIM平台是以模型为数据载体的新一代项目平台。平台内已有3000余个项目、2万余个模型同时在使用；平台用户数已接近3万人。'
          }
      },
      {
          key: 2,
          title: "上海建工智慧工地系统",
          imgHref: project2,
          detail:{
              cooperationUnit: '合作单位：上海建工集团股份有限公司',
              applicationFunction: '系统名称：上海建工智慧工地系统',
              applicationValue: '系统简介：基于BIM信息化的智慧工地系统，以“人、材、机、料、环、测”为核心，具备“数据采集-信息记录-数据分析-快速反应”一体化功能。'
          }
      },
      {
          key: 3,
          title: "中交二航局基建项目管理系统",
          imgHref: project3,
          detail:{
              cooperationUnit: '合作单位：中交第二航务工程局有限公司',
              applicationFunction: '系统名称：中交二航局桩基信息化施工管理系统、钢筋加工生成管理系统、混凝土生成加工管理系统',
              applicationValue: '系统简介：此系统围绕BIM打造，服务于基建项目。其功能包含BIM模型、计划、生产、运输、产品管理等模块，可应用于桥梁隧道等基建工程施工管理。'
          }
      },
      {
          key: 4,
          title: "中建五局EPC全过程项目管理系统",
          imgHref: project4,
          detail:{
              cooperationUnit: '合作单位：中国建筑第五工程局有限公司',
              applicationFunction: '系统名称：工程总承包项目管理集成系统',
              applicationValue: '系统简介：该系统以BIM技术为中心，满足EPC项目启动与策划、计划管理、设计管理、建造管理、商务管理、收尾与移交等阶段信息化管理需求。'
          }
      },
      {
          key: 5,
          title: "无锡市政院BIM协同平台",
          imgHref: project5,
          detail:{
              cooperationUnit: '合作单位：无锡市政设计研究院有限公司',
              applicationFunction: '系统名称：设计协同平台',
              applicationValue: '系统简介：基于EveryBIM图形引擎开发的设计协同平台，实现了各专业基于BIM技术的协同设计，并可在平台上开展设计交底，图纸会审，施工交底等工作。'
          }
      },
      {
          key: 6,
          title: "杭州同瀚BIM管理平台",
          imgHref: project6,
          detail:{
              cooperationUnit: '合作单位：同瀚建筑科技（杭州）有限公司',
              applicationFunction: '系统名称：杭州西站枢纽BIM模型管理平台',
              applicationValue: '系统简介：借助EveryBIM图形引擎开发的杭州西站枢纽BIM模型管理平台，保证了西站50万平方米大体量模型的平稳运行与应用。'
          }
      },
      {
          key: 7,
          title: "青矩全咨云平台",
          imgHref: project7,
          detail:{
              cooperationUnit: '合作单位：青矩技术股份有限公司',
              applicationFunction: '系统名称：青矩全咨云平台',
              applicationValue: '系统简介：该平台是以投资决策为起点，以BIM技术为基础，以造价管控为核心，贯穿工程建设全生命周期的全过程工程管理平台。'
          }
      },
      {
          key: 8,
          title: "光明地产装配式项目管理平台",
          imgHref: project8,
          detail:{
              cooperationUnit: '合作单位：光明房地产集团股份有限公司',
              applicationFunction: '系统名称：光明地产BIM平台',
              applicationValue: '系统简介：基于BIM+物联网技术打造的装配式项目管理平台，实现了进度管控、质量追溯、装配构件跟踪、及信息追溯与BIM的深度融合。'
          }
      },
      {
          key: 9,
          title: "瑞和安惠“惠管理”全过程咨询协同平台",
          imgHref: project9,
          detail:{
              cooperationUnit: '合作单位：瑞和安惠项目管理集团有限公司',
              applicationFunction: '系统名称：惠管理',
              applicationValue: '系统简介：基于BIM轻量化的“惠管理”平台通过投资、质安、物料等主线，不仅实现了项目核心数据清晰可视化的展示，还实现了多层级管控和多维度覆盖。'
          }
      },
      {
          key: 10,
          title: "CMS系统（新加坡）",
          imgHref: project10,
          detail:{
              cooperationUnit: '合作单位：CMS Data Technology Ptd. Ltd.',
              applicationFunction: '系统名称：CMS',
              applicationValue: '系统简介：CMS系统搭载EveryBIM图形引擎，实现了装配式项目的三维可视化管理，并且通过此系统还可将预制构件全过程信息与BIM深度融合。'
          }
      },
      {
          key: 11,
          title: "地厚云图AI项目管理平台",
          imgHref: project11,
          detail:{
              cooperationUnit: '合作单位：北京地厚云图科技有限公司',
              applicationFunction: '系统名称：AI项目管理平台',
              applicationValue: '系统简介：地厚云图AI项目管理平台是一个以工程项目为中心，集施工全过程、管理全体系、业务在线全智能为一体的SaaS平台。'
          }
      },
      {
          key: 12,
          title: "上海域苍IBMS平台",
          imgHref: project12,
          detail:{
              cooperationUnit: '合作单位：上海域苍信息科技有限公司',
              applicationFunction: '系统名称：IBMS',
              applicationValue: '系统简介：IBMS园区展示平台以先进的BIM理念为核心，整合了信息化采集、整合、交互、分析等技术，可将园区运行状况实时可视化展示。'
          }
      }
        
      ]
    },
    {
      key:'2',
      tab:'项目案例',
      content:[
        {
          key: 1,
          title: "天津周大福金融中心项目",
          imgHref: case1,
          detail:{
              cooperationUnit: '合作单位：中国建筑第八工程局有限公司',
              applicationFunction: '项目名称：天津周大福金融中心项目',
              applicationValue: 'BIM应用：该项目总建筑面积39万㎡，建筑总高度530m，建成后为世界第九高楼 。依托BIM轻量化技术成功解决1000+BIM子模型、几十个变更版本、数TB项目数据、平均十万条日记录产生、几十家分包商、数千人现场协同应用等问题。'
          }
      },
      {
          key: 2,
          title: "深圳宝安机场项目",
          imgHref: case2,
          detail:{
              cooperationUnit: '合作单位：中国建筑第八工程局有限公司',
              applicationFunction: '项目名称：深圳宝安机场项目',
              applicationValue: 'BIM应用：机场卫星厅总建筑面积为23.5万㎡，利用轻量化后的BIM模型进行深化设计协同、现场施工辅助、虚实联动展示、构件物联追踪、进度可视化、4D施工管理。'
          }
      },
      {
          key: 3,
          title: "北京环球影城项目",
          imgHref: case3,
          detail:{
              cooperationUnit: '合作单位：中国建筑第二工程局有限公司',
              applicationFunction: '项目名称：北京环球影城项目',
              applicationValue: 'BIM应用：北京环球影城是世界顶级主题乐园，项目体量大、建模软件多、沟通协调难。通过BIM模型轻量化技术和表单审批技术，实现了BIM模型整合应用和无纸化高效办公管理。'
          }
      },
      {
          key: 4,
          title: "禅医健康城综合体项目",
          imgHref: case4,
          detail:{
              cooperationUnit: '合作单位：中国建筑第八工程局有限公司',
              applicationFunction: '项目名称：禅医健康城综合体',
              applicationValue: 'BIM应用：此项目是集医院、养护院、商业、体育馆、住宅等为一体的高层及超高层建筑综合体，总建筑面积达到 20万㎡。通过平台轻量化技术助力项目施工，打造医养结合的样板工程。'
          }
      },
      {
          key: 5,
          title: "济宁市主城区内环高架及连接线项目",
          imgHref: case5,
          detail:{
              cooperationUnit: '合作单位：青矩技术股份有限公司',
              applicationFunction: '项目名称：济宁市主城区内环高架及连接线项目',
              applicationValue: 'BIM应用：该项目通过BIM+GIS、BIM+IOT的立体空间数据应用，可实现精确定位、整体可视化、统筹管理等功能，为项目管理者打造物联一体的综合管理平台。'
          }
      },
      {
          key: 6,
          title: "杭州中天钱塘银座项目",
          imgHref: case6,
          detail:{
              cooperationUnit: '合作单位：中天建设集团有限公司',
              applicationFunction: '项目名称：杭州中天钱塘银座项目',
              applicationValue: 'BIM应用：项目将风管、水管等机电模型构件进行分区分段拆分，并通过生成的二维码对构件进行全过程的流程管控，及查询资料信息、录入施工信息，从而实现施工阶段BIM模型的全过程应用。'
          }
      },
      {
          key: 7,
          title: "西安奥体中心项目",
          imgHref: case7,
          detail:{
              cooperationUnit: '合作单位：陕西建工集团有限公司',
              applicationFunction: '项目名称：西安奥体中心项目',
              applicationValue: 'BIM应用：本项目为陕西省第一重点项目，总建筑面积达15万㎡。项目从人员到硬件均以最高标准配置，主要对钢结构构件的加工、运输、吊装、验收等各环节进行全过程跟踪管理。'
          }
      },
      {
          key: 8,
          title: "西湖大学项目",
          imgHref: case8,
          detail:{
              cooperationUnit: '合作单位：上海建工集团股份有限公司',
              applicationFunction: '项目名称：西湖大学',
              applicationValue: 'BIM应用：该项目以BIM模型为基础，以建造全过程信息化为主导，提升了项目精细化管理水平，旨在构建“智慧工地”标杆性工程，打通建造与运维数字信息，为智慧运维、智慧校园提供数据支持。'
          }
      },
      {
          key: 9,
          title: "四川成都空港新城企业总部项目",
          imgHref: case9,
          detail:{
              cooperationUnit: '合作单位：中建三局集团有限公司',
              applicationFunction: '项目名称：四川成都空港新城企业总部项目',
              applicationValue: 'BIM应用：空港新城项目体量大、现场沟通协调难等问题显著，通过以BIM轻量化模型为载体来做管理，既可解决现场沟通不及时问题，又可解决数据归类关联BIM模型的传输问题，为各方协同提供了有效的手段。'
          }
      },
      {
          key: 10,
          title: "某变电站改造工程",
          imgHref: case10,
          detail:{
              cooperationUnit: '合作单位：上海某电力工程有限公司',
              applicationFunction: '项目名称：某变电站改造工程',
              applicationValue: 'BIM应用：基于BIM的施工平台实现了BIM模型轻量化和移动应用，使项目人员无需高配电脑即可直接应用项目BIM成果。此外，现场还可随时通过手机查看模型信息。'
          }
      },
      {
          key: 11,
          title: "杭州西站项目",
          imgHref: case11,
          detail:{
              cooperationUnit: '合作单位：同瀚建筑科技（杭州）有限公司',
              applicationFunction: '项目名称：杭州西站项目',
              applicationValue: 'BIM应用：此项目模型近30G，体量达50万㎡。经过轻量化后，BIM模型可在网页端流畅应用，从而帮助施工团队实现对BIM模型信息的提取查看，并提升管理人员对BIM模型、施工现场情况、施工进度的把控。'
          }
      },
      {
          key: 12,
          title: "珠海木乃南堤大门口闸段工程",
          imgHref: case12,
          detail:{
              cooperationUnit: '合作单位：惠州市水电建筑工程有限公司',
              applicationFunction: '项目名称：珠海木乃南堤大门口闸段工程',
              applicationValue: 'BIM应用：此项目为水库闸段工程，对现场施工细节管控要求较高。通过将BIM轻量化模型作为载体，使设计单位、业主单位、施工单位和BIM咨询单位连接起来，通过模型共同讨论、记录、修改问题，大大降低后续施工因图纸问题进行返工的几率。'
          }
      },
        
      ]
    }

  ]
};
//联系我们
export const addressWay = [
  {
      key: 1,
      title: "上海（总部)",
      icon: location,
      address: "地址：上海市杨浦区国权北路1688号湾谷科技园A6栋11F",
      name: "联系人：孟祥和",
      number: "电话：+86-18017884835",
  },
  {
      key: 2,
      title: "北京（华北分公司)",
      icon: location,
      address: "地址：北京市海淀区紫竹院南路20号外文文化创意园",
      name: "联系人：张雷",
      number: "电话：+86-13613640140",
  },
  {
      key: 3,
      title: "深圳（华南分公司)",
      icon: location,
      address: "地址：广东省广州市罗湖区嘉宾路2018号深华商业大厦1508A室",
      name: "联系人：蒋顺伟",
      number: "电话：+86-18865693000",
  },
  {
    key: 4,
    title: "贵州(西南分公司)",
    icon: location,
    address: "地址：贵州省贵阳市南明区国际中心1号1209室",
    name: "联系人：李君来",
    number: "电话：+86-15285617195",
  },
  {
      key: 5,
      title: "战略合作部",
      icon: school,
      sign: "战略合作",
      name: "联系人：许海峰",
      number: "电话：+86-13381813180",
  },
  {
    key: 6,
    title: "渠道扩展 ",
    icon: video,
    sign: "渠道合作",
    name: "联系人：叶丽娜",
    number: "电话：+86-15102153886",
},
];

export const developData = {
  title: "开发支持",
  img: developImg,
  expains:[
    {
      key:1,
      text:'拥有成熟企业级智慧建造平台开发体系，已成功为数十家大型企业建设智慧建造平台体系'
    },
    {
      key:2,
      text:'提供从需求分析，UE/UI设计、功能定制到平台整体实施的全过程个性化解决方案'
    },
    {
      key:3,
      text:'依托译筑轻量化BIM图形引擎，为用户提供BIM综合性平台，支持二次开发，第三方集成'
    },
  ]
}



