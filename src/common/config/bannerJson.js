/* banner数据展示 */

import caseBanner from '../img/cases/case_banner.webp';
import caseBannerH5 from '../img/cases/case_bannerH5.webp';
import functionBanner from '../img/function/function_banner.webp';
import functionBannerH5 from '../img/function/function_bannerH5.webp';
import downloadBanner from '../img/download/download_banner.webp';
import downloadBannerH5 from '../img/download/download_bannerH5.webp';
import exampleBanner from '../img/example/BIM_banner.webp';
import exampleBannerH5 from '../img/example/BIM_bannerH5.webp';
import aboutBanner from '../img/about/about_banner.webp';
import aboutBannerH5 from '../img/about/about_bannerH5.webp';

export const bannerConfig =[
    {
        key:'example',
        title:'全场景高性能体验',
        desc:[{
            index: 1,
            text:'秒级加载、极速渲染、大场景流畅体验'
        }],
        bannerImg: exampleBanner,
        bannerImgH5: exampleBannerH5
    },
    {
        key:'function',
        title:'BIM+深度应用',
        desc:[
            {
                index: 1,
                text:'面向全场景、全过程、全流程、多角色'
            }
            
        ],
        bannerImg: functionBanner,
        bannerImgH5: functionBannerH5
    },
    {
        key:'download',
        title:'免费的云开发环境',
        desc:[
            {
                index: 1,
                text:'提供完备的多端口SDK，助你快速搭建BIM应用'
            }
        ],
        bannerImg: downloadBanner,
        bannerImgH5: downloadBannerH5
    },
    {
      key:'cases',
      title:'助力用户成功',
      desc:[
          {
              index: 1,
              text:'600+企业，10000+项目深度应用'
          }
      ],
      bannerImg: caseBanner,
      bannerImgH5: caseBannerH5
    },
    {
        key:'about',
        title:'联系我们',
        desc:[
            {
                index: 1,
                text:'我们致力于为您提供个性化的售前咨询服务，及完善的售后技术服务'
            }
        ],
        bannerImg: aboutBanner,
        bannerImgH5: aboutBannerH5
      }
];

