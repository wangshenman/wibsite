/*下载页数据配置 */
import installPackage from '../img/download/download.webp';
import developApi from '../img/download/api.webp';
import codeExample from '../img/download/code.webp';
import apiDocument from '../img/download/document.webp';

/* PC-QT */
export const QTData = [
  {
    key: 'qt',
    title: '示例软件',
    desc: 'QT基于图形包开发的多功能展示软件',
    imgHref: installPackage,
    type:'EXE',
    docName:'QT-EXE.rar'
  },
  {
    key: 'qt',
    title: '源码示例',
    desc: 'QT基于图形包开发的简易软件源代码',
    imgHref: codeExample,
    type:'DEMO',
    docName:'QT-DEMO.rar'
  },
  {
    key: 'qt',
    title: '开发接口',
    desc: 'QT使用图形包进行自主开发的接口文件',
    imgHref: developApi,
    type:'API',
    docName:'QT-API.rar'
  },
  {
    key: 'qt',
    title: '接口文档',
    desc: 'QT开发图形包的使用说明文档下载',
    imgHref: apiDocument,
    type:'DOC',
    docName:'QT-DOC.chm'
  }
];

/* PC-C# */
export const CData = [
  {
    key: 'csharp',
    title: '示例软件',
    desc: 'C#基于图形包开发的多功能展示软件',
    imgHref: installPackage,
    type:'EXE',
    docName:'CSHARP-EXE.rar'
  },
  {
    key: 'csharp',
    title: '源码示例',
    desc: 'C#基于图形包开发的简易软件源代码',
    imgHref: codeExample,
    type:'DEMO',
    docName:'CSHARP-DEMO.rar'
  },
  {
    key: 'csharp',
    title: '开发接口',
    desc: 'C#使用图形包进行自主开发的接口文件',
    imgHref: developApi,
    type:'API',
    docName:'CSHARP-API.rar'
  },
  {
    key: 'csharp',
    title: '接口文档',
    desc: 'C#开发图形包的使用说明文档下载',
    imgHref: apiDocument,
    type:'DOC',
    docName:'CSHARP-DOC.chm'
  }
];

/* iOS */
export const iOSData = [
  {
    key: 'ios',
    title: '示例软件',
    desc: 'iOS基于图形包开发的多功能展示软件',
    qrcode: true,
    imgHref: installPackage,
    type:'APP',
    //docName:'IOS-APP.ipa'
    docName:'IOS-APP.png' //二维码放图片
  },
  {
    key: 'ios',
    title: '源码示例',
    desc: 'iOS基于图形包开发的简易软件源代码',
    imgHref: codeExample,
    type:'DEMO',
    docName:'IOS-DEMO.zip'
  },
  {
    key: 'ios',
    title: '开发接口',
    desc: 'iOS使用图形包进行自主开发的接口文件',
    imgHref: developApi,
    type:'API',
    docName:'IOS-API.zip'
  },
  {
    key: 'ios',
    title: '接口文档',
    desc: 'iOS开发图形包的使用说明文档下载',
    imgHref: apiDocument,
    type:'DOC',
    docName:'IOS-DOC.zip'
  }
];

/* Android */
export const AndroidData = [
  {
    key: 'android',
    title: '示例软件',
    desc: 'Android基于图形包开发的多功能展示软件',
    qrcode: true,
    imgHref: installPackage,
    type:'APP',
    //docName:'ANDROID-APP.apk',
    docName:'ANDROID-APP.png'  //二维码放图片

  },
  {
    key: 'android',
    title: '源码示例',
    desc: 'Android基于图形包开发的简易软件源代码',
    imgHref: codeExample,
    type:'DEMO',
    docName:'ANDROID-DEMO.zip'
  },
  {
    key: 'android',
    title: '开发接口',
    desc: 'Android使用图形包进行自主开发的接口文件',
    imgHref: developApi,
    type:'API',
    docName:'ANDROID-API.zip'
  },
  {
    key: 'android',
    title: '接口文档',
    desc: 'Android开发图形包的使用说明文档下载',
    imgHref: apiDocument,
    type:'DOC',
    docName:'ANDROID-DOC.zip'
  }
];

/* web端 */
export const WebData = [
  {
    key: 'web',
    title: '源码示例',
    desc: 'WEB基于图形包开发的简易软件源代码',
    imgHref: codeExample,
    type:'DEMO',
    docName:'WEB-DEMO.zip'
  },
  {
    key: 'web',
    title: '开发接口',
    desc: 'WEB开发图形包的使用说明文档下载',
    imgHref: developApi,
    type:'API',
    docName:'WEB-API.zip'
  },
  {
    key: 'web',
    title: '接口文档',
    desc: 'WEB使用图形包进行自主开发的接口文件',
    imgHref: apiDocument,
    type:'DOC',
    docName:'WEB-DOC.zip'
  },
  {
    key: 'web',
    title: '接口文档',
    desc: '服务器端进行自主开发的相关接口文档',
    imgHref: apiDocument,
    type:'DOC',
    docName:'SERVER-DOC.zip'
  }
];

/* Revit */
export const RevitData = [
  {
    key: 'revit',
    title: '示例软件',
    desc: 'Revit基于图形包开发的多功能展示软件',
    imgHref: installPackage,
    type:'EXE',
    docName:'REVIT-INSTALL.exe'
  },
  {
    key: 'revit',
    title: '源码示例',
    desc: 'Revit基于图形包开发的简易软件源代码',
    imgHref: codeExample,
    type:'DEMO',
    docName:'REVIT-DEMO.rar'
  },
  {
    key: 'revit',
    title: '开发接口',
    desc: 'Revit使用图形包进行自主开发的接口文件',
    imgHref: developApi,
    type:'API',
    docName:'REVIT-API.rar'
  },
  {
    key: 'revit',
    title: '接口文档',
    desc: 'Revit开发图形包的使用说明文档下载',
    imgHref: apiDocument,
    type:'DOC',
    docName:'REVIT-DOC.rar'
  }
];

/* Navisworks */
export const NavisworksData = [
  {
    key: 'navisworks',
    title: '示例软件',
    desc: 'Navisworks基于图形包开发的多功能展示软件',
    imgHref: installPackage,
    type:'EXE',
    docName:'NAVISWORKS-INSTALL.exe'
  },
  {
    key: 'navisworks',
    title: '源码示例',
    desc: 'Navisworks基于图形包开发的简易软件源代码',
    imgHref: codeExample,
    type:'DEMO',
    docName:'NAVISWORKS-DEMO.rar'
  },
  {
    key: 'navisworks',
    title: '开发接口',
    desc: 'Navisworks使用图形包进行自主开发的接口文件',
    imgHref: developApi,
    type:'API',
    docName:'NAVISWORKS-API.rar'
  },
  {
    key: 'navisworks',
    title: '接口文档',
    desc: 'Navisworks开发图形包的使用说明文档下载',
    imgHref: apiDocument,
    type:'DOC',
    docName:'NAVISWORKS-DOC.rar'
  }
];