export const functionData = [
    {
        key:'modelMerge',
        title:'模型合并',
        content:[
            {
                details:[
                    {
                        index:1,
                        desc:'不同数据格式的模型文件上传至平台，模型轻量化后用户可任意选择模型整合浏览，无需等待模型二次轻量化解析，方便用户组合并应用BIM模型。'
                    }
                ],
                images:[
                    {
                        index:1,
                        imgHref:'modelMerge.gif'
                    }
                ]
            }
        ]
    },
    {
        key:'modelControl',
        title:'显示控制',
        content:[
            {
                details:[
                    {
                        index:1,
                        desc:'用户可通过显示控制树控制模型构件，使构件实现显示/隐藏，高亮，透明，着色等效果。支持以楼层、专业、系统、属性字段等维度自定义生成显示控制树，满足不同行业用户对构件的分类管理和控制的需求。'
                    }
                ],
                images:[
                    {
                        index:1,
                        imgHref:'3DControl.gif'
                    }
                ]
            }

        ]
    },
    {
        key:'modelSection',
        title:'模型剖切',
        content:[
            {
                details:[
                    {
                        index:1,
                        desc:'由于BIM模型普遍体量大、构件多，用户可通过模型剖切直观查看BIM模型内部构件，帮助用户更好地应用BIM模型。模型剖切支持剖切盒、轴剖切两种方式。'
                    },
                ],
                images:[
                    {
                        index:1,
                        imgHref: 'modelSection1.gif'
                    },
                ]

            },
            {
                details:[
                    {
                        index:1,
                        desc:'剖切盒：初始状态为包含所有构件的半透明长方体盒子，用户可拖动剖切盒六个面自由剖切模型；支持平移、旋转及缩放满足用户多方位剖切需求；支持控制显示/隐藏、剖切反转、重置，方便用户操作。'
                    },
                ],
                images:[
                    {
                        index:1,
                        imgHref: 'modelSection2.png'
                    },
                ]

            },
            {
                details:[
                    {
                        index:1,
                        desc:'轴剖切：初始状态包含一个剖切面+控制轴，用户可拖动或旋转剖切轴剖切模型，轴剖切支持控制显示/隐藏、重置，方便用户操作。'
                    },
                ],
                images:[
                    {
                        index:1,
                        imgHref: 'modelSection3.png'
                    },
                ]

            }
        ]
       

    },
    {
        key:'modelMeasure',
        title:'模型测量',
        content:[
            {
                details:[
                    {
                        index:1,
                        desc:'用户查看BIM模型，可通过测量功能获取模型点到点距离、面到面距离、角度、高程等相关尺寸信息，实现便捷测量。引擎支持自定义设置测量颜色和单位，单个测量结果可删除。'
                    }
                ],
                images:[
                    {
                        index:1,
                        imgHref:'modelMeasure.gif'
                    }
                ]

            }
        ]
    },
    {
        key:'modelMap',
        title:'模型小地图',
        content:[
            {
                details:[
                    {
                        index:1,
                        desc:'用户可通过模型小地图功能快速定位模型位置，地图上可直观查看楼层的平面布置情况，支持楼层切换；支持小地图与三维模型位置联动，点击小地图可快速定位到三维模型中对应位置。'
                    }
                ],
                images:[
                    {
                        index:1,
                        imgHref:'modelMap.gif'
                    },
                ]

            }
        ]
    },
    {
        key:'modelRoam',
        title:'仿真漫游',
        content:[
            {
                details:[
                    {
                        index:1,
                        desc:'用户可通过仿真漫游以第一人称视角获得更真实的模型浏览体验。通过键盘上的W、A、S、D按键或↑↓← →按键水平移动镜头，R、F上下移动镜头，可通过shift按键实现加速浏览。'
                    }
                ],
                images:[
                    {
                        index:1,
                        imgHref:'modelRoam1.gif'
                    }
                ]
            },
            {
                details:[
                    {
                        index:1,
                        desc:'仿真漫游支持第三人称浏览模型，支持重力、碰撞模式，使漫游过程中有碰撞阻挡、爬楼梯等效果，更加贴合实际使用场景。'
                    }
                ],
                images:[
                    {
                        index:1,
                        imgHref:'modelRoam2.png'
                    },
                    {
                        index:1,
                        imgHref:'modelRoam3.png'
                    }
                ]
            }
        ]
    },
    {
        key:'modelGIS',
        title:'BIM+GIS',
        content:[
            {
                details:[
                    {
                        index:1,
                        desc:'支持GIS、倾斜摄影、BIM模型深入结合应用，可基于地理位置、建筑周围环境等场景叠加展示，满足用户基于真实场景的BIM应用和体验。'
                    }
                ],
                images:[
                    {
                        index:1,
                        imgHref:'GISBIM.png'
                    }
                ]

            }
        ]
    },
    {
        key:'modelGrowth',
        title:'模型生长',
        content:[
            {
                details:[
                    {
                        index:1,
                        desc:'模型构件可模拟动画生长效果，用户可自定义设置构件颜色及模拟时长；模型构件可与施工进度计划节点关联，利用三维模型可视化模拟施工建造过程，用户可直观检查施工计划安排是否合理，也可在施工建造过程中动态展示现场实际施工状态。'
                    }
                ],
                images:[
                    {
                        index:1,
                        imgHref:'modelGrowth.gif'
                    }
                ]
            }
        ]
    },
    {
        key:'modelEffect',
        title:'模型效果',
        content:[
            {
                details:[
                    {
                        index:1,
                        desc:'三维模型支持正交、透视两种查看模式；模型展示支持纹理材质、光照阴影、线框等效果；模型背景支持设置单色、渐变色、图片或全局图等方式，丰富用户模型浏览体验。'
                    }
                ],
                images:[
                    {
                        index:1,
                        imgHref:'modelView1.gif'
                    },
                    {
                        index:2,
                        imgHref:'modelView2.gif'
                    }
                ]
            }
        ]
    },
    // {
    //     key:'modelShare',
    //     title:'模型分享',
    //     content:[
    //         {
    //             details:[
    //                 {
    //                     index:1,
    //                     desc:'中间件图形支持BIM模型分享链接或二维码扫描即可查看BIM模型，无限配置账号。'
    //                 }
    //             ],
    //             images:[
    //                 {
    //                     index:1,
    //                     imgHref:modelShare
    //                 }
    //             ]
    //         }
            
    //     ]
    // },
];
