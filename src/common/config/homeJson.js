/* 首页 banner */
import banner1 from "../img/home/bannerH5_1.webp";
import banner2 from "../img/home/bannerH5_2.webp";
import banner3 from "../img/home/bannerH5_3.webp";

/* EveryBIM图形引擎 */
import fullPort from '../img/home/fullPort.webp';

/* 六大产品优势 */
import product1 from "../img/home/product1.webp";
import product2 from "../img/home/product2.webp";
import product3 from "../img/home/product3.webp";
import product4 from "../img/home/product4.webp";
import product5 from "../img/home/product5.webp";
import product6 from "../img/home/product6.webp";

/* 图形引擎功能 */
import icon_1 from "../img/home/engine/icon_1.webp";
import icon_2 from "../img/home/engine/icon_2.webp";
import icon_3 from "../img/home/engine/icon_3.webp";
import icon_4 from "../img/home/engine/icon_4.webp";
import icon_5 from "../img/home/engine/icon_5.webp";
import icon_6 from "../img/home/engine/icon_6.webp";
import icon_7 from "../img/home/engine/icon_7.webp";
import icon_8 from "../img/home/engine/icon_8.webp";
import icon_9 from "../img/home/engine/icon_9.webp";
import icon_10 from "../img/home/engine/icon_10.webp";

import engine_1 from "../img/home/engine/engine_1.webp";
import engine_2 from "../img/home/engine/engine_2.webp";
import engine_3 from "../img/home/engine/engine_3.webp";
import engine_4 from "../img/home/engine/engine_4.webp";
import engine_5 from "../img/home/engine/engine_5.webp";
import engine_6 from "../img/home/engine/engine_6.webp";
import engine_7 from "../img/home/engine/engine_7.webp";
import engine_8_1 from "../img/home/engine/engine_8_1.webp";
import engine_8_2 from "../img/home/engine/engine_8_2.webp";
import engine_9 from "../img/home/engine/engine_9.webp";
import engine_10 from "../img/home/engine/engine_10.webp";

import example1 from '../img/example/example1.png';
import example2 from '../img/example/example2.png';
import example3 from '../img/example/example3.png';
import example4 from '../img/example/example4.png';
import example5 from '../img/example/example5.png';
// import example6 from '../img/example/example6.png';
import example7 from '../img/example/example7.png';
import example8 from '../img/example/example8.png';
import example9 from '../img/example/example9.png';

import scene1 from "../img/sence/bg1.png";
import scene2 from "../img/sence/bg2.png";
import scene3 from "../img/sence/bg3.png";
import scene4 from "../img/sence/bg4.png";
import scene5 from "../img/sence/bg5.png";

import { senceModel } from './websiteConfig';

//首页 - banner
export const homeBannerData = [
  {
      key: 1,
      title: `EveryBIM图形引擎`,
      desc:[
          {
            index: 1, 
            text:'国内领先的BIM图形引擎'
          },
          {
            index: 2, 
            text:'具有完全独立自主知识产权和专利技术'
          },
      ],
      btnLink:{
          linkText: "免费体验",
          linkHref: "/developer",
      },
      btnLive:{
        linkText: "下载体验",
        // linkHref: "/downloads",
      },
      imgHref: banner1
  },
  {
    key: 2,
    title: `EveryBIM图形引擎`,
    desc:[
        {
          index: 1, 
          text:'自助、高效、安全、可控'
        },
        {
          index: 2, 
          text:'专为建筑行业数字化提供BIM轻量化的二次开发服务'
        },
    ],
    btnLink:{
        linkText: "免费体验",
        linkHref: "/developer",
    },
    btnLive:{
      linkText: "下载体验",
      // linkHref: "/downloads",
    },
    imgHref: banner2
},
  {
      key: 3,
      title: `性能卓越的图形引擎`,
      desc:[
          {
            index: 1, 
            text:'平均1/20以上的轻量化比例'
          },
          {
            index: 2, 
            text:'30G+模型秒级加载，支持百万级构件，亿级三角面片'
          },
      ],
      btnLink:{
        linkText: "免费体验",
        linkHref: "/developer",
      },
      btnLive:{
        linkText: "下载体验",
        linkHref: "/downloads",
      },
      imgHref: banner3
  },
];

//首页 - EveryBIM图形引擎
export const homeFullPort = {
  title: 'EveryBIM图形引擎',
  caption: 'EveryBIM图形引擎是译筑科技专为工程建设行业自主研发的轻量化BIM开发组件及二次开发平台。其具备轻量化、大体量、全端口、多数据源等能力，可为行业各类BIM应用及解决方案提供轻量化图形技术支持',
  image: fullPort
};

//首页 - 六大产品优势
export const productAdvantage = [
  {
      key: 1,
      title: "自主研发",
      describe: "具有完全自主的知识产权，曾服务多家纯内网涉密项目，保障数据绝对安全",
      imgHref: product1
  },
  {
      key: 2,
      title: "性能卓越",
      describe: "平均1/20以上的轻量化比例， 30G+模型秒级加载，支持百万级构件、亿级三角面片",
      imgHref: product2
  },
  {
      key: 3,
      title: "功能落地",
      describe: "历经600+企业，10000+项目，50000+人员深度应用，具备领先的稳定性和可靠性",
      imgHref: product3
  },
  {
      key: 4,
      title: "多维融合",
      describe: "BIM、CAD、GIS无缝集成，虚实结合，与物联网感知系统深度集成",
      imgHref: product4
  },
  {
      key: 5,
      title: "灵活开发",
      describe: "无开发语言限制，兼容多种数据库，适配主流操作系统，实现全场景支持",
      imgHref: product5
  },
  {
      key: 6,
      title: "快速交付",
      describe: "周期短，成本低，风险小，支持云架构快速交付",
      imgHref: product6
  }
];

//首页 - 图形引擎功能
export const engineModule = {
  title: "图形引擎功能",
  describe: "针对建筑行业的规划、咨询、设计、建造、运维等全生命周期的业务场景，提供完善的BIM功能，帮助用户打造高效落地的BIM应用",
  tab1:[
    {
      key:'1',
      tab:'大场景应用',
      icon:icon_3,
      content:[
        {
            index:1,
            title:'超大场景高性能展示',
            caption:'20倍以上压缩效率，30G+模型秒级加载；支持百万级构件、亿级三角面片极速渲染',
            imgArray:[
              {
                key: 1,
                href: engine_3
              }
            ]
        },
        
      ]
    },
    {
      key:'2',
      tab:'多终端协同',
      icon:icon_5,
      content:[
        {
            index:2,
            title:'多终端一致性体验',
            caption:'各端口（Web端、PC客户端、移动端）均支持无损轻量化模型浏览展示，可随时随地浏览模型',
            imgArray:[
              {
                key: 1,
                href: engine_5
              }
            ]
        },
        
      ]
    },
    {
      key:'3',
      tab:'移动BIM',
      icon:icon_2,
      content:[
        {
            index:3,
            title:'普通手机轻松玩转BIM模型',
            caption:'轻量化BIM模型触手可及，普通手机也能流畅打开大模型，让BIM模型无需高端硬件即可随时应用',
            imgArray:[
              {
                key: 1,
                href: engine_2
              }
            ]
        },
      ]
    },
    {
      key:'4',
      tab:'高级可视化',
      icon:icon_7,
      content:[
        {
            index:4,
            title:'高级可视化能力',
            caption:'对于光照、阴影、反走样、纹理贴图、场景模拟等高级可视化特性，实现逼真的渲染效果',
            imgArray:[
              {
                key: 1,
                href: engine_7
              }
            ]
        },
        
      ]
    },
    {
      key:'5',
      tab:'行业BIM应用',
      icon:icon_8,
      content:[
        {
            index:5,
            title:'功能贴合工程建设行业应用',
            caption:'完全为工程建设行业量身打造，具有二三维联动、版本管理、测量、剖切、漫游等实用功能',
            imgArray:[
              {
                key: 1,
                href: engine_8_1
              },
              {
                key: 2,
                href: engine_8_2
              }
            ]
        },
        
      ]
    }
  ],
  tab2:[
    {
      key:'6',
      tab:'模型轻量化',
      icon:icon_1,
      content:[
          {
              index:6,
              title:'多模式BIM模型转化',
              caption:'支持便捷的云转化和高效的本地转化，保障BIM模型无损轻量化上传',
              imgArray:[
                {
                  key: 1,
                  href: engine_1
                }
              ]
          },
          
      ]
    },
    {
      key:'7',
      tab:'模拟动效',
      icon:icon_4,
      content:[
        {
            index:7,
            title:'真实动画，沉浸式体验',
            caption:'构件生长、着色，漫游巡检、天气动效、消防模拟、光照阴影等真实场景动画，可沉浸式体验BIM',
            imgArray:[
              {
                key: 1,
                href: engine_4
              }
            ]
        },
        
      ]
    },
    {
        key:'8',
        tab:'多维融合',
        icon:icon_6,
        content:[
          {
              index:8,
              title:'多维多源数据无缝集成',
              caption:'支持2D、3D、GIS多维多源数据无缝集成，兼容主流建模软件的格式，按需使用',
              imgArray:[
                {
                  key: 1,
                  href: engine_6
                }
              ]
          },
          
        ]
    },
    {
        key:'9',
        tab:'工程量提取',
        icon:icon_9,
        content:[
          {
              index:9,
              title:'模型工程量，一键提取',
              caption:'直接利用BIM模型自带的属性信息，一键提取工程量数据；可以按不同维度统计工程量',
              imgArray:[
                {
                  key: 1,
                  href: engine_9
                }
              ]
          },
          
        ]
    }, 
    {
        key:'10',
        tab:'物联集成',
        icon:icon_10,
        content:[
          {
              index:10,
              title:'物联网集成，虚实结合',
              caption:'整合监测数据，结合BIM可视化地逬行图模数据分析，实时发布预警，可随时查看监控点情况',
              imgArray:[
                {
                  key: 1,
                  href: engine_10
                }
              ]
          },
        ]
    }, 
  ],
};

//示例模型
export const modelArray = [
  {
    key: 1,
    title: '示例模型',
    type: 'RVT',
    desc: '材质',
    modelId: '61b30fdaf2a3ee720095ee1c',
    imgHref: example1,
    version:1
  },
  {
    key: 2,
    title: '古典建筑',
    type: 'RVT',
    desc: '古建筑、材质贴图',
    modelId: '61b31003f2a3ee720095ee1f',
    imgHref: example2,
    version:1
  },
  {
    key: 3,
    title: '商务综合广场',
    type:'RVT',
    desc: '综合体、全专业',
    modelId: '61b312c9f2a3ee720095ee65',
    imgHref: example3,
    version:1
  },
  {
    key: 4,
    title: '立交桥',
    type:'RVT',
    desc: '线性工程、道路、桥梁',
    modelId: '61b31093f2a3ee720095ee24',
    imgHref: example4,
    version:1
  },
  {
    key: 5,
    title: '热风炉',
    type: 'IFC',
    desc: '工业',
    modelId: '61b310aff2a3ee720095ee29',
    imgHref: example5,
    version:1
  },
  {
    key: 6,
    title: '机电',
    type: 'RVT',
    desc: '管线工程',
    modelId: '61b3118af2a3ee720095ee3a',
    imgHref: example7,
    version:1
  },
  {
    key: 7,
    title: '空港新城',
    type: 'RVT',
    desc: '大场景、综合区',
    modelId: '61b311acf2a3ee720095ee40',
    imgHref: example8,
    version:1
  },
  {
    key: 8,
    title: '示例图纸',
    type: 'DWG',
    desc: '二维矢量图',
    modelId: '61b2fc36f2a3ee720095ed62',
    imgHref: example9,
    version:1
  },
];

//正式 示例场景
export const sceneList = [
  {
    key: 1,
    link:'/BIMPlatform',
    modelId: senceModel.modelId,
    version: 1,
    imgHref: scene1,
    title: 'BIM协同应用',
    caption:'基于模型视口、选择集、批注等功能，构件在项目协同管理中可视化的协同沟通业务，提高协同效率',
  },
  {
    key: 2,
    link:'/BIMInternet',
    modelId: senceModel.roomModelId,
    version: 1,
    imgHref: scene2,
    title: 'BIM+物联网',
    caption:'基于EveryBIM图形引擎，可视化的管理物联网传感器设备，实现智慧工地、智能化运维等业务应用',
  },
  {
    key: 3,
    link:'/versionCompare',
    modelId: senceModel.modelId,
    version: 2,
    imgHref: scene3,
    title: '版本对比',
    caption:'可视化、智能化管理模型版本，提高版本对比核查、版本变更',
  },
  {
    key: 4,
    link:'/linkage',
    modelId: senceModel.modelId,
    version: 1,
    imgHref: scene4,
    title: '图模联动',
    caption:'基于BIM和图纸的构件联动或者位置联动，快速定位BIM和图纸，最大程度发挥模型和图纸的价值',
  },
  {
    key: 5,
    link:'/digitalMap',
    modelId: senceModel.drawingId,
    version: 1,
    imgHref: scene5,
    title: '图纸在线管理',
    caption:'针对DWG图纸，提供在线预览查看，摆脱传统客户端的限制，实现用户可随时随地应用图纸',
  },
];

export const outDrawingList = [
  {
    key: 1,
    drawingId: senceModel.drawingId,
    drawingVersion: 1,
    drawingName: '示例图纸'
  }
]
