/**
 * 用来保存全局常量
 * 模型服务器配置
 */
let ModelConfig = null;
    
//模型列表 模型服务器配置
ModelConfig = {
  appKey: 'A7A1D7D4F84328B2169419F8',
  appScrect: 'TdCsAGoWmzTs1ACScU7pp5BP22O70y6XXvsyFqIZXVCVRG1p',
  serverIp: '',
};


//打包 server 置空
let server = '';
//let server ='http://everybim.vaiwan.com';

let APIURL = `${server}/api`;


//130测试环境
// let BaseURL = 'http://180.168.170.198:45412';  
// let oauthURL = 'http://180.168.170.198:45410';
// let consoleURL = 'https://console.ezbim.net';
// let senceModel= {
//   modelId: '620dec89b681f31227fea1d3',
//   roomModelId:'620b0a4d4a2be516564028c8',
//   drawingId:'620b0a634a2be516564028d0',
// };

//128测试环境
// let BaseURL = 'http://192.168.1.128:32200'; 
// let oauthURL = 'http://192.168.1.128:32201';
// let consoleURL = 'https://console.ezbim.net';
// let senceModel= {
//   modelId: '62e88c33f0af760f68a49c01',
//   roomModelId: '62e88c47f0af760f68a49c02',
//   drawingId:'62e88c60f0af760f68a49c03',
// }

// 正式 环境
let BaseURL = 'https://everybim.net';  
let oauthURL = 'https://sso.everybim.net';   //运维管理平台地址
let consoleURL = 'https://console.ezbim.net'; //sso服务地址
let senceModel= {
  modelId: '6260b83262c3e033fa78b263',
  roomModelId: '61e7e654ae06e02e242605f8',
  drawingId:'61b2fc36f2a3ee720095ed62',
}

let loginURL = `${oauthURL}?redirect=yizhu`; //译筑通行证地址


module.exports = {
  baseURL: BaseURL,
  ApiURL: APIURL,
  oauthURL: oauthURL,
  loginURL: loginURL,
  consoleURL: consoleURL,
  modelConfig: ModelConfig,
  senceModel: senceModel
}