
import rest from './rest';
import { ApiURL, consoleURL} from '../config/websiteConfig';

export function _concatUrl(url) {
    return `${ApiURL}/v1/account-service/${url}`;
}

function _hookError(error, message) {
    console.log(message + '出现错误 => ', error);
    throw error;
}

export function getExampleList(){
    let url = `${ApiURL}/v1/example/list`
    return rest.get(url,{})
        .then(result => result)
        .catch(err => _hookError(err, "获取示例模型接口")) 
}

export function getExampleToken(){
    let url = `${ApiURL}/v1/example/token`
    return rest.get(url,{})
        .then(result => result)
        .catch(err => _hookError(err, "获取示例模型预览token")) 
}

export function convertToken(config){
    let url  = `${ApiURL}/v1/user/token/convert/everybim`;
    return rest.get(url,config)
       .then(result => result)
       .catch(err => _hookError(err, "获取BIM-token"))
}

export function getToken(appKey,appScrect){
    let url = `${ApiURL}/v3/token/get/${appKey}/${appScrect}`;
    return rest.get(url,{})
        .then(result => result)
        .catch(err => _hookError(err, "获取token"))
}

export function getCategory() {
    return rest.get(`${consoleURL}/api/category?order=sort`,{})
        .then((result) => {
            return result;
        })
        .catch(err => _hookError(err, '获取安装包分类'))
}

export function getPackages(packageId) {
    return rest.get(`${consoleURL}/api/software?where={"categoryId":"${packageId}"}`,{})
        .then((result) => {
            return result;
        }).catch(err => _hookError(err, '获取安装包产品详情'))
}

export function getQrCodeById(id) {
        return rest.get(`${consoleURL}/api/mobile/qrimage?id=${id}&engine=true`,{})
        .then((result) => {
            return result;
        }).catch(err => _hookError(err, '获取二维码'))
}

export function getChangeLog(skip,limit) {
    return rest.get(`${consoleURL}/api/updateLog?skip=${skip}&limit=${limit}`,{})
        .then((result) => {
            return result;
        }).catch(err => _hookError(err, '获取更新日志'))
}

export function getChangeLogCount() {
    return rest.get(`${consoleURL}/api/updateLog?only_count=true`,{})
        .then(result => result)
        .catch(err => _hookError(err, "获取更新日志总数"))
}

export function analysisModelFile(id, config){
    return rest.get(`${ApiURL}/v3/model/view/file/analysis/${id}`, config)
    .then(result => result)
    .catch(err => _hookError(err, "获取更新日志总数"))

}

