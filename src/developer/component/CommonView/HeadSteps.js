import React from 'react';
import img_shape from '../../../common/img/developer/shape.png';
import './commonView.less';

// steps: [{key: xKey, title: xTitle, icon: xIcon}]
// curStep: xKey

class HeadSteps extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let {steps, curStep} = this.props;

        // 当前步骤下标
        let curStepIndex = 0;
        steps.map((stepItem, index) => {
            if (curStep === stepItem.key) {
                curStepIndex = index;
            }
        });

        let stepTool = [];
        for (let i = 0; i < steps.length; i++) {
            if (i !== 0) {
                stepTool.push(<img className="head-step-tool-space" key={`STEP_KEY_IMG_${i}`} src={img_shape} alt=""/>)
            }
            let stepItem = steps[i];
            stepTool.push(<StepToolItem key={stepItem.key || i}
                                        icon={stepItem.icon || (i + 1)}
                                        title={stepItem.title}
                                        isActivity={i <= curStepIndex}/>)
        }

        return (
            <div className="head-steps">
                {stepTool}
            </div>
        )
    }
}

function StepToolItem(props) {
    const {isActivity, icon, title} = props;
    return (
        <div className="head-step-tool-item">
            <div className={`head-step-tool-item-icon ${isActivity ? "head-activity-icon" : null}`}>
                {icon}
            </div>
            <div className={`head-step-tool-item-title ${isActivity ? "head-activity-title" : null}`}>
                {title}
            </div>
        </div>
    )
}

export default HeadSteps;
