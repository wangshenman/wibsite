import React from 'react';
import '../HeaderView/headerView.less';

class FooterView extends React.Component {
    constructor(props) {
        super(props);
        this.state={
        }
    }

    render() {
        return (
            <div className="dev-footer">
                <div className="copyRight">2014-2021© <a href="http://www.ezbim.net/">译筑信息科技(上海)有限公司</a> 旗下产品</div>
            </div>
        )
    }
}

export default FooterView;