import React from 'react';
import { connect } from 'react-redux';
import { developerRouters as routers } from '../../../router/developerRouters';
import { Link, withRouter } from 'react-router-dom';
import { Dropdown, Menu, Avatar, Icon } from 'antd';
import { getLogout } from '../../console/actions';
import { deleteToken } from '../../../common/utils/Utility';
import { loginURL } from '../../../common/config/websiteConfig';
import logo from '../../../common/img/common/developer_logo.png';
import img_admin from '../../../common/img/common/default_admin.png';
import './headerView.less';

class HeaderView extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            navLink: [],
        }
        //退出登录
        this.goLogout = ()=>{
            getLogout().then(res=>{
                if(res && res.code === 200){
                    deleteToken();
                    window.location.href = loginURL;
                }
            }).catch(error => console.log(error && error.message))
        }

        this.onMenuClick = (key) => {
           switch(key){
               case'console':
                 return this.props.history.push('/developer');
               case 'index':
                  return this.props.history.push('/');
               case 'userInfo':
                  return this.props.history.push('/userInfo');
               case 'logout':
                //   deleteToken();
                //   return window.location.href = loginURL;
                  return this.goLogout();
                default:
                 break;
           }
        }
    }
    componentDidMount() {
       let orderLink = routers.filter(item => item.isNav);
       this.setState({navLink: orderLink})
    }

    render() {
        const { navLink } = this.state;
        const { userInfo} = this.props;

        const userName =  userInfo && userInfo.responseDUser && userInfo.responseDUser.nickName;
        const userAvatar = userInfo && userInfo.image; 
        const userMenuConfig = [
            {
                title: '进入官网',
                key: 'index',
                action: (key) => this.onMenuClick(key)
            },
            {
                title: '个人信息',
                key: 'userInfo',
                action: (key)=> this.onMenuClick(key)
            },
            {
                title: '退出登录',
                key: 'logout',
                action: (key) => this.onMenuClick(key)
            },
        ];
        const userMenu = (
            <Menu>
                {
                    (userMenuConfig||[]).map(i => <Menu.Item key={i.key} onClick={()=>i.action(i.key)}>{i.title}</Menu.Item>)
                }
            </Menu>
        );
        let hashRoot = window.location.pathname.slice(1);
        let activeKey = hashRoot.indexOf('/') === -1 ? hashRoot : hashRoot.slice(0, hashRoot.indexOf('/'));
        return (
            <div className="dev-header">
                <div className="dev-header-left">
                    <div className="dev-header-logo" onClick={()=>this.onMenuClick('console')}>
                        <img src={logo} alt="logo"/>
                        <div className="logo-name">EveryBIM 开发者中心</div>
                    </div>
                    <div className="dev-header-navLink">
                        {
                            navLink && navLink.map(item =>
                            <div key={item.title} className="nav-button">
                                {
                                   item.isNav ? 
                                   (<Link to={item.path} className={"/" + activeKey === item.path ? "active" : ""} title={item.title}>
                                       {item.title}
                                   </Link>)
                                   : 
                                   (<span>{item.title}</span>)
                                }
                            </div>)
                        }
                    </div>
                </div>
                <div className="dev-header-right">
                    <Dropdown overlay={userMenu} trigger={['click']} placement="bottomCenter">
                        <div className="user-setting">
                            <div className="user-avatar">
                                <Avatar src={userAvatar || img_admin}/>
                            </div>
                            <div className="user-msg">
                                <span>{userName || '译筑001'}</span>
                            </div>
                            <div className="user-setting-down">
                                <Icon type="caret-down" style={{color:'#E4E4E4'}}/>
                            </div>
                        </div>
                    </Dropdown>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
      userInfo: state.getIn(["user", "user_info"]) && state.getIn(["user", "user_info"]).toJS(),
    };
  }
  
const mapActionCreators = {

};

export default connect(mapStateToProps, mapActionCreators)(withRouter(HeaderView));