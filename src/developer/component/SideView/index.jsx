import React from 'react';
// import { connect } from 'react-redux';
import { Menu } from 'antd';
import './sideview.less';
import infoIcon from '../../../common/img/developer/info_icon.png';
import modelIcon from '../../../common/img/developer/model_icon.png';
import drawingIcon from '../../../common/img/developer/drawing_icon.png';
// import thingIcon from '../../../common/img/developer/thing_icon.png';
// import image_base from '../../../images/ic_home_base.png';
// import {version, date} from '../../../../app.config';

const SubMenu = Menu.SubMenu;

class SideView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };

        this.onMenuSelect = (item) => {
            this.props.sideSelect(item.key);
        };

        this.sideImage = (link) => {
            return null;
        }
    }

    render() {
        let menus = [
                {'title': '应用信息', 'link': 'applicationInfo', 'icon': infoIcon},
                {'title': 'BIM集成', 'link': 'bimModelList', 'icon': modelIcon},
                {'title': '图纸集成', 'link': 'drawingsList', 'icon': drawingIcon},
                // {'title': '物品集成', 'link': 'thingsList', 'icon': thingIcon},
            ];

        let menu_props = {
            theme: 'dark',
            mode: 'inline',
            selectedKeys: this.props.selectedKeys||[],
            onSelect: this.onMenuSelect
        };

        const subTitle = (item, visible) => {
            return <span>
                {
                    visible
                        ? <img className="side-img" src={this.sideImage(item.link)} alt=""/>
                        : null
                }
                {item.title}
            </span>
        };

        const menuItem = (items, level) => {
            return items.map(item => {
                return item.children && item.children.length > 0
                    ? <SubMenu key={`${item.link}`}
                               title={subTitle(item, level === 0)}>
                        {
                            menuItem(item.children, level + 1)
                        }
                    </SubMenu>
                    : <Menu.Item key={`${item.link}`}>
                        {/* <NavLink to={`/developer/${item.link}`}> */}
                            {
                                level === 0
                                    ? <img className="side-img" src={item.icon} alt=""/>
                                    : null
                            }
                            {item.title}
                        {/* </NavLink> */}
                    </Menu.Item>
            })
        };

        return (
            <div className="side">
                <Menu {...menu_props}>
                    {menuItem(menus, 0)}
                </Menu>
            </div>
        )
    }
}

export default SideView;
