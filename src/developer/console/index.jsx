import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../component/HeaderView';
import SideView from '../component/SideView';
import ApplicationInfo from './module/applicationInfo';
import BimModelList from './module/bimModelList';
import DrawingsList from './module/drawingsList';
import ThingsList from './module/thingsList';
import {getUserInfo, getUserToken} from './actions';
import {getURLValueByKey,deleteToken} from '../../common/utils/Utility';
import {loginURL} from '../../common/config/websiteConfig';
import './console.less';

class Console extends Component {
  constructor(props) {
    super(props)
    this.state = {
      active: 'applicationInfo',
      loading: false,
    };

    this.onClickMenu = (activePage)=>{
      this.setState({
        active: activePage,
      },()=>{
        if(activePage === 'applicationInfo'){
          this.request();
        }
      });
    };

    //获取用户信息
    this.request = () =>{
      this.props.getUserInfo();
      this.setState({loading: false});
    };

    this.reflesh =()=>{
      this.props.getUserInfo();
    }

    //获取用户
    this.getToken = (code) =>{
      getUserToken(code).then(res =>{
        if(res && res.code === 200){
          let userToken = res.data && res.data.access_token;
          window.localStorage.setItem('token',userToken);
          if(userToken){
            window.location.href = '/developer';
          }
        }else{
           deleteToken();
           setInterval(getLogin, 2000);
        }
      })
    }

    this.init = () =>{
      let code = getURLValueByKey('code');
      this.setState({loading: true})
      if(code){
        deleteToken();
        this.getToken(code);
      }else{
        this.request();
      }
    };
  }

  componentDidMount(){
    this.init();
  }

  render() {
    const {userInfo} = this.props;
    const {active, loading} = this.state;
    const currentPlan = userInfo && userInfo.currentPlan;
    const getMenuContent = (key) =>{
      switch (key) {
        case 'applicationInfo':
          return <ApplicationInfo userInfo={userInfo} reflesh ={this.reflesh} loading={loading}/>;
        case 'bimModelList':
          return <BimModelList planInfo={currentPlan}/>;
        case 'drawingsList':
          return <DrawingsList planInfo={currentPlan}/>
        case 'thingsList':
          return <ThingsList planInfo={currentPlan}/>
        default:
          return <ApplicationInfo userInfo={userInfo} reflesh ={this.reflesh} loading={loading}/>;
      }
    }

    return (
      <div className="console-page">
        < Header/>
        <div className='console'>
           <div className="dev-side">
             <SideView sideSelect={this.onClickMenu} selectedKeys={[this.state.active]}/>
           </div>
            <div className="dev-contain">
                 { getMenuContent(active)}
            </div>
        </div>
      </div>
    )
  }
}

function getLogin() {
  window.location.href = loginURL;
}

function mapStateToProps(state) {
  return {
    userInfo: state && state.getIn(["user", "user_info"]) && state.getIn(["user", "user_info"]).toJS(),
  };
}

const mapActionCreators = {
  getUserInfo
};

export default connect(mapStateToProps, mapActionCreators)(Console);



