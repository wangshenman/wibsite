import React, { Component } from 'react';
import { withRouter} from 'react-router-dom';
import {  Modal } from 'antd';
import moment from 'moment';
const { confirm } = Modal;
class AppKey extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tipVisible:true,
      isShow: false,
      appSecretText:'***************',
      appInfo:{}
    }
    this.onClose = () =>{
       this.setState({
          tipVisible: false
       })
    }
    this.onShow = () =>{
      const {isShow} = this.state;
      if(this.props.isBind){
        this.setState({
          isShow: !isShow,
          appSecretText: isShow ? '***************' : this.props.appInfo.appSecret
        })
      }else{
        this.props.onImprove();
      }
     }

     this.onReset = () =>{
      let that = this;
      if(this.props.isBind){
        confirm({
          title: '此操作会导致token不可用，确定要重置AppSecret吗？',
          okText:'确认',
          cancelText:'取消',
          onOk() {
            that.props.onReset()
          }
      });
      }else{
        this.props.onImprove();
      }
     }
  }

  componentDidMount(){
    // const {appInfo} = this.props;
    // if(appInfo){
    //   this.state({appInfo})
    // }
  } 

  render() {
    const { isShow, appSecretText } = this.state;
    const { appInfo } = this.props;
    let appkeyItem = (key, value, isOperate) =>{
      return (
        <div className="keyTab-item">
            <span className="item-key">{key}</span>
            <div className={`item-value ${isOperate ?'secret-value':''} ${!isShow ? 'show-text':''}` }>{value}</div> 
            {
              isOperate ? 
              <div className="item-button flex-between">
                 <div className="oper-btn show-btn" onClick={this.onShow}>{`${isShow ? '隐藏' : '显示'}`}</div>
                 <div className="oper-btn reset-btn" onClick={this.onReset}>重置</div>
              </div> : null

            } 
        </div>
      )
    }

    return (
      <div className='appKey'>
        {
          this.state.tipVisible ? 
          <div className="tip-wrapper flex-between">
           <div><span className="main-word">提醒！</span><span>请妥善保管好您的Secret，防止带来不必要的安全风险</span></div>
           <span className="close-btn" onClick={this.onClose}>x</span>
          </div> : null
        }
        <div className="appKey-content flex-between">
           {appkeyItem('AppKey',appInfo && appInfo.appKey)}
           {appkeyItem('AppSecret',appSecretText, true)}
           {appkeyItem('更新时间',moment(appInfo && appInfo.updatedAt).format('YYYY-MM-DD'))}
        </div>
      </div>
    )
  }
}
export default withRouter(AppKey)