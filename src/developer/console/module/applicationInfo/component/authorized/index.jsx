import React, { Component } from 'react';
import { Tooltip } from 'antd';
import modelIcon from '../../../../../../common/img/developer/BIM.png';
import drawingIcon from '../../../../../../common/img/developer/drawing.png';
import thingIcon from '../../../../../../common/img/developer/thing.png';
import gisIcon from '../../../../../../common/img/developer/gis.png';
import {renderSize,getTextByJs} from '../../../../../../common/utils/Utility'

export default class Authorized extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tipVisible: true,
    }
    

    this.onClose = () =>{
      this.setState({
        tipVisible: false
      })
     
   }
  }

  

  render() {
    const {planInfo} = this.props;
    const detailInfo = [
      {
        key:'model',
        title: 'BIM模型',
        icon: modelIcon,
        data:[
          {
            label: 'BIM支持格式：',
            value: getTextByJs(planInfo && planInfo.bimSupport)
          },
          {
            label: '文件大小限制：',
            value: `≤${renderSize(planInfo && planInfo.bimSize)}`
          },
          {
            label: '数量限制：',
            value: `≤${planInfo && planInfo.bimCount}个文件`
          }
        ]
      },
      {
        key:'drawing',
        title: '图纸',
        icon: drawingIcon,
        data:[
          {
            label: '图纸支持格式：',
            value: getTextByJs(planInfo && planInfo.drawingSupport)
          },
          {
            label: '文件大小限制：',
            value: `≤${renderSize(planInfo && planInfo.drawingSize)}`
          },
          {
            label: '数量限制：',
            value: `≤${planInfo && planInfo.drawingCount}个文件`
          }
        ]
      },
      {
        key:'gis',
        title: 'GIS',
        icon: gisIcon,
        data:[
          {
            label: 'GIS当前支持：',
            value: '天地图'
          },
          {
            label: '倾斜摄影文件格式限制：',
            value: '3Dtiles'
          },
          {
            label: 'GIS场景数量限制：',
            value: `100`
          }
        ]
      },
      // {
      //   key:'thing',
      //   title: '物品',
      //   icon: thingIcon,
      //   data:[
      //     {
      //       label: '物品支持格式：',
      //       value: getTextByJs(planInfo && planInfo.thingSupport)
      //     },
      //     {
      //       label: '文件大小限制：',
      //       value: `≤${renderSize(planInfo && planInfo.thingSize)}`
      //     },
      //     {
      //       label: '数量限制：',
      //       value: `≤${planInfo && planInfo.thingCount}个文件`
      //     }
      //   ]
      // },
    ];

    return (
      <div className='authorized'>
        {
          this.state.tipVisible ? 
          <div className="tip-wrapper flex-between">
            <div><span className="main-word">请注意！</span><span>{planInfo && planInfo.planDesc}</span></div>
            <span className="close-btn" onClick={this.onClose}>x</span>
          </div> : null
        }
        <div className="authorized-content flex-start" id="authorizedContent">
          {
            (detailInfo||[]).map((item,index) =>{
              return(
                <div className="authorized-item" key={item.key}>
                  <div className="item-header">
                    <img src={item.icon} alt=""/>
                    <span className="item-header-title">{item.title}</span>
                  </div>
                  <ul className="item-content">
                    {
                      (item.data || []).map((i,index) =>{
                        return(
                         <li className="flex-between" key={index}>
                            <span>{i.label}</span>
                            <Tooltip key={index} 
                                  title={i.value} 
                                  placement="top" 
                                  getPopupContainer={() => document.getElementById('authorizedContent')}
                            >
                                <span className='item-value'>{i.value}</span>
                            </Tooltip>
                         </li>
                        )
                      })
                    }
                  </ul>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}
