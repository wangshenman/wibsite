import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import moment from 'moment';
import { Button, Progress} from 'antd';
import { renderSize, getNewDate} from '../../../../../../common/utils/Utility';
class DetailInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
    this.onEdit = () =>{
      if(this.props.isBind){
        this.props.onUpdate();
      }else{
        this.props.onImprove();
      }
      
    }
    this.onUpdate = () =>{
      if(this.props.isBind){

      }else{
        this.props.onImprove();
      }
    }
  }

  render() {
    const {appInfo} = this.props;
    let app = appInfo && appInfo.responseDUser;
    let plan = appInfo && appInfo.currentPlan;

    let allSize = plan && plan.allSize;
    let usedSize = (app && app.usedSize)|| 0;
    let updatedAt = moment(app && app.updatedAt).format('YYYY-MM-DD');
    
    return (
      <div className='detailInfo flex-between'>
           <div className="detail-item">
             <span className="detail-label">应用名称</span>
             <div className="detail-opreate flex-between">
               <div className="detail-name">{app && app.appName}</div>
                <Button onClick={this.onEdit}>编辑</Button>
             </div>
           </div>
           <div className="detail-item">
             <span className="detail-label">用户版本</span>
             <div className="detail-opreate flex-between">
               <div className="detail-name">{plan && plan.planName}</div>
                {/* <Button type="primary" onClick={this.onUpdate}>升级</Button> */}
             </div>
           </div>
           <div className="detail-item">
             <span className="detail-label">空间总容量</span>
             <div className="detail-opreate flex-between">
                <Progress className="detail-progress" 
                          percent={ parseInt(usedSize) / parseInt(allSize) * 100} 
                          strokeWidth={8} 
                          showInfo={false}
                />
                <span className="sizeShow nowrap">{`${renderSize(usedSize)}/${renderSize(allSize)}`}</span>
             </div>
           </div>
           <div className="detail-item">
             <span className="detail-label">有效期</span>
             <div className="detail-opreate flex-between">
               <div className="detail-name">{ plan && plan.expireTime ? (moment(plan.expireTime).format('YYYY-MM-DD')) : '--'}</div>
               {/* <span>到期自动续期</span> */}
             </div>
           </div>
      </div>
    )
  }
}

export default withRouter(DetailInfo)