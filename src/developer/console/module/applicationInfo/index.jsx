import React, { Component } from 'react';
import { connect } from 'react-redux';
import {  Input,  Cascader,message, Modal, Spin} from 'antd';
import {position} from '../../../../common/config/areaJson';
import AppKey from './component/appKey';
import DetailInfo from './component/detailInfo';
import Authorized from './component/authorized';
import './applicationInfo.less';
import {updateUserInfo,resetAppsecret} from '../../actions';

class ApplicationInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      improveInfoVisible: false,
      editAppNameVisible: false,
      applicationInfo: {}
    }

    this.init = () =>{
      const {userInfo} = this.props;
      if(userInfo){
        this.setState({
          applicationInfo: userInfo
        })
      }
    }

    this.improveModal = () =>{
       this.setState({
           improveInfoVisible: true
       })
    }

    this.updateModal = () => {
      this.setState({
        editAppNameVisible: true
      })
    }

    this.switchState = (key, value) => {
      if(key === 'address'){
        this.setState({
          [key]: `${value[0]}-${value[1]}-${value[2]}`
        });
      }else{
        this.setState({ 
          [key]: value
        })
      }
    };

    this.editInfo = () =>{
      const {appName} = this.state;
      if(!appName){ return message.warning('请输入应用名称~') };
      let parms ={ appName }
      this.updateAppInfo(parms ,'editAppNameVisible')
    };

    this.submitInfo = () =>{
      const {position,company,address} = this.state;
      if(!company){ return message.warning('请输入公司名称~') };
      if(!address){ return message.warning('请输入公司地址~') };
      if(!position){ return message.warning('请输入个人职位~') };
      let data ={ position, company, address};
      this.updateAppInfo(data ,'improveInfoVisible');
    };

    this.updateAppInfo = (data, modalKey) =>{
      this.props.updateUserInfo(data).then(res =>{
        if(res && res.code === 200){
          message.success('提交成功')
        }else{
          res && message.error(res.message)
        }
        this.setState({
          [modalKey]: false
        },()=> this.props.reflesh())

      }).catch(error => 
        error && message.error(error.message)
      )
    };

    this.onReset = ()=>{
      resetAppsecret().then(res =>{
        if(res && res.code === 200){
          message.success('提交成功')
          this.props.reflesh()
        }else{
          res && message.error(res.message)
        }
      }).catch(error => 
        error && message.error(error.message)
      )
    }
  }

  componentDidMount(){
     this.init();
  }
  componentWillReceiveProps(nextProps){
     this.init();
  } 

  render() {
    const {userInfo} = this.props;
    const isBind = userInfo && userInfo.responseDUser && userInfo.responseDUser.bindMessage;
    const {improveInfoVisible,editAppNameVisible} = this.state;

    let getContent = (key) =>{
      switch (key) {
          case 'appKey':
            return <AppKey onImprove={this.improveModal} 
                           onReset={this.onReset}
                           appInfo={ userInfo && userInfo.responseDUser} 
                           isBind={isBind} 
                  />;
          case 'detailInfo':
            return <DetailInfo onImprove={this.improveModal} 
                               onUpdate={this.updateModal} 
                               appInfo={userInfo} 
                               isBind={isBind} 
                  />;
          case 'authorized':
            return <Authorized planInfo={userInfo && userInfo.currentPlan}/>
          default:
            break;
      }
    };

    let baseInfoItem = (title, key) => {
      return (
          <div className="appInfo-item">
              <div className="appInfo-item-title">{title}</div>
              <div className="appInfo-item-content scl">{getContent(key)}</div>
          </div>
      )
    };

    return (
     <Spin spinning={this.props.loading} className="dev-spin"> 
      <div className='applicationInfo'>
          { baseInfoItem('应用密钥','appKey') }
          { baseInfoItem('详细信息','detailInfo') }  
          { baseInfoItem('授权内容','authorized') } 
          {
            improveInfoVisible ?
            <Modal
                title={"请完善用户信息"}
                visible={improveInfoVisible}
                wrapClassName="improve-wrapper"
                width={440}
                centered
                okText={'提交'}
                cancelText={'取消'}
                onOk={this.submitInfo}
                onCancel={() => {this.setState({improveInfoVisible: false})}}
            >
                <div className="improve-content">
                 {/* <div className="input-item flex-between">
                      <span><span className="red">*</span>  邮箱地址：</span>
                      <Input placeholder="请输入邮箱地址" onChange={(e)=>this.switchState('email',e.target.value)}/>
                  </div> */}
                  <div className="input-item flex-between">
                      <span><span className="red">*</span>  公司名称：</span>
                      <Input placeholder="请输入公司名称" onChange={(e)=>this.switchState('company',e.target.value)}/>
                  </div>
                  {/* <div className="input-item flex-between">
                      <span><span className="red">*</span>  公司地址：</span>
                      <Input placeholder="请输入公司地址" onChange={(e)=>this.switchState('address',e.target.value)}/>
                  </div> */}
                   <div className="input-item flex-between">
                      <span><span className="red">*</span>  公司地址：</span>
                      <Cascader placeholder="请选择所在省市" options={position} onChange={(value) =>this.switchState('address',value)}/>
                  </div>
                  <div className="input-item flex-between">
                      <span><span className="red">*</span>  个人职位：</span>
                      <Input placeholder="请输入个人职位" onChange={(e)=>this.switchState('position',e.target.value)}/>
                  </div>
                </div>
            </Modal>:null
          } 
          {
            editAppNameVisible ?
            <Modal
                title={"编辑应用名称"}
                visible={editAppNameVisible}
                wrapClassName="improve-wrapper"
                width={440}
                centered
                okText={'更新'}
                cancelText={'取消'}
                onOk={this.editInfo}
                onCancel={() => {this.setState({editAppNameVisible: false})}}
            >
                <div className="improve-content">
                  <div className="input-item flex-between">
                      <span>应用名称：</span>
                      <Input placeholder="请输入应用名称" onChange={(e)=>this.switchState('appName',e.target.value)}/>
                  </div>
                </div>
            </Modal>:null
          } 
      </div>
      </Spin>
    )
  }
}

function mapStateToProps(state) {
  return {
    userInfo: state.getIn(["user", "user_info"]) && state.getIn(["user", "user_info"]).toJS(),
    userAvatar: state.getIn(["user", "user_avatar"]),
  };
}

const mapActionCreators = {
  updateUserInfo
};

export default connect(mapStateToProps, mapActionCreators)(ApplicationInfo);
