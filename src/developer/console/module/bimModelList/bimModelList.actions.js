
import rest from '../../../../common/utils/rest';
import { ApiURL } from '../../../../common/config/websiteConfig';

//let  config = { headers: {'x-access-token': window.sessionStorage.getItem('token')}};

export function _concatUrl(url) {
    return `${ApiURL}/v1/account-service/${url}`;
}

function _hookError(error, message) {
    console.log(message + '出现错误 => ', error);
    throw error;
}

//获取模型列表
export function getAllModelList(page, pageSize, where) {
    let params = [];
    if(page){
        params.push(`page=${page}`);
    }
    if (pageSize) {
        params.push(`count=${pageSize}`);
    }
    if(where && where.selKey && where.selValue){
        params.push(`${where.selKey}=${encodeURI(where.selValue)}`);
    }
    if(where && where.dateRange && where.dateRange.length>1){
        params.push(`startTime=${where.dateRange[0]}`);
        params.push(`endTime=${where.dateRange[1]}`);
    }
    if(where && where.modelStatus){
        params.push(`preprocessed=${where.modelStatus}`);
    }
    let url = `${ApiURL}/v3/model/list?${params.join('&')}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "获取模型列表"))
}



//根据条件查找模型
export function queryModelList(data) {
    // v3/model/list/query
    let url = `${ApiURL}/v3/model/query`;
    return rest.post(url, data)
        .then(result => result)
        .catch(err => _hookError(err, "根据条件查找模型"))
}

//转换模型
export function transformModelFile(data){
    let url = `${ApiURL}/v3/model/file/analysis`;
    return rest.post(url, data)
        .then(result => result)
        .catch(err => _hookError(err, "转换模型"))
}

//删除模型
export function deleteModelFile(data, config) {
    // let url = `${ApiURL}/v3/model/deletes`;
    let url = `${ApiURL}/v3/model/files/delete`;
    return rest.remove(url, Object.assign(config, {data}))
        .then(result => result)
        .catch(err => _hookError(err, "获取模型列表"))
}

//下载模型
export function downloadModelFile(modelId, version){
    let url = `${ApiURL}/v3/model/file/download/${modelId}/${version}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "下载模型"))
}

//获取存储的数据(x-access-token)
export function saveUserData(saveDay,data) {
    let url = `${ApiURL}/v3/info/key/save/${saveDay}`
    return rest.post(url, data)
        .then(result => result)
        .catch(err => _hookError(err, "存储数据"))
}

//获取存储的数据(x-access-token)
export function getUserData(key) {
    let url = `${ApiURL}/v3/info/key/${key}`
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "获取存储的数据"))
}

//获取模型历史版本
export function getHistoryModels(modelId){
    let url = `${ApiURL}/v3/model/list/${modelId}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "获取模型历史版本")) 
}

//获取模型历史版本
export function getHistories(modelId,config){
    let url = `${ApiURL}/v3/model/list/${modelId}`;
    return rest.get(url,config)
        .then(result => result)
        .catch(err => _hookError(err, "获取模型历史版本")) 
}

//下载指定版本模型
export function downloadHistoryModel(modelId, version){
    let url = `${ApiURL}/v3/model/file/delete/${modelId}/${version}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "删除指定版本模型")) 
}

//删除指定版本模型
export function deleteHistoryModel(modelId, version){
    let url = `${ApiURL}/v3/model/file/delete/${modelId}/${version}`;
    return rest.remove(url)
        .then(result => result)
        .catch(err => _hookError(err, "删除指定版本模型")) 
}

//分享模型链接
export function shareBIMModel(day, optionType, data){
    let url = `${ApiURL}/v3/model/share/${day}/${optionType}`;
    return rest.post(url, data, null, true)
        .then(result => result)
        .catch(err => _hookError(err, "获取分享模型链接")) 
}

//获取分享模型数据
export function getShareModelData(key){
    let url = `${ApiURL}/v3/model/share/${key}`;
    return rest.get(url,{})
        .then(result => result)
        .catch(err => _hookError(err, "获取分享模型数据")) 
}

export function getComponments(modelId, version){
    let url  = `${ApiURL}/v3/model/component/${modelId}/${version}/2?property=true`;
    return rest.get(url)
       .then(result => result)
       .catch(err => _hookError(err, "获取模型属性"))
}

//构件搜索
export function searchComponents(data){
    let url  = `${ApiURL}/v3/model/component/query`;
    return rest.post(url, data)
        .then(result => result)
        .catch(err => _hookError(err, "构件搜索"))
}

//获取组合模型列表
export function getCombineModel(id, config){
    let url  = `${ApiURL}/v3/model/combine/info/${id}`;
    return rest.get(url, config)
        .then(result => result)
        .catch(err => _hookError(err, "获取组合模型列表"))
}











