import React, { Component } from 'react';
import { Table, Input, DatePicker, Select, message, Button, Modal, 
    Upload, Icon, Dropdown, Menu, } from 'antd';
import locale from 'antd/es/date-picker/locale/zh_CN';
import UploadTable from '../componment/UploadTable';
import HistoryTable from '../componment/HistoryTable';
import ShareModel from '../componment/ShareModel';
import IputModel from '../componment/InputModel';
import { ApiURL, baseURL } from '../../../../common/config/websiteConfig';
import { renderSize, switchTimeFormat, getTimePeriod, openWindow, getTextByJs, getFileName } from '../../../../common/utils/Utility';
import { getAllModelList, queryModelList, transformModelFile, deleteModelFile, downloadModelFile,
         deleteHistoryModel, getHistoryModels, saveUserData, shareBIMModel} from './bimModelList.actions';
import { getFileToken } from '../../actions';
import './bimModelList.less';

const InputGroup = Input.Group;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { confirm } = Modal;
//不支持下载的格式限制
const notSupports = ['yz','YZ','crvt','CRVT','cnw','CNW','ctekla','CTEKLA'];

export default class BimModelList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false, //加载状态
      tipVisible: true, //提示语框显隐
      showUploadVisible: false, //上传列表框显隐
      shareModelVisble: false, //分享链接框显隐
      showHistoryVisble: false, //历史版本
      modelStatus: null, //搜索 [模型状态]
      planInfo: {}, //套餐信息
      dateRange: [], //搜索 [上传时间] 
      where:{
        selKey: 'modelName'
      },
      modelList: [], //模型列表
      pagination: { //分页
        current: 1,
        pageSize: 10,
        total: null,
      },
      oauthToken: null,
      histories: [], //历史记录
      selectedRowKeys:[], //被选中数据[模型id]集
      totalSelected:[],
      selectedRows:[],//被选中数据集
      currentRecord: null, //当前被操作的数据
      fileListArray: [], //上传文件列表
      shareModelLink:'', //分享链接
      bimSize: '',
      bimCount: '',
      uploadMessage:'',
      uploadedCount: 0,
      transformRecord:[],
      modelVisible: false
    }

  //初始化 请求获取 token 模型列表
  this.request = () =>{
      this.setState({ 
        loading: true, 
        selectedRowKeys:[], 
        selectedRows:[],
        totalSelected:[],
        currentRecord: null,
      },()=>this.getModelList());
  };

  this.switchState = (key, value) => {
    this.setState({ [key]: value })
  };

  //获得模型列表
  this.getModelList = () =>{
    const { pagination, where } = this.state;
    getAllModelList(pagination.current, pagination.pageSize, where).then((res) => {
      if(res && res.code === 200){
        let totalElements = res.data && res.data.totalElements;
        pagination.total = totalElements;
        let result =  res.data && res.data.content;
        let modelData = this.transferModelData(result);
        this.setState({
          modelList: modelData,
          pagination,
          loading: false,
        })
      }else { 
        this.setState({
          loading: false
        },()=> res && message.error(res.message))
      }
    }).catch((error) => {
       this.setState({loading: false})
       error && message.error( error.message)
   });
  };

  //数据处理（去重、格式转换)
  this.transferModelData = (modelarray) =>{
    let modeldata = new Map();
    let modelLength = modelarray && modelarray.length;
    for (let i = 0; i < modelLength; i++) {
      let model = modelarray[i];
      let modelFile = model && model.modelFile;
      let temp = {};
      temp._id = model._id;
      temp._modelId = model._id; //模型id
      temp._version = model.version;//版本号
      temp._name = model.name; //名称
      temp._type = model.type; //格式
      temp._time = modelFile && modelFile.uploadAt ? switchTimeFormat(modelFile.uploadAt) : '-'; //上传时间
      temp._size = modelFile && modelFile.fileSize ? renderSize(modelFile.fileSize) : "-"; //文件大小
      temp._switchAt = modelFile && modelFile.decodeStart ? switchTimeFormat(modelFile.decodeStart) : '-'; //开始转换时间
      temp._timeConsum = modelFile && modelFile.decodeEnd ? getTimePeriod(modelFile.decodeStart, modelFile.decodeEnd) : '-'; //转换耗时
      temp._originSize = modelFile && modelFile.fileSize ? modelFile.fileSize : '-';
      temp._status = modelFile && modelFile.preprocessed;
      modeldata.set(model._id, temp);
    }
    //将map数据转化成数组 只取value
    let modelMapArray = [...modeldata.values()];
    return modelMapArray
  };
  
  //清除搜索
  this.handleClear = () =>{
    this.setState({
      modelStatus: null,
      dateShow: null,
      dateRange: [],
      where:{
        selKey:'modelName'
      },
      pagination: { //分页
        current: 1,
        pageSize: 10,
        total: null,
      },
    },()=> this.getModelList())
  };
  
  //选择模型名称和ID
  this.handleChangeKey = (value) =>{ 
    this.setState({
       where: Object.assign(this.state.where, {selKey: value}),
    })
  };

  //文件名查询
  this.searchByName = (value) => {
    if(value){
      this.setState({
        where: Object.assign(this.state.where, {selValue: value}),
     });
    }else{
      let {where} = this.state;
      delete where.selValue;
      this.setState({ where });
    } 
  };

  //选择时间范围
  this.searchByTime = (date, dateString) => {
    if(date && date.length){
      this.setState({
        dateShow: date,
        dateRange: dateString,
        where: Object.assign(this.state.where, {dateRange: dateString}),
      }, () => this.getModelList());
    }
  };

  //选择模型状态
  this.searchByStatus = (value) => {
    this.setState({
      modelStatus: value,
      where: Object.assign(this.state.where, {modelStatus: value}),
     }, () => this.getModelList());
  };

  //取消文件上传
  this.uploadAbort = (record) => {
    let self = this;
    if (record.percent && (record.percent === '100' || record.percent === 100) && (record.status === "done")) {
      self.reduceFile(record, self);
    } else {
        confirm({
            title: '确定取消该文件上传吗？',
            okText:'确认',
            cancelText:'取消',
            onOk() {
              self.reduceFile(record, self);
            }
        });
    }
  };

  this.reduceFile = (record, self) =>{
    let { fileListArray, uploadedCount } = this.state;
    if(fileListArray && fileListArray.length){
      let index = fileListArray.findIndex(file => file.uid === record.uid);
      fileListArray.splice(index, 1);
      self.setState({ 
        fileListArray, 
        uploadedCount: uploadedCount <= 0 ? 0 : uploadedCount-1 
      });
    }else{
      self.setState({ uploadMessage:'' });
    }
  }

  //关闭上传内容
  this.closeUploadCon = () => {
      this.setState({
          showUploadVisible: false,
          fileListArray: []
      })
  };

  //模型下载
  this.handleDownload = (record,e) =>{
     if(record && record._id){
       let fileType = record._type;
       //插件上传的格式不支持下载
       if( notSupports.indexOf(fileType)> -1){
           return message.warning('该文件格式暂不支持下载~');
       }else{
          downloadModelFile(record._id, record._version).then(res =>{
            if(res && res.code === 200){
               window.location.href = `${ApiURL}/v3/file/download/${res.data}`;
            }else{
              res && message.error(res.message);
            }
          }).catch(error => error && message.error(error.message))
      }
    }
  };

  //模型删除
  this.handleDelete = (record) =>{
    const {selectedRows} = this.state;
    let deleteData = [];
    if(record && record._id){
      deleteData.push({modelId: record._id, version: 0});
    }else{
      selectedRows.length && selectedRows.map(item => 
        deleteData.push({modelId: item._id, version: 0})
      );
    }

    if(deleteData.length){
      this.setState({ loading: true});
      let config = { headers: {'x-access-token': window.localStorage.getItem('token')} };
      deleteModelFile({ models: deleteData}, config).then(res =>{
        if(res){
          if(res.code === 200){
            message.success('删除成功')
          }else{
            message.error(res.message)
          }
        }
        this.setState({ loading: false },()=>this.request())
      }).catch(error => {
          message.error(error.message);
          this.setState({ loading: false },()=>this.request())
      })
    }else{
      message.warning('请选择要删除的模型~')
    }
  };

  //历史版本
  this.getHistory = (record) =>{
    getHistoryModels(record._id).then(res =>{
      if(res && res.code === 200){
        let records = (res.data.histories || []).map(i => Object.assign(i,{
          name: res.data.name,
          _id: res.data._id,
          _version: i.version,
          _status: i.preprocessed,
          _type: i.type,
        }))
        this.setState({
          showHistoryVisble: true,
          histories: records,
        })
      }
    })
  };

  //删除历史版本
  this.deleteHistory = (record, e) =>{
    if(record && record._id){
      this.setState({ loading: true});
      deleteHistoryModel(record._id, record.version).then(res =>{
        if(res && res.code === 200){
          message.success('删除成功~');
        }else{
          message.error('删除失败~');
        }
        this.setState({
          loading: false,
          showHistoryVisble: false,
          histories:[],
        },()=>this.request())
      }).catch(error =>  error && message.success(error.message))
    }
    
  };

  //模型转换
  this.handleTransform = (record, e) =>{
    const {totalSelected} = this.state;
    let transformData = [];
    if(record && record._id){
       transformData.push({modelId: record._id, version: record._version});
    }else{
      if(!totalSelected.length){
        message.destroy();
        return message.warning('请选择要转换模型~')
      }
      //过滤掉已转换成功/等待转换/转换中的模型
      let needTransforms = totalSelected.filter(i => (i._status === 4 || i._status === 2));
      (needTransforms||[]).map(item => 
         transformData.push({modelId: item._id, version: item._version})
      );

      if(transformData.length){
        // if(transformData.length === 1){
        //    this.setState({ modelVisible: true, transformRecord: transformData})
        // }else{
        //   this.goTransformModel(transformData)
        // }
        this.setState({ modelVisible: true, transformRecord: transformData})
      }else{
        this.request()
        return message.warning('已选中的模型已执行转换~')
      }
    }
      
  };

  this.goTransformModel = (transformData)=>{
      this.setState({ loading: true});
      transformModelFile({ models: transformData }).then(res =>{
        if(res){
           message.destroy();
          if(res.code === 200){
            message.success('正在执行转换...');
          }else{
            message.error(res.message)
          }
        }
        this.setState({ loading: false,modelVisible: false },()=>this.request())
      }).catch(error => error && message.error(error.message))
  }

  //模型分享
  this.shareModel = (record) =>{
    message.destroy();
    switch(record._status){
      case 0: //可使用
      this.setState({
        shareModelVisble: true,
        currentRecord: record
      })
        break;
      case 1: //解析中
        return message.warning(`模型正在解析中，请稍后再试~`);
      case 2: //解析失败
        return message.warning(`模型解析失败，可重新转换后再试~`);
      case 3: //等待解析
        return message.warning(`模型正在等待解析，请稍后再试~`);
      case 4: //未解析
        return message.warning(`模型未解析~`);
      default:
        break;
    }
    
  };

  //批量打开模型
  this.openModels = () =>{
    const { totalSelected} = this.state;
    let models = [];
    let saveDay = 0; //有效期 0: 一天 / 1: 一周 / 2: 一个月
    let canPreviews = totalSelected.filter(i => i._status === 0);
   
    if(!totalSelected.length){
      message.destroy();
      return message.warning('请选择要打开的模型~');
    }
    if(!canPreviews.length){
      message.destroy();
      return message.warning('请选择状态为【可使用】的模型~');
    }

    (canPreviews||[]).map(item =>{
      models.push({ modelId: item._id, version: item._version })
    })

    saveUserData(saveDay, models).then(res =>{
      if(res && res.code === 200){
        let pathname = `/model?key=${res.data}`;
        openWindow(pathname);
      }else{
        res && message.error(res.message);
      }
    })
    
  };

  this.getAllModels = ()=>{
    return Promise.all([getAllModelList(0, 100)]).then(res => res && res[0] && res[0].data);
 };

  //打开单个模型
  this.onRowClick = (record,e) =>{
    // e.stopPropagation();
    message.destroy();
    switch(record._status){
      case 0: //可使用
        let pathname = `/model?modelId=${record._id}&version=${record._version}`;
        openWindow(pathname);
        break;
      case 1: //解析中
        return message.warning(`模型正在解析中，请稍后再试~`);
      case 2: //解析失败
        return message.warning(`模型解析失败，可重新转换后再试~`);
      case 3: //等待解析
        return message.warning(`模型正在等待解析，请稍后再试~`);
      case 4: //未解析
        return message.warning(`模型未解析~`);
      default:
        return message.warning(`模型未解析~`);
    }
  };

  //获取模型分享链接
  this.onShareMode = (day, optionType)=>{
    const { currentRecord } = this.state;
    let record = [{modelId: currentRecord._id, version: currentRecord._version, modelName: currentRecord._name}];
    shareBIMModel(day, optionType, {data: record}).then(res =>{
      if(res && res.code === 200){
        this.setState({
          shareModelLink: `${baseURL}/model?shareCode=${res.data}`
        })
      }else{
        res && message.error(res.message);
      }
    }).catch(error => error && message.error(error.message))
  };

  //分页操作
  this.handleTableChange = (pagination) =>{
    this.setState({
      pagination
    }, () => this.getModelList())
  };

  let beforeUploadCounter = 0; // 上传的次数，可以统计到多少个文件
  let allFilesLength = 0; //上传文件个数+当前文件列表个数

  //上传文件前操作
  this.beforeUpload = (file) => { 
    if(file && file.size){
      const {fileListArray} = this.state;
      const { planInfo} = this.props;
      let bimSize = planInfo && planInfo.bimSize;
      let bimCount = (planInfo && planInfo.bimCount) || 5;

      beforeUploadCounter++;
      allFilesLength = beforeUploadCounter + fileListArray.length;

      return new Promise( async (resolve, reject) => {
        //单个文件上传大小限制
        let limitSize = file && file.size / 1024 > bimSize;
        if(limitSize){
          message.warning(`单个文件上传不得超过${renderSize(bimSize)},请重新上传~`);
          return reject(false);
        } 

        //单次上传个数限制
        if (allFilesLength > bimCount) {
          message.warning(`上传的文件不能超过${bimCount}个，请重新选择`);
          return reject(false);
        }

        //获取上传 token
        let fileInfo = {
          fileSize : file.size || 0,
          fileType : `.${getFileName(file.name)}` || '',
          operation : "upload"
        }
        let oauthData =  await this.getUploadOauth(fileInfo);
        if(oauthData){
          if(oauthData.code === 200){
            this.setState({ oauthToken: oauthData && oauthData.data })
            return resolve(true);
          }else{
            message.error(oauthData.message || '获取上传权限失败~');
            return reject(false);
          }
        }else{
          return reject(false);
        }
      });
    }
  };

  this.getUploadOauth = (fileInfo)=>{
     return Promise.all([getFileToken(fileInfo,'model')]).then(res => res && res[0]);
  };
  
  //选择文件时操作
  this.onChangeUpload = ({file, fileList, event}) => { 
    const { uploadedCount } = this.state;
    const {planInfo} = this.props;
    let bimSize = planInfo && planInfo.bimSize;
    let bimCount = planInfo && planInfo.bimCount;
    let bimSupport = planInfo && planInfo.bimSupport;
    let fileLists =[...fileList];
    fileLists = fileLists.slice(-`${parseInt(bimCount)}`);

    if (file.percent && (file.percent === '100' || file.percent === 100) && (file.status === "done")) {
        if(file.response && file.response.code === 200){
            if(uploadedCount === fileLists.length){
              allFilesLength = 0;
            }
            let count = uploadedCount === fileLists.length ? 0 : uploadedCount + 1;
            this.setState({uploadMessage:`有${count}个文件上传成功`,uploadedCount: count },()=> this.request())
        }else{
            message.error(file.response && file.response.message)
        }
        this.request();
    }else{
      if(file.response && file.response.message){
        message.error(file.response.message)
      }
      this.setState({ 
        uploadMessage: `模型格式限制：${getTextByJs(bimSupport)}；单个文件≤${renderSize(bimSize)}`
      });
    }
    this.setState({ 
      showUploadVisible: true,
      fileListArray: fileLists.filter(file => !!file.status) //限制上传列表的个数
    });

  };

  this.onChangeUpdate=({file, fileList}) => {
    if (file.percent && (file.percent === '100' || file.percent === 100) && (file.status === "done")) {
      if(file.response && file.response.code === 200){
        this.setState({
          uploadMessage:`文件更新成功`, 
          uploadedCount: 1
        },()=> this.request())
      }else{
        message.error(file.response && file.response.message)
      }
      this.request();
    }else{
      if(file.response && file.response.message){
        message.error(file.response.message)
      }
      this.setState({
        uploadMessage:`有1个文件正在更新...`,
        uploadedCount: 1
      })
    }
   
    this.setState({ 
      showUploadVisible: true,
      fileListArray: fileList //限制上传列表的个数
    });
  };

  this.updateAction = (file,record)=>{
    const { oauthToken } = this.state;
    if(oauthToken){
      return new Promise((resolve, reject) => {
        let url = `${ApiURL}/v3/file/model/update/${record._id}?token=${oauthToken}`
        return resolve(url);
      })
    }
  };

  //数组去重操作
  this.uniqueArray = (rawArr) =>{
    let uniqueArray = [];
    let ids = new Set();
    rawArr.forEach(value => {
        if (!ids.has(value._id)) {
            uniqueArray.push(value)
        }
        ids.add(value._id);
    });
    return uniqueArray;
  };
}

  componentDidMount(){
    let pageHeight = document.querySelector('.bimModelList').offsetHeight;
    let tableBody = pageHeight - (136 + 75 + 62);
    this.setState({
      tableScroll: tableBody,
    },()=>this.request())

  } 
  

  render() {
    const {  modelList, pagination, loading, progerss, fileListArray, selectedRowKeys,
       showUploadVisible, shareModelVisble, modelStatus, dateShow, where,totalSelected,
       showHistoryVisble, shareModelLink, histories,uploadMessage,uploadedCount,oauthToken,
       tableScroll, modelVisible
    } = this.state;

    const {planInfo} =  this.props;
    let bimSupport = getTextByJs(planInfo && planInfo.bimSupport , true);

    const uploadHeaders = {'x-access-token': window.localStorage.getItem('token')};
    //模型状态布局 
    const renderStatus =(status,record)=>{
      switch(status){
        case 0:
          return (<span className="status-canuse">可使用</span>)
        case 1:
          return (
            <div className="status-transfrom">
               <span>转换中</span>
               <Icon className="loading" type="loading"/>
          </div>)
        case 2:
          return (
            <div className="status-error">
               <span>转化失败</span>
                <Icon className="reflash" type="redo" onClick={e => this.handleTransform(record,e)}/>
            </div>)
        case 3:
          return (<span className="status-waiting">等待转换</span>)
        case 4:
          return (<span className="status-waiting">未解析</span>)
         default:
           break;
      }
    };
    //单条数据下拉菜单
    const opreateMenu = (record, oauthToken)=>{
      return(
        <Menu>
          {
          (DropdownConfig||[]).map(i => 
            <Menu.Item key={i.key} onClick={e => { e.domEvent.stopPropagation(); i.action && i.action(record)}}>
              <div>
                {
                  i.key === 'update' ?
                  <Upload accept={record._type}
                          headers ={ uploadHeaders }
                          showUploadList= {false}
                          method={'put'}
                          action={(file)=>this.updateAction(file, record)}
                          onChange={this.onChangeUpdate}
                          beforeUpload={this.beforeUpload}
                  >
                  <span>{i.title}</span>
                  </Upload>
                  : 
                  <span>{i.title}</span>
                }
              </div>
            </Menu.Item>)
          }
        </Menu>
      )
    };
    const DropdownConfig =[
      {
        title: '分享',
        key: 'share',
        action: (record,e) => this.shareModel(record,e)
      },
      {
        title: '更新',
        key: 'update',
      },
      {
        title: '历史版本',
        key: 'history',
        action:  (record,e) => this.getHistory(record,e)
      },
      {
        title: '下载',
        key: 'download',
        action: (record,e)=> this.handleDownload(record,e)
      },
      {
        title: '删除',
        key: 'delete',
        action: (record,e) => this.handleDelete(record,e)
      },
     
    ];
    const columns = [
      {
        title: '模型ID',
        dataIndex: '_modelId',
        width: 140
      },
      {
        title: '模型名称',
        dataIndex: '_name',
        width: 185
      },
      {
        title: '格式',
        dataIndex: '_type',
        width: 70,
      },
      {
        title: '上传时间 / 大小',
        dataIndex: '_size',
        width: 190,
        render: (text, record) =>{
          if(text){
            return(
              <div className='record-item'>
                  <div className='upload-time'>{record._time}</div>
                  <div className='upload-size'>{record._size}</div>
              </div>
            )
          }
        }
      },
      {
        title: '开始转换时间 / 耗时',
        dataIndex: '_time',
        width: 190,
        render: (text, record) =>{
          if(text){
            return(
              <div className='record-item'>
                  <div className='upload-time'>{record._switchAt}</div>
                  <div className='upload-size'>{record._timeConsum}</div>
              </div>
            )
          }
        }
      },
      {
        title: '模型状态',
        dataIndex: '_status',
        width: 110,
        render: (text, record) => renderStatus(text,record)
      },
      
      {
        title: '版本号',
        width: 85,
        dataIndex: '_version',
        render: (text, record) =>{
          if(text){
            return(
              <div className='record-version'>
                  <div className='version'>{`V${text}.0`}</div>
              </div>
            )
          }
        }
      },
      {
        title: '操作',
        width: 100,
        dataIndex: '_id',
        align: 'center',
        render: (text, record) =>{
          if(text){
            return(
              <div className='opreate flex-center' id="opreateCell">
                  {/* <span className='view' onClick={e => {e.stopPropagation(); this.onRowClick(record, e)}}>预览</span> */}
                  <Dropdown overlay={() => opreateMenu(record, oauthToken)} 
                            trigger={['hover']} 
                            placement="bottomRight"
                            onClick={e => e.stopPropagation()}
                  >
                           
                      <span className='more'>更多 <Icon type="down" style={{color:'#3D81FA'}}/> </span>
                  </Dropdown>
              </div>
            )
          }
        }
      },
    ];

    const rowSelection = {
      selectedRowKeys,
      onChange: (selectedRowKeys, selectedRows) => {
          //跨页选中（去重）
          let allSelected = this.uniqueArray(totalSelected.concat(selectedRows));
          //过滤selectedRowKeys中选中
          let curSelected = allSelected.filter((item)=> {
            return selectedRowKeys.includes(item._id)
          })
          this.setState({
              selectedRowKeys: selectedRowKeys,
              selectedRows: selectedRows,
              totalSelected: curSelected,
          })
      }
    };

    return (
        <div className='bimModelList'>
          <div className="bimModelList-header flex-between">
              <div className="operate-botton flex-center">
                  <Upload accept={bimSupport}
                          headers ={ uploadHeaders }
                          multiple= {true}
                          showUploadList= {false}
                          action={ oauthToken && `${ApiURL}/v3/file/model/upload?token=${oauthToken}`} 
                          fileList={fileListArray}
                          onChange={oauthToken && this.onChangeUpload}
                          beforeUpload={this.beforeUpload}
                  >
                    {  <Button className="option-btn" type={'primary'}>上传文件</Button>} 
                  </Upload>
                  <Button className="option-btn" onClick={this.openModels}>预览</Button>
                  <Button className="option-btn" onClick={this.handleTransform}>转换</Button>
                  <Button className="option-btn" onClick={this.handleDelete}>删除</Button>
            </div>
          </div>
          <div className='bimModelList-content'>
              <div className="operate-wrapper flex-between">
                  <div className='operate-search flex-center'>
                      <div className="option-item">
                          <InputGroup compact>
                              <Select defaultValue="modelName" 
                                      style={{width: 100}}
                                      value={where.selKey} 
                                      onChange={value => this.handleChangeKey(value)}
                              >
                                <Option value="modelName">模型名称</Option>
                                <Option value="modelId">模型ID</Option>
                              </Select>
                              <Input style={{ width: '220px' }} value={where.selValue} onChange={e => this.searchByName(e.target.value)}/>
                          </InputGroup>
                      </div>
                      <div className="option-item">
                          <span>上传时间：</span>
                          <RangePicker locale={locale} 
                                       allowClear={false}
                                       style={{ width: '220px' }}
                                       onChange={this.searchByTime} 
                                       value={dateShow}
                                       getContainer= {()=>document.getElementById('inputWrapper') } 
                          />
                      </div>
                      <div className="option-item">
                          <span>模型状态：</span>
                          <Select  style={{width: 120}} 
                                   placeholder="请选择状态" 
                                   value={modelStatus}
                                   onChange={value => this.searchByStatus(value)}>
                               <Option value="0">可使用</Option>
                               <Option value="1">转换中</Option>
                               <Option value="2">转换失败</Option>
                               <Option value="3">等待转换</Option>
                               <Option value="4">未解析</Option>
                          </Select>
                      </div>
                      <div className="option-item">
                         <Button type={'primary'} className="option-btn" onClick={this.getModelList}>查询</Button>
                      </div> 
                      <div className="option-item">
                         <Button className="option-btn" onClick={this.handleClear}>清除</Button>
                      </div> 
                  </div>
                  
                 
              </div>
              {/* { 
                tipVisible && modelList.length? 
                <TipShow onClose={this.switchState} toolTip={toolTip}/>: null 
              } */}
              <div className='content-table'>
                <Table rowKey={record => record._id} 
                       className="modelTable"
                      selectedRowKeys = {selectedRowKeys}
                      rowSelection={rowSelection} 
                      columns={columns} 
                      dataSource={modelList} 
                      pagination={pagination}
                      scroll={{y: tableScroll}}
                      onChange={this.handleTableChange}
                      loading={loading}
                      onRow={(record) => {
                        return {
                            onClick: (e) => this.onRowClick(record,e)
                        }
                      }}
                />
              </div>
            </div> 
            {
                showUploadVisible &&
                <UploadTable 
                    fileData={ fileListArray }
                    closeUploadCon={ this.closeUploadCon }
                    uploadAbort={ this.uploadAbort } 
                    uploadedCount={uploadedCount}
                    uploadMessage={uploadMessage}
                    onClose={this.closeUploadCon}
                />
            }
            {
               shareModelVisble && 
                <ShareModel 
                   visible={ shareModelVisble }
                   link={ shareModelLink }
                   onClose={ this.switchState }
                   onShare={ this.onShareMode }
                /> 
            }
            {
              showHistoryVisble &&
                <HistoryTable 
                  visible={ showHistoryVisble }
                  histories={ histories }
                  onClose={this.switchState}
                  onDelete={(record,e)=> this.deleteHistory(record,e)}
                  onDownload={(record,e)=>this.handleDownload(record,e)}
                  onOpen={(record,e)=>this.onRowClick(record,e)}
                  onTransform={(record,e)=>this.handleTransform(record,e)}
                 /> 
            }
            {
              modelVisible && 
              <IputModel 
                  visible={ modelVisible }
                  transformRecord={this.state.transformRecord}
                  onConfirm = {this.goTransformModel}
                  onClose = {()=>this.switchState('modelVisible',false)}
              />
            }
        </div>
    )
  }
}
