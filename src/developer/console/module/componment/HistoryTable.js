import React from 'react';
import { renderSize, switchTimeFormat } from '../../../../common/utils/Utility';
import { Modal, Icon, Table  } from "antd";
import './componment.less';

class HistoryTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            histories: [], // 文件列表
        };

        //预览文件
        this.onPreview = (record,e) =>{
            e.stopPropagation();
            this.props.onOpen(record, e);
        };
        
        //下载文件
        this.onDownload = (record,e) => {
            e.stopPropagation();
            this.props.onDownload(record,e);
        };

        // 删除文件
        this.onDelete = (record,e) => {
            e.stopPropagation();
            this.props.onDelete(record,e);
        };

        //转换文件
        this.onTransform = (record,e) => {
            this.props.onTransform(record, e);
        };

        // 初始化
        this.init = () => {
            this.setState({
                histories: this.props.histories
            })
        };

    }

    componentDidMount() {
        this.init();
    }

    render() {
        let { histories} = this.state;
        let { visible, onClose } = this.props;

        const renderStatus =(status,record)=>{
            switch(status){
              case 0:
                return (<span className="status-canuse">可使用</span>)
              case 1:
                return (
                  <div className="status-transfrom">
                     <span>转换中</span>
                     <Icon className="loading" type="loading"/>
                </div>)
              case 2:
                return (
                  <div className="status-error">
                     <span>转化失败</span>
                      <Icon className="reflash" type="redo" onClick={e => this.onTransform(record,e)}/>
                  </div>)
              case 3:
                return (<span className="status-waiting">等待转换</span>)
              case 4:
                return (<span className="status-waiting">未解析</span>)
              default:
                return ''

            }
          };

        let historyColumns = [
            // {
            //     title: '名称',
            //     dataIndex: 'name',
            //     width: 200,
            //     render: (text, record) => text
            // },
            {
                title: '版本号',
                dataIndex: 'version',
                align: 'center',
                width: 80,
                render: (text,record) => {
                    return(
                        <div className='record-version'>
                            <div className='version'>{`V${text}.0`}</div>
                        </div>
                    )
                }
            },
            {
                title: '大小',
                dataIndex: 'fileSize',
                width: 120,
                align: 'center',
                render: (text) => {
                    return <span>{renderSize(text)}</span>
                }
            },
            {
                title: '上传时间',
                dataIndex: 'uploadAt',
                width: 180,
                align: 'center',
                render: (text) => {
                    return <span>{switchTimeFormat(text)}</span>
                }
            },
            {
                title: '模型状态',
                dataIndex: 'preprocessed',
                width: 120,
                render: (text, record) => renderStatus(text,record)
            },
            {
                title: '操作',
                dataIndex: 'actions',
                width: 160,
                align: 'center',
                render: (text, record) => {
                    return (
                        <div className='opreate flex-center'>
                            <span className='preview' onClick={ e => this.onPreview(record,e)}>预览</span>
                            <span className='download' onClick={ e => this.onDownload(record,e)}>下载</span>
                            <span className='delete' onClick={ e => this.onDelete(record,e)}>删除</span>
                        </div>  
                    )
                }
            }
        ];

        return (
            <Modal
                visible={visible}
                wrapClassName="history-table"
                width={900}
                title='历史版本列表'
                footer={null}
                zIndex={99}
                centered
                onCancel={() => onClose("showHistoryVisble", false)}>
                <div className="history-main scl">
                    <Table columns={historyColumns}
                           rowKey={record => record.version}
                           rowClassName={(record, index) => 'history-table-row'}
                           dataSource={histories}
                           pagination={{ pageSize: 6}}
                           bordered={false}
                           onRow={(record) => {
                                return {
                                    onClick: e => this.onPreview(record,e)
                                }
                           }}
                   />
                </div>
            </Modal>
        )
    }
}

export default HistoryTable;