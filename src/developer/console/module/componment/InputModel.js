/* 构件属性展示 */

import React from 'react';
import { Modal, Input, Row, Col, Radio } from 'antd';

const DEFAULT = 'model_default'; //默认
const CUSTOMIZE = 'model_customize'; //自定义

class InputModel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            modelSetting: DEFAULT,
            enableSection: false,
            modelName: '',
            isPartition: false, 
        }

        this.handleChangeValue = (value, type) =>{
            this.setState({ [type]: value })
        }

        this.handleName = (value)=>{
            this.setState({
                inputName: value
            })
        }
       
        this.handleConfirm = ()=>{
            const { modelName, enableSection, isPartition, } = this.state;
            const{ transformRecord, isDrawing } = this.props;
            if(transformRecord){
                let subData = isDrawing ?
                 {
                    isPartition
                 } : { 
                    viewName: modelName, 
                    enableSection,
                    isPartition
                 }
                transformRecord.map(i=> Object.assign(i, subData));
                this.props.onConfirm(transformRecord);
            }
        };
    }
   
  
    render() {
        const { visible, onClose, isDrawing } = this.props;
        const { modelSetting, modelName, enableSection, isPartition } = this.state;

        const modelItem = ()=>{
            return(
                <Row style={{marginTop:'15px'}}>
                    <Col span={10}>三维视图名称：</Col>
                    <Col span={14}>
                        <Input style={{width: 160}}  type="text" value={modelName} onChange={e => this.handleChangeValue(e.target.value,'modelName')}/>
                    </Col>
                </Row>
            )                  
        };
        
        return( 
            <Modal title="转换设置"
                visible={visible} 
                width={350}
                centered
                okText={'确定'}
                cancelText={'取消'}
                onOk={this.handleConfirm} 
                onCancel= {onClose}
              >
                <div className="modelContent">
                    {
                        !isDrawing ?
                        <div style={{marginBottom:'15px'}}>
                            <Row>
                                <Col span={10}>三维视图设置：</Col>
                                <Col span={14}>
                                    <Radio.Group onChange={(e)=> this.handleChangeValue(e.target.value,'modelSetting')} value={modelSetting}>
                                        <Radio value={DEFAULT}>默认</Radio>
                                        <Radio value={CUSTOMIZE}>自定义</Radio>
                                    </Radio.Group>
                                </Col>
                            </Row>
                            <Row style={{marginTop:'15px'}}>
                                <Col span={10}>是否开启剖切：</Col>
                                <Col span={14}>
                                    <Radio.Group onChange={(e)=> this.handleChangeValue(e.target.value,'enableSection')} value={enableSection}>
                                        <Radio value={false}>关闭</Radio>
                                        <Radio value={true}>开启</Radio>
                                    </Radio.Group>
                                </Col>
                            </Row>
                        </div>
                        : null
                    }
                    <Row>
                        <Col span={10}>是否分块：</Col>
                        <Col span={14}>
                            <Radio.Group onChange={(e)=> this.handleChangeValue(e.target.value,'isPartition')} value={isPartition}>
                                <Radio value={false}>关闭</Radio>
                                <Radio value={true}>开启</Radio>
                            </Radio.Group>
                        </Col>
                    </Row>
                    {
                        modelSetting === CUSTOMIZE && modelItem()
                    }
                </div>
              </Modal> 
        )
    }

}
export default InputModel;