import React from "react";
import copy from "copy-to-clipboard";
import { Input, Modal, message, Radio} from "antd";
import './componment.less';

const {TextArea} = Input;
const RadioGroup = Radio.Group;
const QRCode = require('qrcode.react');

class ShareModel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            saveDay: 1,  //  分享期限 0 ：1天 / 1 ：7天 / 2 ： 30天 / 3 ：永久
            optionType: 0 // 模型权限 0 ：查看 / 1 ：编辑 
        };

        this.copy = () => {
            if (copy(this.props.link)) {
                message.success('链接复制成功');
            } else {
                message.error('链接复制失败');
            }
        };

        this.onDateChange = (e) => {
            this.setState({
                saveDay: e.target.value,
            }, () => {
                this.init();
            });
        };

        this.init = () => {
            let {saveDay,optionType} = this.state;
            this.props.onShare(saveDay,optionType);
        };
    }

    componentDidMount() {
        this.init();
    }
    componentWillReceiveProps(nextProps){
        // this.init();
    } 

    

    render() {
        let { saveDay } = this.state;
        let {visible,link, onClose} = this.props;

        return (
            <Modal
                   visible={visible}
                   wrapClassName="share-wrapper"
                   width={450}
                   title= " "
                   okText={'复制链接'}
                   cancelText={'取消'}
                   onOk={this.copy}
                   onCancel={() => onClose("shareModelVisble", false)}>
                <div className="share-content">
                    <div className="share-header"></div>
                    <div className="share-center">
                        <div className="QR-code">
                            <QRCode value={link} size={120}/>
                        </div>
                        <div className="share-expain">将二维码分享给好友 对方微信扫一扫即可预览文件</div>
                    </div>
                    <div className="share-bottom">
                        <div className="share-con-label flex-between">
                            <span className="share-con-text">有效期：</span>
                            <span>
                                <RadioGroup value={saveDay} onChange={this.onDateChange}>
                                    <Radio value={0}>1天</Radio>
                                    <Radio value={1}>7天</Radio>
                                    <Radio value={2}>30天</Radio>
                                    <Radio value={3}>永久</Radio>
                                </RadioGroup>
                            </span>
                        </div>
                        <div className="share-con-label flex-between">
                            <span className="share-con-text">分享链接：</span>
                            <span className="input-span">
                                <TextArea rows={2} value={ link || ''} style={{width:'300px'}}/>
                            </span>
                        </div>
                    </div>
                </div>
              
            </Modal>
        )
    }
}

export default ShareModel;

