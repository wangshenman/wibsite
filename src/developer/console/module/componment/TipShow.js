
import React from 'react';
import './componment.less';
import tipIcon from '../../../../common/img/developer/tipIcon.png';

class TipShow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}; 
    }

    render() {
        const { toolTip, onClose} = this.props;
        return (
          <div className="tipShow-wrapper flex-between">
            <div>
                {toolTip.isSelect ? <img className="tip-icon" src={tipIcon} alt=""/> :''}
                <span className="main-word">{ toolTip.totalNumInfo }</span><span>{ toolTip.totalSizeInfo }</span>
            </div>
            <span className="close-btn" onClick={()=>onClose("tipVisible", false)}>x</span>
          </div>
        )
    }
}

export default TipShow;