import React from 'react';
import classNames from 'classnames';
import {renderSize} from '../../../../common/utils/Utility';
import {Icon, Table ,Progress} from "antd";
import LoadIcon from './preview/LoadIcon';
import './componment.less';

class UploadTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            timer: null, // 刷新进度计时器
            fileData: [], // 文件列表
            uploadedCount: 0,
            uploadState: true, // 上传控件 -> 最大显示|最小显示
            headerTitle:'上传文件',
            tipVisible: true
        };

        // 放大|缩小 上传控件
        this.shrinkUploadCon = () => {
            this.setState({
                uploadState: !this.state.uploadState
            })
        };

        // 删除文件
        this.onDelete = (record) => {
            this.props.uploadAbort(record);
        };

        // 关闭页面
        this.close = () => {
            this.clearSetInterval();
            this.props.closeUploadCon();
        };

        this.handleCloseTip = ()=>{
            this.setState({
                tipVisible: false
            })
        }

        // 开启定时器
        this.initSetInterval = () => {
            let self = this;
            this.state.timer = setInterval(() => {
                self.setState({
                    uploadedCount: self.props.uploadedCount,
                    fileData: self.props.fileData,
                })
            }, 1000);
        };

        // 清除定时器
        this.clearSetInterval = () => {
            clearInterval(this.state.timer)
        };
    }

    componentDidMount() {
        this.initSetInterval();
    }

    componentWillUnmount() {
        this.clearSetInterval();
    }

    render() {
        let {uploadState, fileData, tipVisible, uploadedCount} = this.state;
        let {uploadMessage} = this.props;
        let uploadColumns = [
            {
                title: '文件名',
                dataIndex: 'name',
                width: 280,
                render: (text, record) => {
                    return (
                        <span className="upload-name">
                            <span>{LoadIcon(record)}</span>
                            <span style={{marginLeft: '5px'}}>{text}</span>
                        </span>
                    )
                }
            },
            {
                title: '大小',
                dataIndex: 'size',
                width: 120,
                render: (text) => {
                    return <span>{renderSize(text)}</span>
                }
            },
            {
                title: '进度',
                dataIndex: 'percent',
                align: 'center',
                width: 160,
                render: (text,record) => {
                    let num = null;
                    if(text === 100){
                        //获取真实的进度条(上传到服务器返回)；
                        num = record && record.status === "done" ? 100 : 98;
                    }else{
                        num = text;
                    }
                    return <Progress percent={parseInt(num)}/>
                }
            },
            {
                title: '操作',
                dataIndex: 'actions',
                width: 60,
                render: (text, record) => {
                    return (
                        <span><Icon type="close" onClick={() => this.onDelete(record)}/></span>
                    )
                }
            }
        ];
        let headerTitle = parseInt(fileData && fileData.length) === 0 ? 
        '上传文件' : `正在上传（${uploadedCount}/${fileData.length}）`;
        
        return (
            <div className={classNames('upload-table', {enter: uploadState}, {leave: !uploadState})}
                 key="upload">
                <div className="upload-table-header">
                    <span className="upload-table-title">{headerTitle}</span>
                    <span className="upload-table-close">
                        {
                            uploadState ?
                                <Icon type="minus"
                                      className="upload-table-close-icon"
                                      onClick={this.shrinkUploadCon}/> :
                                <Icon type="laptop"
                                      className="upload-table-close-icon"
                                      onClick={this.shrinkUploadCon}/>
                        }
                        <Icon type="close"
                              className="upload-table-close-x"
                              onClick={() => this.close()}/>
                    </span>
                </div>
                {
                    tipVisible ?
                    <div className="upload-tip">
                        <span>{uploadMessage}</span><span onClick={this.handleCloseTip}>x</span>
                    </div> : null

                }
               
                <div className="upload-main scl">
                    <Table columns={uploadColumns}
                           rowKey={record => record.uid}
                           rowClassName={(record, index) => 'upload-table-row'}
                           dataSource={fileData}
                           pagination={{pageSize: 5}}
                           bordered={false}/>
                </div>
            </div>
        )
    }
}

export default UploadTable;