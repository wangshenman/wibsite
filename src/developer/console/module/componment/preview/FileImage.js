import React from 'react';
import {
    images_suffix,
    pdf_suffix,
    office_word_suffix,
    office_excel_suffix,
    office_ppt_suffix,
    txt_suffix,
    video_suffix,
    audio_suffix,
    zip_suffix,
    dwg_suffix,
    rvt_suffix,
    judgeFileType
} from './FileType';

import unknown from './images/icon_unknown.png';
import pic from './images/icon_pic.png';
import pdf from './images/icon_pdf.png';
import word from './images/icon_word.png';
import excel from './images/icon_excel.png';
import ppt from './images/icon_ppt.png';
import txt from './images/icon_txt.png';
import audio from './images/icon_music.png';
import video from './images/icon_movie.png';
import zip from './images/icon_zip.png';
import dwg from './images/icon_dwg.png';
import rvt from './images/icon_rvt.png';
import dir from './images/icon_folder.png';
import model from './images/icon_model.png';
import viewport from './images/icon_viewport.png';
import selection from './images/icon_selection.png';
import form from './images/icon_form.png';
// 未识别
export const FI_UNKNOWN = 'FI_UNKNOWN';
// 文件夹
export const FI_FOLDER = 'FI_FOLDER';
// 模型
export const FI_MODEL = 'FI_MODEL';
// 视口
export const FI_VIEWPORT = 'FI_VIEWPORT';
// 选择集
export const FI_SELECTION = 'FI_SELECTION';
// 表单
export const FI_FORM = 'FI_FORM';

export function FileImage(type) {
    return <img src={FilePicture(type)} style={{width: '16px'}} alt=""/>;
}

export function FilePicture(type) {
    switch (type) {
        case FI_FOLDER:
            return dir;
        case FI_MODEL:
            return model;
        case FI_VIEWPORT:
            return viewport;
        case FI_SELECTION:
            return selection;
        case FI_FORM:
            return form;
        case FI_UNKNOWN:
            return unknown;
    }

    if(judgeFileType(type, images_suffix)) {
        return pic;
    } else if (judgeFileType(type, pdf_suffix)) {
        return pdf;
    } else if (judgeFileType(type, office_word_suffix)) {
        return word;
    } else if (judgeFileType(type, office_excel_suffix)) {
        return excel;
    } else if (judgeFileType(type, office_ppt_suffix)) {
        return ppt;
    } else if (judgeFileType(type, txt_suffix)) {
        return txt;
    } else if (judgeFileType(type, video_suffix)) {
        return video;
    } else if (judgeFileType(type, audio_suffix)) {
        return audio;
    } else if (judgeFileType(type, zip_suffix)) {
        return zip;
    } else if (judgeFileType(type, dwg_suffix)) {
        return dwg;
    }else if (judgeFileType(type, rvt_suffix)) {
        return rvt;
    }
}
