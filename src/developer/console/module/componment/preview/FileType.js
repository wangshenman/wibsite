const images_suffix = ['image', 'picture', 'png', 'jpg', 'jpeg', 'bmp', 'gif', 'tiff'];

const pdf_suffix = ['pdf'];

const office_word_suffix = ['doc', 'docx'];
const office_excel_suffix = ['xls', 'xlsx'];
const office_ppt_suffix = ['ppt', 'pptx'];
const office_suffix = [].concat(office_word_suffix).concat(office_excel_suffix).concat(office_ppt_suffix);

const txt_suffix = ['txt'];

const audio_suffix = ['mp3'];

const video_suffix = ['mp4'];

const zip_suffix = ['zip', 'rar', '7z'];

// 图纸
const dwg_suffix = ['dwg'];
// 模型
const rvt_suffix = ['rvt'];

// 支持预览的文件后缀
const preview_suffix = [].concat(images_suffix).concat(pdf_suffix).concat(txt_suffix).concat(video_suffix).concat(audio_suffix).concat(office_suffix).concat(dwg_suffix).concat(rvt_suffix);

// 是否支持预览
function supportPreview(suffix) {
    return judgeFileType(suffix, preview_suffix);
}

// 判断文件类型 suffix in suffixes
function judgeFileType(suffix, suffixes) {
    if (!suffix) {
        return false;
    }

    let index = suffixes.findIndex(item => {
        return new RegExp(`${item}`).test(suffix.toLowerCase());
    });
    return index !== -1;
}

export {
    images_suffix,
    pdf_suffix,
    office_word_suffix,
    office_excel_suffix,
    office_ppt_suffix,
    office_suffix,
    txt_suffix,
    audio_suffix,
    video_suffix,
    zip_suffix,
    dwg_suffix,
    rvt_suffix,
    preview_suffix,
    supportPreview,
    judgeFileType
}
