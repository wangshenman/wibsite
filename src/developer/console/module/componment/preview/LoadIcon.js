
import {FileImage, FI_UNKNOWN, FI_FOLDER, FI_MODEL, FI_VIEWPORT, FI_SELECTION, FI_FORM} from './FileImage';

export default function LoadIcon(realFile) {
    if (realFile) {
        if (realFile.type === "dir") {
            return FileImage(FI_FOLDER);
        }
        if(realFile.type === "viewport") {
            return FileImage(FI_VIEWPORT);
        }
        if(realFile.type === "selection_set") {
            return FileImage(FI_SELECTION);
        }
        if(realFile.type === "form") {
            return FileImage(FI_FORM);
        }
        if (realFile.hasOwnProperty('linkId') || realFile.hasOwnProperty('modelFile')) {
            return FileImage(FI_MODEL);
        }

        let type;
        if (realFile.suffix) {
            type = realFile.suffix;
        } else if (realFile.file && realFile.file.suffix) {
            type = realFile.file.suffix;
        } else if (realFile.filename) {
            let arr = realFile.filename.split('.');
            type = arr[arr.length - 1];
        } else if (realFile.name) {
            let arr = realFile.name.split('.');
            type = arr[arr.length - 1];
        }
        return FileImage(type);
    }
    return FileImage(FI_UNKNOWN);
};
