
import rest from '../../../../common/utils/rest';
import { ApiURL } from '../../../../common/config/websiteConfig';

export function _concatUrl(url) {
    return `${ApiURL}/v1/account-service/${url}`;
}


function _hookError(error, message) {
    console.log(message + '出现错误 => ', error);
    throw error;
}

//获取模型列表
export function getAllModelList(page, pageSize, where) {
    let params = [];
    if(page){
        params.push(`page=${page}`);
    }
    if (pageSize) {
        params.push(`count=${pageSize}`);
    }
    if(where && where.selKey && where.selValue){
        params.push(`${where.selKey}=${encodeURI(where.selValue)}`);
    }
    if(where && where.dateRange && where.dateRange.length>1){
        params.push(`startTime=${where.dateRange[0]}`);
        params.push(`endTime=${where.dateRange[1]}`);
    }
    if(where && where.modelStatus){
        params.push(`preprocessed=${where.modelStatus}`);
    }
    let url = `${ApiURL}/v3/drawing/list?${params.join('&')}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "获取模型列表"))
}

//根据条件查找模型
export function queryModelList(data) {
    let url = `${ApiURL}/v3/drawing/query`;
    return rest.post(url, data)
        .then(result => result)
        .catch(err => _hookError(err, "根据条件查找模型"))
}

//转换模型
export function transformModelFile(data){
    let url = `${ApiURL}/v3/drawing/reanalysis`;
    return rest.post(url, data)
        .then(result => result)
        .catch(err => _hookError(err, "转换模型"))
}
//删除模型
export function deleteModelFile(data,config) {
    // let url = `${ApiURL}/v3/drawing/deletes`;
    let url = `${ApiURL}/v3/drawing/files/delete`;
    return rest.remove(url, Object.assign(config, {data}))
        .then(result => result)
        .catch(err => _hookError(err, "获取模型列表"))
}

//下载模型
export function downloadModelFile(modelId, version){
    let url = `${ApiURL}/v3/drawing/file/download/${modelId}/${version}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "下载模型"))
}

//获取模型历史版本
export function getHistoryModels(modelId){
    let url = `${ApiURL}/v3/drawing/list/${modelId}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "获取模型历史版本")) 
}

//获取模型历史版本
export function getHistories(modelId,config){
    let url = `${ApiURL}/v3/drawing/list/${modelId}`;
    return rest.get(url,config)
        .then(result => result)
        .catch(err => _hookError(err, "获取模型历史版本")) 
}

//下载指定版本模型
export function downloadHistoryModel(modelId, version){
    let url = `${ApiURL}/v3/drawing/file/delete/${modelId}/${version}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "删除指定版本模型")) 
}

//删除指定版本模型
export function deleteHistoryModel(modelId, version){
    let url = `${ApiURL}/v3/drawing/file/delete/${modelId}/${version}`;
    return rest.remove(url)
        .then(result => result)
        .catch(err => _hookError(err, "删除指定版本模型")) 
}

//分享模型链接
export function shareDrawing(day, optionType, data){
    let url = `${ApiURL}/v3/drawing/share/${day}/${optionType}`;
    return rest.post(url,data)
        .then(result => result)
        .catch(err => _hookError(err, "获取分享图纸链接")) 
}

//获取分享模型数据
export function getShareDrawingData(key){
    let url = `${ApiURL}/v3/model/share/${key}`; //获取分享信息与模型保持一致
    return rest.get(url,{})
        .then(result => result)
        .catch(err => _hookError(err, "获取分享图纸数据")) 
}











