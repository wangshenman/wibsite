import React, { Component } from 'react';
import { Table, Input, DatePicker, Select, message, Button, Modal, 
  Upload, Icon, Dropdown, Menu, Progress} from 'antd';
import locale from 'antd/es/date-picker/locale/zh_CN';
import axios from 'axios';
import TipShow from '../componment/TipShow';
import UploadTable from '../componment/UploadTable';
import ShareModel from '../componment/ShareModel';
import { ApiURL } from '../../../../common/config/websiteConfig';
import { renderSize, switchTimeFormat, openWindow } from '../../../../common/utils/Utility';
import { getAllModelList,getModelState} from './thingList.actions';
import '../bimModelList/bimModelList.less';

const { Search } = Input;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { confirm } = Modal;

export default class ThingList extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      tipVisible: true, //提示语框显隐
      showUploadVisible: false, //上传列表框显隐
      shareModelVisble: false, //分享链接框显隐
      where: {},
      fileName: '',
      modelList: [], //模型列表
      pagination: {
        current: 1,
        pageSize: 10,
        total: null,
      },
      selectedRowKeys:[],
      selectedRows:[],
      selectedRecord: null,
      toolTip:{    //提示语文字
        totalNumInfo:'',
        totalSizeInfo:''
      },
      fileList: [], //上传文件列表

    }
    //初始化 请求获取 token 模型列表
  this.request = () =>{
    //获取token本地存储
    let accessToken = window.sessionStorage.getItem('token');
    if(accessToken){
        this.setState({
          loading:true
        })
        this.getModelList();
    }else{
        console.log("获取token失败 重新登录");
    }
  };

  this.switchState = (key, value) => {
    this.setState({ [key]: value })
  };

  //获得模型列表
  this.getModelList = () =>{
    const {fileName, pagination} = this.state;
    //获取已存token
    let modelToken = window.sessionStorage.getItem('token');
    //设置headers
    let config = {headers: {'x-access-token': modelToken}};
    let request = [];
    request.push(getAllModelList(config, pagination.current - 1,pagination.pageSize, { data:fileName }));
    Promise.all(request).then(([res]) => {
      let modelData = [];
      let result = res && res.data;
      pagination.total = result.totalElements;
      modelData = this.transferModelData(result.content);
      this.setState({
        modelList: modelData,
        pagination,
        loading: false,
        toolTip:{
          totalNumInfo:`已全部加载共 ${result.totalElements} 项`,
          totalSizeInfo:`总大小: 1000MB`
        }
      })
    }).catch(() => {this.setState({loading: false})});
  };

  //数据处理（去重、格式转换)
  this.transferModelData =(modelarray) =>{
    let modeldata = new Map()
    let modelLength = modelarray && modelarray.length;
    for (let i = 0; i < modelLength; i++) {
      let model = modelarray[i];
      let temp = {};
      temp._id = model.id;
      temp._modelId = model.modelId; //模型id
      temp._version = model.modelVersion;//版本号
      temp._name = model.fileName; //名称
      temp._time = switchTimeFormat(model.updatedAt); //上传时间
      temp._size = renderSize(model.fileSize); //文件大小
      temp._originSize = model.fileSize;
      temp._status = (i%4);
      modeldata.set(model.modelId, temp);
    }
    //将map数据转化成数组 只取value
    let modelMapArray = [...modeldata.values()]
    return modelMapArray
  };

  //文件名查询
  this.handleSearch = (value)=> {
    this.setState({
      pagination: {
        current: 1,
        pageSize: 10,
        total: null
      },
    })
    if(value){
        this.setState({
            fileName: value,
            where: Object.assign(this.state.where, {name: { $regex: `${value}`, $options: 'i' }}),
        }, () => this.getModelList());
    }else{
        let {where} = this.state;
        delete where.name;
        this.setState({
            fileName: value,
            where
        }, () => this.getModelList());
    }
  }

  //选择时间范围
  this.handleRangeTime = (date, dateString) => {
    this.setState({
      pagination: {
        current: 1,
        pageSize: 10,
        total: null
      },
    })
    if(date && date.length){
      // 开始时间
      let begin = date[0] && new Date(date[0]._d);
      begin && begin.setHours(0, 0, 0, 0);
      // 结束时间
      let end = date[1] && new Date(date[1]._d);
      end && end.setHours(23, 59, 59, 999);

      this.setState({
        where: Object.assign(this.state.where, { updateAt :{ $gte: `${begin.toISOString()}`, $lte: `${end.toISOString()}` }}),
      }, () => this.getModelList());
    }else{
      let {where} = this.state;
      delete where.updateAt;
      this.setState({
          where
      }, () => this.getModelList());
    }
  }

  // 取消文件上传
  this.uploadAbort = (record) => {
    let { fileList } = this.state;
    let self = this;
    if (record.percent && (record.percent === '100' || record.percent === 100) && (record.status === "done")) {
        let index = fileList.findIndex(file => file.uid === record.uid);
        fileList.splice(index, 1);
        self.setState({ fileList });
    } else {
        confirm({
            title: '确定取消该文件上传吗？',
            onOk() {
                let index = fileList.findIndex(file => file.uid === record.uid);
                fileList.splice(index, 1);
                self.setState({ fileList });
            }
        });
    }
  };

  // 关闭上传的内容
  this.closeUploadCon = () => {
      this.setState({
          showUploadVisible: false,
          fileList: []
      })
  };

  //模型下载
  this.download = (record) =>{
     if(record && record._id){
      //获取已存token
      let modelToken = window.sessionStorage.getItem('token');
      //设置headers
      let headers = {'x-access-token': modelToken}
      let downloadApi = `${ApiURL}/v1/models/${record._id}/download`;
      axios.get(downloadApi, {headers,responseType: 'blob'}).then(res => {
        let url = window.URL.createObjectURL(new Blob([res.data]));
        // 注释： 根据后台返回的 content-disposition: 'attachment;filename*=UTF-8''model.zip' 获取文件名
        let header = res && res.headers && res.headers['content-disposition'];
        let fileName = decodeURI(header && header.slice(28));
        let link = document.createElement("a");
        link.style.display = "none";
        link.href = url;
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click()
      })
    }
  };

  //模型删除
  this.delete = (record) =>{
    // e.stopPropagation();
    console.log('删除成功')
  }

  //模型转换
  this.transform = (record) =>{
    // e.stopPropagation();
    console.log('转换成功')
  }

  //模型分享
  this.shareModel = (record) =>{
    this.setState({
      shareModelVisble: true,
      selectedRecord: record
    })
  }

  //批量打开模型
  this.openModels = () =>{
    const { selectedRows} = this.state;
    if(!selectedRows.length){
      return message.warning('请选择要打开的物品~');
    }
    
    let models = [];
    selectedRows.map((item,index)=>{
      models.push({modelId:item._id,version:item._version})
    })
    
    this.props.history.push({
      pathname: `/drawing`,
      state: { models:models, from: 'modelList' }
    });
    
  };

  //获取模型解析状态
  this.getOpenState = (id) =>{
    //设置headers
    let config = {headers: {'x-access-token': window.sessionStorage.getItem('token')}};
    return Promise.all([getModelState(id,config)]).then(res =>res[0] && res[0].data);
  }

  //单个打开模型
  this.onRowClick = async(record) =>{
    //获取模型解析状态
    let state = await this.getOpenState(record._id);
    let pathname =  `/drawing?modelId=${record._modelId}&version=${record._version}`;
    if(state === 0){
      openWindow(pathname);
    }else if(state === 1 || state === 3){
      message.warning(`物品正在解析中，请稍后再试~`);
    }else if(state === 2){
      message.error(`物品解析失败~`);
    }
  }

  //点击分页
  this.handleTableChange = (pagination) =>{
      this.setState({
          pagination
      }, () => this.getModelList())
  };

}

  componentWillMount(){
    this.request();
  } 


  render() {
    const {  modelList, pagination, loading,  fileList, selectedRowKeys, selectedRecord,
      tipVisible, toolTip, showUploadVisible, shareModelVisble } = this.state;

    const renderStatus =(status,record)=>{
      switch(status){
        case 0:
          return (<span className="status-canuse">可使用</span>)
        
        case 1:
          return (
          <div className="status-waiting">
              <div className="flex-between">
                <Progress width={120} percent={0} strokeWidth={6} showInfo={false}/>
                <Icon style={{marginLeft:'8px'}} type="history"/>
              </div>
              <p>等待转换，预计等待30分钟</p>
          </div>)
        
        case 2:
          return (
            <div className="status-error flex-center">
               <span>转化失败</span>
                <Icon className="reflash" type="redo"/>
            </div>)
           
        case 3:
          return (
            <div className="status-transfrom">
                <div><Progress width={120} percent={record.percent || 50} strokeWidth={6}/></div>
                <p>{`转换中…预计耗时${record.time || 10}分钟`}</p>
            </div>)
        
        default:
          break;
      }
    };

    const opreateMenu = (record)=>{
      return(
        <Menu>
          {(DropdownConfig||[]).map(i => <Menu.Item key={i.key} onClick={()=>i.action(record)}>{i.title}</Menu.Item>)}
        </Menu>
      )
    };

    const DropdownConfig =[
      {
        title: '分享',
        key: 'share',
        action: (record) => this.shareModel(record)

      },
      {
        title: '下载',
        key: 'download',
        action: (record)=> this.download(record)
      },
      {
        title: '删除',
        key: 'delete',
        action: (record) => this.delete(record)
      },
    ];

    const columns = [
      {
        title: '物品ID',
        dataIndex: '_modelId',
        width: 180
      },
      {
        title: '物品名称',
        dataIndex: '_name',
        width: 220
      },
      {
        title: '上传时间 / 大小',
        dataIndex: '_size',
        width: 200,
        render: (text, record) =>{
          if(text){
            return(
              <div className='record-item'>
                  <div className='upload-time'>{record._time}</div>
                  <div className='upload-size'>{record._size}</div>
              </div>
            )
          }
        }
      },
      {
        title: '开始转换时间 / 耗时',
        dataIndex: '_time',
        width: 200,
        render: (text, record) =>{
          if(text){
            return(
              <div className='record-item'>
                  <div className='upload-time'>{record._time}</div>
                  <div className='upload-size'>{record._size}</div>
              </div>
            )
          }
        }
      },
      {
        title: '物品状态',
        dataIndex: '_status',
        width: 200,
        align: 'center',
        render: (text, record) => renderStatus(text,record)
      },
      {
        title: '操作',
        width: 160,
        dataIndex: '_id',
        align: 'center',
        render: (text, record) =>{
          if(text){
            return(
              <div className='opreate flex-center'>
                  <span className='view' onClick={() => this.onRowClick(record)}>预览</span>
                  <Dropdown overlay={opreateMenu(record)} trigger={['hover']} placement="bottomRight">
                      <span className='more'>更多 <Icon type="down" style={{color:'#3D81FA'}}/> </span>
                  </Dropdown>
              </div>
            )
          }
        }
      },
    ];

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
          if(selectedRowKeys.length){
            let totalSize = null;
            selectedRows.map(i =>{totalSize += i._originSize});
            this.setState({
              toolTip:{
                isSelect: true,
                totalNumInfo:`已选择 ${selectedRowKeys.length} 项`,
                totalSizeInfo:`总大小: ${renderSize(totalSize)}`
              }
            })
          }else{
            this.setState({
              toolTip:{
                totalNumInfo:`已全部加载共 ${this.state.modelList.length} 项`,
                totalSizeInfo:`总大小: 1000MB`
              }
            })
          }
          this.setState({
              selectedRowKeys: selectedRowKeys,
              selectedRows: selectedRows
          })
      }
    };

    const props = {
      withCredentials: true,
      multiple: true,
      showUploadList:false,
      beforeUpload: ({ file, fileList }) => {  
        this.setState(state => ({
          fileList: [...state.fileList, file],
        }));
      },
      onChange:({ file, fileList }) => {
        if (fileList.length !== this.state.fileList.length) {
          this.setState({
              showUploadVisible: true
          });
          this.setState({ fileList});
        }
      },
      fileList,
    };

    return (
        <div className='bimModelList'>
          <div className="bimModelList-header">物品集成</div>
          <div className='bimModelList-content'>
              <div className="operate-wrapper flex-between">
                  <div className='operate-search flex-center'>
                      <div className="option-item">
                          <Search placeholder="根据文件ID或名称搜索" onSearch={value => this.handleSearch(value)} style={{ width: '220px' }}/>
                      </div>
                      <div className="option-item">
                          <span>上传时间：</span>
                          <RangePicker locale={locale} onChange={this.handleRangeTime} getContainer= {()=>document.getElementById('inputWrapper') } style={{ width: '220px' }}/>
                      </div>
                      <div className="option-item">
                          <span>物品状态：</span>
                          <Select defaultValue="1" style={{ width: 120 }} allowClear>
                               <Option value="0">可使用</Option>
                               <Option value="1">等待转换</Option>
                               <Option value="2">转换失败</Option>
                               <Option value="3">转换中</Option>
                          </Select>
                      </div>
                  </div>
                  
                  <div className="operate-botton flex-center">
                      <Upload  {...props} accept=".rvt,.dgn,.rfa">
                          <Button className="option-btn" type={'primary'}>上传文件</Button>
                      </Upload>
                      <Button className="option-btn" onClick={this.openModels}>批量预览</Button>
                      <Button className="option-btn" onClick={this.openModels}>批量转换</Button>
                      <Button className="option-btn" onClick={this.openModels}>删除</Button>
                  </div>
              </div>
              { tipVisible && modelList.length ? <TipShow onClose={this.switchState} toolTip={toolTip}/> : null }
              <div className='content-table'>
                <Table rowKey={record => record._id} 
                      selectedRowKeys = {selectedRowKeys}
                      rowSelection={rowSelection} 
                      columns={columns} 
                      dataSource={modelList} 
                      pagination={pagination}
                      onChange={this.handleTableChange}
                      loading={loading}
                />
              </div>
            </div> 
            {
              showUploadVisible ?
                <UploadTable 
                    fileList={ fileList }
                    closeUploadCon={ this.closeUploadCon }
                    uploadAbort={ this.uploadAbort } 
                    onClose={this.switchState}
                /> : null
            }
            {
               shareModelVisble ? 
                <ShareModel 
                   visible={ shareModelVisble }
                   selectRows={ selectedRecord }
                   onClose={this.switchState}
                /> : null
            }
        </div>
    )
  }
}
