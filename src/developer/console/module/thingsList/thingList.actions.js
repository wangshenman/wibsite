
import rest from '../../../../common/utils/rest';
import { ApiURL } from '../../../../common/config/websiteConfig';

export function _concatUrl(url) {
    return `${ApiURL}/v1/account-service/${url}`;
}


function _hookError(error, message) {
    console.log(message + '出现错误 => ', error);
    throw error;
}

//获取模型列表
export function getAllModelList(config, page, pageSize, data) {
    let url = `${ApiURL}/modelFile/getModelList/${page}/${pageSize}`;
    return rest.post(url, data, config)
        .then(result => result)
        .catch(err => _hookError(err, "获取模型列表"))
}

//获取图纸列表
export function getAllDrawingList(config, page, pageSize, data) {
    let url = `${ApiURL}/drawingFile/getDrawingList/${page}/${pageSize}`;
    return rest.post(url, data, config)
        .then(result => result)
        .catch(err => _hookError(err, "获取图纸列表"))
}

//获取资料列表
export function getAllFileList(config, parentId, page, pageSize, data) {
    let url = `${ApiURL}/yzFile/getFileList/${parentId}/${page}/${pageSize}`;
    return rest.post(url, data, config)
        .then(result => result)
        .catch(err => _hookError(err, "获取资料列表"))
}

//上传模型
export function uploadModelFile(data,config){
    let url = `${ApiURL}/modelFile/uploadModelFile`
    return rest.post(url, data, config)
        .then(result => result)
        .catch(err => _hookError(err, "上传模型"))
}

//上传图纸
export function uploadDrawingFile(data,config){
    let url = `${ApiURL}/drawingFile/uploadDrawingFile`
    return rest.post(url, data, config)
        .then(result => result)
        .catch(err => _hookError(err, "上传图纸"))
}

//获取存储的数据
export function getUserData(key) {
    let url = `${ApiURL}/wxUser/getData/${key}`
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "获取存储的数据"))
}

//获取用户信息
export function getPhoneUser(config){
    let url = `${ApiURL}/wxUser/getPhoneUser`
    return rest.get(url,config)
        .then(result => result)
        .catch(err => _hookError(err, "获取用户信息"))
}

//获取用户头像
export function getUserAvatar(config){
    let url = `${ApiURL}/wxUser/getUserHeadImage`
    return rest.get(url,config)
        .then(result => result)
        .catch(err => _hookError(err, "获取用户信息"))
}

//获取模型解析状态
export function getModelState(id,config){
    let url = `${ApiURL}/modelFile/getModelState/${id}`
    return rest.get(url,config)
        .then(result => result)
        .catch(err => _hookError(err, "获取模型解析状态"))
}

//获取图纸解析状态
export function getDrawingState(id,config){
    let url = `${ApiURL}/drawingFile/getDrawingState/${id}`
    return rest.get(url,config)
        .then(result => result)
        .catch(err => _hookError(err, "获取图纸解析状态"))
}







