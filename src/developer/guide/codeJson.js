export const codeString = 
`
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>加载多模型</title>
  <style media="screen">
    * {
      margin: 0;
      padding: 0;
    }

    html,
    body {
      height: 100%;
    }

    .buttons {
      font-size: 0;
    }

    .button {
      margin: 5px 0 5px 5px;
      width: 100px;
      height: 30px;
      border-radius: 3px;
      border: none;
      background: #3D81FA;
      color: #FFFFFF;
    }

    .main {
      display: flex;
      flex-direction: column;
      overflow: hidden;
      height: 100%;
    }

    #domId {
      flex: 1;
    }
  </style>
  <!-- 引用BIMFACE的JavaScript显示组件库 -->
  <script src="https://static.bimface.com/api/BimfaceSDKLoader/BimfaceSDKLoader@latest-release.js"
    charset="utf-8"></script>
</head>

<body>
  <!-- 定义DOM元素，用于在该DOM元素中显示模型或图纸 -->
  <div class='main'>
    <div class='buttons'>
      <button class="button" id="btnAddModel" onclick="addModel()">添加模型</button>
      <button class="button" id="btnMoveModel" onclick="moveModel()">平移模型</button>
      <button class="button" id="btnRotateModel" onclick="rotateModel()">旋转模型</button>
      <button class="button" id="btnScaleModel" onclick="scaleModel()">缩放模型</button>
    </div>
    <!-- 定义DOM元素，用于在该DOM元素中显示模型或图纸 -->
    <div class='model' id="domId"></div>
  </div>
  <script type="text/javascript">
    var viewToken = 'a557763d286f4ecab5dd31c7a9127fa1';
    // 声明Viewer及App
    var viewer3D;
    var app;
    var viewAdded = false;
    var modelId = '1641597995361472';
    // 初始化显示组件
    var options = new BimfaceSDKLoaderConfig();
    options.viewToken = viewToken;
    BimfaceSDKLoader.load(options, successCallback, failureCallback);
    function successCallback(viewMetaData) {
      if (viewMetaData.viewType == "3DView") {
        // ======== 判断是否为3D模型 ========        
        // 获取DOM元素
        var dom4Show = document.getElementById('domId');
        var webAppConfig = new Glodon.Bimface.Application.WebApplication3DConfig();
        webAppConfig.domElement = dom4Show;
        // 创建WebApplication
        app = new Glodon.Bimface.Application.WebApplication3D(webAppConfig);
        // 添加待显示的模型
        app.addView(viewToken);
        // 从WebApplication获取viewer3D对象
        viewer3D = app.getViewer();
        // 监听添加view完成的事件
        viewer3D.addEventListener(Glodon.Bimface.Viewer.Viewer3DEvent.ViewAdded, function () {
          viewAdded = true;
          //自适应屏幕大小
          window.onresize = function () {
            viewer3D.resize(document.documentElement.clientWidth, document.documentElement.clientHeight - 40);
          }
          // 渲染3D模型
          viewer3D.render();
        });
      }
    }
    function failureCallback(error) {
      console.log(error);
    }

    // 根据viewToken加载指定模型
    var isModelAdded = false;
    function addModel() {
      if (!viewAdded || isModelAdded) {
        return;
      }
      viewer3D.addView('79b8ddbe4bc14a3fa8c6248515296431');
      isModelAdded = true;
    }

    // 平移模型
    function moveModel() {
      if (!viewAdded || !isModelAdded) {
        return;
      }
      viewer3D.setModelTranslation(modelId, {x: 8000, y: 2500, z: 0});
      viewer3D.render();
    }

    // 基于世界坐标中的某个点，将模型绕z轴旋转
    function rotateModel() {
      if (!viewAdded || !isModelAdded) {
        return;
      }
      viewer3D.setModelRotationZ(modelId, {x: 0, y: 0, z: 0}, Math.PI / 6);
      viewer3D.render();
    }

    // 基于世界坐标中的某个点，将模型进行整体缩放
    function scaleModel() {
      if (!viewAdded || !isModelAdded) {
        return;
      }
      viewer3D.setModelScale(modelId, {x: 0, y: 0, z: 0}, 1.2);
      viewer3D.render();
    }

    // 按钮文字
    function setButtonText(btnId, text) {
      var dom = document.getElementById(btnId);
      if (dom != null && dom.nodeName == "BUTTON") {
        dom.innerText = text;
      }
    }
  </script>
</body>
</html>
`