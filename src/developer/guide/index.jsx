import React, { Component } from 'react';
import CodeMirror from 'react-codemirror';// 引入codemirror
import 'codemirror/lib/codemirror.css';// 引入基本样式
import 'codemirror/keymap/sublime';
import 'codemirror/mode/htmlmixed/htmlmixed';// 引入语言类型(可选)
import 'codemirror/theme/monokai.css';// 引入主题颜色(可选)
import 'codemirror/addon/display/fullscreen.css';
import 'codemirror/addon/display/fullscreen.js';
import {codeString} from './codeJson';
import Header from '../component/HeaderView';
import './guide.less'


export default class Guide extends Component {
  constructor(props) {
    super(props)
    this.state = {
      code: codeString,
    }
    this.handleCodeChange = (code)=>{
      console.log('code111',code)
      this.setState({ code })
    }
    //显示结果
		this.submitTryit = () =>{
      const{ code } = this.state;
      let iframe = document.getElementById("iframeResult");
			let ifrw = (iframe.contentWindow) ? iframe.contentWindow : (iframe.contentDocument.document) ? iframe.contentDocument.document : iframe.contentDocument;
			let ifrwd = ifrw.document;
			ifrwd.open();
			ifrwd.write(code);
			ifrwd.close();
		}
  }

  componentDidMount(){
    this.submitTryit();
  } 

  componentWillUnmount(){}

  render() {
    const options={  
      lineNumbers: true,                     //显示行号  
      mode: {name: "javascript", json: true}, //定义mode  
      extraKeys: {"Ctrl": "autocomplete"},   //自动提示配置  
      theme: "monokai",                  //选中的theme
      // value: 'const a = 0;', // 初始值，字符串或者Document对象
      gutter: true,
      indentUnit: 4, // 缩进单位(默认2)
      smartIndent: true, // 是否智能缩进 默认 true
      tabSize: 4, // tab 宽度
      keyMap: 'default', // 是否配置秘钥映射
      lineWrapping: true, // 是否换行(默认不换行 scroll)
      lineNumbers: false, // 是否显示行号
      firstLineNumber: 3, // 初始行号(默认是1，通常不需要设置)
      readOnly: false, // 只读模式
      undoDepth: 40, // 撤销次数
      // autofocus: true, // 初始自动聚焦
      onDragEvent: true,
      onKeyEvent: true, // 是否允许拖拽事件和键盘事件
    };

    return (
      <div className='guide'>
       <Header/>
       <div className="guide-wrapper">
           <div className="guide-side">
              模型预览功能
           </div>
           <div className="guide-content">
             <div className="code-show">
                <div className="operate">
                     <div className="code-name">模型预览功能</div>
                      <div>
                        <span id="runCode" className="oper-btn" onClick={this.submitTryit}>运行</span>
                        <span id="resetCode" className="oper-btn" onClick={this.submitTryit}>刷新</span>
                      </div>
                </div>
                <CodeMirror
                      ref="editor"
                      value={this.state.code}
                      options={options}
                      onChange={code => this.handleCodeChange(code)}
                      height="680px"
                />
             </div>
             <div className="boundary"></div>
             <div className="preview-show">
                <iframe src="" frameborder="0" id="iframeResult"></iframe>
             </div>
           </div>
       </div>
      </div>
    )
  }
}