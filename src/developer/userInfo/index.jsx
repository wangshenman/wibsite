import React, { Component } from 'react';
import { Tabs} from 'antd';
import { withRouter } from 'react-router-dom';
import Header from '../component/HeaderView';
import Footer from '../component/FooterView';
import PersonalInfo from './module/personalInfo';
import SecuritySetting from './module/securitySetting';

import './userInfo.less';

const { TabPane } = Tabs;

class UserInfo extends Component {
  constructor(props) {
    super(props)
    
    let active = (this.props.location.state && this.props.location.state.active) || '1';
    this.state = {
      activeKey: active,
    };

    this.userTab = [
      {
        key: '1',
        title:'个人信息',
        link:'personalInfo'
      },
      // {
      //   key: '2',
      //   title:'安全设置',
      //   link:'securitySetting'
      // }
    ];
  }

  render() {
    const getMenuContent = (key) =>{
      switch (key) {
        case 'personalInfo':
          return <PersonalInfo />;
        case 'securitySetting':
          return <SecuritySetting/>;
        default:
          return <PersonalInfo />;
      }
    }

    return (
      <div className='userInfo-page'>
         <Header/>
         <div className='userInfo'>
            <div className="userInfo-content">
                <Tabs  activeKey={this.state.activeKey} onChange={activeKey => this.setState({activeKey})} tabPosition={'left'} className="userInfo-tab">
                  {
                    this.userTab.map(item => {
                      return (<TabPane tab={item.title} key={item.key}> { getMenuContent(item.link) }</TabPane>)})
                  }
                </Tabs>
          </div>
        </div>
        <Footer/>
      </div>
    )
  }
}


export default withRouter(UserInfo);