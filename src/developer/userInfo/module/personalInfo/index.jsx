import React, { Component } from 'react';
import { connect } from 'react-redux';
import {  Input,  Cascader, message, Button, Upload, Icon} from 'antd';
import { position } from '../../../../common/config/areaJson';
import { ApiURL } from '../../../../common/config/websiteConfig';
import defaultImg from '../../../../common/img/common/default_admin.png';
import {getUserInfo, updateUserInfo} from '../../../console/actions';
import './personalInfo.less';

class PersonalInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fileListArray:[],
      users: {},
      avatar: ''
    }

    this.init = ()=>{
        this.props.getUserInfo().then(res =>{
          if(res && res.code === 200){
              this.setState({ 
                users: res.data && res.data.responseDUser,
                avatar: res.data && res.data.image
              })
          }
        }).catch(error => error && message.error(error.message))
    }

    this.switchState = (key, value) => {
      if(key === 'address'){
        if(value.length){
          this.setState({
            users: Object.assign(this.state.users, {[key]: `${value[0]}-${value[1]}-${value[2]}`})
          });
        }else{
          this.setState({
            users: Object.assign(this.state.users, {[key]: null})
          });
        }
      }else{
        this.setState({ 
          users: Object.assign(this.state.users, {[key]: value})
        })
      }
    };

    this.onUpdate = () =>{
      const {users} = this.state;
      console.log(users)
      let idCheck = /^(?![\d]+$).{4,20}$/;
      //let emailCheck = /^(?:\w+\.?)*\w+@(?:\w+\.)*\w+$/;
      if(!idCheck.test(users.userName)){ return message.warning('请输入4-20位非纯数字的用户名~')};
      //if(!emailCheck.test(users.email)){ return message.warning('请输入正确格式的邮件地址~')};
      //if(!users.phone){ return message.warning('请输入联系电话~') };
      if(!users.company){ return message.warning('请输入公司名称~') };
      if(!users.address || users.address.indexOf('undefined')> -1){ return message.warning('请输入公司地址~') };
      if(!users.position){ return message.warning('请输入个人职位~') };
      this.updateUserInfo(users) 
    };

    this.updateUserInfo = (data) =>{
      this.props.updateUserInfo(data).then(res =>{
        if(res && res.code === 200){
          message.success('提交成功')
          this.setState({users: res.data},()=>this.init())
        }else{
          res && message.error(res.message)
        }
      }).catch(error => 
        error && message.error(error.message)
      )
    };

    this.beforeUpload = ({file, fileList}) =>{
      let limitSize = file && file.size / 1024 > 5;
      if(limitSize){
        return message.warning('图片文件上传不得超过5MB,请重新上传~')
      } 
    };

    this.onChangeUpload = ({file,fileList}) =>{
      if (file.percent && (file.percent === '100' || file.percent === 100) && (file.status === "done")) {
        if(file.response && file.response.code === 200){
          message.success("头像修改成功");
          window.localStorage.setItem('avatar', file.response.data);
          this.setState({
            avatar: file.response.data
          })
        }else{
          message.error(file.response && file.response.message)
        }
      }
      this.setState({fileListArray: [...fileList]});
    };
    
  }

  componentWillMount(){
    this.init();
  }

  render() {
    const {fileListArray ,users, avatar} = this.state;
    const uploadHeaders = {'x-access-token': window.localStorage.getItem('token')};
    let handleUploadData = (file) =>{
      let data = { headImage: file };
      return data
    };
    return (
      <div className='personalInfo'>
         <div className="per-header">个人信息</div>
         <div className="per-tooltip">请完善以下信息,方便我们更好的为您服务</div>
         <div className="per-content">
             <div className="per-content-left">
               {/* <div className="input-item">
                    <p className="item-label">用户ID</p>
                    <Input placeholder="请输入用户ID" max-length={20} value={users && users.userId} onChange={(e)=>this.switchState('userId',e.target.value)}/>
                </div> */}
                {/* <div className="input-item">
                    <p className="item-label">用户名</p>
                    <Input placeholder="请输入用户ID" max-length={20} value={users && users.userId} onChange={(e)=>this.switchState('userName',e.target.value)}/>
                </div> */}
                <div className="input-item">
                    <p className="item-label">昵称<span className="red">*</span></p>
                    <Input placeholder="请输入昵称" value={users && users.nickName} onChange={(e)=>this.switchState('nickName',e.target.value)}/>
                </div>
                <div className="input-item">
                    <p className="item-label">联系电话</p>
                    <Input placeholder="请输入联系电话" value={users && users.phone} type={'tel'} disabled/>
                </div>
                {/* <div className="input-item">
                    <p className="item-label">邮箱</p>
                    <Input placeholder="请输入邮箱" value={users && users.email} onChange={(e)=>this.switchState('email',e.target.value)}/>
                </div> */}
                <div className="input-item">
                    <p className="item-label">公司名称<span className="red">*</span></p>
                    <Input placeholder="请输入公司名称" value={users && users.company} onChange={(e) =>this.switchState('company',e.target.value)}/>
                </div>
                <div className="input-item">
                    <p className="item-label">公司地址<span className="red">*</span></p>
                    <Cascader placeholder="请选择所在省市" allowClear={true} value={users && users.address ? getAddressArray(users.address) : null} options={position} onChange={(value) =>this.switchState('address',value)}/>
                </div>
                <div className="input-item">
                    <p className="item-label">个人职位<span className="red">*</span></p>
                    <Input placeholder="请输入公司名称" value={users && users.position} onChange={(e) =>this.switchState('position',e.target.value)}/>
                </div>
                <Button type="primary" className="submit-btn" onClick={this.onUpdate}>更新基本信息</Button>
             </div>
             <div className="per-content-right">
                <div className="input-item">
                    <p className="item-label">头像</p>
                    <div className="item-headImg">
                      <img src={avatar || defaultImg} alt="用户头像"/>
                    </div>
                    <Upload   headers={ uploadHeaders }
                              showUploadList={true}
                              action={`${ApiURL}/v1/user/head/image/upload`} 
                              data={(file) => handleUploadData(file)}
                              fileList={fileListArray}
                              onChange={this.onChangeUpload}
                              beforeUpload={this.beforeUpload}>
                              <Button className="create-button icon-button"><Icon type="upload"/>上传头像</Button>
                    </Upload>
               </div>
             </div>
         </div>
      </div>
    )
  }
}

function getAddressArray(address){
  if(address){
    let arr = address.split('-');
    if(arr.indexOf('undefined')> -1){
      return [];
    }else{
      return arr;
    }
  }
 
}

function mapStateToProps(state) {
  return {
    userInfo: state.getIn(["user", "user_info"]) && state.getIn(["user", "user_info"]).toJS(),
  };
}

const mapActionCreators = {
  getUserInfo,
  updateUserInfo
};

export default connect(mapStateToProps, mapActionCreators)(PersonalInfo);

