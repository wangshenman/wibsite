import React, { Component } from 'react';
import { withRouter} from 'react-router-dom';
import './securitySetting.less';

 class SecuritySetting extends Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.edit = (key) =>{
        this.props.history.push(`/verifyPhone?type=${key}`);
    }
  }

  render() {

    return (
      <div className='securitySetting'>
         <div className="sec-header">安全设置</div>
         <div className="sec-content">     
            <div className="setting-item flex-between">
                <div className="item-left">
                    <span className="item-label">账号密码</span>
                    <span className="item-label">当前密码强度：<span>强</span></span>
                </div>
                <span className="item-right" onClick={()=> this.edit('password')}>修改</span>
            </div>
            <div className="setting-item flex-between">
            <div className="item-left">
                    <span className="item-label">手机号</span>
                    <span className="item-label">已绑定手机号：<span>187****9594</span></span>
                </div>
                <span className="item-right" onClick={()=> this.edit('phone')}>修改</span>
            </div>
            <div className="setting-item flex-between">
            <div className="item-left">
                    <span className="item-label">邮箱</span>
                    <span className="item-label">已绑定邮箱：<span>675898834@qq.com</span></span>
                </div>
                <span className="item-right" onClick={()=> this.edit('email')}>修改</span>
             </div>
         </div>
      </div>
    )
  }
}

export default withRouter(SecuritySetting)

