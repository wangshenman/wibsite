import React, { Component } from 'react';
import { Breadcrumb } from 'antd';
import { withRouter } from 'react-router-dom';
import Header from '../component/HeaderView';
import Footer from '../component/FooterView';
import HeadSteps from '../component/CommonView/HeadSteps';
import StepVerify from './module/StepVerify';
import StepEdit from './module/StepEdit';
import StepFinish from './module/StepFinish';
import { getURLValueByKey} from '../../common/utils/Utility'
import './verifyPhone.less';

const STEP_VERIFY = "STEP_VERIFY";
const STEP_EDIT = "STEP_EDIT";
const STEP_FINISH = "STEP_FINISH";

 class VerifyPhone extends Component {
  constructor(props) {
    super(props)
    let type = getURLValueByKey('type') || 'password';
    let editName = type === 'password'? '修改密码': this.type === 'phone'? '修改手机号' : '修改邮箱';
    this.state = {
      type: type,
      editName: editName,
      curStep: STEP_VERIFY,
    }
    this.setStep = (step) =>{
      this.setState({
        curStep: step,
      })
    }
  }

  render() {
    const {type,editName,curStep} = this.state;
    
    let stepsData = [
      {key: STEP_VERIFY, title: '验证账号'},
      {key: STEP_EDIT, title: editName},
      {key: STEP_FINISH, title: '完成'},
    ];

    const getContent = (key) =>{
      switch (key) {
        case 'STEP_VERIFY':
          return <StepVerify goNext={this.setStep}/>;
        case 'STEP_EDIT':
          return <StepEdit type={type} goNext={this.setStep}/>;
        case 'STEP_FINISH':
          return <StepFinish />;
        default:
          return <StepVerify goNext={this.setStep}/>;
      }
    }

    return (
      <div className='verifyPhone-page'>
         <Header/>
         <div className='verifyPhone'>
            <div className="verifyPhone-nav">
                  <Breadcrumb separator=">" className="w1200">
                      <Breadcrumb.Item className="nav-link" onClick={()=> this.props.history.push({ pathname:'/userInfo',state:{ active:'2'} })}>安全设置</Breadcrumb.Item>
                      <Breadcrumb.Item>{editName}</Breadcrumb.Item>
                  </Breadcrumb >
            </div>
            <div className="verifyPhone-content w1200">
                <HeadSteps steps={stepsData} curStep={curStep}/>
                <div className="verifyPhone-stepsWrapper">
                  { getContent(curStep) }  
                </div>
            </div>
        </div>
         <Footer/>
      </div>
    )
  }
}

export default withRouter(VerifyPhone)

