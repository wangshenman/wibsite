
import React, { Component } from 'react';
import { Input,Button } from 'antd';
import '../verifyPhone.less'


export default class StepEdit extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { type } = this.props;

    const editContent = (type) =>{
      switch(type){
        case 'password':
           return (
           <div className="edit-password">
                <div className="check-item flex-start">
                      <span><span className="red">*</span> 新密码：</span>
                      <Input className="check-input" placeholder="请输入新密码"/> 
                </div>
                <div className="check-item flex-start">
                      <span><span className="red">*</span> 确认密码：</span>
                      <Input className="check-input" placeholder="请再次输入确认密码"/> 
                </div>
           </div>)
        case 'phone':
          return (
          <div className="edit-phone">
              <div className="check-item flex-start">
                    <span><span className="red">*</span> 新手机号码：</span>
                    <Input className="check-input" placeholder="请输入新手机号码"/> 
              </div>
              <div className="check-item flex-start">
                    <span><span className="red">*</span> 短信验证码：</span>
                    <Input className="check-input" placeholder="请输入验证码"/> 
                    <Button className="check-btn">获取短信验证码</Button> 
                    <span className="check-mes-text"> 没收到短信验证码？</span>
              </div>
          </div>)
        case 'email':
          return (
          <div className="edit-email">
              <div className="check-item flex-start">
                    <span><span className="red">*</span> 新邮箱号：</span>
                    <Input className="check-input" placeholder="请输入新邮箱号"/> 
              </div>
          </div>)
        default:
          break;
      }
    }

    return (
      <div className='stepEdit edit-item'>
          <div className="check-item flex-start">
              <span>已验证手机：</span>
              <span>+86-136****4043</span>     
          </div>
         { editContent(type) }
         <Button type={'primary'} className="oper-btn" onClick={()=>this.props.goNext('STEP_FINISH')}>确定</Button> 
      </div>
    )
  }
}
