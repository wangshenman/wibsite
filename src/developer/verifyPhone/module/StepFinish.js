import React from 'react';
import successIcon from '../../../common/img/developer/success.png';
import '../verifyPhone.less'

const StepFinish = () =>{
  return (
    <div className='stepFinish flex-vertical-center'>
        <img src={successIcon} alt=""/>
        <span>修改成功</span>
    </div>
  )
}

export default StepFinish
