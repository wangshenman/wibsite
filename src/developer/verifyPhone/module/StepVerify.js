
import React, { Component } from 'react';
import { Input,Button } from 'antd';
import phoneIcon from '../../../common/img/developer/phoneIcon.png';
import tipIcon from '../../../common/img/developer/tipIcon.png';
import '../verifyPhone.less'


class StepVerify extends Component {
  constructor(props) {
    super(props)
    this.state = {
        isSelect: true,
    }
    this.handleChoose = () =>{
        this.setState({
            isSelect: true
        })
    }
  }

  render() {
    const {isSelect} = this.state;

    return (
      <div className='stepVerify'>
          {
              !isSelect ?
              <div className="verify-choose flex-vertical-center">
                  <h2>请选择验证方式</h2>
                  <div className="verify-choose-content flex-between">
                      <div><img src={phoneIcon} alt=""/><span>通过短信验证码验证</span></div>
                      <span className="choose-btn" onClick={this.handleChoose}>立即验证</span>
                  </div>
              </div>
              :
              <div className="verify-check">
                  <span className="check-tip"><img src={tipIcon} alt=""/><span>为确认是您本人操作，请完成一下验证</span></span>
                  <div className="check-item flex-start">
                        <span>已验证手机：</span>
                        <span>+86-136****4043</span>     
                  </div>
                  <div className="check-item flex-start">
                        <span><span className="red">*</span>  短信验证码：</span>
                        <Input className="check-input" placeholder="请输入验证码"/> 
                        <Button className="check-btn">获取短信验证码</Button> 
                        <span className="check-mes-text"> 没收到短信验证码？</span>
                  </div>
                  <Button type={'primary'} className="oper-btn" onClick={()=>this.props.goNext('STEP_EDIT')}>下一步</Button> 
              </div>
          }
        
      </div>
    )
  }
}

export default StepVerify