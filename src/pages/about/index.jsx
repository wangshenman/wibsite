import React, { Component } from 'react';
import './about.less';
import store from "../../store/index";
import { FOOTER_VISIBLE } from '../../router/router.reducer';
import { addressWay,developData } from '../../common/config/aboutJson';
import Header from '../../common/component/Header';
import Footer from '../../common/component/Footer';
import Banner from '../../common/component/Banner';
import downIcon from '../../common/img/about/downIcon.png';

export default class About extends Component {
  // map = () => {
  //   if(window.BMap) {
  //       let map = new window.BMap.Map("mapContainer");          // 创建地图实例
  //       let point = new window.BMap.Point(121.510455,31.349938);  // 创建点坐标
  //       let point1 = new window.BMap.Point(121.510455,31.347938);  // 创建点坐标
  //       map.centerAndZoom(point, 17);                 // 初始化地图，设置中心点坐标和地图级别
  //       map.enableScrollWheelZoom(true);
  //       var marker = new window.BMap.Marker(point1);        // 创建标注
  //       map.addOverlay(marker);
       
  //   }
  // };
  
  componentDidMount() {
    // this.map();
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: true,
    });
  };

  componentWillUnmount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: false,
    });
  };
  render() {
 

  const casesLayout = (cases) =>{
    return(
      <ul className="case-list w1200">
      {
        cases.map((item, index) => {
          return (
            <li className="item" key={index}>
                <img className="img" src={ item.imgHref } alt=""/>
                <div className="img-bg"></div>
                <div className="title"><p>{ item.title }</p></div>
                <div className="detail">
                    <h3>{ item.detail.title }</h3>
                    <p>{ item.detail.desc }</p>
                </div>
            </li>)
        })
      }
      </ul>
    )
}
  return (
      <div className="about-page">
      < Header/>
      <div className="about main"> 
        <Banner page={'about'}/>
        <div className="contact-us">
          <div className="contact-ways w1200">
              <h2 className="con-title contact-title">服务网络</h2>
              <ul className="way-session">
                  {
                      addressWay.map(item => {
                          return(
                              <li key={item.key} className="item">
                                  <p className="title">{item.title}<img src={item.icon} alt=''/></p>
                                  <p className="sign">{item.sign}</p>
                                  <p className="name">{item.name}</p>
                                  <p className="phone">{item.number}</p>
                                  <p className="address">{item.address}</p>
                              </li>
                          )
                      })
                  }
              </ul>
          </div>
          <div className="contact-develop">
              <h2 className="con-title contact-title">开发服务</h2>
              <div className="develop-service w1200">
                <img src={developData.img} alt="" />
                <div className="dev-expains">
                  <p className="dev-title">开发支持</p>
                  {
                    (developData.expains).map(d =>{
                      return(<div className="dev-text" key={d.key}>
                        <img src={downIcon} alt=''/><span>{d.text}</span>
                      </div>)
                    })
                  }
                </div>
              </div>
          </div>
          {/* <div className="company-address w1200">
              <h2 className="con-title contact-title">办公地址</h2>
              <div className="map-card" onClick={this.map}>
                  <p className="p">上海市杨浦区国权北路1688号湾谷科技园A6栋11F</p>
              </div>
              <div className="address-map" id="mapContainer"></div>
          </div> */}
          </div>
        </div>
    <Footer/>
  </div>
  )
  }
}

