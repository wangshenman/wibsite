import React from 'react'; 
import { message } from 'antd';
import './bimDrawing.less';
import ModelHeader from './component/modelHeader/ModelHeader';
import DrawingView from './component/drawingView/drawingView';
import { getURLValueByKey } from '../../common/utils/Utility';
import { getExampleToken, convertToken } from '../../common/utils/commonActions';
import { modelConfig } from '../../common/config/websiteConfig';
import { getHistoryModels, getHistories, getShareDrawingData } from '../../developer/console/module/drawingsList/drawinglList.actions';

class BimDrawing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modelName:'', //模型名称
      histories:[], //历史版本
      pathFrom: '',
      links: [], //模型信息
      config: {},
      isShare: false, //是否为分享
      token: null, //token
    }
      this.init = () =>{
        //获取页面跳转来源
        let pathFrom = getURLValueByKey('from');
        let shareCode = getURLValueByKey('shareCode');
        let modelId = getURLValueByKey('modelId');
        let version = parseInt(getURLValueByKey('version'));
        this.setState({ pathFrom });
       
        //打开单个模型
        if(modelId && version){
           //示例页面
            if(!pathFrom){
              this.getHistory(modelId, version);
            }else{
              this.getExampleDrawingToken(modelId, version);
            }
        }
        //分享页面
        if(shareCode){
          return this.getShareDatas(shareCode);
        }

      }

      //获取历史版本
      this.getHistory = (modelId, version)=>{
        getHistoryModels(modelId).then(res =>{
          if(res && res.code === 200){
            let records = (res.data.histories || []).map(i => Object.assign(i,{
              name: res.data.name,
              _id: res.data._id,
            }))
            this.setState({
              links: [{modelId: modelId, version: version}],
              config: { serverIp: modelConfig.serverIp, token: window.localStorage.getItem('token') },
              modelName: res.data.name,
              histories: records,
            })
          }
        })
      };

      //获取分享模型数据
      this.getShareDatas = (code) =>{
        getShareDrawingData(code).then(res =>{
          if(res && res.code === 200){
             let models = res && res.data && res.data.modelList;
             if(models){
               this.setState({
                 isShare: true,
                 links: models,
                 modelName: models[0] && `${models[0].modelName} V${models[0].version ||1 }.0`,
                 config: { serverIp: modelConfig.serverIp ,token: res.data.sharedToken}
                })
             }
          }
        })
      };

       //获取示例模型 token
       this.getExampleDrawingToken = (modelId, version) =>{
        getExampleToken().then(res =>{
            if(res && res.code === 200){
              let config = { headers: {'x-access-token': res.data.token} };
              getHistories(modelId,config).then(result =>{
                if(result && result.code === 200){
                  let records = (result.data.histories || []).map(i => Object.assign(i,{
                    name: result.data.name,
                    _id: result.data._id,
                   }))
                   this.setState({
                    modelName: result.data.name,
                    histories: records,
                    links:[{ modelId: modelId, version }],
                    config:{ serverIp: modelConfig.serverIp, token: res.data.token}
                  })
                }
              }).catch(err => message.error(err.message));
            }
        }).catch(err => message.error(err.message));
      };


      this.getBIMToken = (token)=>{
        const { config } = this.state;
        let headers = { headers: {'x-access-token': token}};
        convertToken(headers).then(res =>{
          if(res && res.code === 200){
            this.setState({
              config: Object.assign(config,{ token: res.data })
            })
          }
        });
      };

      this.initUpdateConfig =(models) =>{
        this.setState({ links: models })
      };

      this.switchState = (type, value) => {
        this.setState({[type]: value})
      };
    
  }


  componentDidMount() {
    this.init();
  };

  render() {
    let {links, config} = this.state;
    
    return (
      <div className="bimDrawing">
        <ModelHeader name={this.state.modelName}
                     histories={this.state.histories}
                     updateConfig={this.initUpdateConfig}
                     currentModelId={ getURLValueByKey('modelId')}
                     currentVersion={ getURLValueByKey('version')}
        />
       <div className="bimDrawing-content">
          <DrawingView links={links} 
                       config={config}
          />
       </div>
      </div>)
  }

}

export default BimDrawing;