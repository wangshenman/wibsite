import React from 'react';
import {Icon, message} from "antd";
import { compact } from '../../../../../common/utils/Utility';
import {ContextMenu, MenuItem, ContextMenuTrigger, SubMenu} from "react-contextmenu";

class ContextMenuBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            entities:[]
        };
    }

    componentDidMount () {
    }

    componentWillReceiveProps(next) {
       
    }

    render () {
        let { mouse, app, isSelect} = this.props;
        let { entities } = this.state;
        let self = this;
        let contextmenuData = {
        "none": [
            {
            title: '显示全部构件',
            action: () => () => app && app.control().showAllEntities()
            },
        ],
        "single": [
            {
            title: '查看构件属性',
            action: () => this.setState({ propertyVisible:false, showLeftLayer: true, showLeftLayerTab: 'model',currentTab:2 })

            },
            {
            title: '显示全部',
            action: () => () => app && app.control().showAllEntities()

            },
            {
            title: '隐藏选中',
            action:() => app && app.control().hideSelectEntities()

            },
            {
            title: '隔离选中',
            action: () => app && app.control().hideUnselectEntities()

            },
            {
            title: '透明选中',
            action: () => app && app.control().transparentSelectEntities()

            },
            {
            title: '透明未选中',
            action: () => app && app.control().transparentUnselectEntities()
            },
            {
            title: '创建构建集',
            action: () => app && app.data().createComponentSet()
            }

        ],
        "multi": [
            {
            title: '查看构件属性',
            action: () => this.setState({ propertyVisible:false,showLeftLayer: true, showLeftLayerTab: 'model',currentTab:2 })

            },
            {
            title: '显示全部',
            action: () => () => app && app.control().showAllEntities()

            },
            {
            title: '隐藏选中',
            action:() => app && app.control().hideSelectEntities()

            },
            {
            title: '隔离选中',
            action: () => app && app.control().hideUnselectEntities()

            },
            {
            title: '透明选中',
            action: () => app && app.control().transparentSelectEntities()

            },
            {
            title: '透明未选中',
            action: () => app && app.control().transparentUnselectEntities()

            },
        ],
        };
        
        // 右键菜单点击事件
        const menuClick = (action) => {
            let x_entities = compact(entities).map(entity => {
                return {
                'uuid': entity,
                'model': self.getEntityModelId(entity)
                };
            });
            if (entities.length) {
                if (compact(entities).length) {
                action(x_entities);
                } else {
                    message.destroy();
                    message.warning('属性数据请求中，请稍等...')
                }
            } else {
                action(x_entities);
            }
        };

        // 构建右键菜单
        let createMenuItemFn = (options) => {
        return (
            options.map((option, index) => {
                return option.children ? option.children.length ? <div onMouseOver= {this.onHover} key={index}>
                    <SubMenu title={<span title={option['title']}>{option['title'].length > 16 ? `${option['title'].slice(0, 16)}...` : option['title']}<Icon type="right" /></span>}>
                        <div className={'wrapper'}>
                            <div className={'content scroll'}>
                                {createMenuItemFn(option.children)}
                            </div>
                        </div>
                    </SubMenu></div>: null : <MenuItem key={index}
                                                        onClick={() => {
                                                            menuClick(option['action'])
                                                        }}>
                    <span title={option['title']}>{option['title'].length > 16 ? `${option['title'].slice(0, 16)}...` : option['title']}</span>
                </MenuItem>
            })
        )
        };

        // 定义右键菜单
        let rightOperation = [];
        let addOption = (options) => {
            rightOperation.push(createMenuItemFn(options));
        };
        if (contextmenuData) {
            let noneOption = (contextmenuData || {})['none'] || [];
            let singleOption = (contextmenuData || {})['single'] || [];
            let multiOption = (contextmenuData || {})['multi'] || [];
            if (isSelect) {
            addOption(singleOption); 
            }else{
            addOption(noneOption);
            }
        }
        return (
              !(mouse && mouse === 'drag') &&
                <ContextMenu id="some_unique_identifier">
                    {rightOperation}
                </ContextMenu>
        )
    }
}


export default ContextMenuBox;