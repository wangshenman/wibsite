/* 底部菜单 */
import React from 'react';
import { Icon, Tooltip, Menu, Dropdown} from 'antd';
import {  menuBlock, measureBlock } from '../../config/menuConfig';
import './toolbar.less'

class Toolbar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            homeHover: false,
        }
    }

    render() {
        const { click, mouseHover, handleMeasure} = this.props;
        
        //设置点击显示相应的下拉菜单
        const setSubMenu = (key) => {
            switch(key){
                case 'measure':
                    return  <SubMenuLayout menuBlock={measureBlock} click={handleMeasure} mouseHover={mouseHover} type="measure"/>
                default:
                    break;

            }

        };

        let toolItem = (item) => {
            return(
                <span className='tool-button'
                      onClick={() => click(item.selected)}>
                    <Icon className={`${mouseHover[item.selected] && !item.noPickMark ? 'btn-selected' : ''}`} component={item.icon}/>
                </span>
            );

        }

        return (
            <div className="toolbar" id="menuWrapper">
                {
                    menuBlock.map((item, index) => {
                        return (
                            <span key={index}>
                            {
                                !item.isSubMenu ?
                                <Tooltip key={index} 
                                    title={item.title} 
                                    placement="top" 
                                    getPopupContainer={() => document.getElementById('menuWrapper')}
                                >
                                  { toolItem(item) }
                                </Tooltip>
                                :
                                <Dropdown overlay={() => setSubMenu(item.selected)} 
                                          trigger={['hover']}  
                                          placement={'topCenter'} 
                                          getPopupContainer= {()=>document.getElementById('menuWrapper')}>
                                   { toolItem(item) } 
                                </Dropdown>
                            }
                        </span>)
                    })
                }
            </div>)
    }
}

function SubMenuLayout(props) {
    const { menuBlock, click, mouseHover, type} = props;
    return (<Menu>
        {
            menuBlock.map((item, index) => {
                return (
                    <Menu.Item key={item.selected} onClick={() => mouseHover[type] && click(item.selected)}>
                        <span className='subMenu sectionMenu flex-between'>
                           <Icon className={`subMenu-img ${mouseHover[item.selected] ? 'selected' : ''}`} component={item.icon}/>
                            <span className={`subMenu-text ${mouseHover[item.selected] ? 'selected' : ''}`}>{item.title}</span>
                        </span>
                    </Menu.Item>

                )
            })
        }
    </Menu>)
}


export default Toolbar;

