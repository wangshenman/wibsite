import{
    FullScreenIcon, MainViewIcon, MeasureIcon, MeaViewIcon, MeaCancelIcon, MeaEndIcon,
    MeaPointIcon, MeaDistanceIcon, MeaLengthIcon, MeaAngleIcon, MeaAeraIcon, MeaSettingIcon,
} from '../resource/svgIcon';

export const measureTypes = ['dot','angle','area'];

export const menuBlock = [
    {
        title: '主视角',
        selected: 'mainView',
        isSubMenu: false,
        noPickMark: true,
        type:'mainView',
        icon: MainViewIcon,
    },
    {
        title: '测量',
        selected: 'measure',
        isSubMenu: true,
        noPickMark: false,
        type:'measure',
        icon: MeasureIcon,
    },
    {
        title: '全屏',
        selected: 'fullscreen',
        isSubMenu: false,
        noPickMark: true,
        type: 'fullscreen',
        icon: FullScreenIcon,
    }
];

export const measureBlock = [
    {
        title: '点到点',
        selected: 'dot',
        icon: MeaPointIcon,
    },
    {
        title: '角度',
        selected: 'angle',
        icon: MeaAngleIcon,
    },
    {
        title: '面积',
        selected: 'area',
        icon: MeaAeraIcon,
    },
   
];

export const measureOperate=[
    {
        title:'观察',
        selected:'view',
        icon: MeaViewIcon,
    },
    {
        title: '取消',
        selected: 'cancel',
        icon: MeaCancelIcon,
    },
    {
        title: '结束',
        selected: 'end',
        icon: MeaEndIcon,
    },
];


