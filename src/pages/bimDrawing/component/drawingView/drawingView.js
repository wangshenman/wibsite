import React from 'react'; 
import { message,  } from 'antd';
import './drawingView.less';
import Toolbar from './componment/toolbar/toolbar';
import Loading from '../../../../common/component/Loading';
// import ModelMeasure from './componment/modelMeasure/modelMeasure';
import {measureOperate, measureTypes} from './config/menuConfig';

const DrawingWIND = window.WIND.WIND;
const WINDDrawingApp = DrawingWIND.DRAWING.APP;
const BackgroundType = DrawingWIND.DRAWING.BackgroundType;
// const CallbackType = DrawingWIND.DRAWING.CallbackType;
const MeasureType = DrawingWIND.DRAWING.MeasureType;
const MeasureUnit = DrawingWIND.UnitType;
const CallbackType = DrawingWIND.CallbackType;

class DrawingView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      links:[],
      config:{},
      loading: false,
      percent: 0,
      mouse: undefined,
      currentApp: null,
      measureVisible: false,//测量(类型)显隐
      measureHintVisible: false,//测量面板操作提示
      isFullScreen: false,//是否全屏
      mouseHover: {},
      measureValueObject: null,
    
    }
    this.loadModel = this.loadModel.bind(this);
    this.updateDataNode = (type, value) => {
      //数据回调之后, 更新进度条
      switch (type) {
        //加载中
        case CallbackType.DATA_LOAD_CHANGE:
            // 模型加载百分比回调
            let percentNum = Math.min(100, Math.max(0, Math.floor(value.percent))) || 0;
            if (percentNum >= 0 && percentNum < 100) {
                this.setState({ 
                  loading: true,
                  percent: percentNum 
                });
            } 
            break;
        //加载结束  
        case CallbackType.DATA_LOAD_FINISH:
          this.setState({ loading: false });
          break;
        //加载成功
        case CallbackType.DATA_LOAD_SUCCEED:
            console.log('模型加载成功：',value);
            break;
        //加载失败
        case CallbackType.DATA_LOAD_FAIL:
            console.log('模型加载失败：',value);
            return message.error(`模型${value && value.modelId}加载失败`)
        // case CallbackType.MEASURE_NODE_CREATE:
        //     this.setState({ measureValueObject: value});
        //     break;
        // case CallbackType.MEASURE_CONFIG_CHANGE:
        //     this.setState({ measureValueObject: value});
        //     break;
        // case CallbackType.MEASURE_NODE_SELECT:
        //     this.setState({ measureValueObject: value});
        //     break;
        // case CallbackType.MEASURE_NODE_CHANGE:
        //       this.setState({ measureValueObject: value});
        //       break;
        // case CallbackType.MEASURE_NODE_UPDATE:
        //     this.setState({ measureValueObject: value});
        //     break;
        // case CallbackType.MEASURE_NODE_CLEAR:
        //     this.setState({ measureValueObject: value});
        //     break;
        // case CallbackType.MEASURE_NODE_CANCEL:
        //     this.setState({ measureValueObject: value});
        //     break;
        default:
          break;
      }
    };
    
  }

  componentWillMount() {
    if (this.props.onRef) {
        this.props.onRef(this);
    }
  };

  componentDidMount() {
    const {links, config} = this.props;
    this.setState({ links,config},()=>this.loadModel())
  };

  componentWillReceiveProps(nextProps){
    const {config, links} = this.props;
    if(((nextProps.config && nextProps.config.token) !== (config && config.token)) || (nextProps.links !== links)){
      this.setState({
        links: nextProps.links,
        config: nextProps.config,
      },()=>this.loadModel())
    }
  };

  componentWillUnmount() {
    const {currentApp} =this.state;
    if (currentApp) {
        currentApp.data().closeAllDrawingDatas();
    }
  };

  //加载模型
  async loadModel() {
    const { currentApp, config} = this.state;
    if (currentApp) {
      currentApp.data().closeAllDrawingDatas();
    }
    if(config && config.token){
      this.setState({ loading: true})
       //WINDView初始化
      let canvas = document.querySelector('#drawingCanvas');
      let drawingApp = new WINDDrawingApp(canvas);

      await drawingApp.connect(config);

      //模型渲染
      drawingApp.startFrame();
      this.setState({
        currentApp: drawingApp,
      });

      this.openModelData(drawingApp);
      this.bindEvent(this);
    }
  };

  //打开模型
  async openModelData(drawingApp) {
    const { links } = this.state;
    if (links && links.length) {
      let isOpenDrawingSuccess = await drawingApp.data().openDrawingData(links[0].modelId, links[0].version);//打开图纸数据 
      if(isOpenDrawingSuccess){
        this.setState({ loading: false })
      }else{
        this.setState({ loading: false})
        return message.error('图纸打开失败');
      }
    }
  };
  // 模型事件处理
  bindEvent = (context) =>{
    let canvasDiv = document.querySelector('#modelView');
    if (canvasDiv) {
        let startTime;
        let endTime;
        // 右键菜单
        canvasDiv.addEventListener('contextmenu', e => {
            e.preventDefault();
            endTime = Date.now();
            context.selectJuging();
        });

        canvasDiv.addEventListener('click', function (e) {
            context.selectJuging();
        }, false);

        canvasDiv.addEventListener('mousedown', e => {
            startTime = Date.now();
            if (e.button === 2) {
            }

        });
        // 苹果系统快速点击右键，不会触发mouseup
        canvasDiv.addEventListener('mouseup', e => {
            endTime = Date.now();
            if (e.button === 2) {
                if (endTime - startTime > 130) {
                    context.setState({
                        mouse: 'drag'
                    })
                } else {
                    context.setState({
                        mouse: undefined
                    })
                }
            } 
        });
    }
  };

  switchState = (type, value) => {
    this.setState({[type]: value})
  };

  //返回主视角(复位相机)
  mainView = () => {
    const { currentApp } = this.state;
    currentApp.action().homeRoaming();
  };

  //开启关闭测量
  switchMeasure = () => {
    let { currentApp, measureVisible, mouseHover } = this.state;
    let measureState = currentApp.create().getMeasureConfig();//获取测量状态
    if (measureState.opened) {
      currentApp.create().closeMeasure();
      currentApp.create().removeCallback(210);
      let measures = {};
      measureTypes.map(item => {
        measures = Object.assign(mouseHover, { [item]: false })
      })
      this.setState({
         mouseHover: measures
      })
    } else {
      currentApp.create().openMeasure();
      currentApp.create().setMeasureType(MeasureType.DOT);
      currentApp.create().addCallback(210, (type, option) => this.updateDataNode(type, option));
      this.setState({
        mouseHover: Object.assign(mouseHover, { ['dot']: true }),
      })
    }
    this.setState({
      mouseHover: Object.assign(this.state.mouseHover, { ['measure']: !measureVisible }),
      measureVisible: !measureVisible,
    });
  };

  //选择测量
  onClickMeasure = (key) => {
    let { currentApp, mouseHover} = this.state;
    this.setState({
      mouseHover: Object.assign(mouseHover, { [key]: !mouseHover[key] })
    })
    let measures = {}
    measureTypes.filter(item => {
      if (item !== key) {
        measures = Object.assign(mouseHover, { [item]: false })
      }
    })
    this.setState({
      mouseHover: measures,
    })
    
    let MState = currentApp.create().getMeasureConfig();//获取测量状态
    if (!MState.opened) {
      return currentApp.create().openMeasure();
    } else {
      switch (key) {
        case 'dot':
          currentApp.create().setMeasureType(MeasureType.DOT);
          break;
        case 'angle':
          currentApp.create().setMeasureType(MeasureType.ANGLE);
          break;
        case 'area':
          currentApp.create().setMeasureType(MeasureType.AREA);
          break;
        default:
          break;
      }
    }
  };

  //切换全屏
  switchFullScreen = () => {
  const { currentApp, isFullScreen,mouseHover } = this.state;
  let element = document.getElementById("drawingView-wrapper");
  if (!isFullScreen) {
    currentApp.screenFull(element);
    this.switchState('mouseHover', Object.assign(mouseHover, { ['fullScreen']: true}));
  } else {
     currentApp.screenFull(element);
     this.switchState('mouseHover', Object.assign(mouseHover, { ['fullScreen']: false}));
  }
  };

  //toolbar点击事件
  selectMenu = (funcName) => {
    let { mouseHover, measureVisible } = this.state;
    this.setState({
       mouseHover: Object.assign(mouseHover, { [funcName]: !mouseHover[funcName] })
    });
    switch (funcName) {
      case 'mainView':
        this.mainView();
        break;
      case 'measure':
        this.switchMeasure();
        break;
      case 'fullscreen':
        this.switchFullScreen();
        break;
      default:
        this.mainView()
        break;
    }
  }

  render() {
    let {currentApp,loading, percent, measureVisible, mouseHover,measureValueObject } = this.state;
    
    return (
      <div className="drawingView-wrapper" id="drawingView-wrapper">
           { loading && <Loading/> }
            <div className="drawingView" id='drawingView'>
              {/* { loading ?  (<div className="modelView-loading-process"><Progress type="circle" percent={percent}/></div> ): ''} */}
              <canvas id='drawingCanvas' style={{ position: 'absolute', width: '100%', height: '100%' }}></canvas>
            </div>
          <div className="modelView-menu">
              <Toolbar click={ this.selectMenu } 
                       mouseHover={ mouseHover } 
                       handleMeasure={ this.onClickMeasure }

              />
          </div>
         {/* <div className="side-rightBox">
            {
              //测量
              measureVisible  
              && <ModelMeasure currentApp={currentApp}
                               measureVisible={measureVisible}
                               mouseHover={mouseHover}
                               MeasureType={MeasureType}
                               MeasureUnit={MeasureUnit}
                               switchState={this.switchState}
                               measureValueObject={measureValueObject}
                               CallbackType={CallbackType}
                />
            }
            
         </div> */}
        </div>
    )
  }

}

export default DrawingView;