import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Select,} from 'antd';
import navLogo from '../../../../common/img/common/developer_logo.png';
import './header.less';

const { Option } = Select;

class ModelHeader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentVersion: 1,
    }

    this.init = () =>{
      const {currentVersion} = this.props;
      this.setState({
        currentVersion,
      })

    }
    this.handleChangeVersion = (value) =>{
        const { currentVersion } = this.state;
        const { currentModelId } = this.props;
        if( parseInt(currentVersion) !== parseInt(value)){
           this.props.updateConfig([{ modelId: currentModelId, version: value }])
        }
        this.setState({
          currentVersion: value,
        })
    }

  }
  componentDidMount() {
      this.init()
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.currentVersion !== this.props.currentVersion){
      this.init()
    }
  }

  render() {
    const {currentVersion} = this.state;
    const {histories} = this.props;
    return (
        <div className="model-header">
            <div className="model-wrapper">
              <div className="model-front">
                  <img src={navLogo} alt="中间件官网Logo" className="model-logo" />
                  <span className="model-name">{this.props.name}</span>
              </div>
              <Select style={{width: 100}} 
                      className="model-select"
                      value={`V${currentVersion}.0`}
                      onChange={value => this.handleChangeVersion(value)}>
                      {
                        (histories || []).map((i,index) =>{
                        return( <Option value={i.version} key={index}>{`V${i.version}.0`}</Option>)
                        })
                      }
              </Select>
            </div>
        </div>)
  }
}



export default withRouter(ModelHeader);

