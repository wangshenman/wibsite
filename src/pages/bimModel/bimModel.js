import React from 'react'; 
import { message } from 'antd';
import './bimModel.less';
import ModelHeader from './component/modelHeader/ModelHeader';
import ModelView from './component/modelView/modelView';
import { getURLValueByKey } from '../../common/utils/Utility';
import { getExampleToken, convertToken } from '../../common/utils/commonActions';
import { modelConfig } from '../../common/config/websiteConfig';
import { modelArray } from '../../common/config/homeJson';
import { getHistoryModels, getShareModelData, getUserData, getCombineModel } from '../../developer/console/module/bimModelList/bimModelList.actions';

class BimModel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modelName:'', //模型名称
      histories:[], //历史版本
      pathFrom: '',
      links: [], //模型信息
      config: {},
      isShare: false, //是否为分享
      token: null, //token
    }
      this.init = () =>{
        //获取页面跳转来源
        let pathFrom = getURLValueByKey('from');
        let shareCode = getURLValueByKey('shareCode');
        let key = getURLValueByKey('key');
        let modelId = getURLValueByKey('modelId');
        let version = parseInt(getURLValueByKey('version'));
        //示例模型
        if(pathFrom && modelId){
          this.setState({ pathFrom });
          return this.getExampleModelToken();
        }
        //分享页面
        if(shareCode){
          return this.getShareDatas(shareCode);
        }
        //打开批量模型
        if(key){
          return this.getBatchDatas(key);
        }
        //打开单个模型
        if(modelId && version){
          this.getHistory(modelId, version);
        }
      };

      //获取历史版本
      this.getHistory = (modelId, version)=>{
        getHistoryModels(modelId).then(res =>{
          if(res && res.code === 200){
            let records = (res.data.histories || []).map(i => Object.assign(i,{
              name: res.data.name,
              _id: res.data._id,
            }))
            this.setState({
              links:[{ modelId: modelId, version: version }],
              config:{ serverIp: modelConfig.serverIp,token: window.localStorage.getItem('token') },
              modelName: res.data.name,
              histories: records,
            })
          }
        })
      };

      //获取分享模型数据
      this.getShareDatas = (code) =>{
        getShareModelData(code).then(res =>{
          if(res && res.code === 200){
             let models = res && res.data && res.data.modelList;
             if(models){
               this.setState({
                 isShare: true,
                 token: res.data.sharedToken,
                 links: models,
                 modelName: models[0] && `${models[0].modelName} V${models[0].version ||1 }.0`,
                 config:{ serverIp: modelConfig.serverIp, token: res.data.sharedToken}
               })
             }
          }
        })
      };

      //获取批量打开模型数据
      this.getBatchDatas = (key) =>{
        getUserData(key).then(res =>{
          if(res.code === 200){
            if(res.data){
              let models = JSON.parse(res.data) || [];
              this.setState({
                links: models,
                modelName: '组合模型',
                config: { serverIp: modelConfig.serverIp,token: window.localStorage.getItem('token')}
              })
            }else{
              message.warning('访问链接已失效');
            }
          }
        }).catch(error => console.log(error));
      };

      //获取示例模型 token
      this.getExampleModelToken = () =>{
        getExampleToken().then(res =>{
            if(res && res.code === 200){
              this.getCombines(res.data.token)
            }
        }).catch(err => message.error(err.message));
      };

      //获取 bim-token
      this.getBIMToken = (token) =>{
        const { config } = this.state;
        let headers = { headers: {'x-access-token': token}};
        convertToken(headers).then(res =>{
          if(res && res.code === 200){
            this.setState({
              config: Object.assign(config,{ token: res.data })
            })
          }
        });
      };

      this.getCombines = (token) =>{
        let combineId = getURLValueByKey('modelId')
        let headers = { headers: {'x-access-token': token}};
        getCombineModel(combineId, headers).then(res =>{
          if(res && res.code === 200){
              let records = res.data.models;
              let currentModel = modelArray.find(i=> i.modelId === combineId);
              this.setState({
                links: records,
                modelName: currentModel && currentModel.title || '示例模型',
                config:{ serverIp: modelConfig.serverIp, token: token}
              })
          }
        });

      }

      this.initUpdateConfig =(models) =>{
        this.setState({ links: models })
      };

      this.switchState = (type, value) =>{
        this.setState({[type]: value})
      };
    
  }


  componentDidMount () {
    this.init();
  };

  render () {
    let {links, config} = this.state;
    return (
      <div className="bimModel">
        <ModelHeader name={this.state.modelName}
                     histories={this.state.histories}
                     updateConfig={this.initUpdateConfig}
                     isBatch={ getURLValueByKey('key') || getURLValueByKey('from') || getURLValueByKey('shareCode')}
                     currentModelId={ getURLValueByKey('modelId')}
                     currentVersion={ getURLValueByKey('version')}
        />
       <div className="bimModel-content">
          <ModelView links={links} 
                     config={config}
          />
       </div>
       
      </div>)
  }

}

export default BimModel;