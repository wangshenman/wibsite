/* 右键菜单功能组件 */
import React from 'react';
import {Icon, message} from "antd";
// import _ from "lodash";
import {ContextMenu, MenuItem, SubMenu} from "react-contextmenu";
import {FLOOR_TREE, DOMAIN_TREE, SYSTEM_TREE} from './controlTree/controlTree';

class ContextMenuItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.setPushpinStyle = (key)=>{
            this.props.handleCreatePushpin(key);
        }
    }

    render () {
        let { mouse, isControl, app, handleAttribute, selectCount, isPushpin} = this.props;
        let self = this;
        let contextmenuData = {
        "none":[
            {
                title: '显示全部',
                action: () => {
                    app && app.control().showAllComponents();
                    app && app.control().showComponentTree(FLOOR_TREE) 
                    app && app.control().showComponentTree(DOMAIN_TREE) 
                    app && app.control().showComponentTree(SYSTEM_TREE) 
                }
            },
        ],
        "single": [
            {
                title: '查看构件属性',
                action: () => handleAttribute()
            },
            {
                title: '显示全部',
                action: () => {
                    app && app.control().showAllComponents();
                    app && app.control().showComponentTree(FLOOR_TREE) 
                    app && app.control().showComponentTree(DOMAIN_TREE) 
                    app && app.control().showComponentTree(SYSTEM_TREE) 
                }

            },
            {
                title: '隐藏全部',
                action: () => app && app.control().hideAllComponents()

            },
            {
                title: '隐藏选中',
                action:() => app && app.control().hideSelectComponents()
                
            },
            {
                title: '隔离选中',
                action: () => app && app.control().hideUnselectComponents()

            },
            {
                title: '透明选中',
                action: () => app && app.control().freezeSelectComponents()

            },
            {
                title: '透明未选中',
                action: () => app && app.control().freezeUnselectComponents()
            },
        ],
        "multi": [
            {
                title: '显示全部',
                action: () => {
                    app && app.control().showAllComponents();
                    app && app.control().showComponentTree(FLOOR_TREE) 
                    app && app.control().showComponentTree(DOMAIN_TREE) 
                    app && app.control().showComponentTree(SYSTEM_TREE) 
                }

            },
            {
                title: '隐藏全部',
                action: () => app && app.control().hideAllComponents()

            },
            {
                title: '隐藏选中',
                action:() => app && app.control().hideSelectComponents()

            },
            {
                title: '隔离选中',
                action: () => app && app.control().hideUnselectComponents()

            },
            {
                title: '透明选中',
                action: () => app && app.control().freezeSelectComponents()

            },
            {
                title: '透明未选中',
                action: () => app && app.control().freezeUnselectComponents()
            },
        ],
        "pushpin":[
            {
                title: '创建图钉',
                // action: () => app && app.create().createPushpin(),
                children:[
                    {
                        title: '样式一',
                        action: () => this.setPushpinStyle(1),
                    },
                    {
                        title: '样式二',
                        action: () => this.setPushpinStyle(2),
                    },
                    {
                        title: '样式三',
                        action: () => this.setPushpinStyle(3),
                    },
                ]
            },
         ]
        };
        
        // 右键菜单点击事件
        const menuClick = (action) => {
            action();
        };

        // 构建右键菜单
        let createMenuItemFn = (options) => {
            return (
                options.map((option, index) => {
                    return option.children ? option.children.length ? <div key={index}>
                        <SubMenu title={<span title={option['title']}>{option['title'].length > 16 ? `${option['title'].slice(0, 16)}...` : option['title']}<Icon type="right" /></span>}>
                            <div className={'wrapper'}>
                                <div className={'content scroll'}>
                                    {createMenuItemFn(option.children)}
                                </div>
                            </div>
                        </SubMenu></div>: null : <MenuItem key={index} onClick={() => {menuClick(option['action'])}}>
                        <span title={option['title']}>{option['title'].length > 16 ? `${option['title'].slice(0, 16)}...` : option['title']}</span>
                    </MenuItem>
                })
            )
        };

        // 定义右键菜单
        let rightOperation = [];
        let addOption = (options) => {
            rightOperation.push(createMenuItemFn(options));
        };
       
        if (contextmenuData) {
            let noneOption = (contextmenuData || {})['none'] || [];
            let singleOption = (contextmenuData || {})['single'] || [];
            let multiOption = (contextmenuData || {})['multi'] || [];
            let pushpinOption = (contextmenuData || {})['pushpin']||[];
            if (selectCount>1) {
                addOption(multiOption);
                if(isPushpin){
                    addOption(pushpinOption);
                }
            }else if(selectCount === 1){
                addOption(singleOption); 
                if(isPushpin){
                    addOption(pushpinOption);
                }
            }else{
                addOption(noneOption); 
            }
        }
        return (
              !(mouse && mouse === 'drag') && !isControl &&
                <ContextMenu id="some_unique_identifier">
                    {rightOperation}
                </ContextMenu>
        )
    }
}


export default ContextMenuItem;