/* 构件属性展示组件 */
import React from 'react';
import { Drawer, Row, Col, Collapse, message } from 'antd';
import Empty from '../../../../../../common/component/Empty';
import './attributeTab.less';

const { Panel } = Collapse;

class AttributeTab extends React.Component {
    constructor(props) {
        super(props)
        
        this.parmsTab = [
            { tab: "基础属性", sub: "basicProps", key: '1'},
            { tab: "资料", sub: "material", key: '2' },
            { tab: "业务相关", sub: "profession", key: '3'},
        ];
        this.state = {
            componentParms: null,
            propertyParams:[]
        }
    }
    componentDidMount() {
      const { componentParms, hasDrawer } = this.props;
      if(componentParms){
        this.getAllRelationParamter(componentParms);
        this.setState({ hasDrawer })
      }
        
    }

    componentWillReceiveProps(nextProps){
        const { componentParms } = nextProps;
        if(this.props.componentParms !== componentParms){
            this.getAllRelationParamter(componentParms)
        }
    }
    
    //获取构件属性数据
    async getAllRelationParamter(componentParms) {
        let propertyParams = null;
        if(componentParms && componentParms.properties){
            propertyParams = this.handleParamsGroup(componentParms.properties);
        }
        this.setState({
            propertyParams
        })
    };

  //根据group字段将数据进行重新分组
  handleParamsGroup(paramsList) {
    let paramsMap = {};
    let destArray = [];
    for (let i = 0; i < paramsList.length; i++) {
      let params = paramsList[i];
      if (!paramsMap[params.group]) {
        destArray.push({
          key: params.group,
          data: [params]
        });
        paramsMap[params.group] = params;
      } else {
        for (let j = 0; j < destArray.length; j++) {
          let dest = destArray[j];
          if (dest.key === params.group) {
            dest.data.push(params);
            break;
          }
        }
      }
    }
    return destArray;
  }
    //基础属性内容
    basicPropsTab = (componentParms) =>{
        const { propertyParams} = this.state;
        return(
            <div className="tabContainer">
                <div className="list-top-item">
                   <Row>
                     <Col span={6}>名称：</Col>
                     <Col span={18}>{componentParms && componentParms.name}</Col>
                   </Row>
                </div>
                <div className="list-top-item">
                   <Row>
                     <Col span={6}>构件ID：</Col>
                     <Col span={18}>{componentParms && componentParms.entityId}</Col>
                   </Row>
                </div>
                <div className="list-top-item">
                   <Row>
                     <Col span={6}>楼层：</Col>
                     <Col span={18}>{componentParms && componentParms.floor}</Col>
                   </Row>
                </div>
                <div className="list-top-item">
                   <Row>
                     <Col span={6}>专业：</Col>
                     <Col span={18}>{componentParms && componentParms.domainStr}</Col>
                   </Row>
                </div>
                <div className="list-top-item">
                   <Row>
                     <Col span={6}>类型：</Col>
                     <Col span={18}>{componentParms && componentParms.categoryStr}</Col>
                   </Row>
                </div>
                <div className="group-context">
                    <Collapse >
                        {
                            propertyParams && propertyParams.map((item,index) =>{
                                return( <Panel header={item.key} key={item.key}>
                                    <ul>
                                    {
                                       item.data && item.data.map((i,index)=>{
                                           return( <li className="parmShow" key={index}><span className="parmKey">{`${i.key}:`}</span><span className="parmValue">{ i.value ? i.value : '暂无数据' }</span></li>)
                                       })
                                       
                                    }
                                    </ul></Panel>)
                            })

                        }
                    </Collapse>

                </div>
            
            </div>
        )
    }

    render() {
        const { onCancel, componentParms } = this.props;
        const { hasDrawer } = this.state;
        console.log('hasDrawer',hasDrawer);
        return (
            <div className={`attributeTabWrapper ${hasDrawer ? 'maxIndex' : 'minIndex'}`} id="attributeTabWrapper">
              <Drawer className={'attributeTab'}
                      title={'构件属性'}
                      placement='right'
                      visible={true}
                      mask={false}
                      width={300}
                      zIndex={hasDrawer ? 1000 : 99}
                      maskClosable={false}
                      onClose={onCancel}
                      getContainer={document.getElementById("attributeTabWrapper")}
              >
                <div className="attributeTab-container">
                  {
                     componentParms ?  this.basicPropsTab(componentParms) : <Empty message={`暂无数据，请选择构件~`}/>
                  }
                </div>
              </Drawer>
                
            </div>)
    }

}
export default AttributeTab;