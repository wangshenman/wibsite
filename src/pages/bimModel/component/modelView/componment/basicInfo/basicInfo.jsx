import React from 'react';
import {  Modal, Row, Col } from 'antd';
import './basicInfo.less';

class BasicInfo extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            modelStats: null
        }
        //获取 props 
    };

    createBasicInfo = async() =>{
        const {app} = this.props;
        if(app){
            let modelStats = await app.data().getModelStats();
            this.setState({ modelStats });
        }
    }
    componentDidMount (){
        // this.createBasicInfo();
      }

    render() {
        // const { modelStats } = this.state;
        const { modelStats, visible,onCancel } = this.props;
        const propsItem = (title, value) =>{
            return(
                   <div className={`list-top-item`} >
                        <Row>
                            <Col span={8} style={{color:'#666'}}>{`${title} ：`}</Col>
                            <Col span={16} style={{color:'#3d81fa'}}>{value}</Col>
                        </Row>
                    </div>
            )
        };
       
        return (
            <div className="basicInfo">
                <Modal  title="基础信息"
                        okText={'确定'}
                        cancelText={'关闭'}
                        visible = {visible}
                        width={380}
                        centered
                        onCancel={onCancel}
                        footer={null}>
                         <div className="basicInfoWrapper">
                            { propsItem('构件数量',modelStats && modelStats.componentCount) }
                            { propsItem('三角面数量', modelStats && modelStats.faceCount) }
                            { propsItem('顶点数量', modelStats && modelStats.vertexCount) }
                            { propsItem('线段数量', modelStats && modelStats.wireCount) }
                            { propsItem('边线数量', modelStats && modelStats.edgeCount) }
                        </div> 
                </Modal>
            </div>)
    }

}
export default BasicInfo;