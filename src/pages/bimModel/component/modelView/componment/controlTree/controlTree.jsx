/* 构件树组件 */
import React from 'react';
import { Tree, Tabs } from 'antd';
import Empty from '../../../../../../common/component/Empty';
import './controlTree.less'

const { TabPane } = Tabs;
const { TreeNode } = Tree;

//构件生成树 类型
export const FLOOR_TREE = 'FLOOR_TREE';
export const DOMAIN_TREE = 'DOMAIN_TREE';
export const SYSTEM_TREE = 'SYSTEM_TREE';

const ControlTreeCallback = 'CONTROLTREE_CALLBACK';

class ControlTree extends React.Component {
  constructor(props) {
      super(props)
      this.state = {
        floors:  null,
        domains: null,
        systems: null,
        activeKey: "FLOOR_TREE",
        checkedKeys: [],
        selectedKeys: [],
        selectedFloors: null,
        selectedDomains: null,
        selectedSystems: null
      }
      // 图形回调之后, 更新显示控制树
    this.updateTreeNode = (type, option) => {
      const { CallbackType } = this.props;
      const {activeKey} = this.state; 
      if (type === CallbackType.COMPONENT_TREE_DISPLAY) {
          let treeData = option && option.treeData;
          if (treeData) {
              let nodes = [];
              // 遍历option，更新每个节点的pick状态
              let traverseChildren = function (children) {
                  for (let i = 0; i < children.length; i++) {
                      // 0:不选  1:选择 2:半选 
                      if (children[i].state === 1) {
                          nodes.push(children[i].id.toString());
                      }
                      traverseChildren(children[i].children);
                  }
              };
              traverseChildren(treeData.children);
              if (option.treeId === FLOOR_TREE) {
                  this.setState({ selectedFloors: nodes })
              } else if (option.treeId === DOMAIN_TREE) {
                  this.setState({ selectedDomains: nodes })
              } else if(option.treeId === SYSTEM_TREE) {
                  this.setState({ selectedSystems: nodes })
              }
          }
      } 
    };
  }

  //生成构件树
  createComponentTree = async() =>{
    const {app} = this.props;
    if(app){
        let floorTree, domainTree, systemTree;
        //楼层
        floorTree = await app.data().createComponentTree(['model','floor']);
        floorTree && app.control().createComponentTree(FLOOR_TREE , floorTree);
        this.setState({ floors: floorTree })
        //专业
        domainTree = await app.data().createComponentTree(['domain','category','type']);
        domainTree && app.control().createComponentTree(DOMAIN_TREE , domainTree);
        this.setState({ domains: domainTree })
        //系统
        systemTree = await app.data().createComponentTree(['系统类型']);
        systemTree && app.control().createComponentTree(SYSTEM_TREE , systemTree);
        this.setState({ systems: systemTree })
        //控制树更新回调
        app.control().addCallback(ControlTreeCallback,  (type, option) => this.updateTreeNode(type, option));
    }
  }
  
  componentDidMount (){
    this.createComponentTree();
  }

  componentWillUnmount(){
    const {app} = this.props;
    if(app){
      app.control().clearAllComponentTrees();
      app.control().removeCallback(ControlTreeCallback);
    }
    this.setState = (state,callback)=>{ return; };
  }

  render() {
    const { floors, domains, systems, activeKey, selectedFloors, selectedDomains, selectedSystems } = this.state;
    // 组建树结构
    let renderTreeNodes = (data) => {
      return data.map((item) => {
          if (item.children) {
              return (
                  <TreeNode title={item.name} key={item.id} dataRef={item}>
                      {renderTreeNodes(item.children)}
                  </TreeNode>
              );
          }
          return <TreeNode title={item.name} key={item.id} dataRef={item}/>;
      });
  };

  let onCheck = (checkedKeys,  {checked, checkedNodes, node}) => {
      const {activeKey} = this.state;
      const { app } = this.props;
      let record = node.props.dataRef;
      if (record && record.id) {
        if(checked){
          app.control().displayComponentTree(activeKey, parseInt(record.id));
        }else{
          app.control().displayComponentTree(activeKey, parseInt(record.id));
        }
      }
  }

  let onSelect = (selectedKeys, {checked, checkedNodes, node}) => {
    const {activeKey} = this.state;
    let record = node.props.dataRef;
    const { app } = this.props;
    if (selectedKeys && selectedKeys.length && record && record.id) {
      app.control().highlightComponentTree(activeKey, record.id);
    } else {
      app.control().unhighlightAllComponents();
    }
  };
    
    // 查找选中节点
    let findSelected = (data) => {
      return (data||[]).map((item) => {
          return item.id.toString();
      });
    };
    
    let floorsCheckedKeys =  selectedFloors ? selectedFloors :(floors && floors.children ? findSelected(floors.children) : []);
    let domainsCheckedKeys = selectedDomains ? selectedDomains :(domains && domains.children ? findSelected(domains.children) : []);
    let systemsCheckedKeys = selectedSystems ? selectedSystems :(systems && systems.children ? findSelected(systems.children) : []);
    console.log('floorsCheckedKeys',floorsCheckedKeys);

    return (
      <div className="controlTree scl">
        <div className="card-container">
          <Tabs type="card"  activeKey={activeKey} onChange={(activeKey) => this.setState({activeKey})}>
            <TabPane tab="楼层" key="FLOOR_TREE">
                {
                  floors && floors.children.length ?
                   <Tree  checkable
                          checkedKeys={floorsCheckedKeys}
                          onSelect={onSelect}
                          onCheck={onCheck}>
                          { renderTreeNodes(floors && floors.children) }
                  </Tree>
                  : 
                  <Empty message={`暂无数据`}/>
                }
            </TabPane>
            <TabPane tab="构件" key="DOMAIN_TREE">
                {
                  domains && domains.children.length ?
                  <Tree checkable
                      checkedKeys={domainsCheckedKeys}
                      onSelect={onSelect}
                      onCheck={onCheck}>
                         <TreeNode title={'全部构件'} key={domains.id} dataRef={domains}>
                           { renderTreeNodes(domains && domains.children) }
                         </TreeNode>
                  </Tree>
                    : 
                  <Empty message={`暂无数据`}/>

                }
               
            </TabPane>
            <TabPane tab="系统" key="SYSTEM_TREE">
                {
                  systems && systems.children.length ?
                  <Tree checkable
                      checkedKeys={systemsCheckedKeys}
                      onSelect={onSelect}
                      onCheck={onCheck}>
                         <TreeNode title={'全部系统'} key={systems.id} dataRef={systems}>
                           { renderTreeNodes(systems && systems.children) }
                         </TreeNode>
                  </Tree>
                    : 
                  <Empty message={`暂无数据`}/>
                }
            </TabPane>
          </Tabs>
        </div>
      </div>
    );
  }

}
export default ControlTree;
