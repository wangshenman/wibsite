
 import React from 'react';
 import { Icon, TreeSelect } from "antd";
 import './miniMap.less';
 const MiniMapCallback = 'MINIMAP_CALLBACK';
 class MiniMap extends React.Component {
     constructor(props) {
         super(props);
         this.state = {
             floorTreeData: [], // 楼层树数据
             currentFloor: null, // 选中的楼层
             scaleMaxState: false,
             componentCount: null,
         };

         this.updateDataNode = (type, value) => {
            const { CallbackType } = this.props;
            if(type === CallbackType.MINIMAP_FLOOR_ACTIVE || type === CallbackType.MINIMAP_FLOOR_CHANGE){
                this.setState({
                    currentFloor: value && value.name,
                    componentCount: value && value.componentCount
                })
            }
          };

          this.homeMinimapView = ()=>{
             let {currentApp} = this.props;
             currentApp.minimap().homeMinimap();
          }
 
         this.init = (next) => {
             let {currentApp, miniMapVisible } = next || this.props;
             if (miniMapVisible) {
                let state = currentApp.minimap().getMinimapConfig();
                if (state && state.opened) {
                   currentApp.minimap().closeMinimap();
                   currentApp.minimap().removeCallback(MiniMapCallback);
                } else {
                    currentApp.minimap().configMinimap({
                        backgroundRGBA255: [0, 0, 0, 203]
                     });
                    currentApp.minimap().openMinimap(document.getElementById("miniMapView"));
                    currentApp.minimap().addCallback(MiniMapCallback, (type, option) => this.updateDataNode(type, option));
                }
             }
         };
         this.initFloor = async (next) => {
            let {currentApp, models} = next || this.props;
             // 处理小地图楼层
            let floors = currentApp.data().getFloors(models[0].modelId);
            let floor = floors ? floors[0]: null;
            let floorTreeData = [];
             floors.forEach(d => {
                 let children = [];
                 (d.parameter || []).forEach(_d => {
                     let val = `${_d.name}_${d.modelId}`;
                     children.push({title: _d.name, value: val, key: val})
                 });
                 floorTreeData.push({title: d.name, children: children, value: d.name, key: d.name});
             });
             if (floor) {
                currentApp.minimap().activeMinimap(models[0].modelId);
             }
             this.setState({ floorTreeData,floors});
         };
        
         // 选择楼层
         this.onFloorTreeChange = (currentFloor) => {
             this.setState({
                 currentFloor
             }, () => this.locateMinimapFloor())
         };

         // 定位楼层
         this.locateMinimapFloor = () => {
             let {currentApp} = this.props;
             let {currentFloor} = this.state;
             if (currentFloor) {
                currentApp.minimap().switchMinimap(currentFloor, true);
             } else {
                currentApp.minimap().homeMinimap();
             }
         };
         
         // 小地图缩放
         this.scaleBox = () => {
             let { scaleMaxState } = this.state;
             let { currentApp } = this.props;
             this.setState({
                 scaleMaxState: !scaleMaxState
             },()=> currentApp.minimap().resizeMinimap())
         };

         this.onCloseMinimap = ()=>{
            let { currentApp, mouseHover, switchState } = this.props;
            currentApp.minimap().closeMinimap();
            currentApp.minimap().removeCallback(MiniMapCallback); 
            switchState('mouseHover', Object.assign(mouseHover, {['miniMap']:false}));
            switchState('miniMapVisible', false);
          }
     }
 
     componentDidMount() {
        this.init();
        this.initFloor();
     };
 
     componentWillReceiveProps(nextProps) {
         let {currentApp, miniMapVisible} = nextProps;
         if (miniMapVisible !== this.props.miniMapVisible) {
            if (miniMapVisible) {
                this.init(nextProps);
            } else {
                currentApp.minimap().closeMinimap();
                currentApp.minimap().removeCallback(MiniMapCallback);
            }
        }    
     };

     componentWillUnmount(){
        const {currentApp} =this.props;
        if (currentApp) {
            currentApp.minimap().closeMinimap();
            currentApp.minimap().removeCallback(MiniMapCallback);
        }
        this.setState = (state,callback)=>{
            return;
        };
     }

 
     render() {
         let {floorTreeData, currentFloor, scaleMaxState, componentCount} = this.state;
         return (
             <div className={`${!scaleMaxState ? '' : 'scale-max-box'}`} id={'mini-map-wrapper'}>
                 <div className={`mini-map-wrapper-top`}>
                     <TreeSelect style={{ width: `${!scaleMaxState ? '120px' : '150px'}`}}
                                 value={currentFloor}
                                 dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                                 treeData={floorTreeData}
                                 allowClear = {true}
                                 placeholder="楼层"
                                 getPopupContainer={() => document.getElementById('mini-map-wrapper')}
                                 treeDefaultExpandAll
                                 onChange={this.onFloorTreeChange}/>
                     <span className={'tool-box'}>
                         <Icon type="home" onClick={this.homeMinimapView}></Icon>
                         <Icon type={`${!scaleMaxState ? 'zoom-in' : 'zoom-out'}`} onClick={this.scaleBox} />
                         <Icon type="close" onClick={this.onCloseMinimap}/>
                    </span>
                 </div>
                 <div className= "mini-map-view flex-center">
                    <canvas id="miniMapView" style={{ width: '100%', height:'100%'}} ></canvas>
                     {
                         !componentCount && <div className="miniMap-empty">暂无数据~</div>
                     }
                </div>
                 <div className={`${!scaleMaxState ? 'none' : ''} mini-map-wrapper-left`} />
                 <div className= "mini-map-wrapper-right" />
                 <div className={`${!scaleMaxState ? 'none' : ''} mini-map-wrapper-bottom`} />
             </div>
         )
     }
 }
 
 
 export default MiniMap