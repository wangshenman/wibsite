/* 测量功能组件 */
import React from 'react';
import { Tooltip, Select, Icon, Input} from 'antd';
import {measureBlock,measureTypes} from '../../config/menuConfig';
import {getFloat} from '../../../../../../common/utils/Utility';
import {MeaSettingIcon} from '../../resource/svgIcon';
import './modelMeasure.less';

const { Option } = Select;
class ModelMeasure extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentMeasureType: 'dot',
            measureValue: null,
            measureColor: null,
            measureUnits: 0,
            isSetting: false,
        }
        //关闭测量
        this.onClose = ()=>{
          let { currentApp, mouseHover, switchState } = this.props;
          let measures = {}
          measureTypes.filter(item => {
              measures = Object.assign(mouseHover, { ['measure']:false, [item]: false })
          })
          switchState('mouseHover', measures);
          switchState('measureVisible', false);
          switchState('measureValueObject', null);
          currentApp.create().closeMeasure();
          currentApp.create().removeCallback(210);
        }
        //选择测量
        this.onClickMeasure = (key) => {
            let { currentApp, MeasureType, mouseHover, switchState} = this.props;
            switchState('mouseHover', Object.assign(mouseHover, { [key]: !mouseHover[key] }));
            let measures = {}
            measureTypes.filter(item => {
              if (item !== key) {
                measures = Object.assign(mouseHover, { [item]: false })
              }
            })
            this.setState({
              currentMeasureType: key,
              mouseHover: measures,
              measureValue: null
            })
            
            let MState = currentApp.create().getMeasureConfig();//获取测量状态
            if (!MState.opened) {
              return currentApp.create().openMeasure();
            } else {
              switch (key) {
                case 'dot':
                  currentApp.create().setMeasureType(MeasureType.DOT);
                  break;
                case 'distance':
                  currentApp.create().setMeasureType(MeasureType.DISTANCE);
                  break;
                case 'angle':
                  currentApp.create().setMeasureType(MeasureType.ANGLE);
                  break;
                case 'length':
                  currentApp.create().setMeasureType(MeasureType.LENGTH);
                  break;
                case 'height':
                  currentApp.create().setMeasureType(MeasureType.HEIGHT);
                  break;
                case 'view': 
                  currentApp.create().observeMeasure();
                  break;
                case 'cancel':
                  currentApp.create().cancelMeasure();
                  break;
                case 'end':
                  currentApp.create().finishMeasure();
                  break;
                default:
                  break;
              }
            }
        };
        //设置单位
        this.handleChangeUnit = (value) => {
          this.setState({
            measureUnits: value,
          })
        };

        //获取当前测量颜色
        this.getCurrentColor = ()=>{
          let { currentApp, MeasureType, MeasureUnit} = this.props;
          let MState = currentApp.create().getMeasureConfig();
          if(MState){
            this.setState({measureUnits: MState.unitType});
            let currentColor = [];
            switch(MState.type){
                case MeasureType.DOT:
                  currentColor = MState.dotRGB255;
                  break;
                case MeasureType.DISTANCE:
                  currentColor = MState.distanceRGB255;
                  break;
                case MeasureType.ANGLE:
                  currentColor = MState.angleRGB255;
                  break;
                case MeasureType.LENGTH:
                  currentColor = MState.lengthRGB255;
                  break;
                case MeasureType.HEIGHT:
                  currentColor = MState.heightRGB255;
                  break;
            }
            if(currentColor.length){
              let measureColor = this.colorRGBtoHex(currentColor);
              this.setState({measureColor})
            }
          }
          
          
        }
        //选择颜色
        this.handleChangeColor = (value) =>{
          const{ currentApp }= this.props;
          let color = value;
          let hex = color.substring(1);
          if (hex.length === 3) hex += hex;
          let red = parseInt(hex.substring(0, 2), 16);
          let green = parseInt(hex.substring(2, 4), 16);
          let blue = parseInt(hex.substring(4), 16);
          this.setState({
            measureColor: value,
            color:[red,green,blue]
          })
        };
        //设置颜色
        this.setMeasureColor = ()=>{
          const{color} = this.state;
          const { currentApp, MeasureType } = this.props;
          let state = currentApp.create().getMeasureConfig();
          switch(state.type){
              case MeasureType.DOT:
                  currentApp.create().configMeasure({dotRGB255: color});
                  break;
              case MeasureType.DISTANCE:
                  currentApp.create().configMeasure({distanceRGB255: color});
                  break;
              case MeasureType.ANGLE:
                  currentApp.create().configMeasure({angleRGB255: color});
                  break;
              case MeasureType.LENGTH:
                  currentApp.create().configMeasure({lengthRGB255: color});
                  break;
              case MeasureType.HEIGHT:
                 currentApp.create().configMeasure({heightRGB255: color});
                  break;
              default:
                break;
          }
        };

        this.colorRGBtoHex = (color)=>{
          let r = parseInt(color[0]);
          let g = parseInt(color[1]);
          let b = parseInt(color[2]);
          let hex = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).substring(1);
          return hex;
        };

        this.onClickSet = ()=>{
          this.setState({
            isSetting: true
          },()=>this.getCurrentColor())
        };

        this.deleteMeasure = ()=>{
          const { currentApp } = this.props;
          this.setState({
            measureValue: null
          },()=> {
            currentApp.create().deleteMeasure();
            currentApp.create().clearMeasure();
          })
        };

        this.onSureSetting =()=>{
          const { currentApp } = this.props;
          this.setState({
            isSetting: false
          },()=>{
            this.setMeasureColor();
            currentApp.create().configMeasure({ unitType: this.state.measureUnits });
          })
          
        };

        this.onCancelSetting =()=>{
          this.setState({ isSetting: false })
        };
    }

    componentDidMount() {
        const {measureValueObject} = this.props;
        if(measureValueObject){
            this.setState({
              measureValue: measureValueObject
            })
        }
    }
  
    componentWillReceiveProps(nextProps) {
        this.setState({
          measureValue: nextProps.measureValueObject
        })
       
    }
    render() {
        const { mouseHover, MeasureUnit} = this.props;
        const { currentMeasureType, isSetting, measureValue, measureUnits, measureColor} = this.state;
       
        const settingBox = () =>{
          return(
            <div className="measureWrapper">
                   <div className="setting-item flex-start">
                        <span>颜色：</span>
                        <Input style={{width: 100}} type="color" value={measureColor}  onChange={e => this.handleChangeColor(e.target.value)}/>
                    </div>
                    <div className="setting-item flex-start">
                        <span>单位：</span>
                        <Select style={{width: 100}} 
                              className="unit-select"
                              value={measureUnits}
                              onChange={value => this.handleChangeUnit(value)}>
                              <Option value={MeasureUnit.METER}> m </Option>
                              <Option value={MeasureUnit.CENTIMETER}> cm </Option>
                              <Option value={MeasureUnit.MILLIMETER}> mm </Option>
                         </Select>
                    </div>
            </div>
          );

        }
        const measurePane = (type) =>{
            switch (type) {
                case 'dot':
                   return (<div className="sel-item">
                              <div className="item-box"><span>长度： </span><span>{`${getFloat((measureValue && measureValue.length) || 0, 3)} ${(measureValue && measureValue.unit) || 'm'}`} </span></div>
                              <div className="item-box red"><span>X值： </span><span>{`${getFloat((measureValue && measureValue.xLength) || 0, 3)} ${(measureValue && measureValue.unit) || 'm'}`} </span></div>
                              <div className="item-box green"><span>Y值： </span><span>{`${getFloat((measureValue && measureValue.yLength) || 0, 3)} ${(measureValue && measureValue.unit) || 'm'}`} </span></div>
                              <div className="item-box blue"><span>Z值： </span><span>{`${getFloat((measureValue && measureValue.zLength) || 0, 3)} ${(measureValue && measureValue.unit )|| 'm'}`} </span></div>
                          </div>);
                case 'distance':
                   return (<div className="sel-item"><span>净距： </span><span>{`${getFloat((measureValue && measureValue.length) || 0,3)} ${(measureValue && measureValue.unit) || 'm'}`} </span></div>);
                case 'angle':
                    return (<div className="sel-item"><span>角度： </span><span>{`${getFloat((measureValue && measureValue.angle) || 0,3)} ${(measureValue && measureValue.unit) || '°'}`}</span></div>);
                case 'length':
                    return (<div className="sel-item"><span>长度： </span><span>{`${getFloat((measureValue && measureValue.length) || 0,3)} ${(measureValue && measureValue.unit) || 'm'}`} </span></div>);
                case 'height':
                   return (<div className="sel-item"><span>标高： </span><span>{`${getFloat((measureValue && measureValue.height) || 0,3)} ${(measureValue && measureValue.unit) || 'm'}`} </span></div>);
                default:
                    return (<div className="sel-item">
                      <div className="item-box"><span>长度： </span><span>{`${getFloat((measureValue && measureValue.length) || 0, 3)} ${(measureValue && measureValue.unit) || 'm'}`} </span></div>
                      <div className="item-box red"><span>X值： </span><span>{`${getFloat((measureValue && measureValue.xLength) || 0, 3)} ${(measureValue && measureValue.unit) || 'm'}`} </span></div>
                      <div className="item-box green"><span>Y值： </span><span>{`${getFloat((measureValue && measureValue.yLength) || 0, 3)} ${(measureValue && measureValue.unit) || 'm'}`} </span></div>
                      <div className="item-box blue"><span>Z值： </span><span>{`${getFloat((measureValue && measureValue.zLength) || 0, 3)} ${(measureValue && measureValue.unit )|| 'm'}`} </span></div>
                     </div>);

            }
        };
        
        return (
            <div className="modelMeasure">
                <div className="sel-header flex-between">
                    <span className="sel-title">测量</span>
                    <Icon className="sel-close" type="close" onClick={this.onClose}/>
                </div>
                <div className="sel-content">
                    <div className="sel-btns flex-between">
                        {
                            measureBlock.map((item, index) => {
                                return (
                                    <Tooltip key={index} title={item.title} placement="top">
                                        <span className='option-img'  onClick={()=> this.onClickMeasure(item.selected)}>
                                          <Icon className={`${mouseHover[item.selected] ? 'btn-selected' : ''}`} component={item.icon}/>
                                        </span>
                                    </Tooltip>)
                            })
                        }

                    </div>
                    { isSetting ? settingBox() : measurePane(measureValue && measureValue.type)}
                </div>
                <div className="sel-bottom">
                  {
                    isSetting ?
                    (<div className="flex-between">
                      <span className='option-text gray' onClick={this.onCancelSetting}>取消</span>
                      <span className='option-text' onClick={this.onSureSetting}>确定</span>
                    </div>)
                    :
                    (<div className="flex-between">
                        <span className='option-text' onClick={this.deleteMeasure}>删除全部</span>
                        <span className='option-img' onClick={this.onClickSet}>             
                              <Icon  component={MeaSettingIcon}/>
                        </span>
                    </div>)
                    
                  }
                </div>
            </div>)
    }

}
export default ModelMeasure;
