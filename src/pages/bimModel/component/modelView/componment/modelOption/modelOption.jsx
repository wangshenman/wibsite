/* 通用显示框组件 */
import React from 'react';
import { Tooltip,Icon } from 'antd';
import './modelOption.less'

class ModelOption extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        const {options, click, mouseHover} = this.props;
        return (
            <div className="modelOption">
                {
                    options.map((item, index) => {
                        return (
                            <Tooltip key={index} title={item.title} placement="left">
                                    <span className='option-img'  onClick={()=> click(item.selected)}>
                                        <Icon className={`${mouseHover[item.selected] ? 'btn-selected' : ''}`} component={item.icon}/>
                                    </span>
                            </Tooltip>)
                    })
                }
            </div>)
    }

}
export default ModelOption;
