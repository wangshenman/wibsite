/* 漫游功能组件 */
import React from 'react';
import { Tooltip,  Icon, Slider} from 'antd';
import { roamBlock } from '../../config/menuConfig';
import './modelRoaming.less';

class ModelRoaming extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            speedSlider: false,
            speedValue: 4
        }
         //点击选择漫游模式
        this.onClickRoam = (key) => {
            let { app, mouseHover, switchState} = this.props;
            let { speedSlider} = this.state;
            switchState('mouseHover', Object.assign(mouseHover, { [key]: !mouseHover[key] }))
            let state = app.action().getRoamingConfig();
            if(state){
                switch (key) {
                    case 'third':
                        app.action().configRoaming({thirdPersonOpened: !state.thirdPersonOpened});
                        break;
                    case 'gravity':
                        app.action().configRoaming({gravityFallOpened: !state.gravityFallOpened});
                        break;
                    case 'crash':
                        app.action().configRoaming({collisionDetectOpened: !state.collisionDetectOpened});
                        break;
                    case 'orthogonal':
                        app.action().configRoaming({orthogonalOpened: !state.orthogonalOpened});
                        break;
                    case 'speed':
                        this.setState({ speedSlider: !speedSlider})
                        break;
                    case 'help':
                        this.props.switchState('roamHintVisible',true);
                        break;
                    default:
                        break;
                }
            }
          
        };
        this.handleChangeSlider=(value) =>{
            this.setState({
                speedValue: value
            },()=> this.props.app.action().configRoaming({characterMoveSpeed: value}))
        };
    }

    componentDidMount() {}
  
    componentWillReceiveProps(nextProps) {}

    render() {
        const { mouseHover,} = this.props;
        const { speedSlider, speedValue } = this.state;
        const marks = {
            0: '0',
            20: '20'
        };
        
        return (
            <div className="modelRoaming">
                  <div className="roam-operate flex-column-between">
                        {
                            roamBlock.map((item, index) => {
                                return (
                                    <Tooltip key={index} title={item.title} placement="left">
                                        <span className='option-img'  onClick={()=> this.onClickRoam(item.selected)}>
                                            <Icon className={`${mouseHover[item.selected] ? 'btn-selected' : ''}`} component={item.icon}/>
                                        </span>
                                    </Tooltip>)
                            })
                        }
                  </div>
                  {
                      speedSlider && 
                      <div className="speed-wrapper">
                          <span>速度</span>
                          <Slider vertical 
                                 defaultValue={4} 
                                 max={20} 
                                 min={0}
                                 marks={marks}
                                 value={speedValue}
                                 tooltipPlacement={'left'}
                                 onChange={(value)=> this.handleChangeSlider(value)}
                          />
                      </div>
                       
                  }
            </div>)
    }

}
export default ModelRoaming;
