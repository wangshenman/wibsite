
import React from 'react';
import { Button } from 'antd';
import roamRun from '../../resource/roam/roam_run.png';
import roamAround from '../../resource/roam/roam_around.png';
import roamKeyboard from '../../resource/roam/roam_keyboard.png';
import roamUpdown from '../../resource/roam/roam_updown.png';
import './modelRoaming.less';

class RoamTips extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
       
    }
  
    render() {
        const { onClose } = this.props;
        return (
            <div className="roamtips-wrapper flex-vertical-center">
                  <div className="roamtips-content ">
                      <div className="roamtips-items">
                            <img className="tip-item" src={roamKeyboard} alt=""/>
                            <img className="tip-item" src={roamRun} alt=""/>
                            <img className="tip-item" src={roamUpdown} alt=""/>
                            <img className="tip-item" src={roamAround} alt=""/>
                      </div>
                      <Button type="primary" shape="round" className="tip-button" onClick={onClose}>已知晓</Button>
                  </div>
                  
            </div>)
    }

}
export default RoamTips;
