/* 剖切功能组件 */
import React from 'react';
import { Tooltip, Slider, Icon, Radio} from 'antd';
import './modelSection.less';

class ModelSection extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sectionAxis: 'axisX',
            sliderValue: {  // Slider: [0-2]  中间件：[-0.5,0.5]
                x: [0,2],
                y: [0,2],
                z: [0,2]
            },
        }
        
        this.handleChangeSlider = (value,type) =>{
            const{ currentApp } = this.props;
            const{ sliderValue } = this.state;
            let range = [(value[0]-1)/2, (value[1]-1)/2];
            currentApp.action().setSectionBox({ [type]: range });
            this.setState({ sliderValue: Object.assign(sliderValue, { [type]: value }) })
        }
        
        //中间件回调的拖拽值 与 Slider映射
        this.transferAxisValue = (sectionAxisValue) =>{
            const{sliderValue}=this.state;
            let axisArray = Object.keys(sectionAxisValue);
            let axis = axisArray && axisArray[0];
            let axisValue = axis && sectionAxisValue[axis];
            let sectionValue = Object.assign(sliderValue, {[axis]: [axisValue[0]*2+1, axisValue[1]*2+1]});
            return sectionValue;
        }

        this.changeSectionAxis = (e)=>{
            this.props.click(e.target.value);
            this.setState({
                sectionAxis: e.target.value,
            });
        }

        this.closeCube = ()=>{
            const {switchState, mouseHover, currentApp,sectionTypes} =this.props;
            let sections = {};
            sectionTypes.map(item => {
                sections = Object.assign(mouseHover, { [item]: false , ['section']:false})
            })
            currentApp.action().closeSection();
            switchState('sectionType', null);
            switchState('mouseHover', Object.assign(sections));
            switchState('sectionVisible', false);
        }
    }

    componentDidMount() {
        const {sectionAxisValue} = this.props;
        if(sectionAxisValue){
            this.setState({
                sliderValue: this.transferAxisValue(sectionAxisValue)
            })
        }
    }
  
    componentWillReceiveProps(nextProps) {
        if(nextProps.sectionAxisValue !== this.props.sectionAxisValue){
            this.setState({
                sliderValue: this.transferAxisValue(nextProps.sectionAxisValue)
            })
        }
    }

    render() {
        const { sectionType, options, click, mouseHover } = this.props;
        const { sectionAxis, sliderValue } = this.state;
        let sectionItem = sectionType === 'drag' ? options : options.filter(i =>i.selected !== 'overTurn')
        const sectionDragBox = () =>{
            return(
                <div className="sel-section">
                    <div className="sel-item flex-between">
                        <span>X轴 </span><Slider range value={sliderValue.x}  min={0} max={2} step={0.1} tooltipVisible={false} onChange={(value)=> this.handleChangeSlider(value,'x')}/>
                    </div>
                    <div className="sel-item flex-between">
                        <span>Y轴 </span><Slider range value={sliderValue.y} min={0} max={2} step={0.1} tooltipVisible={false} onChange={(value)=> this.handleChangeSlider(value,'y')}/>
                    </div>
                    <div className="sel-item flex-between">
                        <span>Z轴 </span><Slider range value={sliderValue.z} min={0} max={2} step={0.1} tooltipVisible={false} onChange={(value)=> this.handleChangeSlider(value,'z')}/>
                    </div>
                </div>
            )
        };

        const sectionAxisBox = () =>{
            return(
                <div className="sel-section flex-between">
                    <Radio.Group onChange={this.changeSectionAxis} value={sectionAxis}>
                            <Radio value={'axisX'}>X</Radio>
                            <Radio value={'axisY'}>Y</Radio>
                            <Radio value={'axisZ'}>Z</Radio>
                    </Radio.Group>
                </div>
            )
        };
        
        return (
            <div className="modelSection">
                {/* <div className="sel-header flex-between">
                    <span className="sel-title">{ sectionType && sectionType === 'drag' ? '剖切盒' : '剖切面'}</span>
                    <span className="sel-operate">
                         <Icon className="sel-minus" type="minus" onClick={this.hideCube}/>
                        <Icon className="sel-close" type="close" onClick={this.closeCube}/>
                    </span>
                </div> */}
                <div className="sel-content">
                    <div className="sel-btns">
                        {
                            sectionItem.map((item, index) => {
                                return (
                                    <Tooltip key={index} title={item.title} placement="left">
                                        <span className='option-img'  onClick={()=> click(item.selected)}>
                                            <Icon className={`${mouseHover[item.selected] ? 'btn-selected' : ''}`} component={item.icon}/>
                                        </span>
                                    </Tooltip>)
                            })
                        }
                    </div>
                    {/* {  sectionType && sectionType === 'drag' ? null : sectionAxisBox()} */}
                </div>
            </div>)
    }

}
export default ModelSection;
