/* 设置功能组件 */
import React from 'react';
import {  Modal, Switch, Col, Row , Radio, Select, Slider, Checkbox} from 'antd';
import { colorsBlock } from '../../config/menuConfig';
import './modelSetting.less';

const { Option, OptGroup } = Select;

export const AXIS =  "axis"; //轴线
export const WAREFRAME = "wareframe"; //线框
export const REALITY = "reality"; //材质（真实）

export const ORTHO = "orthogonality"; //正交
export const PERSPECT = "perspectivity"; //透视

class ModelSetting extends React.Component {
    constructor(props) {
        super(props)
        //获取 props 的值
        let sessionSetParm = this.props.setParms;
        this.state={
            basicDisplay: sessionSetParm && sessionSetParm.basicDisplay,
            // leftMouseOperation: sessionSetParm && sessionSetParm.leftMouseOperation,
            isOrthogonal: sessionSetParm && sessionSetParm.isOrthogonal,
            bgColor: sessionSetParm && sessionSetParm.bgColor,
            lightVisible: sessionSetParm && sessionSetParm.lightVisible,
            ambientIntensity: sessionSetParm && sessionSetParm.ambientIntensity,
            diffuseIntensity: sessionSetParm && sessionSetParm.diffuseIntensity,
            opacity: sessionSetParm && sessionSetParm.opacity, 
            frameLimitRatecity:sessionSetParm && sessionSetParm.frameLimitRatecity, 
        }  
   };


    handleChangeValue = (value, type)=>{
        this.setState({
            [type]: value
        })
    }

    // 设置光照、线框、真实、背景颜色
    onClickSetting = () => {
        let { bgColor, basicDisplay, isOrthogonal, opacity, lightVisible, ambientIntensity, diffuseIntensity, frameLimitRatecity } = this.state;
        let { app, BackgroundType, GridElevationMode, LeftMouseOperation, switchState, onCancel } = this.props;
        // 背景色设置
        let selectedColor = null;
        for(let i = 0; i < colorsBlock.length; i++){
            let colors = colorsBlock[i].colorItems;
            for(let j = 0; j < colors.length; j++){
                if(colors[j].key === bgColor){
                    selectedColor = colors[j];
                }
            } 
        }
        if(selectedColor){
            let background = selectedColor.color || [];
            let backgroundImage = selectedColor.image || '';
            let backgroundConfig = {};
            switch(selectedColor.type){
                //纯色
                case 'pure':
                    backgroundConfig = { pureRGB255: background[0] };
                    app.effect().configBackground(backgroundConfig);
                    app.effect().setBackgroundType(BackgroundType.PURE_COLOR);
                  break;
                //渐变色
                case 'gradient':
                    backgroundConfig = { 
                        gradientStartRGB255: background[0],
                        gradientEndRGB255: background[1],
                        gradientDirectionType: 0, //1/2/3/4
                    }
                    app.effect().configBackground(backgroundConfig);
                    app.effect().setBackgroundType(BackgroundType.GRADIENT_COLOR);
                    break;
                //静图背景
                case 'flatImg':
                    app.effect().setBackgroundFlatImage(backgroundImage);
                    app.effect().setBackgroundType(BackgroundType.FLAT_MAPPING);
                   
                    break;
                //全景图
                case 'panormaImg':
                    app.effect().setBackgroundPanoramaImage(backgroundImage);
                    app.effect().setBackgroundType(BackgroundType.PANORAMA_MAPPING);
                    break;
                default:
                    break;
            }
        }

        //材质（真实）
        let realityVisible = basicDisplay.indexOf(REALITY) > -1 ? true : false;
        app.effect().toggleRealistic(realityVisible);
        
        //线框
        let wareVisible =  basicDisplay.indexOf(WAREFRAME) > -1 ? true : false;
        app.effect().toggleWireframe(wareVisible);

        //轴网
        let axisVisible =  basicDisplay.indexOf(AXIS) > -1 ? true : false;
        app.assist().configGrid({opened: axisVisible})
        app.assist().setGridElevationMode(GridElevationMode.SHOWN_BOTTOM);
       
        //正交透视
        app.action().configRoaming({ orthogonalOpened: isOrthogonal});
        //透明度
        app.effect().setFreezeAlpha(opacity);

        //光照阴影
        app.effect().toggleLightShadow(lightVisible);
        app.effect().configLighting({ ambientIntensity,diffuseIntensity });

        //流畅度
        app.effect().setFrameLimitRate(frameLimitRatecity)
        
        let settingParm = { basicDisplay, bgColor, isOrthogonal, lightVisible, ambientIntensity, diffuseIntensity, opacity, frameLimitRatecity };
        switchState('setParms', settingParm);
        onCancel();
    }

    render() {
        const {lightVisible, basicDisplay, isOrthogonal, ambientIntensity, diffuseIntensity, opacity, frameLimitRatecity, bgColor} = this.state;

        const modelSetMoudle = () =>{
            return (
                <div className="model-setting-moudle">
                    <Row>
                        <Col span={8}>基础展示</Col>
                        <Col span={16}>
                            <Checkbox.Group onChange={(value)=> this.handleChangeValue(value,'basicDisplay')}  value={basicDisplay} style={{ width: '100%' }}>
                                <Col span={8}>
                                    <Checkbox name="wareframe" value={WAREFRAME}>线框</Checkbox>
                                </Col>
                                <Col span={8}>
                                    <Checkbox name="reality" value={REALITY}>材质</Checkbox>
                                </Col>
                                <Col span={8}>
                                    <Checkbox name="axis" value={AXIS}>轴网</Checkbox>
                                </Col>
                            </Checkbox.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={8}>背景设置</Col>
                        <Col span={16}>
                            <div className='setColors' id={'set-Colors-wrapper'}>
                                <Select value={bgColor}
                                        size={'small'}
                                        dropdownClassName={'model-bg-setting-item'}
                                        style={{width: 160, float: 'left'}}
                                        getPopupContainer={() => document.getElementById('set-Colors-wrapper')}
                                        onChange={(value)=> this.handleChangeValue(value,'bgColor')}
                                > 
                                {
                                    colorsBlock.map((item,index) => {
                                        return (
                                            <OptGroup label={item.label}  key={`${item.key}-${index}`}>
                                            {
                                                item.colorItems.map((d,index)=>{
                                                    return(
                                                    <Option
                                                        key={`${d.key}-${index}`}
                                                        value={d.key}
                                                        className='color-select'>
                                                        <div className='color-item-wrapper' key={d.key}>
                                                            <div className='color-item'>
                                                                {
                                                                    item.type === 'flatImg' || item.type === 'panormaImg'?
                                                                    <img src={d.thumb} alt=""/>
                                                                    :
                                                                    <div style={{ background: item.type === 'gradient' ? `linear-gradient(180deg,rgba(${d.color[0].join(',')},1) 0%,rgba(${d.color[1].join(',')},1) 100%)` : d.key }}></div>
                                                                }
                                                            </div>
                                                            <span className='color-text'> {d.name} </span>
                                                        </div>
                                                    </Option>
                                                    )
                                                })
                                            }
                                            </OptGroup>
                                        )
                                    })
                                }
                                </Select>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={8}>模型视图</Col>
                        <Col span={16}>
                            <Radio.Group onChange={(e)=> this.handleChangeValue(e.target.value,'isOrthogonal')} value={isOrthogonal}>
                                <Radio value={true}>正交 </Radio>
                                <Radio value={false}>透视</Radio>
                            </Radio.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={8}>环境阴影</Col>
                        <Col span={16}>
                            <Radio.Group onChange={(e)=> this.handleChangeValue(e.target.value,'lightVisible')} value={lightVisible}>
                                <Radio value={true}>开启</Radio>
                                <Radio value={false}>关闭</Radio>
                            </Radio.Group>
                        </Col>
                    </Row>
                    <Row>
                            <Col span={8}>环境光强度</Col>
                            <Col span={16}>
                                <Slider max={1} 
                                        step={0.1}
                                        marks={{
                                            0: '0',
                                            1: '1'
                                        }}
                                        style={{width:'220px'}}
                                        value={ambientIntensity}
                                        tooltipPlacement={'top'}
                                        onChange={(value)=> this.handleChangeValue(value,'ambientIntensity')}
                                />
                            </Col>
                    </Row>
                    <Row>
                        <Col span={8}> 散射光强度 </Col>
                        <Col span={16}>
                            <Slider max={1} 
                                    step={0.1}
                                    marks={{
                                        0: '0',
                                        1: '1'
                                    }}
                                    style={{width:'220px'}}
                                    value={diffuseIntensity}
                                    tooltipPlacement={'top'}
                                    onChange={(value)=> this.handleChangeValue(value,'diffuseIntensity')}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col span={8}> 模型透明度 </Col>
                        <Col span={16}>
                            <Slider max={255} 
                                    step={10}
                                    marks={{
                                        0: '0',
                                        255: '255'
                                    }}
                                    style={{width:'220px'}}
                                    value={opacity}
                                    tooltipPlacement={'top'}
                                    onChange={(value)=> this.handleChangeValue(value,'opacity')}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col span={8}> 模型流畅度 </Col>
                        <Col span={16}>
                            <Slider max={100} 
                                    step={5}
                                    marks={{
                                        0: '0',
                                        100: '100'
                                    }}
                                    style={{width:'220px'}}
                                    value={frameLimitRatecity}
                                    tooltipPlacement={'top'}
                                    onChange={(value)=> this.handleChangeValue(value,'frameLimitRatecity')}
                            />
                        </Col>
                    </Row>
                   
                </div>)
        };
        return (
            <div className="model-setting-wrapper" id="model-setting-wrapper" style={{width:'460px'}}>
                <Modal  title="设置"
                        okText={'确定'}
                        cancelText={'取消'}
                        visible = {this.props.visible}
                        width={450}
                        mask={false}
                        maskClosable={false}
                        centered
                        getContainer={() => document.getElementById('model-setting-wrapper')}
                        onOk={this.onClickSetting}
                        onCancel={this.props.onCancel}>
                            { modelSetMoudle() }
                </Modal>
            </div>)
    }

}
export default ModelSetting;