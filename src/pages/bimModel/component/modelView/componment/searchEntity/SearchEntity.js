/* 构件搜索功能组件 */
import React from 'react';
import {message} from "antd";
import SearchModel from './SearchModel';
import SearchResult from './SearchResult';
import {searchComponents } from '../../../../../../developer/console/module/bimModelList/bimModelList.actions';

class SearchEntity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchResultData: [],
            searchModelVisible: true,
            searchResultVisible: false,
            searchResultLoading: false,
        };

        //构件搜索
        this.searchEntitySubmit =  async(param) => {
            const{app,links} = this.props;
            let paramTotal = Object.assign(param, {models: this.props.links});
            this.setState({searchResultLoading: true});
            let result = await app.data().queryComponents(paramTotal);
            if(result){
                this.setState({
                    searchResultData: result,
                    searchResultLoading:false,
                    searchModelVisible: false,
                    searchResultVisible: true,
                })
            }else{
                this.setState({
                    searchResultLoading:false,
                }) 
            }
            // searchComponents(paramTotal).then(res => {
            //     if(res && res.code === 200){
            //         this.setState({
            //             searchResultData: res.data,
            //             searchResultLoading:false,
            //             searchModelVisible: false,
            //             searchResultVisible: true,
            //         })
            //     }else{
            //         this.setState({
            //             searchResultLoading:false,
            //         }) 
            //     }
            // }).catch(err => message.error(err.message));
        };

        //数组去重操作
        this.uniqueArray = (rawArr) =>{
            let uniqueArray = [];
            let ids = new Set();
            rawArr.forEach(value => {
                if (!ids.has(value.entityId)) {
                    uniqueArray.push(value)
                }
                ids.add(value.entityId);
            });
            return uniqueArray;
        };

        //返回构件搜索
        this.goBackSearchEntity = () => {
            this.setState({
                searchModelVisible: true,
                searchResultVisible: false
            });
        };

        //搜索结果 点击收起
        this.packUpSearchResult = () => {
            this.props.switchState('isPackUp', true);
            this.setState({
                searchResultVisible: false
            });
        };

        //关闭事件
        this.onClose = (key) => {
            const{mouseHover} = this.props;
            //构件搜索 关闭
            switch (key) {
                case 1://搜索结果 关闭
                    this.props.onClose();
                    this.setState({
                        searchModelVisible: false
                    });
                    break;
                case 2://构件列表 关闭
                    this.props.onClose();
                    this.setState({
                        searchResultVisible: false
                    },()=> this.reset());
                    break;
                default:
                    break; 
            }
        };

        this.reset = () =>{
            const{ app } = this.props;
            app.action().homeRoaming();
            app.control().unhighlightAllComponents();
            app.control().unfreezeAllComponents();
        }
    }

    render() {
        let { searchResultData, searchModelVisible, searchResultVisible, searchDetailTreeVisible, searchResultLoading} = this.state;
        let { app,links } = this.props;
        return (
            <div className="search-layer">
                {
                    searchModelVisible &&
                        <SearchModel visible ={searchModelVisible}
                                     onClose = {this.onClose}
                                     onSubmit = {this.searchEntitySubmit} />
                }
                {
                    searchResultVisible && !searchModelVisible &&
                        <SearchResult   visible ={searchResultVisible}
                                        onClose = {this.onClose}
                                        loading = { searchResultLoading }
                                        onPackUp = {this.packUpSearchResult}
                                        searchResultData = { searchResultData }
                                        app={app}
                                        links={links}
                                        goback= {this.goBackSearchEntity}
                        />
                }
            </div>
        )

    }
}

export default SearchEntity