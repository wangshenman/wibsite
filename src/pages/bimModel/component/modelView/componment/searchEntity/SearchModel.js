import React from 'react';
import { Modal, message, Row, Col, Input, Checkbox, Radio,Button } from 'antd';
import Draggable from "react-draggable";
import $ from "jquery";
import './searchEntity.less';

const S_Name = 'componentName';
const S_RevitID = 'entityId';
const S_Floor = 'floor';
const S_Domain = 'domain';
const S_Property = 'property';
const S_Type = 'type';

class SearchModel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            modelVisible: true, // 弹框条件显隐
            resultVisible: false, // 搜索结果显隐
            firstKeyWords: '',  // 搜索关键字1
            secondKeyWords: '', // 搜索关键字2
            thirdKeyWords: '',  // 搜索关键字3
            searchType: 'or',  // 搜索类型
            searchRange: [], //  搜索范围
            searchAccuracy: 'like', // 精确度
        }

        // 搜索关键字
        this.handleInput = (val, key) => {
            if (key === 1) {
                this.setState({
                    firstKeyWords: val,
                });
            } else if (key === 2) {
                this.setState({
                    secondKeyWords: val,
                });
            } else if (key === 3) {
                this.setState({
                    thirdKeyWords: val,
                });
            }
        }

        // 搜索类型
        this.handleSearchType = (e) => {
            this.setState({
                searchType: e.target.value,
            });
        }

        // 搜索范围
        this.handleSearchRange = searchRange => {
            this.setState({
                searchRange
            });
        }

        // 精确度
        this.handleSearchAccuracy = (e) => {
            this.setState({
                searchAccuracy: e.target.value,
            });
        }

        // 提交
        this.handleSubmit = () => {
            const { firstKeyWords, secondKeyWords, thirdKeyWords, searchType, searchRange, searchAccuracy } = this.state;
            if (!firstKeyWords && !secondKeyWords && !thirdKeyWords) {
                message.warning('请输入搜索关键字');
            } else {
                let keywords = [];
                // 搜索关键字（去空）
                keywords = [firstKeyWords, secondKeyWords, thirdKeyWords].filter(current => {
                    return current !== null && current !== '';
                })

                if(!searchRange.length){
                    return message.warning('请选择构件搜索范围~');
                }

                // 搜索参数
                let paramWhere = {
                    values: keywords,
                    searchScope: searchRange,
                    searchOp: searchType,
                    searchType: searchAccuracy,
                }
                this.props.onSubmit(paramWhere)
            }
        }
    }

    // 构件选择条件
    searchModel() {
        const { searchType, searchRange, searchAccuracy } = this.state
        return (
            <div className="model-search-body">
                <Row>
                    <Col span={24}>
                        <Input placeholder='请输入搜索关键字1' onChange={e => this.handleInput(e.target.value, 1)} />
                        <Input placeholder='请输入搜索关键字2' onChange={e => this.handleInput(e.target.value, 2)} />
                        <Input placeholder='请输入搜索关键字3' onChange={e => this.handleInput(e.target.value, 3)} />
                    </Col>
                </Row>
                <Row>
                    <Col span={8}>搜索类型：</Col>
                    <Col span={16}>
                        <Radio.Group onChange={this.handleSearchType} value={searchType}>
                           <Radio value={'or'}>或</Radio>
                            <Radio value={'and'}>与</Radio>
                        </Radio.Group>
                    </Col>
                </Row>
                <Row>
                    <Col span={8}>精确度：</Col>
                    <Col span={16}>
                        <Radio.Group onChange={this.handleSearchAccuracy} value={searchAccuracy}>
                            <Radio value={'like'}>模糊搜索</Radio>
                            <Radio value={'equals'}>精确搜索</Radio>
                        </Radio.Group>
                    </Col>
                </Row>
                <Row>
                    <Col span={8}>搜索范围：</Col>
                    <Col span={16}>
                        <Checkbox.Group onChange={this.handleSearchRange} defaultValue={searchRange} value={searchRange}
                            style={{ width: '100%' }}>
                            <Row>
                                <Col span={12}>
                                    <Checkbox name="entityName" value={S_Name}>构件名称</Checkbox>
                                </Col>
                                <Col span={12}>
                                    <Checkbox name="revitID" value={S_RevitID}>构件ID</Checkbox>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={12}>
                                    <Checkbox name="floor" value={S_Floor}>楼层</Checkbox>
                                </Col>
                                <Col span={12}>
                                    <Checkbox name="domain" value={S_Domain}>专业</Checkbox>
                                </Col>
                            </Row>
                            <Row>
                            <Col span={12}>
                                    <Checkbox name="type" value={S_Type}>类型</Checkbox>
                                </Col>
                                <Col span={12}>
                                    <Checkbox name="property" value={S_Property}>属性值</Checkbox>
                                </Col>
                            </Row>
                        </Checkbox.Group>
                    </Col>
                </Row>
                
            </div>)
    }

    render() {
        const { onClose } = this.props;
        return (
        <Draggable
            bounds ={'body'}
            defaultPosition={{x: $(window).width()/2 - 300, y: 100}}
            handle={'.ant-modal-header'}
        >
            <div id="model-search-wrapper">
                <Modal title="构件搜索"
                    width={480}
                    mask={false}
                    maskClosable={false}
                    visible={true}
                    className={'model-entity-search'}
                    onOk={() => this.handleSubmit()}
                    onCancel={() => onClose(1)}
                    getContainer={() => document.getElementById('model-search-wrapper')}
                    footer={[
                        <Button key="back" onClick={() => onClose(1)}>
                           取消
                        </Button>,
                        <Button key="submit" type="primary" onClick={() => this.handleSubmit()}>
                           搜索
                        </Button>,
                    ]}
                >
                    {this.searchModel()}
                </Modal>
            </div>
        </Draggable>
        )
    }
}

export default SearchModel;
