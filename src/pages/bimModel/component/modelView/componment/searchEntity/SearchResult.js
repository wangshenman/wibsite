
import React from 'react';
import { Modal,  Button, Table } from 'antd';
import iconlocation from '../../resource/model/location.png';
import Draggable from "react-draggable";
import $ from "jquery";
import './searchEntity.less'

class SearchResult extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            loading: false,
            searchResultData:[] 
        };

        //定位
        this.onLocation = (record, type) => {
            const {app,links} = this.props;
            let uuids = [];
            if(type){
               uuids.push(record);
            }else{
               uuids = record;
            }
            links.map(i =>{ Object.assign(i, { componentUuids: uuids })})
            app.action().locateAssignment(links);
            app.control().highlightAssignComponents(links);
            app.control().freezeAllComponents();
        };

        // this.onKeyUp = (e) => {
        //     if (e.keyCode === 27) {
        //         const{ app } = this.props;
        //         app.action().homeRoaming();
        //         app.control().unhighlightAllComponents();
        //         app.control().unfreezeAllComponents();
        //     }
        // };
    }
    componentDidMount(){
        this.setState({
            loading: this.props.loading,
            searchResultData: this.props.searchResultData
        })
        // document.addEventListener('keyup', this.onKeyUp, false);

    }
    componentWillReceiveProps(nextProps){
        this.setState({
            loading: nextProps.loading,
            searchResultData: nextProps.searchResultData
        })
    }

    render() {
        let enitiesList = [];
        const { searchResultData,loading } = this.state;
        const { visible, goback , onClose } = this.props;

       //获取所有entityId 全部定位
        searchResultData && searchResultData.map(i =>{
            enitiesList.push(i.uuid)
        });
        const columns = [
            {
              title: '名称',
              dataIndex: 'name',
              width: 160
            },
            {
                title: '类型',
                dataIndex: 'categoryStr',
                width: 100,
                render: (text, record) => text ? text : '-'
            },
            {
                title: '楼层',
                dataIndex: 'floor',
                width: 50,
            },
            {
                title: '专业',
                dataIndex: 'domainStr',
                width: 80,
                render: (text, record) => text ? text : '-'
            },
            {   
                title: '定位',
                dataIndex: 'location',
                width: 50,
                render: (text, record) =>
                searchResultData.length ? (
                    <div className='location' onClick={()=> this.onLocation(record.uuid,'single')}> 
                    <img title="定位" alt="定位" src={iconlocation} />
                    </div>
                ) : null,
            },
        ];
        return (
            <Draggable
                bounds ={'body'}
                defaultPosition={{x: $(window).width()/2 - 300, y: 100}}
                handle={'.ant-modal-header'}
            >
            <div id={'search-entity-rnd-box'}>
                <Modal title="搜索结果"
                   width={600}
                   visible={visible}
                   mask = {false}
                   maskClosable = {false}
                   zIndex = {99}
                   className={'model-search-result'}
                   getContainer={() => document.getElementById('search-entity-rnd-box')}
                   onOk={()=> goback()}
                   onCancel={() => onClose(2)}
                   footer={[
                       <Button key="submit" type="primary"  onClick={ ()=> goback() }>
                         返回搜索框
                       </Button>,

                   ]} 
                >
                    <div className='result-header'>
                       <span>{`搜索结果：共 ${ (searchResultData && searchResultData.length) || 0 } 个`} </span>
                       <div className={'locate-all'} onClick={()=>this.onLocation(enitiesList)}>全部定位</div>
                    </div>
                    <div className='result-table'>
                        <Table rowKey={record => record._id} 
                               columns={columns} 
                               dataSource={searchResultData} 
                               loading={loading}
                               pagination={{ pageSize: 10}}
                        />
                    </div>
                </Modal>
            </div>
            </Draggable>
       )
    }
}
export default SearchResult;