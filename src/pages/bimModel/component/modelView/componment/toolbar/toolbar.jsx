/* 底部菜单组件 */
import React from 'react';
import { Icon, Tooltip, Menu, Dropdown } from 'antd';
import { sectionBlock, menuBlock, H5MenuBlock, mainMenuBlock} from '../../config/menuConfig';
import './toolbar.less'

class Toolbar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { click, handleSection, mouseHover, mainMenu} = this.props;
        let isPc = window.localStorage.getItem('isPc');
        let menuArray = [];
        if(isPc === "true"){
            //主要菜单栏
            menuArray = mainMenu ? mainMenuBlock : menuBlock;
        }else{
            //兼容移动端
            menuArray = H5MenuBlock;
        }
       
       
        //设置点击显示相应的下拉菜单
        const setSubMenu = (key) => {
            switch(key){
                // case 'measure':
                //     return  <SubMenuLayout menuBlock={measureBlock} click={handleMeasure} mouseHover={mouseHover} type="measure"/>
                // case 'roaming':
                //     return  <SubMenuLayout menuBlock={roamBlock} click={handleRoam} mouseHover={mouseHover} type="roaming"/>
                case 'section':
                    return  <SubMenuLayout menuBlock={sectionBlock} click={handleSection} mouseHover={mouseHover} type="section"/>
                default:
                    break;

            }

        };

        let toolItem = (item) => {
            return(
                <span className='tool-button'
                      onClick={() => click(item.selected)}>
                    <Icon className={`${mouseHover[item.selected] && !item.noPickMark ? 'btn-selected' : ''}`} component={item.icon}/>
                </span>
            );

        }

        return (
            <div className="toolbar" id="menuWrapper">
                {
                    menuArray.map((item, index) => {
                        return (
                            <span key={index}>
                            {
                                !item.isSubMenu ?
                                <Tooltip key={index} 
                                    title={item.title} 
                                    placement="top" 
                                    getPopupContainer={() => document.getElementById('menuWrapper')}
                                >
                                  { toolItem(item) }
                                </Tooltip>
                                :
                                <Dropdown overlay={() => setSubMenu(item.selected)} 
                                          trigger={['hover']}  
                                          placement={'topCenter'} 
                                          getPopupContainer= {()=>document.getElementById('menuWrapper')}>
                                   { toolItem(item) } 
                                </Dropdown>
                            }
                        </span>)
                    })
                }
            </div>)
    }
}

function SubMenuLayout(props) {
    const { menuBlock, click, mouseHover, type} = props;
    return (<Menu>
        {
            menuBlock.map((item, index) => {
                return (
                    <Menu.Item key={item.selected} onClick={() => mouseHover[type] && click(item.selected)}>
                        <span className='subMenu sectionMenu flex-between'>
                           <Icon className={`subMenu-img ${mouseHover[item.selected] ? 'selected' : ''}`} component={item.icon}/>
                            <span className={`subMenu-text ${mouseHover[item.selected] ? 'selected' : ''}`}>{item.title}</span>
                        </span>
                    </Menu.Item>

                )
            })
        }
    </Menu>)
}


export default Toolbar;

