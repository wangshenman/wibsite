import{
    MainViewIcon,SettingIcon,FullScreenIcon, ControlTreeIcon,EntityPropertyIcon,RoamIcon,
    SectioningIcon,SecXIcon,SecYIcon,SecZIcon,MeasureIcon,MiniMapIcon,SearchIcon,BasicInfoIcon,
    MeaPointIcon,MeaDistanceIcon,MeaLengthIcon,MeaAngleIcon,MeaHeightIcon,MeaSettingIcon,MeaViewIcon, MeaCancelIcon, MeaEndIcon,
    SecAxisIcon,SecDragIcon,SecTranslationIcon,SecTranIcon,SecScaleIcon,SecRotateIcon,SecShowIcon,SecHideIcon,SecOverturnIcon,SecResetIcon,
    RoamGravityIcon,RoamCrashIcon,RoamPersonIcon,RoamSpeedIcon,RoamHelpIcon,
} from '../resource/svgIcon';

//背景缩略图
import dark from '../resource/model/dark.png';
import sky from '../resource/model/sky.png';
import star from '../resource/model/star.png';

import green from '../resource/model/green.png';

//背景图
import darkBG from '../resource/model/darkBG.png';
import skyBG from '../resource/model/skyBG.png';
import starBG from '../resource/model/starBG.png';

import greenBG from '../resource/model/env4.png';

export const measureTypes = ['dot','distance','length','angle','height','measureSetting'];
export const sectionTypes = ['drag','secTranslate','secScale','secRotate','axisX','axisY','axisZ'];
export const operateTypes = ['magnify','shrink','pick','pan','rotate'];

export const menuBlock = [
    {
        title: '主视角',
        selected: 'mainView',
        isSubMenu: false,
        noPickMark: true,
        type:'mainView',
        icon: MainViewIcon,
    },
    {
        title: '剖切',
        selected: 'section',
        isSubMenu: true,
        noPickMark: false,
        type:'section',
        icon: SectioningIcon,
    },
    {
        title: '测量',
        selected: 'measure',
        isSubMenu: false,
        noPickMark: false,
        type:'measure',
        icon: MeasureIcon,
    },
    {
        title: '漫游',
        selected: 'roaming',
        isSubMenu: false,
        noPickMark: false,
        type:'roaming',
        icon: RoamIcon,
    },
    {
        title: '小地图',
        selected: 'miniMap',
        isSubMenu:false,
        noPickMark: false,
        type:'miniMap',
        icon: MiniMapIcon,
    },
    {
        title: '基本信息',
        selected: 'basicInfo',
        isSubMenu: false,
        noPickMark: false,
        type:'basicInfo',
        icon: BasicInfoIcon,
    },
    {
        title: '构件搜索',
        selected: 'searching',
        isSubMenu: false,
        noPickMark: false,
        type:'searching',
        icon: SearchIcon,
    },
    {
        title: '控制树',
        selected: 'tree',
        isSubMenu: false,
        noPickMark: false,
        type:'tree',
        icon: ControlTreeIcon,
    },
    {
        title: '构件属性',
        selected: 'attributes',
        isSubMenu: false,
        noPickMark: false,
        type: 'attributes',
        icon: EntityPropertyIcon,
    },
    {
        title: '设置',
        selected: 'setting',
        isSubMenu:false,
        noPickMark: false,
        type:'setting',
        icon: SettingIcon,
    },
    {
        title: '全屏',
        selected: 'fullscreen',
        isSubMenu: false,
        noPickMark: true,
        type: 'fullscreen',
        icon: FullScreenIcon,
    }
];

export const mainMenuBlock = [
    {
        title: '主视角',
        selected: 'mainView',
        isSubMenu: false,
        noPickMark: true,
        type:'mainView',
        icon: MainViewIcon,
    },
    {
        title: '剖切',
        selected: 'section',
        isSubMenu: true,
        noPickMark: false,
        type:'section',
        icon: SectioningIcon,
    },
    {
        title: '测量',
        selected: 'measure',
        isSubMenu: false,
        noPickMark: false,
        type:'measure',
        icon: MeasureIcon,
    },
    {
        title: '漫游',
        selected: 'roaming',
        isSubMenu: false,
        noPickMark: false,
        type:'roaming',
        icon: RoamIcon,
    },
    {
        title: '小地图',
        selected: 'miniMap',
        isSubMenu:false,
        noPickMark: false,
        type:'miniMap',
        icon: MiniMapIcon,
    },
    {
        title: '控制树',
        selected: 'tree',
        isSubMenu: false,
        noPickMark: false,
        type:'tree',
        icon: ControlTreeIcon,
    },
    {
        title: '构件属性',
        selected: 'attributes',
        isSubMenu: false,
        noPickMark: false,
        type: 'attributes',
        icon: EntityPropertyIcon,
    },
];

export const H5MenuBlock = [
    {
        title: '主视角',
        selected: 'mainView',
        isSubMenu: false,
        noPickMark: true,
        type:'mainView',
        icon: MainViewIcon,
    },
    // {
    //     title: '剖切',
    //     selected: 'section',
    //     isSubMenu: true,
    //     noPickMark: false,
    //     type:'section',
    //     icon: SectioningIcon,
    // },
    // {
    //     title: '测量',
    //     selected: 'measure',
    //     isSubMenu: false,
    //     noPickMark: false,
    //     type:'measure',
    //     icon: MeasureIcon,
    // },
    {
        title: '控制树',
        selected: 'tree',
        isSubMenu: false,
        noPickMark: false,
        type:'tree',
        icon: ControlTreeIcon,
    },
    {
        title: '构件属性',
        selected: 'attributes',
        isSubMenu: false,
        noPickMark: false,
        type: 'attributes',
        icon: EntityPropertyIcon,
    },
];

export const measureBlock = [
    {
        title: '点到点',
        selected: 'dot',
        icon: MeaPointIcon,
    },
    {
        title: '净距',
        selected: 'distance',
        icon: MeaDistanceIcon,
    },
    {
        title: '长度',
        selected: 'length',
        icon: MeaLengthIcon,
    },
    {
        title: '角度',
        selected: 'angle',
        icon: MeaAngleIcon,
    },
    {
        title: '标高',
        selected: 'height',
        icon: MeaHeightIcon,
    },
    // {
    //     title: '设置',
    //     selected: 'measureSetting',
    //     icon: MeaSettingIcon,
    // },
    
];

export const sectionBlock =[
    {
        title: '剖切盒',
        selected: 'drag',
        icon: SecDragIcon,
    },
    {
        title: '平移剖切',
        selected: 'secTranslate',
        icon: SecTranslationIcon,
    },
    {
        title: '缩放剖切',
        selected: 'secScale',
        icon: SecScaleIcon,
    },
    {
        title: '旋转剖切',
        selected: 'secRotate',
        icon: SecRotateIcon,
    },
    {
        title: 'X轴剖切',
        selected: 'axisX',
        icon: SecXIcon,
    },
    {
        title: 'Y轴剖切',
        selected: 'axisY',
        icon: SecYIcon,
    },
    {
        title: 'Z轴剖切',
        selected: 'axisZ',
        icon: SecZIcon,
    },
];


export const sectionOperate=[
    {
        title:'隐藏框',
        selected:'showHide',
        icon: SecHideIcon,
        pickImg: SecShowIcon,
    },
    {
        title: '重置',
        selected: 'refresh',
        icon: SecResetIcon,
    },
    {
        title: '反转',
        selected: 'overTurn',
        icon: SecOverturnIcon,
    },
];

export const measureOperate=[
    {
        title:'观察',
        selected:'view',
        icon: MeaViewIcon,
    },
    {
        title: '取消',
        selected: 'cancel',
        icon: MeaCancelIcon,
    },
    {
        title: '结束',
        selected: 'end',
        icon: MeaEndIcon,
    },
];

export const roamBlock = [
    {
        title: '重力下落',
        selected: 'gravity',
        icon: RoamGravityIcon,
    },
    {
        title: '碰撞检测',
        selected: 'crash',
        icon: RoamCrashIcon,
    },
    {
        title: '第三人称',
        selected: 'third',
        icon: RoamPersonIcon,
    },
    {
        title: '速度',
        selected: 'speed',
        icon: RoamSpeedIcon,
    },
    {
        title: '提示',
        selected: 'help',
        icon: RoamHelpIcon,
    },
];

export const operateBlock = [
    {
        title:'放大',
        selected:'magnify',
        icon:RoamCrashIcon,
    },
    {
        title:'缩小',
        selected:'shrink',
        icon:RoamCrashIcon,
    },
    {
        title:'平移',
        selected:'translate',
        icon:RoamCrashIcon,
    },
    {
        title:'旋转',
        selected:'rotate',
        icon:RoamCrashIcon,
    }
];

export const colorsBlock = [
    {
        type: 'pure',
        label: '纯色',
        colorItems:[
            {
                color: [[255, 255, 255]],
                type: 'pure',
                key: '#FFFFFF',
                name:'纯白',
            },
            {
                color: [[15,20,35]],
                key: '#0F1423',
                type: 'pure',
                name:'幽静',
            },
            {
                color: [[51,51,51]],
                type: 'pure',
                key: '#333333',
                name:'暗昏',
            }

        ]
    },
    {
        type: 'gradient',
        label: '渐变',
        colorItems:[
            {
                color: [[171, 218, 255], [255, 255, 255]],
                key: '#ABDAFF#FFFFFF',
                type: 'gradient',
                name:'晴天',
            },
            {
                color: [[20, 37, 58], [2, 4, 6]],
                key: '#14253A#020406',
                type: 'gradient',
                name:'深夜',
            },
            {  color: [[206, 222, 220], [247,247,247]],
                key: '#CEDEDC#F7F7F7',
                type: 'gradient',
                name:'宁静',
            }

        ]
    },
    {
        type: 'flatImg',
        label: '静图',
        colorItems:[
            {
                thumb: sky,
                image: skyBG,
                key: 'sky',
                type: 'flatImg',
                name:'蓝天白云',
            },
            {
                thumb: dark,
                image: darkBG,
                key: 'dark',
                type: 'flatImg',
                name:'静谧夜空',
            },
            {
                thumb: star,
                image: starBG,
                key: 'star',
                type: 'flatImg',
                name: '璀璨星河',
            }
        ]
    },
    {
        type: 'panormaImg',
        label: '全景',
        colorItems:[
            {
                thumb: green,
                image: greenBG,
                key: 'green',
                type: 'panormaImg',
                name:'森林草丛',
            },
        ]
    },
];


// export const colorsBlock = [
//     {
//         color: [[171, 218, 255], [255, 255, 255]],
//         key: '#ABDAFF#FFFFFF',
//         gradient: true,
//     },
//     {
//         color: [[196, 215, 213], [255, 255, 255]],
//         key: '#C4D7D5#FFFFFF',
//         gradient: true,
//     },
//     {
//         color: [[255, 255, 255]],
//         key: '#FFFFFF',
//         gradient: false,
//     },
//     {
//         color: [[220, 219, 201]],
//         key: '#DCDBC9',
//         gradient: false,
//     },
//     {
//         color: [[195, 191, 217]],
//         key: '#C3BFD9',
//         gradient: false,
//     }, 
//     {
//         color: [[56, 64, 85]],
//         key: '#384055',
//         gradient: false,
//     },
//     {
//         color: [[0, 0, 0]],
//         key: '#000000',
//         gradient: false,
//     },
// ];
