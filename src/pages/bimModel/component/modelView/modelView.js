import React from 'react'; 
import { message,Progress } from 'antd';
import './modelView.less';
import Toolbar from './componment/toolbar/toolbar';
import ContextMenu from './componment/ContextMenu';
import BasicInfo from './componment/basicInfo/basicInfo';
import SearchEntity from './componment/searchEntity/SearchEntity';
import ControlTree from './componment/controlTree/controlTree';
import AttributeTab from './componment/attributeTab/attributeTab';
import ModelMeasure from './componment/modelMeasure/modelMeasure';
import ModelSetting from './componment/modelSetting/modelSetting';
// import ModelSection from './componment/modelSection/modelSection';
import ModelRoaming from './componment/modelRoaming/modelRoaming';
import ModelOption from './componment/modelOption/modelOption';
import ModelRoamTips from './componment/modelRoaming/roamTips';
import MiniMap from './componment/miniMap/miniMap';
import { ContextMenuTrigger } from "react-contextmenu";
import {WAREFRAME,REALITY} from './componment/modelSetting/modelSetting';
import { sectionOperate,sectionTypes, measureOperate} from './config/menuConfig';
import characterGeometry from './resource/roam/character.dat';
import characterImage from './resource/roam/character.png';

//构件生成树 类型

const ModelWIND = window.WIND.WIND;
const WINDModelApp = ModelWIND.MODEL.APP;
const MeasureType = ModelWIND.MODEL.MeasureType;
const SectionMode = ModelWIND.MODEL.SectionMode;
const BackgroundType = ModelWIND.MODEL.BackgroundType;

const RoamingType = ModelWIND.MODEL.RoamingType;
const LeftMouseOperation = ModelWIND.LeftMouseOperation;
const MeasureUnit = ModelWIND.UnitType;
const CallbackType = ModelWIND.CallbackType;
const GridElevationMode = ModelWIND.MODEL.GridElevationMode;
// const AxisType = window.WIND.Model.AxisType;
console.log('CallbackType',CallbackType)

class ModelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      links:[],
      config:{},
      currentApp: null,
      loading: false,
      percent: 0,
      mouse: null,
      mouseHover: {},
      showToolbar: true,
      componentParms: null,
      selectCount: null,
      sectionVisible: false,//剖切显隐
      measureVisible: false,//测量(类型)显隐
      roamVisible: false,//漫游显隐
      roamHintVisible: false,//漫游面板操作提示
      miniMapVisible: false,//小地图显隐
      settingVisible: false,//设置显隐
      searchVisible: false, //搜索层显隐
      isFullScreen: false,//是否全屏
      basicInfoVisible: false,//基础信息显隐
      sectionType: null,
      setParms: {},
      sectionAxisValue: null,
      measureValueObject: null,
      modelStats:{},
      isControl: false,
    }
    this.loadModel = this.loadModel.bind(this);
    this.updateDataNode = (type, value) => {
      //数据回调之后, 更新进度条
      switch (type) {
        //加载中
        case CallbackType.DATA_LOAD_CHANGE:
            // 模型加载百分比回调
            let percentNum = Math.min(100, Math.max(0, Math.floor(value.percent))) || 0;
            if (percentNum >= 0 && percentNum < 100) {
                this.setState({ 
                  loading: true,
                  percent: percentNum 
                });
            } 
            break;
        case CallbackType.DATA_LOAD_START:
            console.log('data load start!', value);
            break;
         //加载结束 
        case CallbackType.DATA_LOAD_FINISH:
          console.log('data load finished!', value);
          this.setState({ loading: false });
          break;
        //加载成功
        case CallbackType.DATA_LOAD_SUCCEED:
            console.log('模型加载成功：',value);
            break;
        //加载失败
        case CallbackType.DATA_LOAD_FAIL:
            console.log('模型加载失败：',value);
            return message.error(`模型${value && value.modelId}加载失败`)
        case CallbackType.MEASURE_NODE_CREATE:
            this.setState({ measureValueObject: value});
            break;
        case CallbackType.MEASURE_CONFIG_CHANGE:
            this.setState({ measureValueObject: value});
            break;
        case CallbackType.MEASURE_NODE_SELECT:
            this.setState({ measureValueObject: value});
            break;
        case CallbackType.MEASURE_NODE_CHANGE:
              this.setState({ measureValueObject: value});
              break;
        case CallbackType.MEASURE_NODE_UPDATE:
            this.setState({ measureValueObject: value});
            break;
        case CallbackType.MEASURE_NODE_CLEAR:
            this.setState({ measureValueObject: value});
            break;
        case CallbackType.MEASURE_NODE_CANCEL:
            this.setState({ measureValueObject: value});
            break;
        default:
          break;
      }
    };
    
    this.switchState = (type, value) => {
      this.setState({[type]: value})
    };

  }

  componentWillMount() {
    if (this.props.onRef) {
        this.props.onRef(this);
    }
  };

  componentDidMount() {
    const {links, config, showToolbar } = this.props;
    if(showToolbar != undefined || showToolbar != null){
      this.setState({ showToolbar })
    }
    this.setState({ links, config },()=>{
      this.loadModel();
    });
  };

  componentWillReceiveProps(nextProps){
    const {config, links, showToolbar} = this.props;
    if(nextProps.showToolbar != showToolbar){
      this.setState({ showToolbar: nextProps.showToolbar})
    }
    if(((nextProps.config && nextProps.config.token) !== (config && config.token)) || (nextProps.links !== links)){
      this.setState({
        links: nextProps.links,
        config: nextProps.config,
      },()=>this.loadModel())
    }
  };

  componentWillUnmount() {
    const {currentApp} =this.state;
    if (currentApp) {
        currentApp.data().closeAllModelDatas();
    }
  };

  //加载模型
  async loadModel() {
    const { currentApp, config} = this.state;
    if (currentApp) {
      currentApp.data().closeAllModelDatas();
    }

    if(config && config.token){
      this.setState({ loading: true})
      //WINDView初始化
      let canvas = document.querySelector('#canvasView');
      let modelApp = new WINDModelApp(canvas);

      await modelApp.connect(config);
      //设置漫游小人
      await modelApp.action().setCharacterResources(characterGeometry, characterImage);

      //数据加载执行回调
      modelApp.data().addCallback(1,  (type, option) => this.updateDataNode(type, option));


      //设置模型视图背景颜色
      const COLOR = [[171, 218, 255], [255, 255, 255]];
      let bgConfig = {
          gradientStartRGB255: COLOR[0],
          gradientEndRGB255: COLOR[1],
          gradientDirectionType: 0, //1/2/3/4
      }
      modelApp.effect().setBackgroundType(BackgroundType.GRADIENT_COLOR);
      modelApp.effect().configBackground(bgConfig);
      
      //开启导航辅助
      let navConfig = {
         rightMargin: 0,//距离右边的距离
         topMargin: 0, //距离顶部的距离
         viewSize: 120, //尺寸，正方形
      }
      //modelApp.toolkit().setNavigationImages(front, back, top, bottom, left, right);
      modelApp.toolkit().configNavigation(navConfig);
      modelApp.toolkit().openNavigation();

      // // 获取模型效果状态
      let modelState = modelApp.effect().getSubjectConfig();
      // 默认打开真实
      if(!modelState.isRealisticOpened){
         modelApp.effect().toggleRealistic(true);
      }
      // 默认打开线框
      if(!modelState.isWireframeOpened){
         modelApp.effect().toggleWireframe(true);
      }
     
      //模型渲染
      modelApp.startFrame();
      this.setState({
        currentApp: modelApp,
        //设置项初始化数据
        setParms:{
          basicDisplay:[ WAREFRAME, REALITY], //基础展示
          bgColor: '#ABDAFF#FFFFFF',//背景色
          isOrthogonal: false,  //正交透视
          lightVisible: false,  //关照阴影
          ambientIntensity: 0.8,//环境光强度
          diffuseIntensity: 0.5,//散射光强度
          opacity: 26, //透明度
          frameLimitRatecity: 50 //流畅度
        }
      });
      this.openModelData(modelApp);
      this.bindEvent(this);
    }
  };

  //打开模型
  async openModelData(modelApp) {
    const {links} = this.state;
    if (links && links.length) {
      //打开模型
      await modelApp.data().openModelDatas(links);
      //获取模型基本信息数据
      let modelStats = await modelApp.data().getModelStats();
      this.setState({ modelStats });
    }
  };

  // 模型事件处理
  bindEvent = (context) =>{
    let canvasDiv = document.querySelector('#modelView');
    if (canvasDiv) {
        let startTime;
        let endTime;
        // 右键菜单
        canvasDiv.addEventListener('contextmenu', e => {
            e.preventDefault();
            endTime = Date.now();
            context.selectJuging();
        });

        canvasDiv.addEventListener('click', function (e) {
            context.selectJuging();
        }, false);

        //移动端touchend 触摸监听
        canvasDiv.addEventListener('touchend', function (e) {
           e.preventDefault();
           context.selectJuging();
        }, false);

        canvasDiv.addEventListener('mousedown', e => {
            startTime = Date.now();
            if (e.button === 2) {
            }
        });
        // 苹果系统快速点击右键，不会触发mouseup
        canvasDiv.addEventListener('mouseup', e => {
            endTime = Date.now();
            if (e.button === 2) {
                if (endTime - startTime > 130) {
                    context.setState({
                        mouse: 'drag'
                    })
                } else {
                    context.setState({
                        mouse: undefined
                    })
                }
            } 
        });
    }
  };

  selectJuging = async() => {
    let { currentApp } = this.state;
    if(currentApp){
      let selectCount = currentApp.action().getSelectionCount();
      let componentParms = await currentApp.action().grabSelection();

      this.setState({ selectCount, componentParms})
    }
  };

  // 开启关闭剖切盒
  switchCube() {
    let { sectionVisible, currentApp, mouseHover } = this.state;
    let state = currentApp.action().getSectionConfig();
    if (state.opened) {
      currentApp.action().resetSection();
      currentApp.action().closeSection();
      let sections = {};
      sectionTypes.map(item => {
        sections = Object.assign(mouseHover, { [item]: false })
      })
      this.setState({
         mouseHover: sections
      })
    } else {
      currentApp.action().openSection();
      currentApp.action().setSectionMode(SectionMode.CUBE_SURROUND);
      this.setState({
        mouseHover: Object.assign(mouseHover, { 
          ['drag']: true }),
      })
    }
    this.setState({
      mouseHover: Object.assign(this.state.mouseHover, { ['section']: !sectionVisible }),
      sectionVisible: !sectionVisible,
    });
  };

  //点击选择剖切
  onClickSection = (key) => {
    let { currentApp, mouseHover } = this.state;
    let sections = {};
    sectionTypes.filter(item => {
      if (item !== key) {
        sections = Object.assign(mouseHover, { [item]: false })
      }else{
        sections = Object.assign(mouseHover, { [item]: true })
      }
    })
    this.setState({mouseHover: sections})
    switch (key) {
      case 'drag':
         currentApp.action().setSectionMode(SectionMode.CUBE_SURROUND)
          break;
      case 'axisX': 
          currentApp.action().resetSection();
          currentApp.action().setSectionMode(SectionMode.PLANE_X);
          break;
      case 'axisY': 
          currentApp.action().resetSection();
          currentApp.action().setSectionMode(SectionMode.PLANE_Y);
          break;
      case 'axisZ': 
          currentApp.action().resetSection();
          currentApp.action().setSectionMode(SectionMode.PLANE_Z);
          break;
      case 'secTranslate': 
          currentApp.action().resetSection();
          currentApp.action().setSectionMode(SectionMode.CUBE_MOVEMENT);
          break;
      case 'secScale': 
          currentApp.action().resetSection();
          currentApp.action().setSectionMode(SectionMode.CUBE_SCALING);
          break;
      case 'secRotate': 
          currentApp.action().resetSection();
          currentApp.action().setSectionMode(SectionMode.CUBE_ROTATION);
          break;
      default:
          break;
    }
  };

  //点击侧边栏剖切操作
  onSideOperate = (key)=>{
    let { currentApp, mouseHover } = this.state;
    let state = currentApp.create().getMeasureConfig();
    switch (key) {
      case 'showHide':
          currentApp.action().displaySection();
          this.setState({
            mouseHover: Object.assign(mouseHover, { [key]: !mouseHover[key] })
          })
          break;
      case 'refresh':
          currentApp.action().resetSection();
          break;
      case 'overTurn':
          currentApp.action().flipSection();
          break;
      default:
          break;
    }

  };

  //点击侧边栏测量操作
  onClickMeasure = (key) => {
    let { currentApp, mouseHover } = this.state;
   
    switch (key) {
      case 'view': 
          currentApp.create().observeMeasure();
          this.setState({
            mouseHover: Object.assign(mouseHover, { [key]: !mouseHover[key] })
          })
          break;
      case 'cancel':
          currentApp.create().cancelMeasure();
          break;
      case 'end':
          currentApp.create().finishMeasure();
          break;
      default:
          break;
    }
  };

  //开启关闭测量
  switchMeasure = () => {
    let { currentApp, measureVisible } = this.state;
    let measureState = currentApp.create().getMeasureConfig();//获取测量状态
    if (measureState.opened) {
      currentApp.create().closeMeasure();
      currentApp.create().removeCallback(210);
    } else {
      currentApp.create().openMeasure();
      currentApp.create().setMeasureType(MeasureType.DOT);
      currentApp.create().addCallback(210, (type, option) => this.updateDataNode(type, option));
      this.setState({
        mouseHover: Object.assign(this.state.mouseHover, { ['dot']: true }),
      })
    }
    this.setState({
      measureValueObject: null,
      mouseHover: Object.assign(this.state.mouseHover, { ['measure']: !measureVisible }),
      measureVisible: !measureVisible,
    });
  };

  //开启关闭漫游
  switchRoam = () => {
    let { currentApp, roamVisible, mouseHover } = this.state;
    if (roamVisible) {
      currentApp.action().setRoamingType(RoamingType.ORBIT);
    } else {
      currentApp.action().setRoamingType(RoamingType.PERSON);
    }
    this.setState({
      roamVisible: !roamVisible,
      roamHintVisible: !roamVisible,
      mouseHover: Object.assign(mouseHover, { ['help']: !roamVisible})
    })
  };

  //开启关闭小地图
  switchMap = () => {
    const { mouseHover, currentApp, miniMapVisible } = this.state;
    this.setState({
        miniMapVisible: !miniMapVisible,
        mouseHover: Object.assign(mouseHover, { ['miniMap']: !miniMapVisible})
    })
  }; 
   
  //返回主视角(复位相机)
  mainView = async () => {
    const { currentApp } = this.state;
    currentApp.action().homeRoaming();
  };

  //切换全屏
  switchFullScreen = () => {
    const { currentApp, isFullScreen,mouseHover } = this.state;
    let element = document.getElementById("modelView-wrapper");
    if (!isFullScreen) {
      currentApp.screenFull(element);
      this.switchState('mouseHover', Object.assign(mouseHover, { ['fullScreen']: true}));
    } else {
       currentApp.screenFull(element);
       this.switchState('mouseHover', Object.assign(mouseHover, { ['fullScreen']: false}));
    }
  };

  //开启关闭构件属性
  switchAttributes = async() =>{
    let { attributeVisible, mouseHover, componentParms } =this.state;
    if(componentParms){
      this.setState({
        componentParms,
        attributeVisible: !attributeVisible,
      })
    }else{ 
      this.setState({
        mouseHover: Object.assign(mouseHover, { ['attributes']: false }),
        attributeVisible: false,
        componentParms: null
      },()=> message.warning('请选择构件~'))
    }
  };

  //toolbar点击事件
  selectMenu = (funcName) => {
    let {mouseHover, propertyVisible, searchVisible, 
      settingVisible, basicInfoVisible, miniMapVisible} = this.state;
    this.setState({
       mouseHover: Object.assign(mouseHover, { [funcName]: !mouseHover[funcName] })
    });
    switch (funcName) {
      case 'mainView':
        this.mainView();
        break;
      case 'section':
        this.switchCube();
        break;
      case 'measure':
        this.switchMeasure();
        break;
      case 'roaming':
        this.switchRoam();
        break;
      case 'miniMap':
        this.switchState('miniMapVisible', !miniMapVisible);
        break;
      case 'searching':
        this.switchState('searchVisible', !searchVisible);
        break;
      case 'tree':
        this.switchState('propertyVisible', !propertyVisible);
        break;
      case 'attributes':
        this.switchAttributes();
        break;
      case 'basicInfo':
        this.switchState('basicInfoVisible', !basicInfoVisible);
        break;
      case 'setting':
        this.switchState('settingVisible', !settingVisible);
        break;
      case 'fullscreen':
        this.switchFullScreen();
        break;
      default:
        this.mainView()
        break;
    }
  };

  //创建图钉

  render() {
    let { 
          currentApp,  links, isPC, mouse, loading, percent, mouseHover, measureValueObject,
          miniMapVisible,searchVisible,propertyVisible,attributeVisible,roamHintVisible, isControl,
          measureVisible, basicInfoVisible, modelStats, settingVisible, sectionVisible, roamVisible, 
          setParms, selectCount, componentParms, showToolbar,
        } = this.state;
    return (
      <div className="modelView-wrapper" id="modelView-wrapper">
            <ContextMenuTrigger id="some_unique_identifier" disable={mouse && mouse === 'drag'} >
              <div className="modelView" id='modelView'>
                {
                  loading &&
                  <div className="modelView-loading-process">
                      <Progress type="circle" percent={percent}/> 
                  </div>
                } 
                 <canvas id='canvasView' style={{ position: 'absolute', width: '100%', height: '100%' }}></canvas>
              </div>
            </ContextMenuTrigger> 
          <ContextMenu app={currentApp} 
                       mouse={mouse} 
                       isControl={isControl}
                       selectCount={selectCount}
                       isPushpin={this.props.isPushpin}
                       handleCreatePushpin={this.props.handleCreatePushpin}
                       handleAttribute = {this.switchAttributes}
          />
          <div className="modelView-menu">
            {
              showToolbar && 
              <Toolbar click={this.selectMenu} 
                        mouseHover={mouseHover} 
                        handleSection={this.onClickSection}
                        handleRoam={this.onClickRoam}
                        roamVisible = {roamVisible}
                        mainMenu = {this.props.mainMenu}
                        isPC = {isPC}
                />
            }
             
          </div>
          <div className='side-center'>
            {
              //基础信息
              basicInfoVisible &&
              <BasicInfo  visible = {basicInfoVisible}
                          modelStats={modelStats}
                          onCancel = {()=> this.setState({ 
                            basicInfoVisible: false,
                            mouseHover: Object.assign(this.state.mouseHover, { ['basicInfo']: false })
                          })}
              />

            }
            {
              //设置
              settingVisible &&
                <ModelSetting app = {currentApp} 
                              visible = {settingVisible}
                              setParms = {setParms}
                              switchState = {this.switchState}
                              BackgroundType ={BackgroundType}
                              GridElevationMode={GridElevationMode}
                              LeftMouseOperation={LeftMouseOperation}
                              onCancel = {()=> this.setState({ 
                                settingVisible: false,
                                mouseHover: Object.assign(this.state.mouseHover, { ['setting']: false })
                              })}   
                />
            }
            {
              //构件搜索
              searchVisible && 
              <SearchEntity  switchState = {this.switchState} 
                             links={links}
                             app={currentApp}
                             onClose = {()=> this.setState({ 
                              searchVisible: false,
                              mouseHover: Object.assign(this.state.mouseHover, { ['searching']: false })
                            })}
              />
            }
            {
              //漫游提示
              roamHintVisible &&
              <ModelRoamTips  onClose = {()=>
                                 this.setState({
                                  roamHintVisible: false,
                                  mouseHover: Object.assign(this.state.mouseHover, { ['help']: false })
                                })
                              }
              />
            }
            
          </div>
          <div className='side-left'>
             {
               //控制树
                propertyVisible &&
                <ControlTree  app={currentApp}
                              CallbackType={CallbackType}
                />
              }
             {
               //小地图
                miniMapVisible && 
                <MiniMap currentApp={currentApp}
                         miniMapVisible={miniMapVisible}
                         switchState={this.switchState}
                         mouseHover={mouseHover}
                         CallbackType={CallbackType}
                         models={links}
                />
             }
          </div>
          <div className="side-right">
            {
                //构件详情
                attributeVisible && componentParms && 
                <AttributeTab  onCancel={() => this.setState({ 
                                   attributeVisible: !attributeVisible,
                                   mouseHover: Object.assign(this.state.mouseHover, { ['attributes']: false })
                                })}
                                hasDrawer={this.props.hasDrawer}
                                componentParms ={componentParms}
                />
            }
          </div>
          <div className="side-rightBox">
            {/* {
              //剖切
              sectionVisible && sectionType
              && <ModelSection  options={sectionOperate}
                                AxisType={AxisType}
                                currentApp={currentApp}
                                switchState={this.switchState}
                                sectionTypes={sectionTypes}
                                click={this.onClickSection} 
                                mouseHover={mouseHover}
                                sectionAxisValue={this.state.sectionAxisValue}
                                sectionType={sectionType}
              />
            } */}
            {
              //测量
              measureVisible  
              && <ModelMeasure currentApp={currentApp}
                               measureVisible={measureVisible}
                               mouseHover={mouseHover}
                               MeasureType={MeasureType}
                               MeasureUnit={MeasureUnit}
                               switchState={this.switchState}
                               measureValueObject={measureValueObject}
                               CallbackType={CallbackType}
                />

            }
            {
              //漫游操作
              roamVisible 
              && <ModelRoaming app={currentApp}
                               mouseHover={mouseHover}
                               switchState={this.switchState}

                 />
            }
          </div>
          <div className="side-rightCenter">
          {
              //剖切侧边栏操作
              sectionVisible
              && <ModelOption  options={ 
                                  mouseHover['drag'] ? 
                                  sectionOperate : 
                                  sectionOperate.filter(i =>i.selected !== 'overTurn' )
                                }
                                sectionTypes={sectionTypes}
                                click={this.onSideOperate} 
                                mouseHover={mouseHover}
              />
            }
            {
               //测量侧边栏操作
               measureVisible  
               && <ModelOption  options={ 
                                   mouseHover['length'] ? 
                                   measureOperate : 
                                   measureOperate.filter(i =>i.selected !== 'end')
                                } 
                                click={this.onClickMeasure}
                                mouseHover={mouseHover}
                  />
            }
          </div>
      </div>
    )
  }

}

export default ModelView;