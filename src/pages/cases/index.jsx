import React, { Component } from 'react';
import {Tabs} from 'antd';
import './cases.less';
import store from "../../store/index";
import { FOOTER_VISIBLE } from '../../router/router.reducer';
import { cases } from '../../common/config/aboutJson';
import Header from '../../common/component/Header';
import Footer from '../../common/component/Footer';
import Banner from '../../common/component/Banner';

const { TabPane } = Tabs;
export default class Cases extends Component {
  
  componentDidMount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: true,
    });
  };

  componentWillUnmount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: false,
    });
  };
  render() {
  const tabLayout = (tabData)=>{
    return(
        <Tabs defaultActiveKey="1" className={`case-tab`}>
        {
            (tabData || []).map(item => {
            return (<TabPane  key={item.key} tab={item.tab}>{ casesLayout(item.content) }</TabPane>)})
        } 
        </Tabs> 
    )
  }

  const casesLayout = (cases) =>{
    return(
      <ul className="case-list w1200">
      {
        cases.map((item, index) => {
          return (
            <li className="item" key={index}>
                <img className="img" src={ item.imgHref } alt=""/>
                <div className="img-bg"></div>
                <div className="title"><p>{ item.title }</p></div>
                <div className="detail">
                    <p>{ item.detail.cooperationUnit }</p>
                    <p>{ item.detail.applicationFunction }</p>
                    <p>{ item.detail.applicationValue }</p>
                </div>
            </li>)
        })
      }
      </ul>
    )
}
  return (
      <div className="cases-page">
      < Header/>
      <div className="cases main"> 
        <Banner page={'cases'}/>
        <div className="caseItems">
            <h2 className="con-title">{ cases.title }</h2>
            <p className="con-caption">{ cases.desc }</p>
            { tabLayout(cases.data) }  
        </div>
    </div>
    <Footer/>
  </div>
  )
  }
}

