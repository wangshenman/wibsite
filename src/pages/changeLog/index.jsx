import React, { Component } from 'react';
import { Icon, Pagination} from 'antd';
import { FOOTER_VISIBLE } from '../../router/router.reducer';
import store from "../../store/index";
import Header from '../../common/component/Header';
import Footer from '../../common/component/Footer';
import Empty from '../../common/component/Empty';
import LogImg from '../../common/img/log/log_img.png';
import { getChangeLog, getChangeLogCount} from '../../common/utils/commonActions';
import moment from "moment";
import './changeLog.less';

export default class ChangeLog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      logList: [],
      pagination: { //分页
        current: 1,
        pageSize: 5,
        total: null,
      },
    }
    this.getLogList = ()=>{
      const { pagination } = this.state;
      Promise.all([getChangeLog(pagination.current-1, pagination.pageSize),getChangeLogCount()]).then(([data,res])=>{
        if(data){
          pagination.total = res && res.count;
          data.map(item => Object.assign(item, { isShowMore: false }))
          this.setState({
             logList: data,
             pagination
          })
        }
      })
      
    };
    this.handlePagination = (current) =>{
      const {pagination} = this.state;
      pagination.current = current
      this.setState({
        pagination
      }, () => this.getLogList())
    };

    this.handleChangeMore =(record)=>{
      const { logList } = this.state;
      logList.map(item =>{
        if(item._id === record._id){
          item.isShowMore = !record.isShowMore
        }
      })
      this.setState({
        logList
      })
    };
  }

  componentDidMount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: true,
    });
    this.getLogList();
  };

  componentWillUnmount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: false,
    });
  };
 
  render() {
    const { logList, pagination } = this.state;
    const LogItem = (logData)=>{
      return (
        <div className="log-container" 
             key={`${logData._id}`}
             style={{'height': `${(logData && logData.isShowMore) ? 'auto' : '440px'}`}}
        >
          <div className="log-header">
            <h3 className="log-title">{ logData && logData.name }</h3>
            <span className="log-date">{ logData && logData.updatedAt ? moment(logData.updatedAt).format("YYYY-MM-DD") : '-'}</span>
            <hr className="log-line"/>
          </div>
          <div className="log-content">
            <div className="log-show">
            <div className="log-text"
                  style={{'height': `${(logData && logData.isShowMore) ? 'auto' : '280px'}`}}
                  dangerouslySetInnerHTML = {{ __html: logData && logData.desc }}>
            </div>
            {
                  
                  <div className="show-more" onClick={() => this.handleChangeMore(logData)}>
                    <span className="show-more-text">查看详情
                    {
                      logData && logData.isShowMore ? <Icon className="show-more-button" type="up" /> : <Icon className="show-more-button" type="down" />
                    }
                    </span>
                  </div>
            }
            </div>
            <img className="log-image"src={LogImg}  alt="" />
          </div>
                  
        </div>
      )
    }
    return (
      <div className="changeLog-page">
        < Header/>
        <div className="changeLog main">
          <div className="changeLog-list">
            {
               logList.map((item,index)=>{ return LogItem(item) })
            }   
          </div>
          {
            logList.length ?
            <div className="log-pagination">
              <Pagination defaultCurrent={1} 
                          total={ pagination.total } 
                          defaultPageSize={pagination.pageSize} 
                          onChange={this.handlePagination}
              />
            </div>
             : 
            <Empty/>
          }
        </div>
        <Footer/>
      </div>)  
  }
}

