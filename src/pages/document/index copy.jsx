import React, { Component } from 'react';
import { Button } from 'antd';
import { FOOTER_VISIBLE } from '../../router/router.reducer';
import store from "../../store/index";
import Header from '../../common/component/Header';
import Footer from '../../common/component/Footer';
import ReactMarkdown from "react-markdown";
import gfm from 'remark-gfm';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { dark } from 'react-syntax-highlighter/dist/esm/styles/prism';

//import markdownJson from '../../common/config/document.md';
import './document.less';

const markdown1 = `A paragraph with *emphasis* and **strong importance**.

> A block quote with ~strikethrough~ and a URL: https://reactjs.org.

* Lists
* [ ] todo
* [x] done

A table:

| a | b |
| - | - |
`

let markdown2 ='# P01:课程介绍和环境搭建\n' +
  '[ **M** ] arkdown + E [ **ditor** ] = **Mditor**  \n' +
  '> Mditor 是一个简洁、易于集成、方便扩展、期望舒服的编写 markdown 的编辑器，仅此而已... \n\n' +
   '**这是加粗的文字**\n\n' +
  '*这是倾斜的文字*`\n\n' +
  '***这是斜体加粗的文字***\n\n' +
  '~~这是加删除线的文字~~ \n\n'+
  '\`console.log(111)\` \n\n'+
  '# p02:来个Hello World 初始Vue3.0\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n'+
  '***\n\n\n' +
  '# p03:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '# p04:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '#5 p05:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '# p06:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '# p07:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '```var a=11; ```';

  const components = {
    code({ node, inline, className, children, ...props }) {
        const match = /language-(\w+)/.exec(className || '')
        return !inline && match ? (
            <SyntaxHighlighter style={ dark } language={ match[1] } PreTag="div" children={ String(children).replace(/\n$/, '') } { ...props } />
        ) : (
            <code className={ className } { ...props } />
        )
    },
    h3(props) { 
        console.log(props)
        return <h3 style={ { color: 'red' } } { ...props } /> 
    }
  };

export default class Document extends Component {
  constructor(props) {
    super(props)
    this.state = {
     
    }
   
  }
  componentDidMount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: true,
    });
  };

  componentWillUnmount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: false,
    });
  };
 
  render() {
    return (
      <div className="document-page">
        < Header/>
        <div className="document main">
          <ReactMarkdown children={markdown1} 
                         remarkPlugins={[gfm]}
                         components={components}
          />
        </div>
        <Footer/>
      </div>)  
  }
}

