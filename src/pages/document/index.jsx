import React, { Component } from 'react';
import { Button } from 'antd';
import { FOOTER_VISIBLE } from '../../router/router.reducer';
import store from "../../store/index";
import Header from '../../common/component/Header';
import Footer from '../../common/component/Footer';

import './document.less';


export default class Document extends Component {
  constructor(props) {
    super(props)
    this.state = {
     
    }
   
  }
  componentDidMount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: true,
    });
  };

  componentWillUnmount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: false,
    });
  };
 
  render() {
    return (
      <div className="document-page">
        < Header/>
        <div className="document main">
          
        </div>
        <Footer/>
      </div>)  
  }
}

