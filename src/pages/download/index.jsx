import React, { Component } from 'react';
import { Tabs, Button } from 'antd';
import './download.less';
import QRCode from 'qrcode.react';
import store from "../../store/index";
import { FOOTER_VISIBLE } from '../../router/router.reducer';
import Banner from '../../common/component/Banner';
import Header from '../../common/component/Header';
import Footer from '../../common/component/Footer';
import { baseURL,consoleURL } from '../../common/config/websiteConfig';
import { getCategory, getPackages, getQrCodeById } from '../../common/utils/commonActions';
import Empty from '../../common/component/Empty';
import moment from "moment";

const { TabPane } = Tabs;

export default class Download extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tabs: [],
      activeKey:'',
      packageData: [], //pc端
    };

    this.getDownloadTabs = (type) =>{
      getCategory().then(result =>{
        this.setState({
          loading: false,
          tabs: result
        }, () => {
          if(type){
              let packs;
              if(type === 'iOS'){
                  packs = result.find(item => item.name === 'WEB');
                  packs && this.onSelectPackage(packs? packs._id : '');
              }else{
                  packs = result.find(item => item.name === 'Android')
                  packs && this.onSelectPackage(packs? packs._id : '');
              }
          }else{
            this.onSelectPackage((result.length > 0)?result[0]._id:'');
          }  
        })
      }).catch(err => console.log(err && err.meassge))
    };

    //切换面板
    this.onSelectPackage = (key) => {
      this.setState({ activeKey: key }, () => {
          if (key) {
              getPackages(key).then(result => {
                      this.setState({
                        packageData: result,
                      },()=>this.getQrCodeImg(result))
                   
              }).catch(error => {
                    this.setState({
                        packageData: null
                    })
              })
          }
      });
    };

    //获取二维码
    this.getQrCodeImg = (packages)=>{
      (packages || []).map(item =>{
        if(item.isMobile){
          getQrCodeById(item._id).then(res =>{
            let qrCode = res && res.data;
            this.setState({
              packageData: Object.assign(this.state.packageData,{qrCode})
            })
          })
        }
      })
    };
    
    this.getUserAgent = () => {
      let u = navigator.userAgent;
      let isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
      let isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
      if(isAndroid){
         this.getDownloadTabs('Android');
      }else{
         this.getDownloadTabs('iOS');
      }
    };

    //文件包下载
    this.onDownload = (attachmentUrl) => {
      if(attachmentUrl){
        let downloadUrl = `${consoleURL}/api/engine/download/${attachmentUrl}`;
        window.open(downloadUrl);
      }
    };
    
    //打包下载
    this.onBatchDownload = ()=>{
      const { activeKey } = this.state;
      console.log('下载地址',`${consoleURL}/api/category/${activeKey}/download`)
      if(activeKey){
        let downloadUrl = `${consoleURL}/api/category/${activeKey}/download`;
        window.open(downloadUrl);
      }
    };
  }


  componentDidMount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: true,
    });

    if((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
      this.getUserAgent();
    }else {
      this.getDownloadTabs(null);
    }
  };

  componentWillUnmount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: false,
    });
  };

  render() {
    const{ downloadURL, title, tabs, packageData, activeKey } = this.state;
    let appPackge = packageData.find(item => !item.isMobile) || null;
    return (
    <div className="download-page">
    < Header/>
        <div className='download main'>
          <div className='download-wrapper'>
              <Banner page={'download'}/>
              <div className='download-container'>
                  <div className="download-tab"> 
                      <Tabs onChange={(key)=>this.onSelectPackage(key)}>
                      {
                        (tabs || []).map(item => {
                          return (<TabPane tab={item.name} key={item._id}>
                            <ul className="tab-ul w1200">
                              {
                                (packageData && packageData.length) ?
                                (packageData).map((i, index) => {
                                  return (<li className={`tab-li ${!i.isMobile ? '': 'tab-qrcode'}`} key={index}>
                                    <div className="tab-container">
                                      <div className="tab-item flex-center"><img className='tab-img' src={`${i.picUrl}`} alt="" /></div>
                                      <h3 className='tab-title'> {i.typeName} </h3>
                                      <span className='tab-desc'> {i.desc} </span>
                                      <div className={`tab-down ${!i.isMobile ? 'downloadTxt': 'qrcodeTxt'}`} onClick={() =>  !i.isMobile && this.onDownload(i.attachmentUrl)}><span>{ i.isMobile ?'扫码下载':'下载体验'}</span></div>
                                      <div className="tab-version flex-between"><span>{i.version ? `V${i.version}`:''}</span><span>{ i.updatedAt ? moment(i.updatedAt).format("YYYY-MM-DD"): '-'}</span></div>
                                    </div>
                                    <div className="qrcode-container">
                                       <img className='qrcode' style={{width: '100%'}} src={i.qrCode}/>
                                       {/* <QRCode size={210} className='qrcode' value={`${baseURL}download/${i.docName}`}/>  */}
                                       <div className='qrcode-text'>扫二维码下载体验</div>
                                    </div>
                                  </li>)
                                })
                                :
                                <Empty />
                              }
                            </ul>
                          </TabPane>)
                        })
                      }
                    </Tabs>
                    {
                      packageData && packageData.length && <div className="download-all flex-center"><Button className="all-btn" onClick={this.onBatchDownload}>打包下载</Button></div>
                    }
                    
                  </div>
                  {
                    appPackge && 
                    <div className="download-card">
                      <h3>{ title }</h3>
                      <div className='card-wrapper'>
                          <img className='tab-img' style={{width: '100%'}} src={appPackge.qrCode}/>
                          <h3 className='tab-title'> { appPackge.typeName } </h3>
                          <span className='tab-desc'>{ appPackge.desc } </span>
                          <div className={`tab-down`} ><span>扫上方二维码下载</span></div>
                          <div className="tab-version flex-between"><span>{appPackge.version ? `V${appPackge.version}`:''}</span><span>{ appPackge.updatedAt ? moment(appPackge.updatedAt).format("YYYY-MM-DD"): '-'}</span></div>
                      </div>
                    </div>
                  }
                  
              </div>
          </div>
        </div>
      <Footer/>
    </div>
    )
  }
}