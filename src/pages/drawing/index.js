//用于 oda 测试工具 （ PC 嵌套图纸打开页面）
import React from 'react';
import { Progress, message,} from 'antd';
import './drawing.less';
import { getURLValueByKey } from '../../common/utils/Utility';
import { modelConfig} from '../../common/config/websiteConfig';


const DrawingWIND = window.WIND.WIND;
const WINDDrawingApp = DrawingWIND.DRAWING.APP;


class Drawing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      percent: 0,
      mouse: undefined,
      currentApp: null,
      links: [],
      token: null
    }
    this.loadModel = this.loadModel.bind(this);
    this.initLoadModel = (links, token)=>{
      this.setState({
        links,
        token
      },()=>this.loadModel())
    };
    // 注册PC监听方法
    this.onLoad = () => {
      if (typeof(QCefClient) == 'undefined') {
          return;
      }
    };

  }

  componentDidMount() {
    this.onLoad();
    //获取 token
    let modelToken = getURLValueByKey('token');
    let modelId = getURLValueByKey('modelId');
    let version = getURLValueByKey('version');
    if(modelToken && modelId ){
      this.initLoadModel([{id: modelId, version: version}],modelToken);
    }else{
      return message.error('获取图纸信息失败~')
    }
  };
  componentWillUnmount() {
    const { currentApp } =this.state;
    if (currentApp) {
      currentApp.data().closeAllDrawingDatas();
    }
  }

  //加载模型
  async loadModel() {
    const { currentApp, token} = this.state;
    if (currentApp) {
      currentApp.data().closeAllDrawingDatas();
    }
    //WINDData初始化
    let config, drawingApp;
   
    //WINDView初始化
    let canvas = document.querySelector('#drawingView');
    drawingApp = new WINDDrawingApp(canvas);

    //模型服务器设置
    config = modelConfig;

    await drawingApp.connect({serverIp: config.serverIp, token: token});

    this.setState({
      loading: false,
      percent: 0,
    });

    // 数据加载执行回调
    //drawingApp.data().addCallback(1,  (type, option) => this.updateDataNode(type, option));

    //图纸渲染
    drawingApp.startFrame();

    this.setState({
      currentApp: drawingApp,
    });

    this.openModelData(drawingApp);
    this.bindEvent(this);
    
  };


  //打开模型

  async openModelData(drawingApp) {
    const { links } = this.state;
    if (links && links.length) {
      let isOpenDrawingSuccess = await drawingApp.data().openDrawingData(links[0].id, links[0].version);
      //模型打开状态通知pc端
      if (typeof(QCefClient) !== 'undefined') {
        let query = {
            request: "OpenedToNotifyPC##" + JSON.stringify({"id": links[0].id,"version":links[0].version,"ret": isOpenDrawingSuccess ? true : false}),
            onSuccess: function(response) {},
            onFailure: function(error_code, error_message) {}
        };
        window.QCefQuery(query);
      }

      if(!isOpenDrawingSuccess){
         return message.error('图纸打开失败');
      } 
    }
  };
  

  // 事件处理
  bindEvent = (context) => {
    let canvasDiv = document.querySelector('.lay-wrap');
    let tabLayer = document.querySelector('.layout');
   
    if (canvasDiv) {
      let startTime;
      let endTime;
      tabLayer.addEventListener('contextmenu', e => {
        e.preventDefault();
      });

      canvasDiv.addEventListener('contextmenu', e => {
        e.preventDefault();
        context.selectJuging();
      });

      canvasDiv.addEventListener('click', function (e) {
        context.selectJuging();
      }, false);

       //移动端touchend 触摸监听
      canvasDiv.addEventListener('touchend', function (e) {
        e.preventDefault();
        context.selectJuging();
      }, false);

      canvasDiv.addEventListener('mousedown', e => {
        startTime = Date.now();
        if (e.button === 2) {
            context.setState({
                //  mouse: 'drag',
                operateRight:false
            })
        }
      });
    
      canvasDiv.addEventListener('mouseup', e => {
          endTime = Date.now();
          if (e.button === 2) {
              if (endTime - startTime > 200) {
                  context.setState({
                      // mouse: 'drag',
                      operateRight:false
                  })
              } else {
                  context.setState({
                      mouse: undefined,
                      operateRight:true
                  })
              }
          }
      });

    }
  }

  switchState = (type, value) => {
    this.setState({[type]: value})
  };

   // 监听事件
  watchFullScreen = () => {
    const _self = this;
    document.addEventListener(
        "fullscreenchange",
        function () {
            _self.setState({
                isFullScreen: document.fullscreen
            });
        },
        false
    );
    document.addEventListener(
        "mozfullscreenchange",
        function () {
            _self.setState({
                isFullScreen: document.mozFullScreen
            });
        },
        false
    );
    document.addEventListener(
        "webkitfullscreenchange",
        function () {
            _self.setState({
                isFullScreen: document.webkitIsFullScreen
            });
        },
        false
    );
  };

  

  
  render() {
    let { loading, percent,} = this.state;
    return (
        <div className="drawingTest">
              <div className="layout">
                { loading ?  (<div className="layout-loading-process"><Progress type="circle"  style={{color:'#fff'}} percent={percent}/></div> ): ''}
                <canvas id='drawingView' style={{ position: 'absolute', width: '100%', height: '100%' }}></canvas>
              </div>
       </div>
    )
  }

}

export default Drawing;