import React, { Component } from 'react';
import { Button } from 'antd';
import { FOOTER_VISIBLE } from '../../router/router.reducer';
import store from "../../store/index";
import Header from '../../common/component/Header';
import Footer from '../../common/component/Footer';
import Banner from '../../common/component/Banner';
import { modelArray } from '../../common/config/homeJson';
import { openWindow } from '../../common/utils/Utility';
import './example.less';
export default class Example extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modelList: [],
  }
    //单个打开模型
    this.onModelClick = (record) =>{
      let pathname = "";
      let type = record && record.type;
      if(type.indexOf('DWG') !== -1){
         pathname = `/drawing?modelId=${record.modelId}&version=${record.version}&from=example`;
      }else{
         pathname = `/model?modelId=${record.modelId}&from=example`;
      }
      openWindow(pathname);
    };

    this.goConsole = ()=>{
      openWindow('/developer');
    };

  }

  componentDidMount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: true,
    });
    // this.init();
  };

  componentWillUnmount() {
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: false,
    });
  };
  render() {
    // const { modelList } = this.state;
    return (
      <div className="example-page">
        < Header/>
        <div className="example main">
          <Banner page={'example'} />
          <h2 className="con-title">高效的图形应用体验</h2>
          <p className="con-caption">高效可视化与数据转换的图形引擎让模型处理的更快，流畅度渲染更出色，完美的3D图形与BIM的效果展示</p>
          <ul className="example-list">
            {
              modelArray.map((item, index) => {
                return (
                    <li className="example-item" key={index} onClick={()=>this.onModelClick(item)}>
                      <div className="example-header flex-between">
                          <span className="example-name">{ item.title }</span>
                          <span className={`example-type S_${item.type}`}>{ item.type }</span>
                      </div>
                      <span className="example-desc">{ item.desc }</span>
                      <img className="example-img" src={ item.imgHref} alt='图片'/>
                    </li>)
              })
            }
          </ul>
          <div className="example-bottom flex-center">
            <Button className="upload-btn" type='primary' onClick={this.goConsole}>上传模型/图纸 </Button>
          </div>
        </div>
        <Footer/>
      </div>)  
  }
}

