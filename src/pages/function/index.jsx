import React, { Component } from 'react';
import { Anchor, Collapse, Icon} from 'antd';
import LazyLoad from 'react-lazyload';
import { FOOTER_VISIBLE } from '../../router/router.reducer';
import store from "../../store/index";
import Header from '../../common/component/Header';
import Footer from '../../common/component/Footer';
import Banner from '../../common/component/Banner';
import { functionData } from '../../common/config/functionJson';
import { baseURL } from '../../common/config/websiteConfig';
import $ from "jquery";
import './function.less';

const { Panel } = Collapse;
const { Link } = Anchor;

let anchorBody = document.body;
export default class Function extends Component {
  constructor(props) {
    super(props)
   
    this.state = {
      targetOffset: undefined,
    }
    
  }
  unitScroll = () =>{
    let mainTopArr = [593, 1307, 2098, 4214, 4955, 5745, 7773, 8545, 9310];
    anchorBody.addEventListener("scroll", ()=>this.getScrollTop(mainTopArr));
  }

  getScrollTop = (mainTopArr)=>{
    let scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
    let k = 0;
    for(let i = 0; i < mainTopArr.length; i++){
      if(scrollTop >= mainTopArr[i]){
          k = i;
          $(".ant-anchor").find($(".ant-anchor-link")).eq(k).addClass("ant-anchor-link-active").siblings().removeClass("ant-anchor-link-active");
      }
    }
    
    if (scrollTop > 460 &&  scrollTop < 10000) {
      $('.anchor-left').removeClass('pos-relative').addClass('pos-fixed');
    } else{
      $('.anchor-left').removeClass('pos-fixed').addClass('pos-relative');
    }
  }

  componentWillMount(){
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: true,
    });
  }
  
  componentDidMount() {
    this.unitScroll();
    this.setState({ targetOffset: window.innerHeight / 2 });
  }

  componentWillUnmount() {
    anchorBody.removeEventListener("scroll", this.getScrollTop);
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: false,
    });
  };

  render() {

    const { targetOffset } = this.state;
    let offset = parseFloat(targetOffset) - 300;

    const anchorItem = () =>{
      return( 
        <Anchor className={`anchor-left`}
                id='anchor-left'
                targetOffset={offset} 
                getContainer={()=> document.getElementById('func-anchor')}
        >
          {
            functionData.map((item,index)=>{
              return (<Link href={`#${item.key}`} key={index} title={item.title} />) 
            })
          }
       </Anchor>)

    };
    
    const collapseContent = (data) =>{
      return(
         <div className="con-wrapper" id={`${data.key}`} key={`${data.key}`}>
            <h2 className="con-title">{data && data.title}</h2>
            {
              (data.content || []).map((item,index) =>{
                return(
                  <div key={index}>
                    {
                      item.details.map((item,index)=>{
                        return(<p key={index} className="con-caption"> {item.desc} </p>)
                      })
                    }
                    {
                      item.images.map((item,index)=>{
                        return(<img key={index} className="con-img" alt={item.key} src={`${baseURL}/resources/pictures/function/${item.imgHref}`}/>)
                      })
                    }
                  </div>
                )
              })
            }
        </div>
      )
    };

    return (
      <div className="function-page">
        < Header/>
        <div className="function main">
          <Banner page={'function'}/>
          <div className='function-container'>
            <div className="func-anchor flex-between" id="func-anchor">
              { anchorItem() }
              <div className="anchor-right">
                <LazyLoad  height={500}
                           scroll={true}
                           scrollContainer='.anchor-right'
                >
                {
                  functionData.map((tab,index)=>{
                    return collapseContent(tab)
                  })
                }
                </LazyLoad>
              </div>
            </div>
            
            <div className="func-collapse">
                <Collapse bordered={false} defaultActiveKey={['1']} 
                            expandIconPosition={'right'}
                            expandIcon={({ isActive }) =>  <Icon type={isActive ?'up':'down'} style={{color:'#8C8F9F'}}/>}       
                  >
                  {
                    functionData.map(item => {
                      return (<Panel  key={item.key}  header={item.title} > { collapseContent(item) }</Panel>)})
                  }
                </Collapse>
            </div>
          </div>
       </div>
       <Footer/>
    </div>)
  }
}

