import React,{Component} from "react";
import { withRouter } from 'react-router-dom';
import {Button} from "antd";
import { consoleURL } from '../../../../common/config/websiteConfig';
import "./banner.less";

class Banner extends Component {
    constructor(props) {
        super(props)
        this.state = {
        
        }
        this.onDownload = ()=>{
            let downloadApi = `${consoleURL}/api/experienceCenter/latest`;
            window.open(downloadApi);
        }
    }
    render(){
        const {info} = this.props;
        return (
            <div className={`title-banner w1200`}>
                <div className="right-session">
                    <img src={info.imgHref} alt=""/>
                </div>
                <div className="left-session">
                    <h1 className="title">{info.title}</h1>
                    <ul className="describe">
                    {
                        info.desc.map(i => {
                            return <li className="desc-item" key={i.index}>{i.text}</li>
                        })
                    }
                    </ul>
                    <div className="link-header">
                     {
                        info.btnLink &&
                        <Button className="link1" type="primary" >{info.btnLink.linkText}<a href={info.btnLink.linkHref} target="_blank" rel="noopener noreferrer"/></Button>
                     }
                     {
                        info.btnLive &&
                        <Button className="link2" onClick={this.onDownload}>{info.btnLive.linkText}</Button>
                     }
                    </div>
                </div>
            </div>
        )
    }
    
}

export default withRouter(Banner); 
