//解决方案 【左文右图/左图右文 水平布局】
//接受 type: rightword/leftword

import React,{Component} from "react";
import { Col, Row} from "antd";
import './horizontalLayout.less'
class HorizontalLayout extends Component {
    constructor(props) {
       super(props)
    }
    render(){
        const {moduleInfo,type,titleIndex} = this.props;
        return (
            <div className={`horizontalLayout w1200 ${type ==='leftword'?'hasBg':''}`}>
                <div className="layoutPc">
                    {
                        type ==='leftword' ?
                        (<Row className="leftword w1200">
                            <Col className="left-side" xl={12} lg={12} md={12} sm={12} xs={12}>
                                <div className="word-show">
                                    <h2 className="containTitle title">{moduleInfo.title}</h2>
                                    <p className="containDesc describe">{moduleInfo.caption}</p>
                                </div>
                            </Col>
                            <Col className="right-side" xl={12} lg={12} md={12} sm={12} xs={12}>
                                <img className="img-show" src={moduleInfo.image} alt=""/>
                            </Col>
                    </Row>):
                    (<Row className="rightword w1200">
                            <Col className="left-side" xl={12} lg={12} md={12} sm={12} xs={12}>
                                <img className="img-show" src={moduleInfo.image} alt=""/>
                            </Col>
                            <Col className="right-side" xl={12} lg={12} md={12} sm={12} xs={12}>
                                <div className="word-show">
                                        <h2 className="containTitle title">{moduleInfo.title}</h2>
                                        <p className="containDesc describe">{moduleInfo.caption}</p>
                                </div>
                            </Col>  
                    </Row>)

                    }

                </div>
                <div className="layoutH5">
                    <h2 className="lay-title con-title">
                        {
                           titleIndex ? `${moduleInfo.key}） ${moduleInfo.title}`: moduleInfo.title
                        }
                    </h2>
                    <p className="lay-caption con-caption">{moduleInfo.caption}</p>
                    <img src={moduleInfo.image} alt={`${moduleInfo.title}`}/>
                </div>
            </div>
        )
    }
    
}

export default HorizontalLayout
