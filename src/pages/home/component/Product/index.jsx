import React from "react";
import "./product.less"

function ProductAdvantage(props) {
    const {bannerInfo, title} = props;
    return (
        <div className="product-advantage w1200">
            <h2 className="title-name con-title">{title}</h2>
            <ul className="session">
                {
                    bannerInfo.map(item => {
                        return (
                            <li className="item" key={item.key}>
                                <img className="img" src={item.imgHref} alt=""/>
                                <p className="title">{item.title}</p>
                                <p className="describe">{item.describe}</p>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

export default ProductAdvantage
