//解决方案 【上文下图 垂直布局】

import React,{Component} from "react";
import './verticalLayout.less'

class VerticalLayout extends Component {
    constructor(props) {
        super(props)
        this.state = {
        
        }
    }
    render(){
        const {moduleInfo} = this.props;
        return (
            <div className="verticalLayout w1200">
                <h2 className="con-title">{moduleInfo.title}</h2>
                <p className="con-caption">{moduleInfo.caption}</p>
                <img src={moduleInfo.image} alt={`${moduleInfo.title}`}/>
            </div>
        )
    }
    
}

export default VerticalLayout

