import React, { Component } from 'react';
import {Tabs,Icon} from 'antd';
import Swiper from 'swiper/js/swiper.js';
import { FOOTER_VISIBLE } from '../../router/router.reducer';
import store from "../../store/index";
import 'swiper/css/swiper.min.css';
import Header from '../../common/component/Header';
import Footer from '../../common/component/Footer';
import VerticalLayout from './component/VerticalLayout';
import ProductAdvantage from "./component/Product";
import Banner from './component/Banner';
import { openWindow } from '../../common/utils/Utility';
import { homeBannerData, productAdvantage, engineModule, homeFullPort }from '../../common/config/homeJson';
// import userLogo from '../../common/img/home/userLogo.webp';
// import userLogoH5 from '../../common/img/home/userLogoH5.webp';
import ownerx from '../../common/img/home/cooperate/ownerx.webp'
import achievementx from '../../common/img/home/cooperate/achievementx.webp'
import constructionx from '../../common/img/home/cooperate/constructionx.webp'
import projectx from '../../common/img/home/cooperate/projectx.webp'
import $ from "jquery";
import './home.less'; 

const { TabPane } = Tabs;
let homeBody = document.body;
export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isScroll: false
    }

  }

  handleSelctTab = (key) =>{
      if(key === 1){
          $('.stand-tab2').removeClass('activeTab');
          $('.stand-tab1 .ant-tabs-content').removeClass('hideTanel').addClass('showTanel');
          $('.stand-tab2 .ant-tabs-content').removeClass('showTanel').addClass('hideTanel');
          $(".stand-tab1 .ant-tabs-nav div").find(".removeActive").addClass('ant-tabs-tab-active');
          $(".stand-tab2 .ant-tabs-nav div").find(".ant-tabs-tab-active").removeClass('ant-tabs-tab-active').addClass('removeActive');
      }else{
          $('.stand-tab2').addClass('activeTab');
          $('.stand-tab1 .ant-tabs-content').removeClass('showTanel').addClass('hideTanel');
          $('.stand-tab2 .ant-tabs-content').removeClass('hideTanel').addClass('showTanel');
          $(".stand-tab2 .ant-tabs-nav div").find(".removeActive").addClass('ant-tabs-tab-active');
          $(".stand-tab1 .ant-tabs-nav div").find(".ant-tabs-tab-active").removeClass('ant-tabs-tab-active').addClass('removeActive');
      } 
  };

  handleChangeTab =(key) =>{
    let that = this;
    if(Number(key) === 8){
      setTimeout(that.imageSwiper, 500);
    }
  };

  //首页-banner轮播
  bannerSwiper = () => {
    let bannerSwiper = new Swiper('#bannerSwiper', {
        loop: true, // 循环模式选项
        autoplay: {delay: 20000},
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            paginationClickable: true,
            type:'custom',
            // 自定义特殊类型分页器，当分页器类型设置为自定义时可用
            renderCustom: function (swiper, current, total) {
                let customPaginationHtml = "";
                for(let i = 0; i < total; i++) {
                    //判断哪个分页器此刻应该被激活
                    if(i === (current - 1)) {
                        customPaginationHtml += '<div class="swiper-pagination-customs swiper-pagination-customs-active"><span class="customs-item"></span></div>';
                    } else {
                        customPaginationHtml += '<div class="swiper-pagination-customs"><span class="customs-item"></span></div>';
                    }
                }
                return customPaginationHtml;
            }
        }
    });
    $('#bannerSwiper .swiper-pagination').on('click','.swiper-pagination-customs',function(){ 
        let index = $(this).index() + 1; 
        bannerSwiper.slideTo(index, 500, false);
    })
  };

  imageSwiper = () =>{
    new Swiper('#imageSwiper', {
        loop: true, // 循环模式选项
        autoplay: { delay: 2500 },
        observer: true,
        observeParents: true,
        // 如果需要分页器
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            paginationClickable: true,
        },
    });
  };

  initTabShow = () =>{
    $('.stand-tab1 .ant-tabs-content').addClass('showTanel');
    $('.stand-tab2 .ant-tabs-content').addClass('hideTanel');
    $(".stand-tab2 .ant-tabs-nav div").find(".ant-tabs-tab-active").removeClass('ant-tabs-tab-active').addClass('removeActive');
  };

  initScrollTop = ()=>{
    let scrollTop = document.scrollTop || document.body.scrollTop || document.documentElement.scrollTop;
    this.setState({ isScroll: scrollTop > 0 ? true : false })
  };

  componentDidMount() {
    homeBody.addEventListener("scroll", this.initScrollTop);
    this.bannerSwiper();
    this.initTabShow();
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: true,
    });
  };

  componentWillUnmount() {
    homeBody.removeEventListener("scroll", this.initScrollTop);
    store.dispatch({
      type: FOOTER_VISIBLE,
      payload: false,
    });
  };

  render() {
      const { isScroll } = this.state;
      let totalTabs = (engineModule.tab1).concat(engineModule.tab2);
      const captionShow = (captions) =>{
          return(
              <div className='captionShow'>
                {
                  captions && captions.map((m,index) =>{
                    return(
                        <div className="captionItem" key={index}>
                          <div className="captionText">
                              <h4>{m.title}</h4>
                              <p>{m.caption}</p>
                            </div>
                            {
                              m.imgArray && m.imgArray.length > 1 ?
                              (<div className="swiper-container" id="imageSwiper">
                                  <div className="swiper-wrapper">
                                    {
                                        m.imgArray.map(item => {
                                            return (<div className="swiper-slide" key={item.key}><img src={item.href} alt=""/></div>)
                                        })
                                    }
                                  </div>
                                  <div className="swiper-pagination"></div>
                            </div>)
                            : 
                            (<div className="caption-img"><img src={m.imgArray[0].href} alt=""/></div>)
                            }
                            
                        </div>
                    )
                  })
                }
            </div>
          )
      }
      //标准功能 tab切换(PC)
      const tabItemLayout = (index, tabData)=>{
          return(
              <Tabs defaultActiveKey="1" className={`stand-tab${index}`} animated={false}  onChange={ (activeKey) => this.handleChangeTab(activeKey)}  onTabClick={() => this.handleSelctTab(index)}>
              {
                  (tabData || []).map(item => {
                  return (<TabPane  key={item.key} 
                                    tab={
                                          <div className="tabshow">
                                              <div className="tab-bg"><img className="tab-img" src={item.icon} alt=""/></div>
                                              <span className="tab-title">{item.tab}</span>
                                          </div>
                                      }
                          >
                          { captionShow(item.content) }
                    </TabPane>)
                  })
              } 
              </Tabs> 
          )
      }
      return (
        <div className="home-page">
          < Header headerType={isScroll}/>
          <div className="home main">
            <div className="swiper-container" id="bannerSwiper">
                    <div className="swiper-wrapper">
                        {
                            homeBannerData.map((item,index)=> {
                                return (<div className={`swiper-slide swiper-${index+1}`} key={item.key}>
                                            <Banner info={item}/>
                                    </div>)
                            })
                        }
                    </div>
                    <div className="swiper-pagination"></div>
            </div>
            <div className="fullPort">
                  <VerticalLayout moduleInfo={homeFullPort}/>
            </div>
            <div className="back-color">
                  <ProductAdvantage title="六大产品优势" bannerInfo={ productAdvantage }/>
            </div>
            <div className="engineFunction">
                <h2 className="con-title">{engineModule.title}</h2>
                <p className="con-caption">{engineModule.describe}</p>
                <div className="standardTab">
                    {tabItemLayout(1, engineModule.tab1)} 
                    {tabItemLayout(2, engineModule.tab2)} 
                </div>
                <div className="standH5Tab">
                    {tabItemLayout(1, totalTabs)} 
                </div>
            </div>
            <div className="back-color">
              <div className="cooperative-partner w1200">
                    <h2 className="con-title paddingTop-88">代表合作用户</h2>
                    <div className="cooperative">
                        {/* <img className="user-img" src={userLogo} alt=""/>
                        <img className="user-img-h5" src={userLogoH5} alt=""/> */}
                        <div className="cooperative-left">
                            <img src={ownerx} alt="" />
                            <img src={achievementx} alt="" />
                        </div>
                        <div className="cooperative-right">
                            <img src={constructionx} alt="" />
                            <img src={projectx} alt="" />
                        </div>
                    </div>
                    <div className="more-button">
                      <span className="see-more" onClick={() => openWindow('/cases')}>
                        查看案例<Icon type="arrow-right" className="arrow"/>
                      </span>
                    </div>
              </div>
            </div>
          </div>
          <Footer/>
        </div>
      )
  }
}


