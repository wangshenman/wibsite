//用于 oda 测试工具 （ PC 嵌套模型打开页面）
import React from 'react'; 
import {  Progress, message, } from 'antd';
import './model.less';
// import { ContextMenu, MenuItem, ContextMenuTrigger, SubMenu } from "react-contextmenu";
import { modelConfig } from '../../common/config/websiteConfig';
import {  getURLValueByKey } from '../../common/utils/Utility';


const ModelWIND = window.WIND.WIND;
const WINDModelApp = ModelWIND.MODEL.APP;
const CallbackType = ModelWIND.CallbackType;
const BackgroundType = ModelWIND.MODEL.BackgroundType;


class Model extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      percent: 0,
      mouse: undefined,
      currentApp: null,
      token: null,
      links: [],
    }
      this.loadModel = this.loadModel.bind(this);
      this.updateDataNode = (type, value) => {
        //数据回调之后, 更新进度条
        switch (type) {
          //加载中
          case CallbackType.DATA_LOAD_CHANGE:
              // 模型加载百分比回调
              let percentNum = Math.min(100, Math.max(0, Math.floor(value.percent))) || 0;
              if (percentNum >= 0 && percentNum < 100) {
                  this.setState({ 
                    loading: true,
                    percent: percentNum 
                  });
              } 
              break;
          //加载结束  
          case CallbackType.DATA_LOAD_FINISH:
            this.setState({ loading: false });
            break;
          //加载成功
          case CallbackType.DATA_LOAD_SUCCEED:
              console.log('模型加载成功：',value);
              break;
          //加载失败
          case CallbackType.DATA_LOAD_FAIL:
              console.log('模型加载失败：',value);
              return message.error(`模型${value && value.modelId}加载失败`)
          case CallbackType.MEASURE_NODE_CREATE:
              this.setState({ measureValueObject: value});
              break;
          case CallbackType.MEASURE_CONFIG_CHANGE:
              this.setState({ measureValueObject: value});
              break;
          case CallbackType.MEASURE_NODE_SELECT:
              this.setState({ measureValueObject: value});
              break;
          case CallbackType.MEASURE_NODE_CHANGE:
                this.setState({ measureValueObject: value});
                break;
          case CallbackType.MEASURE_NODE_UPDATE:
              this.setState({ measureValueObject: value});
              break;
          case CallbackType.MEASURE_NODE_CLEAR:
            this.setState({ measureValueObject: value});
            break;
          default:
            break;
        }
      };
  
      this.initLoadModel = (links,token)=>{
        this.setState({
          links,
          token
        },()=>this.loadModel())
      };

      this.switchState = (type, value) => {
        this.setState({[type]: value})
      };

      // 注册PC监听方法
      this.onLoad = () => {
        if (typeof(QCefClient) == 'undefined') {
            return;
        }
      };
    
  }

  componentDidMount() {
    this.onLoad();
    //获取 token
    let modelToken = getURLValueByKey('token');
    let modelId = getURLValueByKey('modelId');
    let version = getURLValueByKey('version');
    if(modelToken && modelId ){
      this.initLoadModel([{modelId: modelId, version: version}],modelToken);
    }else{
      return message.error('获取模型信息失败~');
    }
  };

  componentWillUnmount() {
    const {currentApp} =this.state;
    if (currentApp) {
        currentApp.data().closeAllModelDatas();
    }
  };

  //加载模型
  async loadModel() {
    const { currentApp, token} = this.state;
    if (currentApp) {
      currentApp.data().closeAllModelDatas();
    }
    //WINDData初始化
    let config, modelApp;
   
    //WINDView初始化
    let canvas = document.querySelector('#View');
    modelApp = new WINDModelApp(canvas);

    config = { serverIp: modelConfig.serverIp, token: token };
    await modelApp.connect(config);
   
    this.setState({
      loading: false,
      percent: 0,
    });

    // 数据加载执行回调
    modelApp.data().addCallback(1,  (type, option) => this.updateDataNode(type, option));

     //设置模型视图背景颜色
     const COLOR = [[171, 218, 255], [255, 255, 255]];
     let bgConfig = {
         gradientStartRGB255: COLOR[0],
         gradientEndRGB255: COLOR[1],
         gradientDirectionType: 0, //1/2/3/4
     }
     modelApp.effect().setBackgroundType(BackgroundType.GRADIENT_COLOR);
     modelApp.effect().configBackground(bgConfig);
     
     //开启导航辅助
     let navConfig = {
        rightMargin: 0,//距离右边的距离
        topMargin: 0, //距离顶部的距离
        widthSize: 100, //尺寸，正方形
     }
     modelApp.toolkit().configNavigation(navConfig);
     modelApp.toolkit().openNavigation();
    //模型渲染
    modelApp.startFrame();

    this.setState({
      currentApp: modelApp,
    });

    this.openModelData(modelApp);
    this.bindEvent(this)
    
  };

  //打开模型
  async openModelData(modelApp) {
    const {links} = this.state;
    if (links && links.length) {
      // let isOpenModelSuccess = await modelApp.data().openModelData(links[0].modelId, links[0].version);
      let isOpenModelSuccess = await modelApp.data().openModelDatas(links);
      if (typeof(QCefClient) !== 'undefined') {
        let query = {
            request: "OpenedToNotifyPC##" + JSON.stringify({"id": links[0].id, "version": links[0].version, "ret": isOpenModelSuccess ? true : false}),
            onSuccess: function(response) {},
            onFailure: function(error_code, error_message) {}
        };
        window.QCefQuery(query);
      }

      if(isOpenModelSuccess){
        this.setState({loading: false})
      }else{
        return message.error('模型打开失败');
      }
    }
  };

  
  // 模型事件处理
  bindEvent = (context) =>{
    let canvasDiv = document.querySelector('#layout');
    if (canvasDiv) {
        let startTime;
        let endTime;
        // 右键菜单
        canvasDiv.addEventListener('contextmenu', e => {
            e.preventDefault();
            endTime = Date.now();
            context.selectJuging();
        });

        canvasDiv.addEventListener('click', function (e) {
            context.selectJuging();
        }, false);

        canvasDiv.addEventListener('mousedown', e => {
            startTime = Date.now();
            if (e.button === 2) {
                // context.setState({
                //     mouse: 'drag'
                // })
            }

        });
        // 苹果系统快速点击右键，不会触发mouseup
        canvasDiv.addEventListener('mouseup', e => {
            endTime = Date.now();
            if (e.button === 2) {
                if (endTime - startTime > 130) {
                    context.setState({
                        mouse: 'drag'
                    })
                } else {
                    context.setState({
                        mouse: undefined
                    })
                }
            } 
        });
    }
}

  switchState = (type, value) => {
    this.setState({[type]: value})
  };

  //关闭模型
  closeModelDatas() {
    const { currentData } = this.state;
    currentData.getWINDDataLoader().closeAllModelDatas();
  };
 
  

  render() {
    let { loading, percent} = this.state;
  
    return (
      <div className="modelTest">
        {/* <ModelHeader name={this.state.modelName}
                     histories={this.state.histories}
                     openModel={this.initLoadModel}
                     currentModelId={ getURLValueByKey('modelId')}
                     currentVersion={ getURLValueByKey('version')}
                     
        /> */}
        <div className="layout"  id="layout">   
            { loading ?  (<div className="layout-loading-process"><Progress type="circle" percent={percent}/></div> ): ''}
            <canvas id='View' style={{ position: 'absolute', width: '100%', height: '100%' }}></canvas>
        </div>
      </div>
      
    )
  }

}

export default Model;