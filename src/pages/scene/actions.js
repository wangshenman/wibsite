
import rest from '../../common/utils/rest';
import { ApiURL } from '../../common/config/websiteConfig';

export function _concatUrl(url) {
    return `${ApiURL}/v1/account-service/${url}`;
}

function _hookError(error, message) {
    console.log(message + '出现错误 => ', error);
    throw error;
}


//获取模型版本对比结果
export function versionContrast(modelId,masterVersion,slaveVersion,contrastType) {
    let url = `${ApiURL}/v3/model/contrast/${modelId}/${masterVersion}/${slaveVersion}/${contrastType}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "获取模型版本对比结果"))
}

//根据构件id获取构件属性
export function getComponents(modelId, version, id) {
    let url = `${ApiURL}/v3/model/component/${modelId}/${version}/${id}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "根据构件id获取构件属性"))
}

//获取构件 id
export function getComponentsUUId(modelId, entityId, fVersion, sVersion){
    let url= `${ApiURL}/v3/model/contrast/second/query/${modelId}/${entityId}/${fVersion}/${sVersion}`;
    return rest.get(url)
        .then(result => result)
        .catch(err => _hookError(err, "根据构件entityId获取构件id"))
}




