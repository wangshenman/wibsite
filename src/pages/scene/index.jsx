import React, { Component } from 'react';
import './scene.less';
import Header from '../../developer/component/HeaderView';
import { sceneList } from '../../common/config/homeJson';
import {openWindow} from '../../common/utils/Utility';
// import Header from '../../common/component/Header';
// import Footer from '../../common/component/Footer';
// import Banner from '../../common/component/Banner';

export default class Scene extends Component {
  constructor(props) {
    super(props)
    //单个打开模型
    this.state={}
  }
  render() {
    return (
      <div className="scene-page">
        < Header/>
        <div className="scene main">
          <ul className="scene-list">
            {
              sceneList.map((item, index) => {
                let Url = `${item.link}?modelId=${item.modelId}&version=${item.version}`;
                return (
                  <li key={item.key} onClick={()=> openWindow(Url)}>
                      <img className="scene-img" src={ item.imgHref } alt={ item.title } />
                      <div className="scene-detail">
                          <h2 className="scene-title">{ item.title }</h2>
                          <p className="scene-caption">{ item.caption }</p>
                          <span className="scene-btn">查看详情</span>
                      </div>
                  </li>)
              })
            }
          </ul>
        </div>
      </div>)  
  }
}

