/* 图钉展示 */
import React from 'react';
import { Drawer, Button, Icon, Tree} from 'antd';
import empty_img from '../../../../../common/img/sence/empty_img.png';
import Empty from '../../../../../common/component/Empty';
import { LocationIcon } from '../../component/svgIcon';

const { TreeNode } = Tree;
export default class Pushpin extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            pushpinList: [],
            checkedKeys:[],
            currentSelect: null
        }

        //定位图钉关系
        this.handleLocation = (item) =>{
            const{ app }= this.props;
            this.setState({
                currentSelect: item
            },()=>{
                if(item && item.id){
                    app.create().locatePushpin(item.id);
                }
            })
        };

         //图钉数据整合
        this.transferPushpinData = (pushpinList)=>{
            let paramsMap = {};
            let destArray = [];
            for (let i = 0; i < pushpinList.length; i++) {
                let params = pushpinList[i];
                if (!paramsMap[params.styleId]) {
                    let numText = params.styleId === 1 ? '一' : params.styleId === 2 ? '二' : '三';
                    destArray.push({
                        id: params.styleId,
                        styleId: params.styleId,
                        name: `样式${numText}`,
                        pushpins: [params]
                    });
                    paramsMap[params.styleId] = params;
                } else {
                    for (let j = 0; j < destArray.length; j++) {
                        let dest = destArray[j];
                        if (dest.styleId === params.styleId) {
                            dest.pushpins.push(params);
                            break;
                        }
                    }
                }
            }
            return destArray;
        };

        this.getCheckedKeys = ()=>{
            const {pushpinList} = this.state;
            let checkedKeys = [];
            let traverseChildren = function (pushpins) {
                for (let i = 0; i < pushpins.length; i++) {
                    checkedKeys.push(pushpins[i].id.toString());
                    if(pushpins[i].pushpins){
                        traverseChildren(pushpins[i].pushpins);
                    }
                }
            };
            traverseChildren(pushpinList);
            this.setState({
                checkedKeys
            })

        };

        this.handleLocation = (data) =>{
            this.setState({currentSelect: data});
            const{ app }= this.props;
            if(app){
                app.create().locatePushpin(data.id);   
            }
        };
    }
     componentDidMount () {
        const {pushpinList} = this.props;
        this.setState({
            pushpinList: this.transferPushpinData(pushpinList),
        },()=> this.getCheckedKeys())
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.pushpinList  !== this.props.pushpinList){
            this.setState({
                pushpinList: this.transferPushpinData(nextProps.pushpinList),
            },()=>this.getCheckedKeys())
        }
    }
    
    componentWillUnmount(){
        const{ app }= this.props;
        if(app){
            app.create().clearAllPushpins();   
        }
    }
  
    render() {
        const { onClose, onCreate } = this.props;
        const { pushpinList, checkedKeys } = this.state;

        const onCheck = (checkedKeys,  {checked, checkedNodes, node}) => {
            const { app } = this.props;
            app.create().hideAllPushpins();
            if (checkedKeys.length) {
                (checkedKeys||[]).map(i =>{
                    app.create().showPushpin(i);
                })
            }
            this.setState({ checkedKeys })
        }

        const moreOperate = (data)=>{
            let isHeader = data.id === 1 || data.id === 2 || data.id === 3;
            return(
                <div className="section-item flex-between" key={data.id} >
                     <span className="section-name">{data.name}</span> 
                     {
                         !isHeader &&  <Icon className="location" component={LocationIcon}  onClick={()=>this.handleLocation(data)}/>
                     }                         
                </div>
            )
        }

        const treeNodeView = (item, selectable = true) => {
            return (
              <TreeNode
                key={item.id}
                selectable={selectable}
                title={moreOperate(item)}
              >
                {(item.pushpins||[]).map((i) => treeNodeView(i, true))}
              </TreeNode>
            );
        };

        const listShow = () =>{
            return(
                <div className="pushpinList">
                    <div className="pushpinContent">
                        {
                            pushpinList && pushpinList.length?
                             <Tree
                                showIcon
                                blockNode
                                checkable
                                onCheck={onCheck}
                                checkedKeys={checkedKeys}
                                defaultExpandAll
                            >
                                {pushpinList.map((item) => treeNodeView(item))}
                            </Tree>
                            :
                            <Empty message={'可在构件任意位置添加IOT设备进行管理'}
                                   img={empty_img}
                                   imgHeight={198}
                            />
                        }
                        
                    </div> 
                </div>
            )
              
        };
       
        return( 
          <div className={`bimSenceWrapper`} id="pushpin">
              <Drawer 
                      title="设备图钉列表"
                      placement='right'
                      visible={true}
                      mask={false}
                      width={300}
                      maskClosable={false}
                      zIndex={100}
                      keyboard={false}
                      onClose={onClose}
                      getContainer={document.getElementById("pushpin")}
              >
                 <div className="bimSenceContent">
                     { listShow() }
                </div>
              </Drawer>
          </div>) 
      }

}
