/* 房间展示 */
import React from 'react';
import { Drawer, Button, Icon} from 'antd';
import empty_img from '../../../../../common/img/sence/empty_img.png';
import Empty from '../../../../../common/component/Empty';
import { LocationIcon, ColorIcon, ShowIcon, HideIcon } from '../../component/svgIcon';

const BGCOLOR = [
    [30, 144, 255, 255],
    [255, 20, 147, 255],
    [127, 255, 170, 255],
    [138, 43, 226, 255],
    [255, 192, 203, 255]
];
export default class Room extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            roomList: [],
            currentSelect: null,
        }

        //房间定位
        this.handleLocation = (data) =>{
            this.setState({currentSelect: data});
            const{ app, models}= this.props;
            const{ roomList } = this.state;
            if(app){
                let allRoomUuids = [];
                roomList.map(i=> allRoomUuids.push(i.uuid));
                //取消其他房间上色
                app.assist().discolorAssignRooms([
                    { 
                        modelId: models[0].modelId,
                        version: models[0].version,
                        roomUuids: allRoomUuids
                    }
                ]);
                models.map(i => Object.assign(i,{roomUuids:[data.uuid]}));
                //定位房间并高亮红色
                app.assist().locateAssignRooms(models); 
                app.assist().colorAssignRooms(models, [255,0,0,255]);  
            }
            
        }
        //房间上色
        this.colorAssignRooms = (data)=>{
            this.setState({currentSelect: data});
            const{ app, models}= this.props;
            if(app){
                models.map(i => Object.assign(i,{roomUuids:[data.uuid]}));
                let randomInt = Math.floor(Math.random()*6);
                app.assist().colorAssignRooms(models, BGCOLOR[randomInt]);
            }
        };

        //房间显隐
        this.displayAssignRooms = (data)=>{
            const{ app, models}= this.props;
            const{ roomList } = this.state;
            if(app){
                models.map(i => Object.assign(i,{roomUuids:[data.uuid]}));
                if(data.isShow){
                    app.assist().hideAssignRooms(models)
                }else{
                    app.assist().showAssignRooms(models)
                }
                roomList.map( i => {
                    if(data.viewId === i.viewId){
                        i.isShow = !data.isShow
                    }
                })
                this.setState({
                    roomList,
                    currentSelect: data
                });
            }
            
        };


    }
    componentDidMount() {
        const {roomList} = this.props;
        if(roomList.length){
            roomList.map(i => Object.assign(i,{isShow: true}))
            this.setState({
                roomList,
            })
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.roomList  !== this.props.roomList){
            nextProps.roomList.map(i => Object.assign(i,{isShow: true}))
            this.setState({
                roomList: nextProps.roomList
            })
        }
    } 
    componentWillUnmount(){
        const{ app }= this.props;
        if(app){
            app.assist().closeAllRoomDatas();   
        }
    }
  
    render() {
        const { onClose, onColor } = this.props;
        const { roomList, currentSelect } = this.state;
        const listShow = () =>{
            return(
                <div className="roomList">
                    <div className="roomContent">
                        {
                            roomList.length?
                            <ul>
                            {
                                (roomList||[]).map((i, index)=>{
                                    return(<li className={`section-item flex-between ${(currentSelect && currentSelect.viewId) === i.viewId ? 'selectedItem' : 'unSelectedItem'}`} key={index}>
                                                <span className="section-name">{i.name}</span>
                                                <div>
                                                   <Icon className="location" component={LocationIcon}  onClick={()=>this.handleLocation(i)}/>
                                                   <Icon className="detail-icon" component={ColorIcon} style={{marginLeft:'6px'}} onClick={()=>this.colorAssignRooms(i)}/>
                                                  {
                                                      i.isShow ?
                                                       <Icon className={`display-icon`} component={ShowIcon} style={{marginLeft:'6px'}} onClick={()=>this.displayAssignRooms(i)}/>
                                                       :
                                                       <Icon className={`display-icon`} component={HideIcon} style={{marginLeft:'6px'}} onClick={()=>this.displayAssignRooms(i)}/>
                                                  }
                                                </div>
                                        </li>)
                                })
                            }
                            </ul>
                            :
                            <Empty img={empty_img}
                                   imgHeight={198}
                            />
                        }
                    </div> 
                </div>
            )
              
        };
       
        return( 
          <div className={`bimSenceWrapper`} id="room">
              <Drawer 
                      title="房间列表"
                      placement='right'
                      visible={true}
                      mask={false}
                      width={300}
                      maskClosable={false}
                      zIndex={100}
                      keyboard={false}
                      onClose={onClose}
                      getContainer={document.getElementById("room")}
              >
                 <div className="bimSenceContent">
                     { listShow() }
                </div>
              </Drawer>
          </div>) 
      }

}
