import React from 'react'; 
import { message } from 'antd';
import './bimInternet.less';
import ModelHeader from '../component/modelHeader/ModelHeader';
import ModelView from '../../../bimModel/component/modelView/modelView';
import Pushpin from './component/pushpin';
import Room from './component/room';
import MoveModel from './component/moveModel';
import { getToken,getExampleToken } from '../../../../common/utils/commonActions';
import { getURLValueByKey } from '../../../../common/utils/Utility';
import { modelConfig } from '../../../../common/config/websiteConfig';
import pushpinImg1 from '../../../../common/img/sence/icon/pushpin1.png';
import pushpinImg2 from '../../../../common/img/sence/icon/pushpin2.png';
import pushpinImg3 from '../../../../common/img/sence/icon/pushpin3.png';
import {RoomIcon, PushpinIcon} from '../component/svgIcon';

const ModelWIND = window.WIND.WIND;
const CallbackType = ModelWIND.MODEL.CallbackType;
const PushpinCallback = 'PUSHPIN_CALLBACK';

const PUSHSTYLE = [
  //图钉样式一
  {
    boardWidth: 25,
    boardHeight: 37,
    boardImage: pushpinImg1,
  },
  //图钉样式二
  {
    boardWidth: 25,
    boardHeight: 28,
    boardImage: pushpinImg2,
  },
  //图钉样式三
  {
    boardWidth: 60,
    boardHeight: 37,
    boardImage: pushpinImg3,
    textContent:'示例图钉',
    textOffset: [0.5,0.4],
    textSize: 10,
    textFont: "sans-serif",
    textRGB255: [255,255,255,255]
  }
];


class BIMInternet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      config: null,
      models: [],
      pushpinList:[],
      pushpinVisible: false, 
      pushpinModelVisible: false,
      roomList:[],
      roomVisible: false,
      position:[],
      drawerKey: null
    }
    this.onRef = (ref) =>{
      this.child = ref
    };

    this.onRef2 = (ref) =>{
      this.child2 = ref
    };

    this.closeDrawer = (type, value) =>{
      const {currentApp} = this.child.state;
      this.setState({
        drawerKey: null,
      },()=> currentApp.screenResize())
      this.setState({[type]: value},()=> this.child2.clearCurrent())
    };

    this.toolkitCallback = (type, value) =>{
      if (type === CallbackType.LOCATION_LEFT_CLICK || type === CallbackType.LOCATION_RIGHT_CLICK) {
          this.setState({ position: value })
      }
    };

    this.init =()=>{
      let modelId = getURLValueByKey('modelId');
      let version = parseInt(getURLValueByKey('version'));
      if( modelId && version){
        getExampleToken().then(res =>{
          if(res && res.code == 200){
            let modelToken = res.data.token;
            this.setState({
              config: { serverIp: modelConfig.serverIp, token: modelToken },
              models:[{ modelId: modelId, version}],
            })
          }
        })
      }
    };
  }

  componentDidMount() {
    this.init();
  }

  switchState = (type, value) => {
    this.setState({[type]: value})
  };

  //打开Drawer
  handleOpenDrawer = (key) =>{
    const {currentApp} = this.child.state;
    const {drawerKey}= this.state;
    if(drawerKey === key){
       return false;
    }
    switch (key) {
      case 'pushpin':
        this.setState({
          pushpinList:[],
          pushpinVisible: true, //图钉
          roomVisible: false, 
          drawerKey: 'pushpin',
        },()=> {currentApp.screenResize(); currentApp.toolkit().addCallback(PushpinCallback, (type, value)=>this.toolkitCallback(type, value))}
        )
        break;
      case 'room':
        this.setState({
          roomList:[],
          pushpinVisible: false, 
          drawerKey: 'room',
        },()=>{currentApp.screenResize(); this.getRoomList()})
        break;
      default:
        break;
    }

  };

  toggleDrawer = () =>{
    const { isShow, drawerKey } = this.state;
    switch (drawerKey) {
      case 'pushpin':
        this.setState({pushpinVisible: isShow ? false : true})
       break;
      case 'room':
        this.setState({roomVisible: isShow ? false : true})
       break;
    }
    this.setState({
      isShow: !isShow
    })
  };


  //房间列表
  getRoomList = async()=>{
    const {currentApp} = this.child.state;
    const {models} = this.state;
    currentApp.assist().openRoomData(models[0].modelId);
    let roomList = await currentApp.assist().grabRooms(models[0].modelId, models[0].version, false);
    this.setState({ 
      roomList: roomList, 
      roomVisible: true, 
      currentApp 
    });
  }

  //确认创建图钉
  handleCreatePushpin = async(key) =>{
    const {currentApp} = this.child.state;
    const {pushpinList, position}=this.state;
    //唯一id，随机生成
    let id = Math.ceil(Math.random()*1000);
    //获取选择集
    let entities = currentApp.action().getSelections();
    //创建图钉
    currentApp.toolkit().openLocation();
    currentApp.create().openPushpin();
    //图钉样式
    let pushpinStyle = position ? Object.assign(PUSHSTYLE[key-1],{position: [position.x, position.y, position.z]}) : PUSHSTYLE[key-1];
    let isCreate = await currentApp.create().createPushpin(`${id}`, pushpinStyle, entities);
    if(isCreate){
      pushpinList.push({ id:`${id}`, styleId: key, name:`设备图钉_${id}`});
      this.setState({ 
        pushpinList: pushpinList.slice(-10),
        pushpinModelVisible: false,
        currentApp
      },()=> {
        currentApp.create().showPushpin(`${id}`);
      })
    }else{
       message.error('创建失败~')
    }
  };

  closePushpins = (id)=>{
    const {currentApp} = this.child.state;
    if(id){
      currentApp.create().hidePushpin(id);
    }else{
      currentApp.create().hideAllPushpins();
    }
  }

  render() {
    let { config,models, currentApp, pushpinVisible, pushpinModelVisible, pushpinList,
      roomList, roomVisible, isShow, drawerKey
     } = this.state;
    const btnInfo = [
      {
        title: '房间管理',
        icon: RoomIcon,
        action: () => this.handleOpenDrawer('room')
      },
      {
        title: '设备图钉',
        icon: PushpinIcon,
        action: ()=> this.handleOpenDrawer('pushpin')
      }
    ];

    return (
      <div className="BIMInternet">
        <ModelHeader name={'BIM+物联网'} 
                     btns={btnInfo}
                     isShow={isShow}
                     toggleDrawer={this.toggleDrawer}
                     onRef2 ={this.onRef2}
        />
        <div className="BIMInternet-content">
          <div className={`bim-wrapper ${drawerKey? 'drawerWidth' : 'fullWidth'}`}>
            <ModelView  config = { config } 
                       links = { models }
                       mainMenu = {true}
                       isPushpin = {pushpinVisible}
                       handleCreatePushpin = {this.handleCreatePushpin}
                       onRef = { this.onRef }
                       hasDrawer = { drawerKey }
           />
          </div>
          
            <div className="drawer-wrapper">
              {
                pushpinVisible && <Pushpin visible={pushpinVisible}
                                           app={currentApp}
                                           pushpinList={pushpinList}
                                           onCreate= {()=>this.handleCreatePushpin('pushpinModelVisible',true) }
                                           onClose={()=>{ this.closeDrawer('pushpinVisible',false) }}
                                  />
              }
              {
                roomVisible && <Room visible={roomVisible}
                                     app={currentApp}
                                     models={models}
                                     roomList={roomList}
                                     onClose={()=>{this.closeDrawer('roomVisible',false)}}
                              />

              }
            </div>
            <div className="model-wrapper">
              {
                pushpinModelVisible && <MoveModel title="创建图钉"
                                                  keyWords={'pushpin'}
                                                  visible= {pushpinModelVisible}
                                                  onCreate= {this.confirmCreatePushpin}
                                                  onClose={()=>this.switchState('pushpinModelVisible',false)}
                                        />
              }
            </div>
        </div>
     </div>
    )
  }

}

export default BIMInternet;
