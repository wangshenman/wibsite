/* 构件属性展示 */

import React from 'react';
import { Modal, Input } from 'antd';
const { TextArea } = Input;

class MoveModel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            inputName:''
        }

        this.handleName = (value)=>{
            this.setState({
                inputName: value
            })
        }
       
        this.handleConfirm = ()=>{
            const { inputName } = this.state;
            if(inputName){
                this.props.onCreate(inputName)
            }
        };
    }
   
  
    render() {
        const { keyWords, visible, title, onClose } = this.props;
        const { inputName } = this.state;

        const modelItem = (value)=>{
            switch (value) {
                case 'comment':
                    return(
                        <div className="modelItem flex-start">
                          <span>批注描述：</span>
                          <TextArea style={{width: 180}} rows={2} value={inputName} onChange={e => this.handleName(e.target.value)}/>
                        </div>
                    )
                default:
                    return(
                    <div className="modelItem flex-start">
                        <span>名称：</span>
                        <Input style={{width: 180}}  type="text" value={inputName} onChange={e => this.handleName(e.target.value)}/>
                    </div>
                    )                  
            }

        };
        
        return( 
            <Modal title={title}
                visible={visible} 
                width={320}
                centered
                okText={'确定'}
                cancelText={'取消'}
                onOk={this.handleConfirm} 
                onCancel= {onClose}
              >
                <div className="modelContent">
                   { modelItem(keyWords) }
                </div>
              </Modal> 
        )
    }

}
export default MoveModel;