/* 位置联动展示 */
import React from 'react';
import { Drawer, Button, Icon} from 'antd';
import { LocationIcon, DetailIcon } from '../../component/svgIcon';
import Empty from '../../../../../common/component/Empty';
import empty_img from '../../../../../common/img/sence/empty_img.png';

class SectionSet extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sectionSets: [],
            currentSelect: null
        }

        //选择集定位
        this.handleLocation = (data) =>{
            this.setState({
                currentSelect: data
            },()=> this.getLocation(data))
        };

        this.getLocation = (data)=>{
            const {app} = this.props;
            app.control().unhighlightAllComponents();
            app.control().unfreezeAllComponents();
            if (app) {
                app.control().freezeAllComponents();
                app.control().highlightComponentSet(data.setId);
                app.action().locateSelection();
            }
        }

        //选择集定位
        this.handleDetail = (data) =>{
            const {onDetail} = this.props;
            this.setState({
                currentSelect: data
            },()=> onDetail(data))
        };
    }
    componentDidMount() {
        const {sectionSets} = this.props;
        if(sectionSets.length){
            this.setState({
                sectionSets,
                currentSelect: sectionSets[0]
            })
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.sectionSets  !== this.props.sectionSets){
            this.setState({
                sectionSets: nextProps.sectionSets
            })
        }
    } 

    render() {
        const { onClose, onCreate, onDetail } = this.props;
        const { sectionSets, currentSelect } = this.state;

        const listShow = () =>{
            return(
                <div className="sectionSetList">
                    <div className="sectionSetContent">
                        {
                            sectionSets.length?
                            <ul>
                            {
                                (sectionSets||[]).map((i, index)=>{
                                    return(<li className={`section-item flex-between ${(currentSelect && currentSelect.setId) == i.setId ? 'selectedItem' : 'unSelectedItem'}`} key={index}>
                                                <span className="section-name">{i.name}</span>
                                                <div>
                                                   <Icon className="location" component={LocationIcon}  onClick={()=>this.handleLocation(i)}/>
                                                   <Icon className="detail-icon" component={DetailIcon} style={{marginLeft:'10px'}} onClick={()=>this.handleDetail(i)}/>
                                                </div>
                                                
                                        </li>)
                                })
                            }
                            </ul>:
                            <Empty  message={'选中多个构件，可创建选择集'}
                                    img={empty_img}
                                    imgHeight={198}
                            />
                        }
                        
                    </div> 
             </div>
            )
        };
       
        return( 
          <div className={`bimSenceWrapper`} id="sectionSet">
              <Drawer 
                      title="选择集列表"
                      placement='right'
                      visible={true}
                      mask={false}
                      width={300}
                      maskClosable={false}
                      zIndex={100}
                      keyboard={false}
                      onClose={onClose}
                      getContainer={document.getElementById("sectionSet")}
              >
                 <div className="bimSenceContent">
                     { listShow() }
                </div>
                <div className="bottomOperate">
                     <Button style={{width:'260px',height:'32px'}} type="primary" onClick={onCreate}>创建选择集</Button>
                </div> 
              </Drawer>
          </div>) 
      }

}
export default SectionSet;