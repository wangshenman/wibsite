/* 构件属性展示 */
import React from 'react';
import { Modal, Table, Button, message } from 'antd';
import _ from "lodash";
export default class SectionSetTable extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            sectionSetData:[],
            selectedRows:[],
            selectedRowKeys:[]
        }
       
        this.handleLocation = ()=>{
            const { app, models } = this.props;
            const { selectedRows } = this.state;
            if(selectedRows.length){
                let uuids = [];
                let links = _.cloneDeep(models)
                selectedRows.map(i =>{ uuids.push(i.uuid)});
                links.map(m => {
                    Object.assign(m, { componentUuids: uuids });
                })
                app.control().unhighlightAllComponents();
                app.action().locateAssignment(links);
                app.control().highlightAssignComponents(links,true);
            }else{
                message.warning('请选择要定位的构件~');
            }
            
        };
    }

    componentDidMount(){
        this.setState({
            loading: this.props.loading,
            sectionSetData: this.props.sectionSetsData
        })

    }
    componentWillReceiveProps(nextProps){
        this.setState({
            loading: nextProps.loading,
            sectionSetData: nextProps.sectionSetsData
        })
    }

   
  
    render() {
        const { keyWords, visible, onClose } = this.props;
        const { sectionSetData,loading,selectedRowKeys } = this.state;

        const columns = [
            {
              title: '构件名称',
              dataIndex: 'name',
              width: 150
            },
            {
                title: '构件ID',
                dataIndex: 'entityId',
                width: 80
              },
            {
                title: '楼层',
                dataIndex: 'floor',
                width: 80,
            },
            {
                title: '专业',
                dataIndex: 'domainStr',
                width: 80,
                render: (text, record) => text ? text : '-'
            },
            {
                title: '类型',
                dataIndex: 'categoryStr',
                width: 100,
                render: (text, record) => text ? text : '-'
            },
        ];

        const rowSelection = {
            selectedRowKeys,
            onChange: (selectedRowKeys, selectedRows) => {
                this.setState({ selectedRowKeys,selectedRows })
            }
          };

        return( 
            <Modal title={'构建列表'}
                visible={visible} 
                width={600}
                centered
                onCancel= {onClose}
                footer={null} 
              >
                <div className='detail-header flex-between' style={{marginBottom: '15px'}}>
                    <span>{`构建数量： ${ (sectionSetData && sectionSetData.length) || 0 } 个`} </span>
                    <Button type="primary" onClick={this.handleLocation}>定位</Button>
                </div>
                <div className='detail-table'>
                    <Table rowKey={record => record._id} 
                            selectedRowKeys = {selectedRowKeys}
                            rowSelection={rowSelection} 
                            columns={columns} 
                            dataSource={sectionSetData} 
                            loading={loading}
                            pagination={{ pageSize: 10}}
                    />
                </div>
              </Modal> 
        )
    }

}