import React from 'react'; 
import { message } from 'antd';
import './bimPlatform.less';
import ModelHeader from '../component/modelHeader/ModelHeader';
import ModelView from '../../../bimModel/component/modelView/modelView';
import SectionSet from './component/sectionSet';
import ViewPoint from './component/viewPoint';
import Comment from './component/comment';
import DrawTool from './component/drawTool';
import MoveModel from './component/moveModel';
import {SectionSetIcon, ViewPointIcon, CommentIcon} from '../component/svgIcon';
import SectionSetTable from './component/sectionSetTable';
import { getToken, getExampleToken } from '../../../../common/utils/commonActions';
import { getURLValueByKey } from '../../../../common/utils/Utility';
import { modelConfig } from '../../../../common/config/websiteConfig';
import _ from "lodash";

const GraffitiType = window.WIND.WIND.GraffitiType;
const ActionSaveType = window.WIND.WIND.ActionSaveType;

class BIMPlatform extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentApp: null,
      config: null,
      models: [],
      entities: [],
      isShow: false,
      drawToolVisible: false, //涂鸦工具
      commentList: [],//涂鸦列表
      commentVisible: false, //涂鸦窗口
      commentModelVisible:false,//创建涂鸦
      sectionSets: [], //选择集列表
      sectionSetsData:[],
      sectionSetVisible: false, //选择集窗口
      sectionModelVisible:false, //创建选择集
      sectionTableVisible:false, //选择集详情
      viewPoints:[], //视口列表
      viewPointVisible: false, //视口窗口
      viewPointModelVisible:false,//创建视口
      screenShot: null, //截图
      drawerKey: null
    };

    this.onRef = (ref) =>{
      this.child = ref;
    };
    this.onRef2 = (ref) =>{
      this.child2 = ref
    };
    this.closeDrawer = (type, value) =>{
      const {currentApp} = this.child.state;
      this.setState({
        drawerKey: null,
      },()=> currentApp.screenResize())
      this.setState({[type]: value},()=> this.child2.clearCurrent())
    };
    this.init =()=>{
      let modelId = getURLValueByKey('modelId');
      let version = parseInt(getURLValueByKey('version'));
      if( modelId && version){
        getExampleToken().then(res =>{
          if(res && res.code == 200){
            let modelToken = res.data.token;
            this.setState({
              config: { serverIp: modelConfig.serverIp, token: modelToken },
              models:[{ modelId: modelId, version}],
            })
          }
        })
      }
    };
    
    
  };

  componentDidMount() {
    this.init();
  }

  switchState = (type, value) => {
    this.setState({[type]: value})
  };

  //打开Drawer
  handleOpenDrawer = (key) =>{
    const {drawerKey}= this.state;
    if(drawerKey === key){
      return false;
    }
    switch (key) {
      case 'viewPoint':
        this.setState({
          viewPoints:[], //视口列表
          commentVisible: false, //涂鸦
          sectionSetVisible: false, //选择集
          viewPointVisible: true, //视口
          drawerKey: 'viewPoint',
      },()=>this.revertStatus())
        break;
      case 'comment':
        this.setState({
          commentList: [],
          commentVisible: true, //涂鸦
          sectionSetVisible: false, //选择集
          viewPointVisible: false, //视口
          drawerKey: 'comment',
        },()=>this.revertStatus())
        break;
      case 'sectionSet':
        this.setState({
          sectionSets: [], //选择集列表
          sectionSetVisible: true, //选择集
          commentVisible: false, //涂鸦
          viewPointVisible: false, //视口
          drawerKey: 'sectionSet',
        },()=>this.revertStatus())
        break;
      default:
        break;
    }

  }

  revertStatus = () =>{
    const {currentApp} = this.child.state;
    currentApp.screenResize();
    currentApp.control().unhighlightAllComponents();
    currentApp.control().unfreezeAllComponents();
    currentApp.control().clearAllComponentSets();
  };

  handleCreateViewPoints = ()=>{
    const {currentApp} = this.child.state;
    this.setState({
      viewPointModelVisible: true,
      currentApp
    },()=> currentApp.configDevice({ keyboardOpened: false }))
  }

  //确认创建视口
  confirmCreateViewPoints = async(name) =>{
    const {currentApp} = this.child.state;
    const {viewPoints} =this.state;
    if (!name){ return message("请输入视口名称~");} 
    let WIND = window.WIND.WIND;
    let viewData = currentApp.saveViewport(WIND.MODEL.ViewportMask.DEFAULT | WIND.MODEL.ViewportMask.MEASURE);
    if(viewData){
      viewPoints.push({ noteName: name, data: viewData, id: Math.random().toString(36)});
      message.success('视口创建成功~');
      this.setState({
        viewPoints:  _.reverse(viewPoints.slice(-10)),
        viewPointModelVisible: false,
        currentApp
      },()=> currentApp.configDevice({ keyboardOpened: true }))
    }
  };

  //点击创建选择集
  handleCreate = (type, value)=>{
    const {currentApp} = this.child.state;
    let entities = currentApp.action().getSelections();
    if(!entities.length){
      return message.warning('请在模型上选择构件集~');
    }
    this.setState({ entities });
    this.switchState(type, value);
  };

  //确认创建选择集
  confirmCreateSection = async(name) =>{
    const {currentApp} = this.child.state;
    const {entities, sectionSets} = this.state;
    if (!name){ return message("请输入选择集名称~"); } 
    let sectionSetData = await currentApp.data().createSelectionSet(entities,name);
    if(sectionSetData){
      currentApp.control().createComponentSet(sectionSetData.setId, entities);
      sectionSets.push(sectionSetData);
      message.success('选择集创建成功~');
      this.setState({
        sectionSets: _.reverse(sectionSets.slice(-10)),
        sectionModelVisible: false,
        currentApp
      })
    }
  };

  handleDetail = (item)=>{
    this.setState({
       sectionTableVisible: true,
    },()=>this.getSectionDetail(item.setId))
  }

  //获取选择集构件详情
  getSectionDetail = async(setId) =>{
    const { currentApp } = this.child.state;
    const { models } = this.state;
    let selectionSetDetail = await currentApp.data().grabSelectionSet(setId, models[0].modelId, models[0].version);
    this.setState({
      sectionSetsData: selectionSetDetail,
      currentApp
    })
  };

  //创建涂鸦
  createComment = ()=>{
    const {currentApp} = this.child.state;
    this.exitComment();
    let graffiConfig = {
      lineRGB255: [255, 0, 0], //批注颜色
      textSize: 14, //批注字体
      lineWidth: 3 //批注线框
    }
    currentApp.toolkit().configGraffiti(graffiConfig);
    currentApp.toolkit().openGraffiti();
    this.setState({
      drawToolVisible: true,
      currentApp
    })
  };

  //退出涂鸦
  exitComment = ()=>{
    const {currentApp} = this.child.state;
    // let state = currentApp.create().getAppState();
    // if (state._annotationOpened) {
    //重置
    let annoConfig = {
      lineRGB255: [255, 0, 0], //批注颜色
      textSize: 14, //批注字体
      lineWidth: 3 //批注线框
    }
    currentApp.toolkit().setGraffitiType(GraffitiType.ARROW);
    currentApp.toolkit().configGraffiti(annoConfig)
    currentApp.toolkit().closeGraffiti();
    // }
    this.setState({ drawToolVisible: false })
  };

  //保存涂鸦
  saveComment = ()=>{
    const {currentApp} = this.child.state;
    let imgHref = currentApp.screenShot();
    this.setState({
      commentModelVisible: true,
      screenShot: imgHref
    })
  };

  //确认创建涂鸦
  confirmCreateComment = (name) =>{
    const {currentApp} = this.child.state;
    const {commentList, screenShot} =this.state;
    if (!name){ return message("请输入批注描述~");} 
    commentList.push({ noteName: name, img: screenShot, id: Math.random().toString(36)});
    message.success('保存成功~')
    this.setState({
      commentList: _.reverse(commentList.slice(-10)),
      commentModelVisible: false,
      drawToolVisible: false,
      screenShot: null
    },()=>this.exitComment())
    
  };

  
  render() {
    let { config, models,currentApp, commentVisible, viewPointVisible, sectionSetVisible,isShow,
          sectionModelVisible, sectionTableVisible, commentModelVisible, viewPointModelVisible, 
          drawToolVisible, sectionSets, commentList, viewPoints, sectionSetsData, drawerKey} = this.state;
    const btnInfo = [
      {
        title: '视口',
        icon: ViewPointIcon,
        action: () => this.handleOpenDrawer('viewPoint')
      },
      {
        title: '选择集',
        icon: SectionSetIcon,
        action: () => this.handleOpenDrawer('sectionSet')
      },
      {
        title: '批注',
        icon: CommentIcon,
        action: () => this.handleOpenDrawer('comment')
      },
    ];

    

    return (
      <div className="BIMPlaform">
        <ModelHeader name={'BIM协同应用'} 
                     btns={btnInfo}
                     onRef2 ={this.onRef2}
        />
        <div className="BIMPlaform-content">
            <div className={`bim-wrapper ${drawerKey? 'drawerWidth' : 'fullWidth'}`}>
              <ModelView  config = { config } 
                          links = { models }
                          mainMenu = { true }
                          onRef = { this.onRef }
                          hasDrawer = { drawerKey }
              />
            </div>
            <div className="drawer-wrapper">
              {
                sectionSetVisible && <SectionSet visible={sectionSetVisible}
                                                 app={currentApp}
                                                 sectionSets={sectionSets}
                                                 handleDetail
                                                 onDetail={this.handleDetail}
                                                 onCreate= {()=>this.handleCreate('sectionModelVisible',true) }
                                                 onClose={()=>{ 
                                                   this.revertStatus(); 
                                                   this.closeDrawer('sectionSetVisible',false);
                                                  }}
                                      />
              }
              {
                viewPointVisible && <ViewPoint visible={viewPointVisible}
                                               app={currentApp}
                                               viewPoints={viewPoints}
                                               onCreate= {this.handleCreateViewPoints}
                                               onClose={()=>{this.closeDrawer('viewPointVisible',false);}}
                                    />
              }
              {
                commentVisible && <Comment visible={commentVisible}
                                           app={currentApp}
                                           commentList = {commentList}
                                           onCreate= {this.createComment}
                                           onSave ={this.saveComment}
                                           onClose={()=>{
                                             this.exitComment(); 
                                             this.closeDrawer('commentVisible',false)
                                            }}
                                  />
              }
            </div>
            <div className="model-wrapper">
              {
                sectionModelVisible && <MoveModel title="创建选择集"
                                                  keyWords={'sectionSet'}
                                                  visible= {sectionModelVisible}
                                                  onCreate= {this.confirmCreateSection}
                                                  onClose={()=>this.switchState('sectionModelVisible',false)}
                                        />
              }
              {
                sectionTableVisible && <SectionSetTable 
                                                  app={currentApp}
                                                  models={models}
                                                  sectionSetsData={sectionSetsData}
                                                  visible={sectionTableVisible}
                                                  onClose={()=>this.switchState('sectionTableVisible',false)}
                                        />
              }
              {
                commentModelVisible && <MoveModel title="创建批注"
                                                  keyWords={'comment'}
                                                  visible= {commentModelVisible}
                                                  onCreate= {this.confirmCreateComment}
                                                  onClose={()=>this.switchState('commentModelVisible',false)}
                                        />
              }
              {
                viewPointModelVisible && <MoveModel title="创建视口"
                                                  keyWords={'viewPoints'}
                                                  visible= {viewPointModelVisible}
                                                  onCreate= {this.confirmCreateViewPoints}
                                                  onClose={()=>this.switchState('viewPointModelVisible',false)}
                                        />
              }
            
            </div>
            <div className="drawTool-wrapper">
              { drawToolVisible && <DrawTool onSelect={this.commentTypeOperate} 
                                             GraffitiType={GraffitiType}
                                             app={currentApp}
                                             onCancel={this.exitComment}
                                             onSave={this.saveComment}
                                  />
              }
            </div>
        </div>
     </div>
    )
  }

}

export default BIMPlatform;
