/**
 * Created by yk on 2021/2/20.
 * 功能: 使被包裹的组件实现右拖拽变大
 */

import React from 'react';
import {Rnd} from 'react-rnd';
import '../../scene.less';

class ListDrag extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width: props.defaultWidth || (props.minWidth > 300 ? props.minWidth : 300),
            height: '100%',
            x: 0,
            y: 0,
            dragData: {}
        };

        // this.onSave = (width) => {
        //     const {project, user, dragKey} = this.props;
        //     let {dragData} = this.state;
        //     if(project && project.type && user && user._id && dragKey) {
        //         dragData[project.type + '_' + dragKey] = width;
        //         setConfig(USER, user._id, WEB_LIST_DRAG, dragData).then().catch();
        //     }
        // }

        this.request = () => {
            const { dragKey, defaultWidth, onChange} = this.props;
            // if(project && project.type && user && user._id && dragKey) {
            //     getConfig(USER, user._id, WEB_LIST_DRAG).then(res => {
            //         let initWidth = (res && res[project.type + '_' + dragKey]) || defaultWidth || this.state.width;
            //         this.setState({
            //             dragData: res || {},
            //             width: initWidth,
            //         })
            //         onChange && onChange(initWidth)
            //     });
            // } else {
                defaultWidth && this.setState({width: defaultWidth});
                onChange && onChange(defaultWidth || 800);
            // }
        }

    }

    componentDidMount() {
        this.request();
        if (this.props.onRef) {
            this.props.onRef(this);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if((prevProps.defaultWidth !== this.props.defaultWidth) ||
            (prevProps.dragKey !== this.props.dragKey)) {
            this.request();
        }
    }

    render() {
        const {className, leftContent, rightContent, minWidth, maxWidth, onChange, noDrag} = this.props;
        const {width, height, x, y} = this.state;

        return (
            <div className={`react-rnd-list-drag ${className ? className : ''}`}>
                {
                    leftContent ? <Rnd size={{width, height}}
                                       position={{x, y}}
                                       className='list-drag-left'
                                       onDragStop={(e, d) => { this.setState({ x: d.x, y: d.y }) }}
                                       enableResizing = {{
                                           bottom: false,
                                           bottomLeft: false,
                                           bottomRight: false,
                                           left: false,
                                           right: !noDrag,
                                           top: false,
                                           topLeft: false,
                                           topRight: false,
                                       }}
                                       disableDragging = {true}
                                       maxWidth={maxWidth ? maxWidth : 300}
                                       minWidth={minWidth ? minWidth : 100}
                                       onResize={(e, direction, ref, delta, position) => {
                                           this.setState({width: ref.style.width})
                                           onChange && onChange(parseFloat(ref.style.width))
                                       }}
                                       onResizeStop={(e, direction, ref, delta, position) => {
                                         
                                       }}
                                       >
                        {leftContent}
                    </Rnd> : null
                }
                {
                    rightContent ? <div className="list-drag-right" style={{left: leftContent ? width : 0}}>
                        {rightContent}
                    </div> : null
                }
            </div>
        )
    }
}

export default ListDrag;
