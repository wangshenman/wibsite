export const colorsBlock = [
    {
        color: [[171, 218, 255], [255, 255, 255]],
        key: '#ABDAFF#FFFFFF',
        gradient: true,
    },
    {
        color: [[196, 215, 213], [255, 255, 255]],
        key: '#C4D7D5#FFFFFF',
        gradient: true,
    },
    {
        color: [[255, 255, 255]],
        key: '#FFFFFF',
        gradient: false,
    },
    {
        color: [[220, 219, 201]],
        key: '#DCDBC9',
        gradient: false,
    },
    {
        color: [[195, 191, 217]],
        key: '#C3BFD9',
        gradient: false,
    }, 
    {
        color: [[56, 64, 85]],
        key: '#384055',
        gradient: false,
    },
    {
        color: [[0, 0, 0]],
        key: '#000000',
        gradient: false,
    },
];
