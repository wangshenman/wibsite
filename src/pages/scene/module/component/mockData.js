export const drawingsData = [
    {
        modelId: "6049e7013d0e6b5b6cf41c3d",
        modelLid: 132,
        name: "Project Views",
        type: "Project Views",
        version: 1,
        viewId: "10D",
        _id: "6049e7261818780270ef0c9e"
    },
    {
        modelId: "6049e7013d0e6b5b6cf41c3d",
        modelLid: 132,
        name: "F1",
        type: "楼层平面",
        version: 1,
        viewId: "169",
        _id: "6049e7261818780270ef0c9f",
    },
    {
        modelId: "6049e7013d0e6b5b6cf41c3d",
        modelLid: 132,
        name: "F1",
        type: "天花板平面",
        version: 1,
        viewId: "16F",
        _id: "6049e7261818780270ef0ca0",
    },
    {
        modelId: "6049e7013d0e6b5b6cf41c3d",
        modelLid: 132,
        name: "东",
        type: "立面",
        version: 1,
        viewId: "907",
        _id: "6049e7261818780270ef0ca1"
    },
    {
        modelId: "6049e7013d0e6b5b6cf41c3d",
        modelLid: 132,
        name: "南",
        type: "立面",
        version: 1,
        viewId: "92C",
        _id: "6049e7261818780270ef0ca2"
    }, 
    {
        modelId: "6049e7013d0e6b5b6cf41c3d",
        modelLid: 132,
        name: "西",
        type: "立面",
        version: 1,
        viewId: "938",
        _id: "6049e7261818780270ef0ca3"
    },
    {
        modelId: "6049e7013d0e6b5b6cf41c3d",
        modelLid: 132,
        name: "北",
        type: "立面",
        version: 1,
        viewId: "944",
        _id: "6049e7261818780270ef0ca4"
    },
];
