import React, { Component } from 'react';
import {  withRouter } from 'react-router-dom';
import {  Button, Icon } from 'antd';
import navLogo from '../../../../../common/img/common/developer_logo.png';
import './header.less';


class ModelHeader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentItem: null,
    }
    this.handleClick = (item)=>{
      this.setState({
        currentItem: item
      },()=> item.action())
    }
  }

  componentDidMount() {
    if (this.props.onRef2) {
        this.props.onRef2(this);
    }
  };
  
  clearCurrent = () =>{
    this.setState({
        currentItem: null
    })
  };

  render() {
    const { name, btns, version, slaveVersion, visible, isShow,toggleDrawer} = this.props;
    const { currentItem } = this.state;
  
    return (
        <div className="model-header">
            <div className="model-wrapper">
              <div className="model-front">
                  <img src={navLogo} alt="中间件官网Logo" className="model-logo" />
                  <span className="model-name">{`${name}`}</span>
                  {
                    version &&
                    <span className="model-version">{`V${version}.0`}</span>
                  }

                  {
                    visible &&
                    <span>
                       <span className="model-text">VS</span>
                       <span className="model-version">{`V${slaveVersion}.0`}</span>
                    </span>
                   
                  }
              </div>
              <div className={`model-btns flex-between ${ currentItem? 'hasFold':'noFold'}`}>
                  <div className="flex-between">
                  {
                    btns && (btns || []).map((i,index)=>{
                      return(
                          <span className={`btn-item ${(currentItem && currentItem.title) === i.title ? 'select-item' : ''}`} onClick={()=>this.handleClick(i)} key={index}>
                              <Icon className="btn-icon" component={i.icon}> </Icon>
                              <span>{i.title}</span>
                          </span>
                      )
                    })
                  }
                  </div>
                  {/* {
                    currentItem  && 
                    <span className='foldIcon' onClick={()=>toggleDrawer()}>
                      {
                          isShow ? <Icon className="btn-icon" type="menu-unfold" /> : <Icon className="btn-icon" type="menu-fold" />
                      }
                    </span> 
                  } */}
              </div>
            </div>
        </div>)
  }
}



export default withRouter(ModelHeader);

