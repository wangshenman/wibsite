/* 图纸批注展示 */
import React from 'react';
import Zmage from 'react-zmage';
import { Drawer, Button, Icon} from 'antd';
import Empty from '../../../../../common/component/Empty';
import empty_img from '../../../../../common/img/sence/empty_img.png';
import moment from 'moment';

class Comment extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            drawingsData: [],
            currentItem: null
        }

        //定位批注视口
        this.handleLocation = (item) =>{
            const {app} = this.props;
            if(item && item.data){
                this.setState({
                  currentItem: item
                },()=> app.create().loadAnnotation(item.data, true))
               
            }
        };
    }
    componentDidMount() {
        const {commentList} = this.props;
        if(commentList.length){
            this.setState({
                drawingsData: commentList,      
            })
           
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.commentList  !== this.props.commentList){
            this.setState({
                drawingsData: nextProps.commentList,
            })
        }
    } 
  
    render() {
        const { onClose, onCreate } = this.props;
        const { drawingsData ,currentItem } = this.state;

        const listShow = () =>{
            return(
                <div className="commentsList">
                    <div className="commentsContent">
                        {
                            drawingsData && drawingsData.length?
                            <ul>
                            {
                                (drawingsData||[]).map((i, index)=>{
                                    return(<li className={'comment-item flex-start'} key={index}>
                                            <Zmage className="item-left" 
                                                   src={i.img} 
                                                   alt="批注图片" 
                                                   zIndex={1000}
                                            />
                                            <div className="item-right flex-column-between">
                                                <span className="item-name nowrap">{i.noteName}</span>
                                                <div className="item-info flex-between">
                                                   <span className="item-date"> { moment(i.updateAt).format('YYYY-MM-DD')}</span>
                                                   <Icon type="zoom-in" className="location" onClick={()=>this.handleLocation(i)}/>
                                                </div>
                                            </div>
                                        </li>)
                                })
                            }
                            </ul>:
                            <Empty message="可利用模型批注与其他人快速交流BIM业务"
                                   img={empty_img}
                                   imgHeight={198}
                            />
                        }
                        
                    </div> 
                </div>
            )
              
        };
       
        return( 
          <div className={`bimSenceWrapper`} id="comment">
              <Drawer 
                      title="批注列表"
                      placement='right'
                      visible={true}
                      mask={false}
                      width={300}
                      maskClosable={false}
                      keyboard={false}
                      zIndex={100}
                      onClose={onClose}
                      getContainer={document.getElementById("comment")}
              >
                 <div className="bimSenceContent">
                     { listShow() }
                </div>
                <div className="bottomOperate">
                    <Button style={{width:'260px',height:'32px'}} type="primary" onClick={onCreate}>创建批注</Button>
                </div> 
              </Drawer>
          </div>) 
      }

}
export default Comment;