/* 构件属性展示 */
import React from 'react';
import { Tooltip, Icon, Input ,Select} from 'antd';
import { PaintIcon, ArrowIcon, ReactIcon, CircleIcon, 
         CloudIcon, TextIcon, SaveIcon, CancelIcon, 
} from '../../component/svgIcon';

const { Option } = Select;

class DrawTool extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentType: 'arrow',
            inputName:'',
            color: '#FF0000',
            fontSize: 14,
            lineWidth: 3
        }

        this.handleName = (value) =>{
            this.setState({
                inputName: value
            })
        }

        //颜色
        this.handleChangeColor = (value) =>{
            const{ app }=this.props;
            let color = value;
            let hex = color.substring(1);
            if (hex.length === 3) hex += hex;
            let red = parseInt(hex.substring(0, 2), 16);
            let green = parseInt(hex.substring(2, 4), 16);
            let blue = parseInt(hex.substring(4), 16);
            this.setState({
                color: value
            },()=> app.create().configAnnotation({ lineRGB255: [red, green, blue]}))
        };

        //字号
        this.handleChangeSize = (value) =>{
            const{ app }=this.props;
            const{ currentType } =this.state;
            this.setState({
              fontSize: value
            },()=> app.create().configAnnotation({ textSize: value}))
        };

        //线宽
        this.handleChangeWidth = (value) =>{
            const{ app }=this.props;
            this.setState({
                lineWidth: value
            },()=> app.create().configAnnotation({ lineWidth: value}))
        };

        //类别操作
        this.commentTypeOperate = (key) =>{
            const { app, AnnotationType, onSave, onCancel } = this.props;
            this.setState({
                currentType: key
            })
            switch (key) {
              case 'arrow':
                app.create().setAnnotationType(AnnotationType.ARROW);
                break;
              case 'cloud':
                app.create().setAnnotationType(AnnotationType.CLOUD_WIRE);
                break;
              case 'rect':
                app.create().setAnnotationType(AnnotationType.RECT_WIRE);
                break;
              case 'circle':
                app.create().setAnnotationType(AnnotationType.CIRCLE_WIRE);
                break;
              case 'text':
                app.create().setAnnotationType(AnnotationType.TEXT);
                break;
              case 'pencil':
                app.create().setAnnotationType(AnnotationType.BRUSH);
                break;
              case 'close':
                onCancel();
                break;
              case 'save':
                onSave();
                break;
              default:
                break;
            }
            
        };
    }
   
  
    render() {
        const { color, fontSize, lineWidth, currentType} = this.state;
        let drawTool = [
            {
                title: '矩形',
                type: 'rect',
                icon: ReactIcon
            },
            {
                title: '圆形',
                type: 'circle',
                icon: CircleIcon
            },
            
            {
                title: '云图',
                type: 'cloud',
                icon: CloudIcon
            },
            {
                title: '箭头',
                type: 'arrow',
                icon: ArrowIcon
            },
            {
                title: '画笔',
                type: 'pencil',
                icon: PaintIcon
            },
            // {
            //     title: '文字',
            //     type: 'text',
            //     icon: TextIcon
            // },
            {
                title: '取消',
                type: 'close',
                icon: CancelIcon
            },
            {
                title: '保存',
                type: 'save',
                icon: SaveIcon
            }
        ];
        
        return( 
            <div className="draw-tool-layout ">
                <div className="draw-tool-setting flex-between">
                    <div className="setting-item flex-start">
                        <span>颜色：</span>
                        <Input style={{width: 60}} type="color" value={color}  onChange={e => this.handleChangeColor(e.target.value)}/>
                    </div>
                    <div className="setting-item flex-start">
                        <span>大小：</span>
                        <Select style={{width: 60}} 
                                value={lineWidth}
                                onChange={value => this.handleChangeWidth(value)}>
                                <Option value={1}>1</Option>
                                <Option value={3}>3</Option>
                                <Option value={6}>6</Option>
                                <Option value={10}>10</Option>
                                <Option value={16}>16</Option>
                        </Select> 
                    </div>
                    <div className="setting-item flex-start">
                        <span>字号：</span>
                        <Select style={{width: 60}} 
                                value={fontSize}
                                onChange={value => this.handleChangeSize(value)}>
                                <Option value={14}>14</Option>
                                <Option value={16}>16</Option>
                                <Option value={24}>24</Option>
                                <Option value={32}>32</Option>
                                <Option value={40}>40</Option>
                        </Select> 
                    </div>
                </div>
                <div className="draw-tool-type flex-between">
                {
                    drawTool.map((item, index) => {
                        return (
                            <Tooltip key={index} title={item.title} placement="top">
                                <span className={`tool-item ${currentType === item.type ? 'selectedType' : ''}`} onClick={() => this.commentTypeOperate(item.type)}>
                                    <Icon className="tool-icon" component={item.icon}> </Icon>
                                </span>      
                            </Tooltip>)
                    })
                }
                </div>
                
            </div>
        )
    }

}
export default DrawTool;