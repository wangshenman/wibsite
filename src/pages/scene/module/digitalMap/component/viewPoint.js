import React from 'react';
import { Drawer, Button, Icon} from 'antd';
import { LocationIcon } from '../../component/svgIcon';
import Empty from '../../../../../common/component/Empty';
import empty_img from '../../../../../common/img/sence/empty_img.png';


class ViewPoint extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            viewPoints: [],
            currentSelect: null,
        }
        //定位视口
        this.handleLocation =(item) =>{
            this.setState({
                currentSelect: item
            },()=>this.goLocation(item.data)) 
        }

        this.goLocation = (data) =>{
            const {app} = this.props;
            if(app && data){
                app.loadViewport(data);
            }
        }
    }
    componentDidMount() {
        const {viewPoints} = this.props;
        if(viewPoints.length){
            this.setState({
                viewPoints,
                currentSelect: viewPoints[0]
            }, ()=>this.handleLocation(viewPoints[0])) 
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.viewPoints  !== this.props.viewPoints){
            this.setState({
                viewPoints: nextProps.viewPoints,
                currentSelect: nextProps.viewPoints[0]
            }, ()=>this.handleLocation(nextProps.viewPoints[0])) 
        }
    } 
  
    render() {
        const { onClose, onCreate } = this.props;
        const { viewPoints, currentSelect } = this.state;

        const listShow = () =>{
            return(
                <div className="sectionSetList">
                    <div className="sectionSetContent">
                        {
                            viewPoints && viewPoints.length?
                            <ul>
                            {
                                (viewPoints||[]).map((i, index)=>{
                                    return(<li className={`section-item flex-between ${(currentSelect && currentSelect.id) == i.id ? 'selectedItem' : 'unSelectedItem'}`} key={index}>
                                                <span className="section-name">{i.noteName}</span>
                                                <Icon className="location" component={LocationIcon} onClick={()=>this.handleLocation(i)}/>
                                        </li>)
                                })
                            }
                            </ul>:
                            <Empty message={'视口保存当前模型状态，再次打开可快速定位模型位置'}
                                   img={empty_img}
                                   imgHeight={198}
                            />
                        }
                        
                    </div> 
                </div>
            )
        };
       
        return( 
          <div className={`bimSenceWrapper`} id="viewPoint">
              <Drawer 
                      title="视口列表"
                      placement='right'
                      visible={true}
                      mask={false}
                      width={300}
                      maskClosable={false}
                      keyboard={false}
                      zIndex={100}
                      onClose={onClose}
                      getContainer={document.getElementById("viewPoint")}
              >
                 <div className="bimSenceContent">
                     { listShow() }
                </div>
                <div className="bottomOperate">
                    <Button style={{width:'260px',height:'32px'}} type="primary" onClick={onCreate}>创建视口</Button>
                </div> 
              </Drawer>
          </div>) 
      }
}

export default ViewPoint;