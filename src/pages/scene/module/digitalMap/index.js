import React from 'react'; 
import {message } from 'antd';
import './digitalMap.less';
import ModelHeader from '../component/modelHeader/ModelHeader';
import DrawingView from '../../../bimDrawing/component/drawingView/drawingView';
import ViewPoint from './component/viewPoint';
import Comment from './component/comment';
import MoveModel from './component/moveModel';
import DrawTool from './component/drawTool';
import { ViewPointIcon, CommentIcon } from '../component/svgIcon';
import { getToken, getExampleToken } from '../../../../common/utils/commonActions';
import { getURLValueByKey } from '../../../../common/utils/Utility';
import { modelConfig } from '../../../../common/config/websiteConfig';
import _ from "lodash";

const AnnotationType = window.WIND.WIND.AnnotationType;
const ActionSaveType = window.WIND.WIND.ActionSaveType;

class DigitalMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      config: null,
      models: [],
      currentApp: null,
      commentVisible: false, //批注
      viewPoints:[],
      viewPointVisible: false, //视口
      commentModelVisible: false,
      viewPointModelVisible: false,
      drawToolVisible: false, //批注工具
      commentList:[], //选择集列表
      screenShot: null
    }

    this.onRef = (ref) =>{
      this.child = ref
    };

    this.onRef2 = (ref) =>{
      this.child2 = ref
    };

    this.closeDrawer = (type, value) =>{
      this.setState({ drawerKey: null },()=> this.onResizeModel());
      this.setState({[type]: value},()=> this.child2.clearCurrent());
    };

    this.init =()=>{
      let modelId = getURLValueByKey('modelId');
      let version = parseInt(getURLValueByKey('version'));
      if( modelId && version){
        getExampleToken().then(res =>{
          if(res && res.code == 200){
            let modelToken = res.data.token;
            this.setState({
              config: { serverIp: modelConfig.serverIp, token: modelToken },
              models:[{ modelId: modelId, version}],
            })
          }
        })
      }
    };
    
  }

  componentDidMount() {
    this.init();
  };

  switchState = (type, value) => {
    this.setState({[type]: value})
  };

  //打开Drawer
  handleOpenDrawer = (key) =>{
    switch (key) {
      case 'viewPoint':
        this.setState({
          viewPoints:[],
          commentVisible: false, //批注
          viewPointVisible: true, //视口
          drawToolVisible: false,
          drawerKey:'viewPoint'
        },()=> this.onResizeModel())
        break;
      case 'comment':
        this.setState({
          commentList: [],
          commentVisible: true, //批注
          viewPointVisible: false, //视口
          drawToolVisible: false,
          drawerKey:'comment'
        },()=> this.onResizeModel())
        break;
      default:
        break;
    }
  };

  //确认创建视口
  confirmCreateViewPoints = async(name) =>{
    const {currentApp} = this.child.state;
    const {viewPoints} =this.state;
    if (!name){ return message("请输入视口名称~")} 
    let viewData = currentApp.saveViewport();
    if(viewData){
      viewPoints.push({ noteName: name, data: viewData, id: Math.random().toString(36)});
      message.success('视口创建成功~');
      this.setState({
        viewPoints: _.reverse(viewPoints.slice(-10)),
        viewPointModelVisible: false,
        currentApp
      })
    }
  };

  //创建批注
  createComment = ()=>{
    const {currentApp} = this.child.state;
    this.exitComment();
    let annoConfig = {
        lineRGB255: [255, 0, 0], //批注颜色
        textSize: 14, //批注字体
        lineWidth: 3 //批注线框
    }
    currentApp.create().configAnnotation(annoConfig)
    currentApp.create().openAnnotation();
    this.setState({
      drawToolVisible: true,
      currentApp
    })
  };

  //退出批注
  exitComment = ()=>{
    const {currentApp} = this.child.state;
    // let state = currentApp.create().getAppState();
    // if (state._annotationOpened) {
    //重置
    let annoConfig = {
      lineRGB255: [255, 0, 0], //批注颜色
      textSize: 14, //批注字体
      lineWidth: 3 //批注线框
    }
    currentApp.create().setAnnotationType(AnnotationType.ARROW);
    currentApp.create().configAnnotation(annoConfig)
    currentApp.create().closeAnnotation();
    // }
    this.setState({ drawToolVisible: false })
  };

  //保存批注
  saveComment = ()=>{
    const {currentApp} = this.child.state;
    let imgHref = currentApp.screenShot();
    this.setState({
      commentModelVisible: true,
      screenShot: imgHref
    })
  };

  //确认创建批注
  confirmCreateComment = async(name) =>{
    const { currentApp } = this.child.state;
    const { commentList, screenShot } =this.state;
    if (!name){ return message("请输入批注描述~");} 
    let annotationData = await currentApp.create().saveAnnotation();
    // if(annotationData){
      commentList.push({ noteName: name, data: annotationData, img: screenShot, id: Math.random().toString(36)});
      message.success('批注创建成功~')
      this.setState({
        commentList: _.reverse(commentList.slice(-10)),
        commentModelVisible: false,
        drawToolVisible: false,
        screenShot: null,
        currentApp
      },()=> this.exitComment())
    // }
  };

  onResizeModel = ()=>{
    const { currentApp } = this.child.state;
    currentApp.screenResize();
  }
  
  render() {
    let { config, models, currentApp, commentList, viewPoints, commentVisible, viewPointVisible,
      drawerKey, commentModelVisible, viewPointModelVisible, drawToolVisible} = this.state;
    const btnInfo = [
      
      {
        title: '视口',
        icon: ViewPointIcon,
        action: () => this.handleOpenDrawer('viewPoint')
      },
      {
        title: '批注',
        icon: CommentIcon,
        action: () => this.handleOpenDrawer('comment')
      },
    ];

    return (
      <div>
        <ModelHeader name={'图纸在线管理'} 
                     btns={btnInfo}
                     onRef2 = { this.onRef2 }
        />
        <div className="digitalMap"  id="digitalMap">
           <div className={`bim-wrapper ${drawerKey? 'drawerWidth' : 'fullWidth'}`}>
              <DrawingView  config = { config } 
                            links = { models }
                            onRef = { this.onRef }
              />
            </div>
            <div className="drawer-wrapper">
              {
                viewPointVisible && <ViewPoint visible={viewPointVisible}
                                                app={currentApp}
                                                viewPoints={viewPoints}
                                                onCreate= {()=>this.switchState('viewPointModelVisible',true)}
                                                onClose={()=>this.closeDrawer('viewPointVisible',false)}
                                    />
              }
              {
                commentVisible && <Comment visible={commentVisible}
                                           app={currentApp}
                                           commentList ={commentList}
                                           onCreate= {this.createComment}
                                           onClose={()=>{
                                             this.exitComment();
                                             this.closeDrawer('commentVisible',false)
                                           }}
                                  />
              }
            </div>
            <div className="model-wrapper">
              {
                commentModelVisible && <MoveModel title="创建批注"
                                                  keyWords={'comment'}
                                                  visible= {commentModelVisible}
                                                  onCreate= {this.confirmCreateComment}
                                                  onClose={()=>this.switchState('commentModelVisible',false)}
                                        />
              }
              {
                viewPointModelVisible && <MoveModel title="创建视口"
                                                  keyWords={'viewPoints'}
                                                  visible= {viewPointModelVisible}
                                                  onCreate= {this.confirmCreateViewPoints}
                                                  onClose={()=>this.switchState('viewPointModelVisible',false)}
                                        />
              }
            </div>
            <div className="drawTool-wrapper">
              { drawToolVisible && <DrawTool AnnotationType={AnnotationType} 
                                             app={currentApp}
                                             onCancel={this.exitComment}
                                             onSave={this.saveComment}
                                  />
              }
            </div>
        </div>
     </div>
    )
  }

}

export default DigitalMap;
