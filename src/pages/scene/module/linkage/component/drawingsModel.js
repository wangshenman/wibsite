/* 构件属性展示 */

import React from 'react';
import { Modal, message,Table } from 'antd';
import { outDrawingList } from '../../../../../common/config/homeJson';

class DrawingsModel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedRows: null
        }

        this.onRowClick = (record,e)=>{
            this.setState({selectedRows: record})
        };

        this.openOuterDrawing = ()=>{
            const { selectedRows } = this.state;
            if(selectedRows){
                this.props.openOuterDrawing(selectedRows);
            }else{
                message.warning('请选择要打开的图纸~');
            } 
        };

    }
    componentDidMount() {}

    componentWillReceiveProps(nextProps){} 
  
    render() {
        const { visible, switchState, } = this.props;
        let columns = [
            {
                title: '图纸名称',
                dataIndex: 'drawingName',
                width: 240,
                render: (text, record) => text
            },
            {
                title: '图纸版本',
                dataIndex: 'drawingVersion',
                width: 150,
                render: (text, record) => text
            },
           
        ];
        const rowRadioSelection={
            type:'radio',
            columnTitle:"选择",
            onSelect: (selectedRowKeys, selectedRows) => {
              this.setState({selectedRows: selectedRowKeys})
            },
        }
  
        return( 
            <Modal title="图纸列表" 
                visible={visible} 
                okText={'打开'}
                cancelText={'取消'}
                centered
                onOk={this.openOuterDrawing} 
                onCancel= {()=> switchState( 'drawingModalVisible', false)}
              >
                <div className="outerDrawings">
                    <Table columns={columns}
                           rowKey={record => record.key}
                           rowSelection={rowRadioSelection}
                           dataSource={outDrawingList}
                           pagination={{pageSize: 5}}
                           bordered={false}
                     />
                </div>
              </Modal> 
        )
    }

}
export default DrawingsModel;