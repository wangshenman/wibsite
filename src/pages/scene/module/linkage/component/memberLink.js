/* 构件联动展示 */
import React from 'react';
import { Drawer, Tree,  Select, Spin, Icon } from 'antd';
import { PreprocessIcon, UnpreprocessIcon } from '../../component/svgIcon';
const { TreeNode } = Tree;
const { Option } = Select;

class MemberLink extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            currentVersion: 1,
            histories: [],
            drawingList:[],
            originList:[],
            selectedKeys: null, 
        }

        //获取内部图纸列表
        this.getModelViewLists = async () =>{
            const { app, models } = this.props;
            if(app){
                let modelViews = await app.network().grabModelViewLists(models[0].modelId, models[0].version);
                let drawingList = modelViews && modelViews.length && this.transferDrawingData(modelViews);
                this.setState({
                    drawingList,
                    originList: modelViews
                })
                
            }
        };
        //内部图纸数据整合
        this.transferDrawingData = (drawingList)=>{
            let paramsMap = {};
            let destArray = [];
            for (let i = 0; i < drawingList.length; i++) {
                let params = drawingList[i];
                if (!paramsMap[params.type]) {
                    destArray.push({
                        _id: i,
                        name: params.type,
                        drawings: [params]
                    });
                    paramsMap[params.type] = params;
                } else {
                    for (let j = 0; j < destArray.length; j++) {
                        let dest = destArray[j];
                        if (dest.name === params.type) {
                            dest.drawings.push(params);
                            break;
                        }
                    }
                }
            }
            return destArray;
        };

        //切换模型版本
        this.handleChangeVersion = (value) =>{
            const { currentModelId } = this.props;
            this.setState({
              currentVersion: value,
            },()=> {
                this.props.closeDrawing();
                this.getModelViewLists();
            })
        };

        this.onSelect= (key) =>{
            const {originList} = this.state;
            let selectedKey = key[0];
            let selectedDrawing = originList.find(i => i._id === selectedKey);
            //子节点点击触发
            if(selectedKey && selectedDrawing){
                this.setState({ selectedKeys: key },()=>{
                    this.props.openDrawing(selectedDrawing);
                }) 
            } 
        }

        
    }

    componentDidMount() {
        if (this.props.onRef3) {
            this.props.onRef3(this);
        }
        this.getModelViewLists();
    };

    componentWillReceiveProps(nextProps){
    };

    setSelectedKeys = ()=>{
        this.setState({ selectedKeys: null}) 
    };
  
    render() {
        const { onClose, } = this.props;
        const { loading, currentVersion, selectedKeys, drawingList } = this.state;

        const moreOperate = (data)=>{
            let isItem = parseInt((data._id).length) === 24;
            return(
                <div className="section-item flex-between" key={data._id} >
                     <span className="section-name">{data.name}</span> 
                    {
                        isItem && (data.preprocessed === 0 ?  
                        <Icon component={ PreprocessIcon } /> : 
                        <Icon component={ UnpreprocessIcon }/>)
                    }
                </div>
            )
        }

        const treeNodeView = (item, selectable = true) => {
            return (
              <TreeNode
                key={item._id}
                selectable={selectable}
                title={moreOperate(item)}
              >
                {(item.drawings||[]).map((i) => treeNodeView(i, true))}
              </TreeNode>
            );
          };

        return( 
          <div className="linkWrapper" id="memberLinkWrapper">
              <Drawer className={'memberLink'}
                      title={'构件联动'}
                      placement='right'
                      visible={true}
                      mask={false}
                      width={300}
                      zIndex={100}
                      keyboard={false}
                      maskClosable={false}
                      onClose={onClose}
                      getContainer={document.getElementById("memberLinkWrapper")}
              >
                  <Spin spinning={loading}/>
                  <div className="linkContent memberLinkContent">
                      <div className="linkModelHeader">
                          <span>模型视图：</span> 
                          <Select style={{width: 100}} 
                            className="model-select"
                            value={`V${currentVersion}.0`}
                            onChange={value => this.handleChangeVersion(value)}>
                                <Option value={1} >{`V1.0`}</Option>
                                {/* <Option value={2} >{`V2.0`}</Option> */}
                         </Select>
                     </div>
                     <div className="linkDrawingList">
                            <Tree
                                showIcon
                                blockNode
                                onSelect={this.onSelect}
                                selectedKeys={selectedKeys}
                                defaultExpandAll>
                                {drawingList.map((item) => treeNodeView(item))}
                            </Tree>
                     </div>
                  </div>
              </Drawer>
          </div>) 
      }

}
export default MemberLink;
