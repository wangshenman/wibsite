/* 位置联动展示 */
import React from 'react';
import { Drawer, Button, Icon, Modal} from 'antd';
import Empty from '../../../../../common/component/Empty';
import empty_img from '../../../../../common/img/sence/empty_img.png';

const { confirm } = Modal;
class PositionLink extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectionIds:[],
            currentStep: 0,
            drawingsData: [],
            currentItem: null
        }
        
        //取消
        this.handleCancel = () =>{
            this.props.onCancel()
        };

        //确认关联
        this.handleConfirm = ()=>{
           this.props.onSave()
        };
        
        //添加关联
        this.addLinkage = () =>{
            this.props.switchState('drawingModalVisible',true)
            this.props.switchState('showToolbar',false)
        };

        //打开外部图纸关联关系
        this.handleOpenLinkage = (item) =>{
            if(item){
                this.setState({
                    currentItem: item
                },()=>
                this.props.onOpen(item))
            }
        };

        this.deleteLinkage = (item)=>{
            let that = this;
            confirm({
                content: '是否删除当前图纸联动？删除位置关联后将无法恢复',
                centered: true,
                okText:'确定',
                cancelText:'取消',
                onOk() {
                    if(item && item._id){
                        that.props.onDelete(item._id);
                    }
                },
            }); 
        }
    }
    componentDidMount() {
        if (this.props.onRef4) {
            this.props.onRef4(this);
        }
        const {relationList,onOpen} = this.props;
        if(relationList && relationList.length){
            this.setState({
                drawingsData: relationList,
                currentItem: relationList[0],
            },()=>onOpen(relationList[0]))
        }
    };

    componentWillReceiveProps(nextProps){
        if(nextProps.relationList  !== this.props.relationList){
            this.setState({
                drawingsData: nextProps.relationList,
            })
            if(nextProps.relationList && nextProps.relationList.length){
                this.setState({
                    drawingsData: nextProps.relationList,
                    currentItem: nextProps.relationList && nextProps.relationList[0],
                },()=>this.props.onOpen(nextProps.relationList[0]))
            }
           
        }
    }; 

    setSelectedKeys = ()=>{
        this.setState({ currentItem: null}) 
    };
  
    render() {
        const {  onClose, current } = this.props;
        const {  drawingsData, currentItem } = this.state;

        const listShow = () =>{
            return(
                <div className="positionLinkList">
                    <div className="positionLinkContent">
                        {
                           drawingsData && drawingsData.length?
                            <ul>
                            {
                                (drawingsData||[]).map((i, index)=>{
                                    return(<li className={`drawing-item ${(currentItem && currentItem._id) === i._id ? 'selectedItem':''}`} key={index}>
                                            <div className="item-top flex-start">
                                                <span className="draw-version">{`V${i.drawingVersion}.0`}</span>
                                                <span className="draw-name nowrap">{i.drawingName}</span>
                                            </div>
                                            <div className="item-bottom flex-between">
                                                <span className="bottom-name nowrap">{i.name}</span>
                                                <span>
                                                    <Icon type="zoom-in" className="location bottom-icon" onClick={()=>this.handleOpenLinkage(i)}/>
                                                    <Icon type="delete" className="delete bottom-icon" onClick={()=>this.deleteLinkage(i)}/>
                                                    {/* <Icon type="edit" className="delete bottom-icon" onClick={()=>this.props.onUpdate(i)}/> */}
                                                </span>
                                            </div>
                                        </li>)
                                })
                            }
                            </ul>
                            :
                            <Empty  message={'暂无位置联动'}
                                    img={empty_img}
                                    imgHeight={198}
                            />
                        }
                        
                    </div> 
                </div>
            )
              
        };
        // const editShow =()=>{
        //     return(
        //         <div className="positionLinkOperate">
        //              <div className="linkModelHeader">
        //                  <span className="draw-version">{`V${current && current.version}.0`}</span>
        //                  <span className="draw-name">{`${current && current.name}`}</span>
        //             </div> 
        //             <Steps direction="vertical"  current={currentStep}>
        //                 <Step title="选择图纸位置" description="提示：请在图纸上按照顺序选择两个点" />
        //                 <Step title="选择模型位置" description="提示：请在模型上按照顺序选择两个点" />
        //                 <Step title="确认关联位置" description="提示：请确认图纸与模型关联位置" />
        //             </Steps>
        //             <div className="linkModelBottom"> 
        //                <Button style={{marginRight:'15px'}}  onClick={this.handleCancel}>取消</Button>
        //                {
        //                     (currentStep === 1 || currentStep === 2) &&  
        //                     <Button type="primary" style={{marginRight:'15px'}} onClick={()=>this.goNext(false)}>上一步</Button>
        //                }
        //                {
        //                    currentStep === 2 ?
        //                    <Button type="primary" onClick={this.handleConfirm}>确认关联</Button>
        //                    :
        //                    <Button type="primary" onClick={()=>this.goNext(true)}>下一步</Button>
        //                }
        //             </div>

        //         </div>
        //     )

        // };

        // const titleLayout = ()=>{
        //     return(
        //         <div className="drawer-title flex-between">
        //             <span>位置联动</span>
        //             <Icon className="switch-hide" 
        //                   type="minus" 
        //                   onClick={()=> {
        //                     this.props.switchState('positionDrawerVisible',false);
        //                   }}
        //            />
        //         </div>
        //     )
        // }
        return( 
          <div className={`linkWrapper`} id="positionLinkWrapper">
              <Drawer className={'positionLink'}
                      title='位置联动'
                      placement='right'
                      visible={true}
                      mask={false}
                      maskClosable={false}
                      width={300}
                      zIndex={100}
                      keyboard={false}
                      onClose={onClose}
                      getContainer={document.getElementById("positionLinkWrapper")}
              >
                 <div className="linkContent positionLinkContent">
                     {
                        listShow()
                     }
                      <div className="bottomOperate">
                         <Button style={{width:'260px',height:'32px'}} type="primary" onClick={this.addLinkage}>添加联动</Button>
                     </div> 
                </div>
              </Drawer>
          </div>) 
      }

}
export default PositionLink;