/* 构件属性展示 */

import React from 'react';
import { Modal,Input } from 'antd';

class SaveLinkageModel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
           LinkageName:''
        }
        this.handleLinkageName = (value)=>{
            this.setState({
                LinkageName: value
            })
        }
       
        this.onSave = ()=>{
            const { LinkageName } = this.state;
            if(LinkageName){
                this.props.onSave(LinkageName)
            }
        };

    }
   
  
    render() {
        const { visible, onCancel, current} = this.props;
        const { LinkageName } = this.state;
        return( 
            <Modal title="保存联动" 
                visible={visible} 
                width={320}
                okText={'确定'}
                cancelText={'取消'}
                onOk={this.onSave} 
                onCancel= {onCancel}
              >
                <div className="saveModelContent">
                    <div className="saveItem flex-start">
                        <span>联动名称：</span>
                        <Input style={{width: 120}}  type="text" value={LinkageName} onChange={e => this.handleLinkageName(e.target.value)}/>
                    </div>
                    <div className="saveItem flex-start">
                        <span>联动图纸：</span>
                        <div className="draw-info">
                            <span className="draw-version">{`V${current && current.drawingVersion}.0`}</span>
                            <span className="draw-name">{current && current.drawingName}</span>
                        </div>
                    </div>
                   
                </div>
              </Modal> 
        )
    }

}
export default SaveLinkageModel;