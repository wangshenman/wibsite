import React from 'react'; 
import { Button, Icon, message, Select, Modal, Progress } from 'antd';
import './linkage.less';
import ModelHeader from '../component/modelHeader/ModelHeader';
import ModelView from '../../../bimModel/component/modelView/modelView';
import PositionLink from './component/positionLink';
import MemberLink from './component/memberLink';
import { MemberLinkIcon, PositionLinkIcon } from '../component/svgIcon';
import DrawingsModel from './component/drawingsModel';
import SaveLinkageModel from './component/saveLinkageModel';
import { analysisModelFile, getExampleToken } from '../../../../common/utils/commonActions';
import { getURLValueByKey } from '../../../../common/utils/Utility';
import { modelConfig } from '../../../../common/config/websiteConfig';
import _ from "lodash";

const { Option } = Select;
const { confirm } = Modal;

const ModelWIND = window.WIND.WIND;
const CallbackType = ModelWIND.CallbackType;
const LinkageCallback = 'LINKAGE_CALLBACK';
class Linkage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      linkageLoading: false,
      percent: 0,
      link: null,
      currentApp: null,
      drawerKey: null,
      onEditDrawing: false,
      memberDrawerVisible: false,
      positionDrawerVisible: false,
      current: null, //当前打开图纸的数据
      isSplitscreen: false, //是否分屏
      drawingVisible: false, //图纸小窗口
      drawingModalVisible: false,
      isDrawingLinkage: false,
      linkHight: null, //位置联动 高度
      drawingSettingVisible: false,
      floorData: [],
      relationList:[],
      linkAgeRelation: false,
      saveModelVisible:false,
      showToolbar: true
    }

    this.onRef = (ref) =>{
      this.child = ref
    };

    this.onRef2 = (ref) =>{
      this.child2 = ref
    };

    this.onRef3 = (ref) =>{
      this.child3 = ref
    };

    this.onRef4 = (ref) =>{
      this.child4 = ref
    };

   
    this.init =()=>{
      let modelId = getURLValueByKey('modelId');
      let version = parseInt(getURLValueByKey('version'));
      if( modelId && version){
        getExampleToken().then(res =>{
          if(res && res.code === 200){
            let modelToken = res.data.token;
            this.setState({
              config: { serverIp: modelConfig.serverIp, token: modelToken },
              models:[{ modelId: modelId, version}],
            })
          }
        })
      }
    };
  }

  componentDidMount() {
    this.init();
  }

  switchState = (type, value) => {
    this.setState({[type]: value})
  };

  //切换分屏
  handleSplitscreen = () =>{
    const { isSplitscreen}  = this.state;
    const { currentApp } = this.child.state;
    this.setState({
      isSplitscreen: !isSplitscreen
    },()=> {
      currentApp.screenResize();
      currentApp.linkage().resizeLinkage();    
    })
  }

  //***** 点击构件联动  *****
  handleMemberLink = () =>{
    const { currentApp } = this.child.state;
    const { drawerKey } = this.state;
    if(drawerKey === 'member'){
      return false;
    }
    this.setState({
      currentApp,
      positionDrawerVisible: false,
      drawingSettingVisible: false,
      memberDrawerVisible: true,
      drawerKey: 'member',
      isSplitscreen: false,
      isDrawingLinkage: false,
      relationList: []
    },()=> {
      this.handleCloseDrawing();
       currentApp.screenResize()
    })
  };

  //打开内部对应图纸
  handleOpenDrawing = (item) =>{
    if(item && item.preprocessed === 0){
      this.setState({ 
        current: item, 
        drawingVisible: true 
      },()=>this.doOpen2DLinkage(item))
    }else{
      let that = this;
      confirm({
        title: '确认解析',
        content: '当前图纸还未解析，请确认是否立即解析？',
        okText: "确认",
        cancelText: "取消",
        centered: true,
        onOk() {
           that.handleTransform(item)
        },
        onCancel() {
          that.child3.setSelectedKeys();
        },
      });
    }
    
  };

  //打开内部图纸
  doOpen2DLinkage = async(item) =>{
    const { currentApp } = this.child.state;
    currentApp.linkage().closeLinkage();
    //打开内部图纸（构件联动）
    let linkageCanvas = document.getElementById("drawingView");
    //配置内部图纸背景颜色
    //currentApp.linkage().configLinkage({backgroundRGB255:[0,0,0]})
    currentApp.linkage().openLinkage(linkageCanvas);
    currentApp.linkage().addCallback(LinkageCallback, (type, option) => this.updateDataNode(type, option));
    //打开模型二维联动数据
    await currentApp.linkage().openMViewLinkageData(item._id);
    
  };

  updateDataNode = (type, value) => {
    console.log('linkage-type',type)
    console.log('linkage-value',value)
    //数据回调之后, 更新进度条
    switch (type) {
      //加载中
      case CallbackType.DATA_LOAD_CHANGE:
          // 模型加载百分比回调
          let percentNum = Math.min(100, Math.max(0, Math.floor(value.percent))) || 0;
          if (percentNum >= 0 && percentNum < 100) {
              this.setState({ 
                linkageLoading: true,
                percent: percentNum 
              });
          } 
          break;
      case CallbackType.DATA_LOAD_START:
          console.log('data load start!', value);
          break;
       //加载结束 
      case CallbackType.DATA_LOAD_FINISH:
        console.log('data load finished!', value);
        this.setState({ linkageLoading: false });
        break;
      //加载成功
      case CallbackType.DATA_LOAD_SUCCEED:
          console.log('加载成功：',value);
          break;
      default:
        break;
    }
  };

  //内部图纸转换
  handleTransform = (record) =>{
    const { config } = this.state;
    let headers = { headers: {'everybim-token': config && config.token}};
    if(record && record._id){
      analysisModelFile(record._id, headers).then(res =>{
        if(res && res.code === 200){
            message.success('正在执行转换...');
         }
      }).catch(error => {
         error && message.error(error.message)
      })
        
    }
  };

  //关闭图纸小窗口
  handleCloseDrawing = ()=>{
    const { isSplitscreen, memberDrawerVisible,positionDrawerVisible} = this.state;
    const { currentApp } = this.child.state;
    currentApp.linkage().closeLinkage();
    this.setState({ 
      current: null, 
      drawingVisible: false 
    });
    if(memberDrawerVisible) { 
      this.child3.setSelectedKeys(); 
    }
    if(positionDrawerVisible) { 
      this.child4.setSelectedKeys(); 
    }
    if(isSplitscreen){
      this.setState({ 
        isSplitscreen: false
      },()=> currentApp.screenResize())
    }
    currentApp.linkage().removeCallback(LinkageCallback);
    
  };

  onResizeModel = ()=>{
    const { currentApp } = this.child.state;
    currentApp.screenResize();
  }

  closeMemberLink = ()=>{
    const { currentApp } = this.child.state;
    this.setState({
      drawerKey: null,
      memberDrawerVisible: false
    },()=> {
      currentApp.screenResize(); 
      this.child2.clearCurrent()
      this.handleCloseDrawing()
    })
  }

  //***** 点击位置联动  *****
  handlePositionLink = () =>{
    const { drawerKey } = this.state;
    if(drawerKey === 'position'){
      return false;
    }
    this.setState({
      memberDrawerVisible: false,
      drawingSettingVisible: false,
      onEditDrawing: false,
      positionDrawerVisible: true,
      drawerKey: 'position',
      isSplitscreen: false,
      isDrawingLinkage: false,
      relationList:[]
    },()=> { 
      this.getLinkAgeRelationList();
      this.handleCloseDrawing(); 
    })
    
  };

  //打开外部图纸
  handleOpenOuterDrawing = (item) =>{
    this.setState({
      current: item, 
      drawingVisible: true,
      drawingSettingVisible: true,
      isDrawingLinkage: true,
      drawerKey: null,
      showToolbar: false,
      positionDrawerVisible: false,
      drawingModalVisible: false,
    },()=> {
      this.doOpenDrawing(item);
      this.getFloorData(); 
      this.onResizeModel();
    })
  };

  getFloorData = () =>{
    const { currentApp } = this.child.state;
    const { models } = this.state;
    let floorData = currentApp.data().getFloors(models[0].modelId);
    
    if(floorData){
      this.setState({ 
        floorData
       });
    }
    
  }

  //执行打开外部图纸
  doOpenDrawing = async(item)=>{
    const { currentApp } = this.child.state;
    currentApp.linkage().closeLinkage();
    //打开外部图纸
    let linkageCanvas = document.getElementById("drawingView");
    currentApp.linkage().openLinkage(linkageCanvas); 
    currentApp.linkage().addCallback(LinkageCallback, (type, option) => this.updateDataNode(type, option));
    this.beginLinkageEdit();
  }

  handleChangeFloor = (key) =>{
    const { currentApp } = this.child.state;
    const {floorData} = this.state;
    let floorItem = floorData.find(i => i.uuid === key);
    if(floorItem){
      let linkHight = parseFloat(floorItem.elevation + floorItem.height);
      let linkageConfig = {
          editPlaneType: 0,
          editPlaneDirection:[0, 0, -1], //可选
          editPlanePosition:[0, 0, linkHight] //height为楼层顶标高(低标高+楼层高度)
      }
      this.setState({ 
        linkHight: key
      },()=> currentApp.linkage().configDrawingLinkageEdit(linkageConfig))

    }
    
     
  }

  //开始选点
  beginLinkageEdit = () =>{
      const { currentApp } = this.child.state;
      const { current } = this.state;
      currentApp.linkage().beginDrawingLinkageEdit(current.drawingId, current.drawingVersion);
  };

  //结束选点
  endLinkageEdit = () =>{
    this.setState({ saveModelVisible: true })
  };

   //取消选点
   cancelLinkageEdit = ()=>{
      const { currentApp } = this.child.state;
      this.setState({
        drawingVisible: false,
        drawingSettingVisible: false,
        isDrawingLinkage: false,
        positionDrawerVisible: true,
        saveModelVisible: false,
      },()=> {
        currentApp.screenResize(); 
        currentApp.linkage().cancelDrawingLinkageEdit();
      })
  }

  previewDrawingLinkage = (item)=>{
    this.setState({
      drawingVisible: true,
      current: item
    },()=>this.openLinkAgeRelation(item))

  }

  //打开联动关系
  openLinkAgeRelation = (item) =>{
    const { currentApp } = this.child.state;
    currentApp.linkage().closeLinkage();
    //打开外部图纸
    let linkageCanvas = document.getElementById("drawingView");
    currentApp.linkage().openLinkage(linkageCanvas);
    currentApp.linkage().openDrawingLinkageData(item);  
    currentApp.linkage().addCallback(LinkageCallback, (type, option) => this.updateDataNode(type, option));
  };

  //保存联动关系
  saveOuterLinkAge = async(name) =>{
    const { currentApp } = this.child.state;
    const { current } = this.state;
    let linkageData = await currentApp.linkage().endDrawingLinkageEdit();
    if(linkageData){
      currentApp.network().createDrawingLinkage(Object.assign(linkageData,{ name, drawingName: current.drawingName}));
      message.success('保存成功') 
    }else{
      return message.error('请在图模上各选取两个点~');
    }
  
    this.setState({ 
      drawingSettingVisible: false,
      positionDrawerVisible: true,
      drawerKey: 'position',
      saveModelVisible: false,
      isDrawingLinkage: false,
      showToolbar: true,
      linkHight: null 
    },()=>{ 
      this.getLinkAgeRelationList()
    }) 
  };

  //取消保存
  cancelSaveLinkage = () =>{
    this.setState({
      saveModelVisible: false,
    })
  }

  //获取联动关系列表
  getLinkAgeRelationList = async() =>{
    const { currentApp } = this.child.state;
    const { models } = this.state;
    let relations = await currentApp.network().listDrawingLinkage(models[0].modelId, models[0].version);
    if(relations){
      let linkageList =  _.reverse(relations.slice(0,10));
      this.setState({
        relationList: linkageList
      },()=>this.onResizeModel())
    }
    
  };

  deleteDrawingLinkage = async(linkageId)=>{
    const { currentApp } = this.child.state;
    let isDelete = await currentApp.network().deleteDrawingLinkage(linkageId);
    if(isDelete){
      message.success('删除成功');
      this.handleCloseDrawing(); 
      this.getLinkAgeRelationList();
    }
  };

  closePositionLink = () =>{
    const { currentApp } = this.child.state;
    this.setState({ 
      positionDrawerVisible:false,
      drawingSettingVisible: false,
      onEditDrawing: false,
      drawerKey: null,
    },()=> {
      currentApp.screenResize(); 
      this.child2.clearCurrent();
      this.handleCloseDrawing();
    })
  };

  render() {
    let { currentApp, config, models, current, memberDrawerVisible, positionDrawerVisible, drawingVisible,
      isSplitscreen, drawingModalVisible, floorData, drawingSettingVisible, linkHight, relationList, drawerKey,
      linkageLoading, percent, isDrawingLinkage, saveModelVisible, showToolbar } = this.state;


    const btnInfo = [
      {
        title: '构件联动',
        icon: MemberLinkIcon,
        action: this.handleMemberLink
      },
      {
        title: '位置联动',
        icon: PositionLinkIcon,
        action: this.handlePositionLink
      }
    ];

    const drawingLayout = () =>{
      let drawingTitle = memberDrawerVisible ? `${current && current.type}-${current && current.name}` : `${current && current.drawingName}`;
      let drawingVersion = memberDrawerVisible ? (current && current.version) : (current && current.drawingVersion);
       return(
        <div className='drawing-center'>
            <div className={`header-2D`}>
                <div className="draw-2D">
                    <span className="draw-version">{`V${drawingVersion}.0`}</span>
                    <span className="draw-name">{drawingTitle}</span>
                </div>
                {
                  !isDrawingLinkage ?
                    <div className="btn-2D">
                    {
                      isSplitscreen ?
                      <Icon type="zoom-out" className="fullscreen-exit screen-btn" onClick={this.handleSplitscreen}/>
                      :
                      <Icon type="zoom-in" className="fullscreen-btn screen-btn" onClick={this.handleSplitscreen}/>
                    }
                    <Icon type="close" className="close-btn" onClick={this.handleCloseDrawing}/>
                  </div> : null

                }
              
            </div>
            <div className="container-2D">
                {
                  linkageLoading &&
                  <div className="linkage-loading-process">
                      <Progress type="circle" percent={percent}/> 
                  </div>
                } 
                <canvas id='drawingView' style={{ position: 'absolute',width:'100%',height:'100%'}}></canvas>
            </div>
        </div>
       )
    };

    const settingLayout = () =>{
      return(
        <div className="model-setting flex-between">
               <div className="model-setting-item flex-start">
                <span>楼层：</span>
                <Select style={{width: 100}} 
                        value={linkHight}
                        placeholder="请选择"
                        onChange={value => this.handleChangeFloor(value)}>
                        {
                          floorData.map((item,index) =>{
                             return(<Option key={index} value={item.uuid}>{item.name}</Option>)
                          })
                        }
                </Select> 
               </div>
            </div>
      )
    };

    const posLinkageHeader = () =>{
      return(
        <div className="posLinkageHeader flex-between"> 
            <div className="text-header">
                  <p className="text-item">
                   提示：
                   1、请在模型中选择模型的楼层来切换高度剖面；
                   2、需要在模型和图纸中分别选择两个联动的点，来确定联动关系。
                   </p>
            </div>
            <div className="btn-header"> 
                <Button className="btn-item" onClick={this.cancelLinkageEdit}>取消</Button>
                <Button className="btn-item" type="primary" onClick={this.endLinkageEdit}>保存</Button>
            </div>
        </div>
      )

    }
   
    return (
      <div className="linkage">
        {
          isDrawingLinkage ? posLinkageHeader() : 
          <ModelHeader name={'图模联动'} 
                       btns={btnInfo} 
                       onRef2 ={this.onRef2}
          />
        }
        <div className="linkage-content">
            <div className={`${isDrawingLinkage ? 'layout-linkage': isSplitscreen ? 'layout-split' : drawerKey ? 'drawerWidth':'layout-normal'}` }>
               <ModelView  config = { config } 
                           links = { models }
                           mainMenu = { true }
                           onRef = { this.onRef }
                           hasDrawer = { drawerKey }
                           showToolbar = { showToolbar }
               />
            </div>
            <div className={`${isDrawingLinkage ? 'drawer-hide' : 'linkage-drawer'}`}>
              { 
                memberDrawerVisible ?
                <MemberLink visible= {memberDrawerVisible}
                            app = {currentApp}
                            models={models}
                            isSplitscreen={isSplitscreen}
                            currentModelId = {models[0].modelId}
                            openDrawing= {this.handleOpenDrawing}
                            closeDrawing= {this.handleCloseDrawing}
                            switchState= {this.switchState}
                            openModel ={this.initLoadModel}
                            onClose= {this.closeMemberLink}
                            onRef3 ={this.onRef3}
                /> : null
              }
              {
                positionDrawerVisible ? 
                <PositionLink visible= {positionDrawerVisible}
                              app= {currentApp}
                              current={current}
                              isSplitscreen={isDrawingLinkage}
                              onOpen= {this.previewDrawingLinkage}
                              onDelete={this.deleteDrawingLinkage}
                              onEdit={this.state.onEditDrawing}
                              onSave= {this.saveOuterLinkAge}
                              onUpdate={this.editDrawingLinkage}
                              relationList = {relationList}
                              switchState= {this.switchState}
                              onClose= {this.closePositionLink}
                              onRef4 ={this.onRef4}
                /> : null
              }
             
            </div>
            <div className={`linkage-drawing-layout ${isDrawingLinkage ? 'drawing-linkage' : isSplitscreen ? 'drawing-split' : 'drawing-small'}`}>
                {drawingVisible && drawingLayout()}
            </div>
            <div className="model-box">
              {
                drawingModalVisible &&
                <DrawingsModel visible= {drawingModalVisible}
                              app = {currentApp}
                              openOuterDrawing={this.handleOpenOuterDrawing}
                              switchState= {this.switchState} 
                />
              }
              {
                saveModelVisible &&  
                <SaveLinkageModel visible= {saveModelVisible}
                                  app= {currentApp}
                                  current= {current}
                                  onSave={this.saveOuterLinkAge}
                                  onCancel={this.cancelSaveLinkage}
                                  switchState= {this.switchState} 
                />
              }
            </div>
            <div className="model-setting-wrapper"> 
                { drawingSettingVisible && settingLayout() }
            </div> 
        </div>
      </div>
    )
  }
}

export default Linkage;
