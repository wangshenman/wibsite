import React from 'react'; 
import { Select, Switch} from 'antd';
import './modelEffect.less';
import ModelHeader from '../component/modelHeader/ModelHeader';
import ModelView from '../../../bimModel/component/modelView/modelView';
import { convertToken } from '../../../../common/utils/commonActions';
import { colorsBlock } from '../component/menuConfig';
import { modelConfig } from '../../../../common/config/websiteConfig';

const { Option } = Select;

const BackgroundType = window.WIND.BackgroundType;
const WeatherType =  window.WIND.WeatherType;
class ModelEffect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      config: null,
      links: [],
      modelSkyVisible: false,
      backgroundType: 'SimpleSky',
      backgroundColor: '#ABDAFF#FFFFFF',
      rainVisible: false,
      snowVisible: false,

    }
    this.onRef = (ref) =>{
      this.child = ref
    };

    this.getBIMToken = (token)=>{
      const { config } = this.state;
      let headers = { headers: {'x-access-token': token}};
      convertToken(headers).then(res =>{
        if(res && res.code === 200){
          this.setState({
            config: Object.assign(config,{ token: res.data })
          })
        }
      });
    }
  }

  switchState = (type, value) => {
    this.setState({[type]: value})
  };

  handleModelSkyEffect = ()=>{
    this.setState({
      modelSkyVisible: true
    })
  }

  // 设置天空盒
  onChangeEnvironment = (value) => {
     const {currentApp} = this.child.state;
     switch(value){
        case 'SimpleSky':
          currentApp.effect().setSkyMapping(false);
          currentApp.effect().setBackgroundType(BackgroundType.SIMPLE_SKY);
          break;
        case 'SimpleCloudSky':
          currentApp.effect().setSkyMapping(true);
          currentApp.effect().setBackgroundType(BackgroundType.SIMPLE_SKY);
          break;
        case 'Normal':
          break;
        default:
          break;
     }
     this.setState({
       backgroundType: value
     })
  };

  // 设置背景色
  onChangeBackground = (value) => {
    const {currentApp} = this.child.state;
    let selectedColors = colorsBlock.find(d => d.key === value);

    if(selectedColors){
      if(currentApp){
        let background = selectedColors.color;
        if (selectedColors.gradient) {
            currentApp.effect().setGradientColor(background[0], background[1], 1);
            currentApp.effect().setBackgroundType(BackgroundType.GRADIENT_COLOR);
        } else {
            currentApp.effect().setPureColor(background[0]);
            currentApp.effect().setBackgroundType(BackgroundType.PURE_COLOR);
        }
      }
    }
    
    this.setState({
      backgroundColor: value
     })
  };

  // 下雨效果 开关
  onChangeRainEffect = (checked) => {
    const {snowVisible} = this.state;
    const {currentApp} = this.child.state;
   
    this.setState({rainVisible: checked})
    if(currentApp){
      if(checked){
        if(snowVisible) {
          currentApp.effect().closeWeather();
          this.setState({ snowVisible: false })
        }
        currentApp.effect().openWeather();
        currentApp.effect().setWeatherType(WeatherType.RAIN);
      }else{
        currentApp.effect().closeWeather();
      }
    }
    
  }

  // 下雪效果 开关
  onChangeSnowEffect = (checked) => {
    const { rainVisible } = this.state;
    const {currentApp} = this.child.state;
    this.setState({ snowVisible: checked })
    if(currentApp){
      if(checked){
        if(rainVisible) {
          currentApp.effect().closeWeather();
          this.setState({ rainVisible: false })
        }
        currentApp.effect().openWeather();
        currentApp.effect().setWeatherType(WeatherType.SNOW);
      }else{
        currentApp.effect().closeWeather();
      }
    }
    
  }

  componentDidMount(){
    let config =  { serverIp: modelConfig.serverIp };
    let models = [{ id: '61dbaa291b039c3045b1b31e', version: 1}];
    this.setState({
      config,
      links: models
    },()=> this.getBIMToken(window.localStorage.getItem('token')))
  }

  render() {
    let { config,links, backgroundType, backgroundColor, rainVisible, snowVisible, modelSkyVisible} = this.state;
    const btnInfo = [
      {
        title: '天空效果',
        action: this.handleModelSkyEffect
      },
    ];

    const skyEffectLayout = ()=>{
      return(
        <div className="effect-wrapper">
              <div className="effect-item">
                    <span>天空效果：</span>
                    <div>
                        <Select style={{width: 140}} 
                                value={backgroundType}
                                onChange={value => this.onChangeEnvironment(value)}>
                                <Option value={'SimpleSky'}>晴天明朗</Option>
                                <Option value={'SimpleCloudSky'}>阴天多云</Option>
                        </Select> 
                    </div>
              </div>
              <div className="effect-item">
                    <span>背景色：</span>
                    <div id={'set-Colors-wrapper'}>
                          <Select value={backgroundColor}
                                  dropdownClassName={'model-bg-setting-item'}
                                  style={{width: 140}}
                                  getPopupContainer={() => document.getElementById('set-Colors-wrapper')}
                                  onChange={value => this.onChangeBackground(value)}
                          >
                          {
                              colorsBlock.map((d, index) => {
                                  return (
                                      <Option
                                          key={index}
                                          value={d.key}
                                          className={'color-select'}>
                                          <div className={'color-item'}><div style={{background: d.gradient ? `linear-gradient(180deg,rgba(${d.color[0].join(',')},1) 0%,rgba(${d.color[1].join(',')},1) 100%)` : d.key}} /></div>
                                      </Option>
                                  )
                              })
                          }
                         </Select>
                    </div>
              </div>
              <div className="effect-item">
                    <span>下雨效果：</span>
                    <Switch checked={rainVisible} onChange={this.onChangeRainEffect}/>
              </div>
              <div className="effect-item">
                    <span>下雪效果：</span>
                    <Switch checked={snowVisible} onChange={this.onChangeSnowEffect}/>
              </div>
            </div>
      )
    }

    return (
      <div className="modelEffect">
        <ModelHeader name={'模型效果'} btns={btnInfo}/>
        <div className="modelEffect-content">
           <ModelView  config = { config } 
                       links = { links }
                       onRef = { this.onRef }
           />
           { modelSkyVisible ? skyEffectLayout() : null }
        </div>
        
      </div>
    )
  }

}

export default ModelEffect;
