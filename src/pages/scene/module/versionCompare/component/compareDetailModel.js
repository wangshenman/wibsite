/* 属性对比结果展示 */

import React from 'react';
import { Modal, Collapse, Row, Col, Icon} from 'antd';
import { diffObject, filterObject } from '../../../../../common/utils/Utility';
const { Panel } = Collapse;
const FILTER_ARR = ['_id', 'version', 'viewId','type'];
export default class CompareDetailModel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            components: null,
            contrast: null,
            current: null,
            changedNum: 0
        }
        this.handleDataType = async (data) =>{
            let contrast = filterObject(data.contrast, FILTER_ARR);
            let current = filterObject(data.current, FILTER_ARR);
            let computed = await diffObject(contrast, current);
            if(computed){
                let new_val = computed && computed.new_val;
                let pro_val = new_val && new_val.properties &&  (Object.keys(new_val.properties).length);
                let keys_val = new_val && Object.keys(new_val) && (Object.keys(new_val).length);
                let change_val = pro_val ? parseInt(keys_val - 1) : parseInt(keys_val);
                this.setState({ changedNum: parseInt(pro_val + change_val)})
            }
            Object.assign(contrast, { properties: this.handleParamsGroup(contrast && contrast.properties)});
            Object.assign(current, { properties: this.handleParamsGroup(current && current.properties)});
            this.setState({ computed, contrast, current })
        }
    }

    //组分类
    handleParamsGroup = (paramsList) =>{
        let paramsMap = {};
        let destArray = [];
        for (let i = 0; i < paramsList.length; i++) {
          let params = paramsList[i];
          if (!paramsMap[params.group]) {
            destArray.push({
              key: params.group,
              data: [params]
            });
            paramsMap[params.group] = params;
          } else {
            for (let j = 0; j < destArray.length; j++) {
              let dest = destArray[j];
              if (dest.key === params.group) {
                dest.data.push(params);
                break;
              }
            }
          }
        }
        return destArray;
    };

    componentDidMount() {
        const {components} = this.props;
        if(components){
            this.setState({
                components
            },()=> this.handleDataType(components))
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.components  !== this.props.components){
            this.setState({
                components: nextProps.components
            },()=> this.handleDataType(nextProps.components))
        }
    } 
   
    render() {
        const { models, slaveVersion, visible, onCancel} = this.props;
        const { contrast, current, computed, changedNum } = this.state;
       
       
        const basicPropsLayout = (contrast, current) =>{
            return(
                <Panel header="基础属性">
                    { propsItem('名称',contrast && contrast.name, current && current.name) }
                    { propsItem('设备ID', contrast && contrast.entityId, current && current.entityId) }
                    { propsItem('楼层', contrast && contrast.floor, current && current.floor) }
                    { propsItem('专业', contrast && contrast.domainStr, current && current.domainStr) }
                    { propsItem('类型', contrast && contrast.categoryStr, current && current.categoryStr) }
                </Panel>
            )
        };

        const propsItem =(title, compareValue, currentValue) =>{
            return(
                   <div className={`list-top-item}`} key={title}>
                        <Row>
                            <Col span={4}>{`${title} ：`}</Col>
                            <Col span={10}>{compareValue}</Col>
                            <Col span={10} style={{color: `${compareValue !== currentValue ? '#F5A623' : '#333'}`}}>{currentValue}</Col>
                        </Row>
                    </div>
            )
        };

        return( 
            <Modal title="属性对比结果" 
                className='compareDetailModel'
                visible={visible} 
                width={760}
                centered
                okText={'确定'}
                cancelText={'关闭'}
                onOk={onCancel} 
                onCancel= {onCancel}
            >
                <div className="compareLayout">
                    <div className="compare-title" style={{paddingBottom:'15px', borderBottom:'1px solid #e8e8e8',color:'#F5A623'}}>
                        {`【 ${current && current.entityId}_ ${current && current.name}】： 共${changedNum}处变化`} 
                    </div>
                    <div className="compare-header" style={{padding:'15px 0'}}>
                      <Row>
                        <Col span={4}>属性名称</Col>
                        <Col span={10}>{`V${slaveVersion}.0(修改前)`}</Col>
                        <Col span={10}>{`V${models[0].version}.0(修改后)`}</Col>
                    </Row>
                    </div>
                    <Collapse bordered={false}
                              defaultActiveKey={['1']}
                              expandIcon={({ isActive }) =>  <Icon type={isActive ?'up':'down'} style={{color:'#8C8F9F'}}/>} 
                    >
                       { basicPropsLayout(contrast, current) }
                       {
                          (contrast && contrast.properties || []).map((item, index) =>{
                                let curGroup = (current && current.properties).find(cur => cur.key === item.key);
                                if(curGroup){
                                  return( 
                                    <Panel header={item.key} key={item.key}>
                                      {
                                          (item.data || []).map((i, index) =>{
                                                let curData = (curGroup.data).find(c => c.key === i.key);
                                                if(curData){
                                                   return propsItem(i.key, i.value, curData.value);
                                                }  
                                          })
                                      }
                                  </Panel>)   

                                }
                          })
                      } 
                    </Collapse>
                   
                </div>
            </Modal> 
        )
    }

}






