/* 构件联动展示 */
import React from 'react';
import { Collapse, Drawer, Icon, Select, Button } from 'antd';
import _ from "lodash";

const { Panel } = Collapse;
const { Option } = Select;


class VersionDrawer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            contrastResult: [],
            allContrastResult:[],
            filterModify: null,
            isContrast: false
        }

        //获取版本比对结果
        this.getVersionContrastResult = async (props) =>{
            const {models, app, contrastComponents} = props;
            let contrastResult = [
                {
                    key: 'add',
                    title: '新增构件',
                    data: (contrastComponents &&  contrastComponents.add && contrastComponents.add.components) || [],
                },
                {
                    key: 'delete',
                    title: '删除构件',
                    data: (contrastComponents &&  contrastComponents.delete && contrastComponents.delete.components) || [],
                },
                {
                    key: 'modify',
                    title: '修改构件',
                    data: (contrastComponents &&  contrastComponents.modify && contrastComponents.modify.components) || [],
                },
            ];
            this.setState({
                contrastResult,
                allContrastResult: contrastResult
            })  
        };

        //定位
        this.goLocation = async(key, uuid) =>{
            const { app, models } = this.props;
            // let colorConfig = { 
            //     addStateRGBA255:[241, 155, 37, 255], 
            //     deleteStateRGBA255:[255, 0, 0, 255],
            //     modifyStateRGBA255: [4, 165, 253, 120],
            // };
            if(app && uuid){
                let params = {
                    modelId: models[0].modelId,
                    uuid: uuid
                };
                // app.contrast().configContrast(colorConfig); //版本对比配置
                app.contrast().displayContrast(); //对比上色
                app.contrast().locateContrast(params);
            }
        };

        //
        this.handleToggle = ()=>{
            const { app } = this.props;
            const { isContrast } = this.state;
            this.setState({
                isContrast: !isContrast
            })
            if(!isContrast){
                app.contrast().displayContrast(); //对比上色
            }else{
                app.contrast().revertContrast(); //对比上色
            }
        }

        //筛选
        this.handleFilter = (value) =>{
            this.setState({
                filterModify: value,
            })
        }
        
        this.getContrastResultDetail = async(uuid) =>{
            const { models, app, slaveVersion} = this.props;
            if(app && uuid){
               let currentDetail = await app.data().grabComponent(models[0].modelId, models[0].version, uuid, true);
               let contrastDetail = await app.data().grabComponent(models[0].modelId, slaveVersion, uuid, true);
               let componentsData = {
                  current: currentDetail,
                  contrast: contrastDetail
               }
               this.props.getComponents(componentsData)

            }
            
        };

        this.filterContrast = (contrastResult, filterModify)=>{
            let modifyArray = [];
            if (filterModify) {
                (_.cloneDeep(contrastResult)).forEach((item, index)=>{
                    modifyArray[index] = item;
                    if(item.key === 'modify'){
                        modifyArray[index].data = item.data.filter((item) => {
                            if(filterModify === 1){
                                return item.geometryChanged
                            }else if(filterModify === 2){
                                return item.locationChanged
                            }else if(filterModify === 3){
                                return item.materialChanged
                            }else if(filterModify === 4){
                                return item.propertyChanged
                            }
                        })   
                    }
                })
            }else{
                modifyArray = _.cloneDeep(contrastResult);
            }
            return modifyArray;
        }

    }

    componentDidMount() {
        this.getVersionContrastResult(this.props);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.contrastComponents  != this.props.contrastComponents){
            this.getVersionContrastResult(nextProps);
        } 
    } 

    render() {
        const {contrastResult, filterModify} = this.state;
        const {onClose} = this.props;

        let modifyArray = this.filterContrast(contrastResult, filterModify);

        const collapseHeader = (item)=>{
            return(
                <div className='result-header flex-between'>
                   <span className="result-show">
                       <span className={`result-color ${item.key}`}></span>
                        <span className='result-title'>{`${item.title}（${ (item.data && item.data.length) || 0 } 个）`}</span>
                   </span>
                </div>
            )
        }

        const drawerHeader = ()=>{
            return(
                <div className='drawerHeader flex-start'>
                    <div style={{marginRight: '15px'}}>构件详情</div>
                    <Icon style={{fontSize: '14px'}} type="swap" onClick={this.handleToggle}/>
                </div>
            )
        }

        return( 
          <div className="versionResult" id="versionResult">
              <Drawer 
                      title={drawerHeader()}
                      placement='right'
                      visible={true}
                      mask={false}
                      width={340}
                      maskClosable={false}
                      zIndex={100}
                      keyboard={false}
                      onClose={onClose}
                      getContainer={document.getElementById("versionResult")}
              >
               <Collapse expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
                         accordion={true}
                         defaultActiveKey={3}
               >
                    {
                        (modifyArray || []).map((item,index) =>{
                            return( 
                            <Panel header={collapseHeader(item)} key={item.key}>
                                
                                {
                                    item.key === 'modify' && (item.data && item.data.length) &&
                                    <div>
                                      <span>可根据构件修改性质筛选：</span>
                                        <Select style={{width: 100,height:'15px',fontSize:'12px'}} 
                                            placeholder="请选择"
                                            allowClear
                                            className="result-select"
                                            onChange={value => this.handleFilter(value)}>
                                                <Option value={1} >几何</Option>
                                                <Option value={2} >位置</Option>
                                                <Option value={3} >材质</Option>
                                                <Option value={4} >属性</Option>
                                        </Select>
                                    </div>
                                }
                                <ul>
                                {
                                    item.data && item.data.map((i,index)=>{
                                        return( <li className="parmShow flex-between" key={index} onClick={()=> this.goLocation(item.key, i.uuid)}>
                                            <span className="parmValue">{ i.name }</span>
                                            {
                                               i.propertyChanged &&  <Icon type="diff"  onClick={()=> this.getContrastResultDetail(i.uuid)}/>
                                            }
                                            </li>)
                                           
                                    })
                                    
                                }
                                </ul>
                            </Panel>)
                        })
                    }
                </Collapse>
            </Drawer>
          </div>) 
      }

}
export default VersionDrawer;
