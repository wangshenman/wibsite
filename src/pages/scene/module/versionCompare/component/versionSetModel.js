/* 构件属性展示 */

import React from 'react';
import { Modal, Select} from 'antd';
const { Option } = Select;

export default class VersionSetModel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentVersion: 1, //历史版本
        }

        //切换模型版本
        this.handleChangeVersion = (value) =>{
            this.setState({
                currentVersion: value,
            })
        };

        this.openNewVersion = ()=>{
            const {currentVersion} = this.state;
            if(currentVersion){
                this.props.onOpen(currentVersion);
            }
        };

    }
  
    render() {
        const { visible, version, onClose } = this.props;
        const {currentVersion} = this.state;
    
        return( 
            <Modal title="选择对比版本" 
                visible={visible} 
                width={300}
                okText={'确定'}
                cancelText={'取消'}
                centered
                onOk={this.openNewVersion} 
                onCancel= {onClose}
              >
                 <div className="setModelContent">
                    <div className="setItem flex-start" style={{ marginBottom:'15px' }}>
                        <span>当前版本：</span>
                        <div className="current-version">{`V${version}.0`}</div>
                    </div>
                    <div className="setItem flex-start">
                        <span>历史版本：</span>
                        <Select style={{width: 80}} 
                            className="model-select"
                            value={`V${currentVersion}.0`}
                            onChange={value => this.handleChangeVersion(value)}>
                                <Option value={1} >{`V1.0`}</Option>
                         </Select>
                        
                    </div>
                   
                </div>
              </Modal> 
        )
    }

}
