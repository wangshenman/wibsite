import React from 'react'; 
import { Button } from 'antd';
import './versionCompare.less';
import ModelHeader from '../component/modelHeader/ModelHeader';
import ModelView from '../../../bimModel/component/modelView/modelView';
import VersionSetModel from './component/versionSetModel';
import VersionResultDrawer from './component/versionResultDrawer';
import CompareDetailModel from './component/compareDetailModel';
import Loading from '../../../../common/component/Loading';
import { getToken,getExampleToken } from '../../../../common/utils/commonActions';
import { getURLValueByKey } from '../../../../common/utils/Utility';
import { modelConfig } from '../../../../common/config/websiteConfig';
import { VersionIcon, ExitVersionIcon} from '../component/svgIcon';
class VersionCompare  extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      config: null,
      models: [],
      loading: false,
      selectItem: null,
      currentApp: null,
      currentVersion: null,
      versionSetVisible: false,
      versionsResultVisible: false,
      compareDetailVisible: false,
      components: null,
      contrastComponents: null
    }
    this.onRef = (ref) =>{
      this.child = ref
    };

    this.onRef2 = (ref) =>{
      this.child2 = ref
    };

    
    this.init =()=>{
      let modelId = getURLValueByKey('modelId');
      let version = parseInt(getURLValueByKey('version'));
      if( modelId && version){
        getExampleToken().then(res =>{
          if(res && res.code == 200){
            let modelToken = res.data.token;
            this.setState({
              config: { serverIp: modelConfig.serverIp, token: modelToken },
              models:[{ modelId: modelId, version}],
            })
          }
        })
      }
    };
  }

  componentDidMount() {
    this.init();
  }

  componentWillUnmount(){
    const { currentApp } = this.child.state;
    currentApp.contrast().closeContrast();
  }

  switchState = (type, value) => {
    this.setState({[type]: value})
  };

  handleVersionContrast = ()=>{
    this.setState({
      versionSetVisible: true
    })
  }

  //确定版本对比
  handleOpenVersion = (slaveVersion) =>{
    const {currentApp} = this.child.state;
    this.setState({ 
      versionSetVisible: false,
      versionsResultVisible: true ,
      currentVersion: slaveVersion,
      loading: true
    },()=> {
      this.doOpenVersion(slaveVersion);
    })
  };

  //打开版本对比数据
  doOpenVersion = async(slaveVersion)=>{
    const { currentApp } = this.child.state;
    const { models } = this.state;
    currentApp.screenResize();
    let versionCanvas = document.getElementById("versionView");
    currentApp.contrast().openContrast(versionCanvas);
    
    let isOpen = await currentApp.contrast().openContrastData(models[0].modelId, slaveVersion);
    if(isOpen){
      let contrastComponents = await currentApp.contrast().getContrastComponents(models[0].modelId);
      let colorConfig = { 
        addStateRGBA255: [241, 155, 37, 255], 
        deleteStateRGBA255: [255, 0, 0, 255],
        modifyStateRGBA255: [4, 165, 253, 120],
        advanceDisplayed: true,
      };
      currentApp.contrast().configContrast(colorConfig); //版本对比配置
      currentApp.contrast().displayContrast();
      if(contrastComponents){
        this.setState({
          currentApp, 
          contrastComponents,
          loading: false
        })
      }
    }
  };

  //退出版本对比
  closeVersionContrast = () =>{
    const { currentApp } = this.child.state;
    this.setState({
      versionsResultVisible: false,
    },()=>{
      currentApp.contrast().closeContrast();
      currentApp.screenResize();
      currentApp.action().homeRoaming();
      this.child2.clearCurrent()
    })
  };

  //获取构件属性
  getComponentsData = (componentsData) =>{
    this.setState({
      components: componentsData,
      compareDetailVisible: true
    })
  };

  render() {
    let { config, models, currentApp, currentVersion, versionSetVisible, versionsResultVisible, drawerKey,
      compareDetailVisible, components, selectItem, contrastComponents, loading} = this.state;
    const btnInfo = [
      {
        title: versionsResultVisible ? '退出版本对比' : '版本对比',
        icon: versionsResultVisible ? ExitVersionIcon : VersionIcon,
        action: versionsResultVisible ? this.closeVersionContrast : this.handleVersionContrast
      }
    ];

    const versionModelLayout = () =>{
      return(
        <div className={`model-center ${versionsResultVisible ? 'show' : 'hide'}`}>
            <div className="container-2D">
               <canvas id='versionView' style={{ position: 'absolute',width:'100%',height:'100%'}}></canvas>
            </div>
        </div>
      )
      
    }
    return (
      <div className="versionCompare">
        < ModelHeader name= {'版本对比'} 
                      btns= { btnInfo} 
                      version= {getURLValueByKey('version')}
                      visible= {versionsResultVisible}
                      slaveVersion= {currentVersion}
                      onRef2 = { this.onRef2 }
        />
        {
           loading && <Loading message="版本对比中，请稍等..."/> 
        }
        <div className="versionCompare-content">
            <div className={`${versionsResultVisible ? 'layout-split':'layout-normal'}`}>
               <ModelView  config = { config } 
                           links = { models }
                           mainMenu = { true }
                           onRef = { this.onRef }
               />
              </div>
            <div className="versionLayout">
              { versionModelLayout()}
            </div>
            <div className="versionSetModel">
              { 
                versionSetVisible &&  <VersionSetModel  onOpen={this.handleOpenVersion}
                                                        version={getURLValueByKey('version')} 
                                                        visible={versionSetVisible}
                                                        onClose={()=>{this.switchState('versionSetVisible',false);}}
                                          /> 
              }
            </div>
            <div className="versionDrawer">
              { 
                versionsResultVisible &&  <VersionResultDrawer  onOpen={this.handleOpenVersion}
                                                                models={models} 
                                                                app={currentApp}
                                                                loading={loading}
                                                                slaveVersion={currentVersion}
                                                                contrastComponents={contrastComponents}
                                                                switchState={this.switchState}
                                                                getComponents={this.getComponentsData}
                                                                onClose={()=>{this.closeVersionContrast()}}
                                          /> 
              }
            </div>
            <div className='versionDetailModel'>
              { 
                compareDetailVisible &&  <CompareDetailModel  
                                                              models={models}
                                                              visible={compareDetailVisible}
                                                              slaveVersion={currentVersion}
                                                              components={components}
                                                              switchState= {this.switchState}
                                                              onCancel={()=>this.switchState('compareDetailVisible',false)}
                                        /> 
              }
            </div>
        </div>
     </div>
    )
  }

}

export default VersionCompare;
