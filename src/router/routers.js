
import { AsyncComponent } from '../common/utils/AsyncComponent'

export const routers = [
    {
        path: '/',
        exact: true,
        isNav:true,
        title: '首页',
        component: AsyncComponent(() => import("../pages/home")),
    },
    {
        path: '/function',
        exact: true,
        isNav:true,
        title: '功能介绍',
        component: AsyncComponent(() => import("../pages/function")),
    },
    {
        path: '/example',
        exact: true,
        isNav:true,
        title: 'BIM体验',
        component: AsyncComponent(() => import("../pages/example")),
    },
    {
        path: '/cases',
        exact: true,
        isNav:true,
        title: '客户案例',
        component: AsyncComponent(() => import("../pages/cases")),
    },
    {
        path: '/documents',
        exact: true,
        isNav: true,
        isSubMenu: true,
        title: '文档',
        component: AsyncComponent(() => import("../pages/changeLog")),
    },
    {
        path: '/downloads',
        exact: true,
        isNav:true,
        title: '下载',
        component: AsyncComponent(() => import("../pages/download")),
    },
    {
        path: '/about',
        exact: true,
        isNav:true,
        title: '关于我们',
        component: AsyncComponent(() => import("../pages/about")),
    },
    {
        path: '/changeLog',
        exact: true,
        title: '更新日志',
        component: AsyncComponent(() => import("../pages/changeLog")),
    },
    {
        path: '/model',
        exact: true,
        title: '模型预览-引擎官网',
        component: AsyncComponent(() => import("../pages/bimModel/bimModel")),
    },
    {
        path: '/drawing',
        exact: true,
        title: '图纸预览-引擎官网',
        component: AsyncComponent(() => import("../pages/bimDrawing/bimDrawing")),
    },

    {
        path: '/odaTestModel',
        exact: true,
        title: '模型预览-oda测试',
        component: AsyncComponent(() => import("../pages/model")),
    },
    {
        path: '/odaTestDrawing',
        exact: true,
        title: '图纸预览-oda测试',
        component: AsyncComponent(() => import("../pages/drawing")),
    },
    {  
        path: '/modelEffect',
        exact: true,
        title: '场景-模型效果',
        component: AsyncComponent(() => import("../pages/scene/module/modelEffect")),
    },
    {  
        path: '/linkage',
        exact: true,
        title: '场景-图模联动',
        component: AsyncComponent(() => import("../pages/scene/module/linkage")),
    },
    {  
        path: '/versionCompare',
        exact: true,
        title: '场景-版本对比',
        component: AsyncComponent(() => import("../pages/scene/module/versionCompare")),
    },
    {  
        path: '/BIMPlatform',
        exact: true,
        title: '场景-BIM协同应用',
        component: AsyncComponent(() => import("../pages/scene/module/BIMPlatform")),
    },
    {  
        path: '/BIMInternet',
        exact: true,
        title: '场景-BIM+物联网',
        component: AsyncComponent(() => import("../pages/scene/module/BIMInternet")),
    },
    {  
        path: '/digitalMap',
        exact: true,
        title: '场景-图纸在线管理',
        component: AsyncComponent(() => import("../pages/scene/module/digitalMap")),
    },
];